//
// Created by champ on 2022/7/31.
//

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* partition(ListNode* node, int x) {
        ListNode* biggerH = new ListNode();
        ListNode* bigger = biggerH;
        ListNode* smallerH = new ListNode();
        ListNode* smaller = smallerH;

        while (node != NULL) {
            ListNode* next = node->next;
            if (node->val < x) {
                smaller->next = node;
                smaller = smaller->next;
            } else {
                bigger->next = node;
                bigger = bigger->next;
            }
            node = next;
        }
        bigger->next = NULL;
        smaller->next = biggerH->next;
        return smallerH->next;
    }
};
