package _816_Double_A_Number_Represented_As_A_Linked_List

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func doubleIt(head *ListNode) *ListNode {
	h := head
	nums := make([]int, 0)
	for h != nil {
		nums = append(nums, h.Val)
		h = h.Next
	}
	length := len(nums)
	reminder := 0
	for i := length - 1; i >= 0; i-- {
		num := nums[i] * 2
		nums[i] = num%10 + reminder
		reminder = num / 10
	}

	fmt.Printf("reminder %+v nums %+v\n", reminder, nums)

	result := &ListNode{}
	last := result
	if reminder != 0 {
		newOne := &ListNode{
			Val: reminder,
		}
		result.Next = newOne
		last = newOne
	}

	for i := 0; i < len(nums); i++ {
		newOne := &ListNode{
			Val: nums[i],
		}
		last.Next = newOne
		last = last.Next
	}

	return result.Next
}
