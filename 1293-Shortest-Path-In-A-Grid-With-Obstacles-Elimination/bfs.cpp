//
// Created by champ on 2022/4/18.
//

class Solution {
public:
    int shortestPath(vector<vector<int>>& grid, int k) {
        int m = grid.size();
        int n = grid[0].size();
        // visted store the obstacles which we visited here.
        vector<vector<int>> visited(m, vector<int>(n, -1));
        queue<vector<int>> q; // {x, y, length, number of obstacles we can move}

        q.push({0, 0, 0, k});

        while(!q.empty()) {
            auto g = q.front();
            int x = g[0], y = g[1];
            q.pop();

            if (x < 0 || y < 0 || x >= m || y >= n) {
                continue;
            }

            if (x == m-1 && y == n-1) {
                return g[2];
            }

            if (grid[x][y] == 1) {
                if (g[3] >= 1) {
                    g[3]--;
                } else {
                    continue;
                }
            }

            if (visited[x][y] != -1 && visited[x][y] >= g[3]) {
                // means we have visted before and number of obstacles we can move of last time >= this time.
                continue;
            }

            visited[x][y] = g[3];

            q.push({x+1, y, g[2]+1, g[3]});
            q.push({x-1, y, g[2]+1, g[3]});
            q.push({x, y+1, g[2]+1, g[3]});
            q.push({x, y-1, g[2]+1, g[3]});
        }
        return -1;
    }
};
