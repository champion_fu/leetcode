package _41_Valid_Mountain_Array

func validMountainArray(arr []int) bool {
	length := len(arr)
	i := 0

	for i+1 < length && arr[i] < arr[i+1] {
		i++
	}

	if i == 0 || i == length-1 {
		return false
	}

	for i+1 < length && arr[i] > arr[i+1] {
		i++
	}
	return i == length-1
}
