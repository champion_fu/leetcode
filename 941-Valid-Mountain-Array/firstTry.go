package _41_Valid_Mountain_Array

func validMountainArray(arr []int) bool {
	if len(arr) < 3 {
		return false
	}
	hillIndex := -1
	for i := 0; i < len(arr)-1; i++ {
		if hillIndex == -1 {
			// go up
			if arr[i] < arr[i+1] {
			} else if arr[i] > arr[i+1] {
				if i == 0 {
					return false
				}
				hillIndex = i
			} else {
				return false
			}
		} else {
			// go down
			if arr[i] < arr[i+1] {
				return false
			} else if arr[i] > arr[i+1] {
				continue
			} else {
				return false
			}
		}
	}
	if hillIndex != -1 {
		return true
	}
	return false
}
