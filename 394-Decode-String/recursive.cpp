//
// Created by champ on 2022/8/8.
//
class Solution {
public:
    string decodeString(string s) {
        int index = 0;
        return decodeString(s, index);
    }

    string decodeString(const string& s, int& index) {
        string res;

        while(index < s.size() && s[index] != ']') {
            if (!isdigit(s[index])) {
                res += s[index++];
            } else {
                // is digit
                int k = 0;
                while (index < s.size() && s[index] != '[') {
                    k = k*10 + s[index++] - '0';
                }

                // ignore open bracket [
                index++;
                string decodedString = decodeString(s, index);

                while (k--) {
                    res += decodedString;
                }
                // ignore close bracket ]
                index++;
            }
        }

        return res;
    }
};
