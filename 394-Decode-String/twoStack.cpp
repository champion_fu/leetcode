//
// Created by champ on 2022/8/8.
//

class Solution {
public:
    string decodeString(string s) {
        stack<string> stringStack;
        stack<int> countStack;
        string currentString;
        int k = 0;
        for (auto c : s) {
            if (isdigit(c)) {
                k = k*10 + c - '0';
            } else if (c == '[') {
                countStack.push(k);
                stringStack.push(currentString);
                currentString = "";
                k = 0;
            } else if (c == ']') {
                string decodeString = stringStack.top();
                stringStack.pop();

                for (int currentK = countStack.top(); currentK > 0; currentK--) {
                    decodeString += currentString;
                }
                countStack.pop();
                currentString = decodeString;
            } else {
                currentString += c;
            }
        }
        return currentString;
    }
};
