//
// Created by champ on 2022/4/16.
//

class Solution {
public:
    string countAndSay(int n) {
        string ret = "1";
        if (n == 1) {
            return ret;
        }

        while(--n) ret = transform(ret);
        return ret;
    }
private:
    string transform(string s) {
        string ret = "";
        // cout << "s length " << s.length() << endl;
        for (int i = 0; i < s.length(); i++) {
            int length = 1;
            while(s[i] == s[i+1] && i+1 < s.length()) {
                length++;
                i++;
            }
            ret += to_string(length) + s[i];
        }
        return ret;
    }
};