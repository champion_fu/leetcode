package _78_Kth_Smallest_Element_In_A_Sorted_Matrix

import "container/heap"

func kthSmallest(matrix [][]int, k int) int {
	n := len(matrix)
	h := &minHeap{}
	heap.Init(h)

	for i := 0; i < min(n, k); i++ {
		heap.Push(h, &element{val: matrix[i][0], row: i, col: 0})
	}

	var e *element
	for k > 0 {
		e = heap.Pop(h).(*element)

		if e.col < n-1 {
			heap.Push(
				h, &element{
					val: matrix[e.row][e.col+1],
					row: e.row,
					col: e.col + 1,
				},
			)
		}
		k -= 1
	}
	return e.val
}

func min(i, j int) int {
	if i > j {
		return j
	}
	return i
}

type element struct {
	val int
	row int
	col int
}

type minHeap []*element

func (h minHeap) Len() int {
	return len(h)
}
func (h minHeap) Less(i, j int) bool {
	return h[i].val < h[j].val
}
func (h minHeap) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}
func (h *minHeap) Push(x interface{}) {
	a := x.(*element)
	*h = append(*h, a)
}
func (h *minHeap) Pop() interface{} {
	n := len(*h)
	x := (*h)[n-1]
	*h = (*h)[:n-1]
	return x
}
