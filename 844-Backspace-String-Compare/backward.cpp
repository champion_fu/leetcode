//
// Created by champ on 2022/7/22.
//

class Solution {
public:
    bool backspaceCompare(string s, string t) {

        int skipI = 0, skipJ = 0;

        for(int i = s.size() - 1, j = t.size() - 1; i >= 0 || j >= 0; i--, j--) {
            while (i >= 0) {
                char sc = s[i];
                if (sc == '#') {
                    skipI++;
                    i--;
                } else if (skipI > 0) {
                    skipI--;
                    i--;
                } else {
                    break;
                }
            }

            while (j >= 0) {
                char st  = t[j];
                if (st == '#') {
                    skipJ++;
                    j--;
                } else if (skipJ > 0) {
                    skipJ--;
                    j--;
                } else {
                    break;
                }
            }

            if (i >= 0 && j >= 0 && s[i] != t[j]) return false;

            // compare char to nothing
            if ((i >= 0) != (j >= 0)) return false;
        }
        return true;
    }
};
