//
// Created by champ on 2022/9/22.
//

class Solution {
public:
    int matchPlayersAndTrainers(vector<int>& players, vector<int>& trainers) {
        sort(players.begin(), players.end(), greater<int>());
        sort(trainers.begin(), trainers.end(), greater<int>());
        int j = 0;
        for (int i = 0; i < players.size() && j < trainers.size(); i++) {
            if (players[i] <= trainers[j]) {
                j++;
            }
        }

        return j;
    }
};