package _09_Expressive_Words

func expressiveWords(s string, words []string) int {
	ret := 0
	for _, w := range words {
		if check(s, w) {
			ret += 1
		}
	}
	return ret
}

func check(s string, w string) bool {
	n := len(s)
	m := len(w)
	i, j, i2, j2 := 0, 0, 0, 0
	for i < n && j < m {
		if s[i] != w[j] {
			return false
		}
		for i2 < n && s[i2] == s[i] {
			i2++
		}
		for j2 < m && w[j2] == w[j] {
			j2++
		}
		if i2-i != j2-j && i2-i < max(3, j2-j) {
			// chars in s need to more than w
			return false
		}
		i = i2
		j = j2
	}
	return i == n && j == m
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}
