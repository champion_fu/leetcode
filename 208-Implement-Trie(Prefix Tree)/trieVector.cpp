//
// Created by champ on 2022/7/25.
//

class Trie {
public:
    Trie() {

    }

    void insert(string word) {
        Trie* node = this;
        for (char w : word) {
            w -= 'a';
            if (!node->next[w]) { // not find
                node->next[w] = new Trie();
            }
            node = node->next[w];
        }
        node->isWord = true;
    }

    bool search(string word) {
        Trie* node = this;
        for (char w : word) {
            w -= 'a';
            if (!node->next[w]) return false;
            node = node->next[w];
        }
        return node->isWord;
    }

    bool startsWith(string prefix) {
        Trie* node = this;
        for (char w : prefix) {
            w -= 'a';
            if (!node->next[w]) return false;
            node = node->next[w];
        }
        return true;
    }
private:
    Trie* next[26] = {};
    bool isWord = false;
};

/**
 * Your Trie object will be instantiated and called as such:
 * Trie* obj = new Trie();
 * obj->insert(word);
 * bool param_2 = obj->search(word);
 * bool param_3 = obj->startsWith(prefix);
 */
