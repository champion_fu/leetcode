package _08_Implement_Trie_Prefix_Tree_

type Trie struct {
	root *TrieNode
}

/** Initialize your data structure here. */
func Constructor() Trie {
	return Trie{
		root: newTrieNode(),
	}
}

/** Inserts a word into the trie. */
func (this *Trie) Insert(word string) {
	cur := this.root
	for i := range word {
		a := word[i]
		if cur.get(a) == nil {
			cur.push(a, newTrieNode())
		}
		cur = cur.get(a)
	}
	cur.setEnd()
	cur.setWord(word)
	return
}

func (this *Trie) searchWithPrefix(prefix string) *TrieNode {
	cur := this.root
	for i := range prefix {
		a := prefix[i]
		if cur.get(a) == nil {
			return nil
		}
		cur = cur.get(a)
	}
	return cur
}

/** Returns if the word is in the trie. */
func (this *Trie) Search(word string) bool {
	n := this.searchWithPrefix(word)
	if n != nil && n.isEnd() {
		return true
	}
	return false
}

/** Returns if there is any word in the trie that starts with the given prefix. */
func (this *Trie) StartsWith(prefix string) bool {
	n := this.searchWithPrefix(prefix)
	if n != nil {
		return true
	}
	return false
}

func newTrieNode() *TrieNode {
	return &TrieNode{
		link: make(map[byte]*TrieNode, 0),
	}
}

type TrieNode struct {
	link map[byte]*TrieNode
	end  bool
	word string
}

func (n *TrieNode) get(a byte) *TrieNode {
	return n.link[a]
}

func (n *TrieNode) push(a byte, trieNode *TrieNode) {
	n.link[a] = trieNode
}

func (n *TrieNode) isEnd() bool {
	return n.end
}

func (n *TrieNode) setEnd() {
	n.end = true
}

func (n *TrieNode) unsetEnd() {
	n.end = false
}

func (n *TrieNode) setWord(w string) {
	n.word = w
}

/**
 * Your Trie object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Insert(word);
 * param_2 := obj.Search(word);
 * param_3 := obj.StartsWith(prefix);
 */
