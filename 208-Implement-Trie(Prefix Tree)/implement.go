package _08_Implement_Trie_Prefix_Tree_

type Trie struct {
	root *TrieNode
}

/** Initialize your data structure here. */
func Constructor() Trie {
	return Trie{
		root: newTrieNode(),
	}
}

/** Inserts a word into the trie. */
func (this *Trie) Insert(word string) {
	node := this.root
	for i := range word {
		c := word[i]
		if !node.containsKey(c) {
			node.put(c, newTrieNode())
		}
		node = node.get(c)
	}
	node.setEnd()
}

func (this *Trie) searchPrefix(word string) *TrieNode {
	node := this.root
	for i := range word {
		c := word[i]
		if node.containsKey(c) {
			node = node.get(c)
		} else {
			return nil
		}
	}
	return node
}

/** Returns if the word is in the trie. */
func (this *Trie) Search(word string) bool {
	n := this.searchPrefix(word)
	if n != nil && n.isEnd() {
		return true
	}
	return false
}

/** Returns if there is any word in the trie that starts with the given prefix. */
func (this *Trie) StartsWith(prefix string) bool {
	n := this.searchPrefix(prefix)
	if n != nil {
		return true
	}
	return false
}

type TrieNode struct {
	links []*TrieNode
	end   bool
}

func newTrieNode() *TrieNode {
	return &TrieNode{
		links: make([]*TrieNode, 26),
		end:   false,
	}
}

func (t *TrieNode) containsKey(c byte) bool {
	return t.links[c-'a'] != nil
}

func (t *TrieNode) get(c byte) *TrieNode {
	return t.links[c-'a']
}

func (t *TrieNode) put(c byte, trie *TrieNode) {
	t.links[c-'a'] = trie
}

func (t *TrieNode) setEnd() {
	t.end = true
}
func (t *TrieNode) isEnd() bool {
	return t.end
}
