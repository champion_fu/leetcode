//
// Created by champ on 2022/7/23.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    bool isSameTree(TreeNode* p, TreeNode* q) {
        stack<pair<TreeNode*, TreeNode*>> s;
        s.push(pair<TreeNode*, TreeNode*>{p, q});

        while (!s.empty()) {
            auto tmp = s.top();
            s.pop();
            if (tmp.first == NULL && tmp.second == NULL) continue;
            else if (tmp.first == NULL || tmp.second == NULL) return false;
            else if (tmp.first->val != tmp.second->val) return false;

            s.push(pair<TreeNode*, TreeNode*>{tmp.first->left, tmp.second->left});
            s.push(pair<TreeNode*, TreeNode*>{tmp.first->right, tmp.second->right});
        }
        return true;
    }
};
