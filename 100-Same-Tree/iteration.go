package _00_Same_Tree

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func isSameTree(p *TreeNode, q *TreeNode) bool {
	if p == nil && q == nil {
		return true
	}

	s := stack{}
	s.Push(&Pair{l: p, r: q})

	for !s.IsEmpty() {
		pair := s.Pop()
		if pair.l == nil && pair.r == nil {
			continue
		}
		if pair.l == nil || pair.r == nil {
			return false
		}
		if pair.l.Val != pair.r.Val {
			return false
		}
		s.Push(&Pair{l: pair.l.Left, r: pair.r.Left})
		s.Push(&Pair{l: pair.l.Right, r: pair.r.Right})
	}
	return true
}

type Pair struct {
	l *TreeNode
	r *TreeNode
}

type stack []*Pair

func (s *stack) Push(n *Pair) {
	*s = append(*s, n)
}

func (s *stack) IsEmpty() bool {
	return len(*s) == 0
}

func (s *stack) Pop() *Pair {
	if !s.IsEmpty() {
		e := (*s)[len(*s)-1]
		(*s) = (*s)[:len(*s)-1]
		return e
	}
	return nil
}
