//
// Created by champ on 2022/7/26.
//

class Solution {
public:
    int totalFruit(vector<int>& fruits) {
        unordered_map<int, int> m;

        int i = 0, j = 0, ans = 0;
        while (j < fruits.size()) {
            m[fruits[j]]++;

            while (m.size() > 2) {
                m[fruits[i]]--;
                if (m[fruits[i]] == 0) {
                    m.erase(fruits[i]);
                }
                i++;
            }
            j++;
            ans = max(ans, j - i);
        }

        return ans;
    }
};
