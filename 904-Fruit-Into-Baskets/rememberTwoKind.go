package _04_Fruit_Into_Baskets

func totalFruit(fruits []int) int {
	kind1 := -1
	kind2 := -1
	var cumulate, continueSameKind, size int

	for _, v := range fruits {
		// fmt.Printf("v %v, k1 %v, k2 %v, continueSameKind %v, culmulate %v\n",v, kind1, kind2, continueSameKind, culmulate)
		if v != kind1 && v != kind2 {
			cumulate = continueSameKind + 1
		} else {
			// at least kind is one of them
			cumulate += 1
		}
		if v != kind2 {
			kind1 = kind2
			kind2 = v
			continueSameKind = 1
		} else {
			continueSameKind += 1
		}
		if cumulate > size {
			size = cumulate
		}
	}
	return size
}
