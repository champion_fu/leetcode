//
// Created by champ on 2022/10/10.
//
class UnionFind {
private:
    int cnt;
    vector<int> id, rank;
public:
    UnionFind(int cnt) : cnt(cnt) {
        id = vector<int>(cnt);
        rank = vector<int>(cnt, 0);
        for (int i = 0; i < cnt; i++) id[i] = i;
    }

    int find(int p) {
        if (id[p] == p) return p;
        return id[p] = find(id[p]);
    }

    bool connected(int p, int q) {
        return find(p) == find(q);
    }

    void connect(int p, int q) {
        int i = find(p);
        int j = find(q);
        if (i == j) return;

        if (rank[i] == rank[j]) {
            id[j] = i;
            rank[j]++;
        } else if (rank[i] < rank[j]) {
            id[i] = j;
        } else {
            // rank[i] > rank[j]
            id[j] = i;
        }
        cnt--;
    }
};

class Solution {
public:
    int numberOfGoodPaths(vector<int>& vals, vector<vector<int>>& edges) {
        int N = vals.size();
        int res = 0;
        vector<vector<int>> adj(N);
        // need using map not unordered_map
        map<int, vector<int>> sameValues;

        // assign sameValues
        for (int i = 0; i < N; i++) {
            sameValues[vals[i]].push_back(i);
        }

        // assign adjacent
        for (auto& edge : edges) {
            int u = edge[0];
            int v = edge[1];

            if (vals[u] >= vals[v]) {
                adj[u].push_back(v);
            } else if (vals[v] >= vals[u]) {
                adj[v].push_back(u);
            }
        }

        UnionFind uf(N);

        for (auto &[_, allNodes] : sameValues) {

            // for all nodes connect its adj
            for (int u : allNodes) {
                for (int v : adj[u]) {
                    uf.connect(u, v);
                }
            }

            // group
            unordered_map<int, int> group;

            // same union find is same group, summarize
            for (int u : allNodes) {
                group[uf.find(u)]++;
            }

            // add each individual node
            res += allNodes.size();

            // add path between individual nodes
            for (auto& [_, size] : group) {
                res += (size * (size - 1) / 2);
            }
        }

        return res;
    }
};
