package _85_Max_Consecutive_Ones

func findMaxConsecutiveOnes(nums []int) int {
	ret := 0
	tmp := 0
	for _, value := range nums {
		if value == 1 {
			tmp += 1
		} else {
			tmp = 0
		}
		if ret < tmp {
			ret = tmp
		}
	}
	return ret
}
