//
// Created by champ on 2022/6/22.
//

class SparseVector {
    unordered_map<int, int> m;
public:

    SparseVector(vector<int> &nums) {
        for (int i = 0; i < nums.size(); i++) {
            if (nums[i] != 0) {
                m[i] = nums[i];
            }
        }
    }

    // Return the dotProduct of two sparse vectors
    int dotProduct(SparseVector& vec) {
        int result = 0;
        for (pair<int, int> element : vec.m) {
            if (m[element.first] > 0 && element.second > 0) {
                result += element.second * m[element.first];
            }
        }
        return result;
    }
};

// Your SparseVector object will be instantiated and called as such:
// SparseVector v1(nums1);
// SparseVector v2(nums2);
// int ans = v1.dotProduct(v2);
