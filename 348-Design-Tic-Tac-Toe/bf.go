package _48_Design_Tic_Tac_Toe

type TicTacToe struct {
	Matrix [][]int
	N      int
}

/** Initialize your data structure here. */
func Constructor(n int) TicTacToe {
	var matrix = make([][]int, n)
	for i := 0; i < n; i++ {
		matrix[i] = make([]int, n)
	}
	return TicTacToe{
		Matrix: matrix,
		N:      n,
	}
}

/** Player {player} makes a move at ({row}, {col}).
  @param row The row of the board.
  @param col The column of the board.
  @param player The player, can be either 1 or 2.
  @return The current winning condition, can be either:
          0: No one wins.
          1: Player 1 wins.
          2: Player 2 wins. */
func (this *TicTacToe) Move(row int, col int, player int) int {
	this.Matrix[row][col] = player
	if this.checkCol(col, player) || this.checkRow(row, player) ||
		(row == col && this.checkDiagonal(player)) ||
		(row == this.N-col-1 && this.checkAntiDiagonal(player)) {
		return player
	}
	return 0
}

func (this *TicTacToe) checkCol(col int, player int) bool {
	for i := 0; i < this.N; i++ {
		if this.Matrix[i][col] != player {
			return false
		}
	}
	return true
}

func (this *TicTacToe) checkRow(row int, player int) bool {
	for i := 0; i < this.N; i++ {
		if this.Matrix[row][i] != player {
			return false
		}
	}
	return true
}

func (this *TicTacToe) checkDiagonal(player int) bool {
	for i := 0; i < this.N; i++ {
		if this.Matrix[i][i] != player {
			return false
		}
	}
	return true
}

func (this *TicTacToe) checkAntiDiagonal(player int) bool {
	for i := 0; i < this.N; i++ {
		if this.Matrix[i][this.N-i-1] != player {
			return false
		}
	}
	return true
}
