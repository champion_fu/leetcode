package _48_Design_Tic_Tac_Toe

type TicTacToe struct {
	Rows     []int
	Cols     []int
	Diag     int
	AntiDiag int
	N        int
}

/** Initialize your data structure here. */
func Constructor(n int) TicTacToe {
	var row = make([]int, n)
	var col = make([]int, n)
	return TicTacToe{
		Rows:     row,
		Cols:     col,
		Diag:     0,
		AntiDiag: 0,
		N:        n,
	}
}

/** Player {player} makes a move at ({row}, {col}).
  @param row The row of the board.
  @param col The column of the board.
  @param player The player, can be either 1 or 2.
  @return The current winning condition, can be either:
          0: No one wins.
          1: Player 1 wins.
          2: Player 2 wins. */
func (this *TicTacToe) Move(row int, col int, player int) int {
	var curPlayer int
	if player == 1 {
		curPlayer = 1
	} else {
		curPlayer = -1
	}

	this.Rows[row] += curPlayer
	this.Cols[col] += curPlayer

	if row == col {
		this.Diag += curPlayer
	}
	if col == this.N-row-1 {
		this.AntiDiag += curPlayer
	}

	if abs(this.Rows[row]) == this.N ||
		abs(this.Cols[col]) == this.N ||
		abs(this.Diag) == this.N ||
		abs(this.AntiDiag) == this.N {
		return player
	}
	return 0
}

func abs(a int) int {
	if a > 0 {
		return a
	}
	return -a
}
