//
// Created by champ on 2022/4/23.
//

class Solution {
public:
    string findReplaceString(string s, vector<int>& indices, vector<string>& sources, vector<string>& targets) {
        vector<pair<int, int>> sorted;
        // n log n
        for (int i = 0; i < indices.size(); i++) {
            sorted.push_back(pair<int, int>(indices[i], i));
        }
        sort(sorted.rbegin(), sorted.rend());

        // n
        for (auto ind : sorted) {
            cout << ind.first << "" << ind.second << endl;
            int i = ind.first;
            string src = sources[ind.second];
            string trg = targets[ind.second];
            if (s.substr(i, src.length()) == src) {
                s = s.substr(0, i) + trg + s.substr(i+src.length());
            }
        }

        return s;
    }
};
