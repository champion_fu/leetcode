package _33_Find_And_Replace_In_String

import (
	"fmt"
	"sort"
)

func findReplaceString(s string, indices []int, sources []string, targets []string) string {
	sorted := make([]*element, 0)
	for i, index := range indices {
		sorted = append(sorted, &element{indexOfS: index, i: i})
	}
	sort.Slice(
		sorted,
		func(i, j int) bool {
			return sorted[i].indexOfS < sorted[j].indexOfS
		},
	)

	extraIndex := 0
	for _, v := range sorted {
		fmt.Printf("sorted %+v\n", v)
		index := v.indexOfS + extraIndex
		var extra int
		s, extra = matchAndReplace(s, index, sources[v.i], targets[v.i])
		extraIndex += extra
	}

	return s
}

func matchAndReplace(s string, index int, source string, target string) (string, int) {
	n := len(source)
	extra := 0
	if s[index:index+n] == source {
		s = s[:index] + target + s[index+n:]
		extra += len(target) - n
	}
	return s, extra
}

type element struct {
	indexOfS int
	i        int
}
