package _1_Decode_Ways

import "strconv"

func numDecodings(s string) int {
	n := len(s)
	if s[0] == '0' {
		return 0
	}

	dp1 := 1
	dp2 := 1

	for i := 1; i < n; i++ {
		cur := 0
		// check one digit
		if s[i] != '0' {
			cur = dp1
		}

		// check two digits
		twoDigits, _ := strconv.Atoi(s[i-1 : i+1])
		if twoDigits <= 26 && twoDigits >= 10 {
			cur += dp2
		}

		dp2 = dp1
		dp1 = cur
	}
	return dp1
}
