package _1_Decode_Ways

import "strconv"

func numDecodings(s string) int {
	n := len(s)
	dp := make([]int, n+1)
	dp[0] = 1
	if s[0] == '0' {
		dp[1] = 0
	} else {
		dp[1] = 1
	}

	for i := 2; i < n+1; i++ {
		// check single digit
		if s[i-1] != '0' {
			dp[i] = dp[i-1]
		}

		// check double digit
		twoDigits, _ := strconv.Atoi(s[i-2 : i])
		if twoDigits >= 10 && twoDigits <= 26 {
			dp[i] += dp[i-2]
		}
	}
	return dp[n]
}
