//
// Created by champ on 2022/2/28.
//

class Solution {
private:
    int tri(int n, unordered_map<int, int>& hashTable) {
        if (!hashTable.count(n) > 0) {
            if (n == 0) {
                hashTable[0] = 0;
            } else if (n == 1 or n == 2) {
                hashTable[n] = 1;
            } else {
                hashTable[n] = tri(n-1, hashTable) +
                               tri(n-2, hashTable) +
                               tri(n-3, hashTable);
            }
        }
        return hashTable[n];
    }
public:
    int tribonacci(int n) {
        unordered_map<int, int> hashTable;
        return tri(n, hashTable);
    }
};
