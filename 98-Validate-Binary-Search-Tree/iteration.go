package _8_Validate_Binary_Search_Tree

import "math"

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func isValidBST(root *TreeNode) bool {
	if root == nil {
		return true
	}
	s := stack{}
	s.Push(root, math.MinInt32-1, math.MaxInt32+1)

	for !s.IsEmpty() {
		pair := s.Pop()
		n := pair.node
		min := pair.min
		max := pair.max

		if n == nil {
			continue
		}
		val := n.Val
		if val <= min || val >= max {
			return false
		}
		s.Push(n.Left, min, val)
		s.Push(n.Right, val, max)
	}
	return true
}

type pair struct {
	node *TreeNode
	min  int
	max  int
}

type stack []*pair

func (s *stack) IsEmpty() bool {
	return len(*s) == 0
}

func (s *stack) Push(a *TreeNode, min int, max int) {
	*s = append(*s, &pair{node: a, min: min, max: max})
}

func (s *stack) Pop() *pair {
	if s.IsEmpty() {
		return nil
	} else {
		index := len(*s) - 1
		element := (*s)[index]
		*s = (*s)[:index]
		return element
	}
}
