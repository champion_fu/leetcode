//
// Created by champ on 2022/7/26.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
private:
    bool validate(TreeNode* node, TreeNode* low, TreeNode* high) {
        if (node == NULL) return true;

        if ((low != NULL && node->val <= low->val) ||
            (high != NULL && node->val >= high->val)
                ) return false;

        return validate(node->right, node, high) && validate(node->left, low, node);
    }

public:
    bool isValidBST(TreeNode* root) {
        return validate(root, NULL, NULL);
    }
};
