package _8_Validate_Binary_Search_Tree

import "math"

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

var prev int

func isValidBST(root *TreeNode) bool {
	prev = math.MinInt32 - 1
	return recursiveInorder(root)
}

func recursiveInorder(n *TreeNode) bool {
	if n == nil {
		return true
	}
	if !recursiveInorder(n.Left) {
		return false
	}
	if n.Val <= prev {
		return false
	}
	prev = n.Val
	return recursiveInorder(n.Right)
}
