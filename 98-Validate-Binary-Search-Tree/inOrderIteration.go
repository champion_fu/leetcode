package _8_Validate_Binary_Search_Tree

import "math"

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func isValidBST(root *TreeNode) bool {
	s := stack{}
	prev := math.MinInt32 - 1
	curr := root

	for curr != nil || !s.IsEmpty() {
		for curr != nil {
			s.Push(curr)
			curr = curr.Left
		}
		curr = s.Pop()
		if curr.Val <= prev {
			return false
		}
		prev = curr.Val
		curr = curr.Right
	}
	return true
}

type stack []*TreeNode

func (s *stack) IsEmpty() bool {
	return len(*s) == 0
}

func (s *stack) Push(a *TreeNode) {
	*s = append(*s, a)
}

func (s *stack) Pop() *TreeNode {
	if s.IsEmpty() {
		return nil
	} else {
		index := len(*s) - 1
		element := (*s)[index]
		*s = (*s)[:index]
		return element
	}
}
