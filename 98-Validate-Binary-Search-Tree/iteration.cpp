//
// Created by champ on 2022/7/26.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
    stack<TreeNode*> stk, lower, upper;
private:
    void update(TreeNode* root, TreeNode* low, TreeNode* high) {
        stk.push(root);
        lower.push(low);
        upper.push(high);
    }

public:
    bool isValidBST(TreeNode* root) {
        TreeNode* low = NULL;
        TreeNode* high = NULL;
        update(root, low, high);

        while(!stk.empty()) {
            root = stk.top();
            stk.pop();
            low = lower.top();
            lower.pop();
            high = upper.top();
            upper.pop();

            if (root == NULL) continue;

            if (low != NULL && root->val <= low->val) return false;
            if (high != NULL && root->val >= high->val) return false;

            update(root->right, root, high);
            update(root->left, low, root);
        }
        return true;
    }
};
