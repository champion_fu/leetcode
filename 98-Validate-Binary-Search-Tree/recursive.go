package _8_Validate_Binary_Search_Tree

import "math"

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func isValidBST(root *TreeNode) bool {
	if root == nil {
		return true
	}
	return isValid(root, math.MinInt32-1, math.MaxInt32+1)
}

func isValid(node *TreeNode, min int, max int) bool {
	if node.Val <= min {
		return false
	}
	if node.Val >= max {
		return false
	}

	left := true
	right := true

	if node.Left != nil {
		left = isValid(node.Left, min, node.Val)
	}
	if node.Right != nil {
		right = isValid(node.Right, node.Val, max)
	}
	return left && right
}
