//
// Created by champ on 2022/7/26.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
    TreeNode* prev = NULL;
private:
    bool recursiveInorder(TreeNode* node) {
        if (node == NULL) return true;

        if (!recursiveInorder(node->left)) return false;

        if (prev != NULL && node->val <= prev->val) {
            return false;
        }

        prev = node;
        return recursiveInorder(node->right);

    }

public:
    bool isValidBST(TreeNode* root) {
        return recursiveInorder(root);
    }
};
