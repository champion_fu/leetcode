package _2_Reverse_Linked_List_II

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func reverseBetween(head *ListNode, left int, right int) *ListNode {
	if left == right {
		return head
	}

	dummyHead := &ListNode{Val: -1, Next: head}
	prev := dummyHead
	for i := 0; i < left-1; i++ {
		prev = prev.Next
	}

	end := head
	for i := 0; i < right-1; i++ {
		end = end.Next
	}

	reverseLinkList(prev, prev.Next, end.Next, right-left)
	return dummyHead.Next
}

func reverseLinkList(prev, cur, end *ListNode, times int) {
	cnt := 0
	for cnt < times {
		next := cur.Next
		cur.Next = end

		end = cur
		cur = next
		cnt++
	}
	cur.Next = end
	prev.Next = cur
}
