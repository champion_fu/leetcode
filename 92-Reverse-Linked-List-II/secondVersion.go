package _2_Reverse_Linked_List_II

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func reverseBetween(head *ListNode, left int, right int) *ListNode {
	if left == right {
		return head
	}

	dummyHead := &ListNode{Val: -1, Next: head}
	cur := head
	prev := dummyHead
	for left > 1 {
		prev = cur
		cur = cur.Next
		left--
		right--
	}

	// need to remember previous
	con := prev
	tail := cur

	for right > 0 {
		next := cur.Next
		cur.Next = prev

		prev = cur
		cur = next
		right--
	}

	con.Next = prev
	tail.Next = cur
	return dummyHead.Next
}
