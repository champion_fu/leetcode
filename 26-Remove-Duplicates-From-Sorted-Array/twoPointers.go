package _6_Remove_Duplicates_From_Sorted_Array

func removeDuplicates(nums []int) int {
	i, j := 0, 1
	length := len(nums)
	for j < length {
		if nums[i] != nums[j] {
			nums[i+1] = nums[j]
			i++
		}
		j++
	}
	return i + 1
}
