package _33_Clone_Graph

/**
 * Definition for a Node.
 * type Node struct {
 *     Val int
 *     Neighbors []*Node
 * }
 */

func cloneGraph(node *Node) *Node {
	if node == nil {
		return nil
	}

	nodeMap := make(map[int]*Node)
	oldToNew := make(map[*Node]*Node)
	q := queue{}
	q.Push(node)

	for !q.IsEmpty() {
		// pop
		n := q.Pop()

		// new node
		newNode := &Node{
			Val:       n.Val,
			Neighbors: make([]*Node, 0),
		}
		nodeMap[n.Val] = n
		oldToNew[n] = newNode

		// push
		for _, v := range n.Neighbors {
			if _, m := nodeMap[v.Val]; !m {
				q.Push(v)
			}
		}
	}

	// assign neighbors
	for _, oldNode := range nodeMap {
		newNode := oldToNew[oldNode]
		for _, v := range oldNode.Neighbors {
			newNode.Neighbors = append(newNode.Neighbors, oldToNew[v])
		}
	}
	return oldToNew[node]
}

type queue []*Node

func (q queue) Len() int {
	return len(q)
}
func (q queue) IsEmpty() bool {
	return q.Len() == 0
}
func (q *queue) Push(n *Node) {
	*q = append(*q, n)
}
func (q *queue) Pop() *Node {
	n := len(*q)
	e := (*q)[n-1]
	*q = (*q)[:n-1]
	return e
}
