//
// Created by champ on 2022/7/25.
//

/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> neighbors;
    Node() {
        val = 0;
        neighbors = vector<Node*>();
    }
    Node(int _val) {
        val = _val;
        neighbors = vector<Node*>();
    }
    Node(int _val, vector<Node*> _neighbors) {
        val = _val;
        neighbors = _neighbors;
    }
};
*/

class Solution {
    unordered_map<Node*, Node*> visited;
public:
    Node* cloneGraph(Node* node) {
        if (node == NULL) return node;

        // visited before, return clone form the visited dic.
        if (visited.find(node) != visited.end()) return visited[node];

        Node* cloneNode = new Node(node->val);
        visited[node] = cloneNode;

        for (auto n : node->neighbors) {
            cloneNode->neighbors.push_back(cloneGraph(n));
        }
        return cloneNode;
    }
};
