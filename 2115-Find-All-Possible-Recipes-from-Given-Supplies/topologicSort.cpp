//
// Created by champ on 2022/4/20.
//

class Solution {
public:
    vector<string> findAllRecipes(vector<string>& recipes, vector<vector<string>>& ingredients, vector<string>& supplies) {
        // topologic sort
        unordered_map<string, vector<string>> g; // ingredient -> [recipes...]
        map<string, int> indegress; // recipes -> count of ingredient

        for (int i = 0; i < recipes.size(); i++) {
            for (auto j : ingredients[i]) {
                g[j].push_back(recipes[i]);
                indegress[recipes[i]]++;
            }
        }

        queue<string> q;

        for (auto item: supplies) {
            if (indegress[item] == 0) {
                // include supply itself
                // cout << "item " << item << endl;
                q.push(item);
            }
        }

        while(!q.empty()) {
            auto i = q.front(); q.pop();

            for (auto recipe : g[i]) {
                indegress[recipe]--;
                if (indegress[recipe] == 0) {
                    q.push(recipe);
                }
            }
        }

        vector<string> ans;

        for (string recipe: recipes) {
            if (indegress[recipe] == 0) {
                ans.push_back(recipe);
            }
        }

        return ans;
    }
};
