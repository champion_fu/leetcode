package _26_Invert_Binary_Tree

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func invertTree(root *TreeNode) *TreeNode {
	if root == nil {
		return nil
	}

	var q []*TreeNode
	q = append(q, root)

	for len(q) != 0 {
		cur := q[0]
		q = q[1:]
		left := cur.Left
		cur.Left = cur.Right
		cur.Right = left
		if cur.Left != nil {
			q = append(q, cur.Left)
		}
		if cur.Right != nil {
			q = append(q, cur.Right)
		}
	}
	return root
}
