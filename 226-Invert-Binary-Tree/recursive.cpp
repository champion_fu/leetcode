//
// Created by champ on 2022/7/18.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    TreeNode* invertTree(TreeNode* root) {
        if (root == NULL) return root;

        invertTree(root->left);
        invertTree(root->right);

        swap(root);

        return root;
    }
private:
    void swap(TreeNode* node) {
        TreeNode* tmp = node->left;
        node->left = node->right;
        node->right = tmp;
    }
};
