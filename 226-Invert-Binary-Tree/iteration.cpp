//
// Created by champ on 2022/7/18.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    TreeNode* invertTree(TreeNode* root) {
        if (root == NULL) return root;
        queue<TreeNode*> q;
        q.push(root);

        while (!q.empty()) {
            TreeNode* p = q.front();
            if (p != NULL) {
                swap(p);
            }
            q.pop();

            if (p->left != NULL) q.push(p->left);
            if (p->right != NULL) q.push(p->right);
        }
        return root;
    }
private:
    void swap(TreeNode* node) {
        TreeNode* tmp = node->left;
        node->left = node->right;
        node->right = tmp;
    }
};
