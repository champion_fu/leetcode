package _97_Serialize_And_Deserialize_Binary_Tree

import (
	"strconv"
	"strings"
)

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

type Codec struct {
}

func Constructor() Codec {
	return Codec{}
}

// Serializes a tree to a single string.
func (this *Codec) serialize(root *TreeNode) string {
	ret := ""
	if root == nil {
		return ret
	}

	q := queue{}
	q.Push(root)

	for !q.IsEmpty() {
		ret += ","
		n := q.Pop()
		if n == nil {
			ret += "null"
			continue
		} else {
			ret += strconv.Itoa(n.Val)
		}
		q.Push(n.Left)
		q.Push(n.Right)
	}
	return ret[1:]
}

// Deserializes your encoded data to tree.
func (this *Codec) deserialize(data string) *TreeNode {
	d := strings.Split(data, ",") // d == []string
	val, err := strconv.Atoi(d[0])
	if err != nil {
		return nil
	}
	root := &TreeNode{
		Val:   val,
		Left:  nil,
		Right: nil,
	}
	q := queue{}
	q.Push(root)

	i := 1
	for i < len(d) {
		node := q.Pop()
		// Left First
		if d[i] != "null" {
			val, err := strconv.Atoi(d[i])
			if err != nil {
				return nil
			}
			l := &TreeNode{
				Val:   val,
				Left:  nil,
				Right: nil,
			}
			node.Left = l
			q.Push(l)
		}
		i++
		// Then right
		if d[i] != "null" {
			val, err := strconv.Atoi(d[i])
			if err != nil {
				return nil
			}
			r := &TreeNode{
				Val:   val,
				Left:  nil,
				Right: nil,
			}
			node.Right = r
			q.Push(r)
		}
		i++
		// Then next Left
	}
	return root
}

type queue []*TreeNode

func (q *queue) IsEmpty() bool {
	if len(*q) == 0 {
		return true
	}
	return false
}

func (q *queue) Push(n *TreeNode) {
	*q = append(*q, n)
}

func (q *queue) Pop() *TreeNode {
	if !q.IsEmpty() {
		e := (*q)[0]
		(*q) = (*q)[1:]
		return e
	}
	return nil
}

/**
 * Your Codec object will be instantiated and called as such:
 * ser := Constructor();
 * deser := Constructor();
 * data := ser.serialize(root);
 * ans := deser.deserialize(data);
 */
