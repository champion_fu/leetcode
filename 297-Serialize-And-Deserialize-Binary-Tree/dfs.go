package _97_Serialize_And_Deserialize_Binary_Tree

import (
	"fmt"
	"strconv"
	"strings"
)

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

type Codec struct {
}

func Constructor() Codec {
	return Codec{}
}

// Serializes a tree to a single string.
func (this *Codec) serialize(root *TreeNode) string {
	var builder strings.Builder
	traverse(root, &builder)

	return builder.String()[1:]
}

func traverse(node *TreeNode, builder *strings.Builder) {
	builder.WriteByte(',')
	if node == nil {
		return
	}

	builder.WriteString(strconv.Itoa(node.Val))
	traverse(node.Left, builder)
	traverse(node.Right, builder)
}

// Deserializes your encoded data to tree.
func (this *Codec) deserialize(data string) *TreeNode {
	str := strings.Split(data, ",")
	root := (*TreeNode)(nil)
	fmt.Printf("root %T %+v address %+v\n", root, root, &root)
	build(str, 0, &root)
	return root
}

func build(str []string, index int, node **TreeNode) int {
	curr := str[index]
	index++

	if curr == "" {
		*node = nil
		return index
	}

	val, _ := strconv.Atoi(curr)

	n := TreeNode{Val: val}

	if index < len(str) {
		index = build(str, index, &(n.Left))
	}
	if index < len(str) {
		index = build(str, index, &(n.Right))
	}
	*node = &n
	return index
}

/**
 * Your Codec object will be instantiated and called as such:
 * ser := Constructor();
 * deser := Constructor();
 * data := ser.serialize(root);
 * ans := deser.deserialize(data);
 */
