package _10_Split_Array_Largest_Sum

func splitArray(nums []int, m int) int {
	var r, l int
	for _, v := range nums {
		r += v
		if l < v {
			l = v
		}
	}

	// solve f(x), which can split array into m parts
	// sum of each part <= x
	for l <= r {
		mid := l + (r-l)/2
		s := 0
		count := 1
		for i := 0; i < len(nums); i++ {
			if s+nums[i] > mid {
				count += 1
				s = nums[i]
			} else {
				s += nums[i]
			}
		}
		if count <= m {
			r = mid - 1
		} else {
			l = mid + 1
		}
	}
	return l
}
