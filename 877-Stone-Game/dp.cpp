//
// Created by champ on 2022/4/12.
//

class Solution {
public:
    bool stoneGame(vector<int>& piles) {
        int n = piles.size();
        // dp[i][j] means the biggest number of stone you can get more than opponent picking piles in piles[i] ~ piles[j]
        vector<vector<int>> dp(n, vector<int>(n, 0));

        for (int i = 0; i < n; i++) dp[i][i] = piles[i]; // dp[0][0] always is piles[i]

        for (int d = 1; d < n; d++) {
            for (int i = 0; i + d < n; i++) {
                // if n = 4,
                // d= 1, i = 0 ~ 2, d = 2, i = 0 ~ 1, d = 3, i = 0
                dp[i][i+d] = max(piles[i] - dp[i+1][i+d], piles[i+d] - dp[i][i+d-1]);
            }
        }
        return dp[0][n-1] > 0;
    }
};
