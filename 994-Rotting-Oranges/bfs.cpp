//
// Created by champ on 2022/7/27.
//

class Solution {
public:
    int orangesRotting(vector<vector<int>>& grid) {
        int m = grid.size();
        int n = grid[0].size();

        queue<pair<int,int>> q;

        int freshOranges = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 2) {
                    q.push({i, j});
                } else if (grid[i][j] == 1) {
                    freshOranges++;
                }
            }
        }

        // no rotten orange
        if (q.empty()) {
            if (freshOranges > 0) return -1;
            else return 0;
        }

        // no remaining fresh orange
        if (freshOranges == 0) return 0;

        q.push({-1, -1});
        int res = -1;

        while (!q.empty()) {
            pair<int,int> p = q.front();
            q.pop();
            int i = p.first;
            int j = p.second;
            if (i == -1) {
                // finish one round
                res++;
                if (!q.empty()) q.push({-1, -1});
            } else {
                if (i >= 1 && grid[i-1][j] == 1) {
                    grid[i-1][j] = 2;
                    q.push({i-1, j});
                    freshOranges--;
                }
                if (j >= 1 && grid[i][j-1] == 1) {
                    grid[i][j-1] = 2;
                    q.push({i, j-1});
                    freshOranges--;
                }
                if (i < m-1 && grid[i+1][j] == 1) {
                    grid[i+1][j] = 2;
                    q.push({i+1, j});
                    freshOranges--;
                }
                if (j < n-1 && grid[i][j+1] == 1) {
                    grid[i][j+1] = 2;
                    q.push({i, j+1});
                    freshOranges--;
                }
            }
        }

        return freshOranges == 0 ? res : -1;
    }
};
