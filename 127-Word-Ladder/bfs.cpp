//
// Created by champ on 2022/8/12.
//

class Solution {
public:
    int ladderLength(string beginWord, string endWord, vector<string>& wordList) {
        unordered_set<string> words(wordList.begin(), wordList.end());
        queue<string> q;
        q.push(beginWord);
        int res = 1;

        while (!q.empty()) {
            int size = q.size();
            for (int i = 0; i < size; i++) {
                string word = q.front();
                q.pop();
                if (word == endWord) return res;

                for (int j = 0; j < word.size(); j++) {
                    // change char from index 0 to end
                    char originC = word[j];
                    for (int k = 'a'; k <= 'z'; k++) {
                        word[j] = k;
                        if (words.find(word) != words.end()) {
                            // find
                            q.push(word);
                            words.erase(word);
                        }
                    }
                    word[j] = originC;
                }
            }
            res++;
        }
        return 0;
    }
};
