package _0_Valid_Parentheses

func isValid(s string) bool {
	stk := stack{}

	for i := range s {
		if s[i] == '(' || s[i] == '{' || s[i] == '[' {
			stk.Push(s[i])
			continue
		}
		e := stk.Pop()
		if e == byte(0) {
			return false
		}
		if !match(e, s[i]) {
			return false
		}
	}
	if !stk.isEmpty() {
		return false
	}
	return true
}

func match(b1, b2 byte) bool {
	if b1 == '[' && b2 == ']' {
		return true
	}
	if b1 == '(' && b2 == ')' {
		return true
	}
	if b1 == '{' && b2 == '}' {
		return true
	}
	return false
}

type stack []byte

func (s *stack) Len() int {
	return len(*s)
}
func (s *stack) isEmpty() bool {
	return len(*s) == 0
}
func (s *stack) Push(b byte) {
	*s = append(*s, b)
}
func (s *stack) Pop() byte {
	if len(*s) == 0 {
		return byte(0)
	}
	n := len(*s)
	e := (*s)[n-1]
	*s = (*s)[:n-1]
	return e
}
