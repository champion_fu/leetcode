//
// Created by champ on 2022/7/18.
//

class Solution {
public:
    bool isValid(string s) {
        stack<char> stack;

        for (char c : s) {
            if (c == '(' || c == '{' || c == '[') stack.push(c);
            else {
                if (stack.empty()) return false;

                char top = stack.top();
                if ((top == '(' && c != ')') || (
                        top == '[' && c != ']') || (
                            top == '{' && c != '}')) return false;
                stack.pop();
            }
        }
        return stack.empty();
    }
};
