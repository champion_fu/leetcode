package _18_Pascal_s_Triangle

import "fmt"

func generate(numRows int) [][]int {
	var ret = make([][]int, 0)
	for i := 0; i < numRows; i++ {
		var level = make([]int, 0)
		for j := 0; j < i+1; j++ {
			fmt.Printf("i %v, j %v\n", i, j)
			if j == 0 || j == i {
				level = append(level, 1)
				continue
			}
			level = append(level, ret[i-1][j-1]+ret[i-1][j])
		}
		ret = append(ret, level)
	}
	fmt.Printf("ret %+v\n", ret)
	return ret
}
