//
// Created by champ on 2022/4/18.
//

/*
// Definition for a Node.
class Node {
public:
    int val;
    Node* left;
    Node* right;
    Node* next;

    Node() : val(0), left(NULL), right(NULL), next(NULL) {}

    Node(int _val) : val(_val), left(NULL), right(NULL), next(NULL) {}

    Node(int _val, Node* _left, Node* _right, Node* _next)
        : val(_val), left(_left), right(_right), next(_next) {}
};
*/

class Solution {
public:
    Node* connect(Node* root) {
        if (root == NULL) {
            return root;
        }

        deque<Node*> queue;
        queue.push_back(root);

        Node* dummyHead = new Node();
        dummyHead->next = root;

        while(!queue.empty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                auto n = queue.front();
                queue.pop_front();

                if (i == size-1) {
                    n->next = NULL;
                } else {
                    n->next = queue.front();
                }
                if (n->left) {
                    queue.push_back(n->left);
                }
                if (n->right) {
                    queue.push_back(n->right);
                }
            }
        }
        return dummyHead->next;
    }
};
