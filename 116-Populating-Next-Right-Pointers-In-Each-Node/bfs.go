package _16_Populating_Next_Right_Pointers_In_Each_Node

/**
 * Definition for a Node.
 * type Node struct {
 *     Val int
 *     Left *Node
 *     Right *Node
 *     Next *Node
 * }
 */

func connect(root *Node) *Node {
	if root == nil {
		return nil
	}

	q := queue{}
	q.Push(root)

	for !q.IsEmpty() {
		size := q.Size()

		for i := 0; i < size; i++ {
			n := q.Pop()

			if i < size-1 {
				n.Next = q.Top()
			}

			if n.Left != nil {
				q.Push(n.Left)
			}
			if n.Right != nil {
				q.Push(n.Right)
			}
		}
	}
	return root
}

type queue []*Node

func (q *queue) Size() int {
	return len(*q)
}

func (q *queue) IsEmpty() bool {
	return q.Size() == 0
}

func (q *queue) Push(n *Node) {
	*q = append(*q, n)
}

func (q *queue) Pop() *Node {
	if q.IsEmpty() {
		return nil
	} else {
		element := (*q)[0]
		*q = (*q)[1:]
		return element
	}
}

func (q *queue) Top() *Node {
	if q.IsEmpty() {
		return nil
	} else {
		return (*q)[0]
	}
}
