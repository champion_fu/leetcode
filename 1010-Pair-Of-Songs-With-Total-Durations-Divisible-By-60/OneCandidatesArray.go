package _010_Pair_Of_Songs_With_Total_Durations_Divisible_By_60

func numPairsDivisibleBy60(time []int) int {
	ret := 0
	candidates := make(map[int]int)

	for _, value := range time {
		reminder := value % 60
		if reminder == 0 {
			ret += candidates[0]
		} else if _, match := candidates[60-reminder]; match {
			ret += candidates[60-reminder]
		}
		candidates[reminder] += 1
	}
	return ret
}
