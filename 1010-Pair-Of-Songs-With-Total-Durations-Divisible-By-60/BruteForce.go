package _010_Pair_Of_Songs_With_Total_Durations_Divisible_By_60

func numPairsDivisibleBy60(time []int) int {
	ret := 0

	for i := 0; i < len(time)-1; i++ {
		for j := i + 1; j < len(time); j++ {
			if (time[i]+time[j])%60 == 0 {
				ret += 1
			}
		}
	}
	return ret
}
