package _70_Closest_Binary_Search_Tree_Value

import "math"

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func closestValue(root *TreeNode, target float64) int {
	rootValFloat64 := float64(root.Val)
	if rootValFloat64 > target && root.Left != nil {
		left := closestValue(root.Left, target)
		if math.Abs(rootValFloat64-target) < math.Abs(float64(left)-target) {
			return root.Val
		} else {
			return left
		}
	}
	if rootValFloat64 < target && root.Right != nil {
		right := closestValue(root.Right, target)
		if math.Abs(rootValFloat64-target) < math.Abs(float64(right)-target) {
			return root.Val
		} else {
			return right
		}
	}
	return root.Val
}
