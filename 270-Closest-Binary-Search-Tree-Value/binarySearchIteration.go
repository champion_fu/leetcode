package _70_Closest_Binary_Search_Tree_Value

import (
	"fmt"
	"math"
)

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func closestValue(root *TreeNode, target float64) int {
	closest := root.Val
	for root != nil {
		val := float64(root.Val)
		closestFloat64 := float64(closest)
		fmt.Printf("closeset %v, val %v\n", closest, val)
		if closestFloat64 == target {
			return closest
		}
		if math.Abs(val-target) < math.Abs(closestFloat64-target) {
			closest = root.Val
		}
		if val > target {
			root = root.Left
		} else {
			root = root.Right
		}
	}
	return closest
}
