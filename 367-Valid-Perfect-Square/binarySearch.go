package _67_Valid_Perfect_Square

func isPerfectSquare(num int) bool {
	if num == 1 {
		return true
	}

	l := 2
	r := num / 2

	for l <= r {
		m := l + (r-l)/2
		square := m * m
		if square == num {
			return true
		}
		if square > num {
			r = m - 1
		} else {
			l = m + 1
		}
	}
	return false
}
