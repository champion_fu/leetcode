package _18_The_Skyline_Problem

import (
	"container/heap"
	"fmt"
	"sort"
)

func getSkyline(buildings [][]int) [][]int {
	mappings := map[int][]*Skyline{}

	for _, building := range buildings {
		skyline := &Skyline{
			left:   building[0],
			right:  building[1],
			height: building[2],
		}

		mappings[skyline.left] = append(mappings[skyline.left], skyline)
		mappings[skyline.right] = append(mappings[skyline.right], skyline)
	}

	positions := []int{}
	for pos := range mappings {
		positions = append(positions, pos)
	}
	sort.Ints(positions)
	ret := [][]int{}

	h := Heap{&Skyline{left: -1, right: -1, height: 0}}

	for _, pos := range positions {
		curr := h[0].height
		fmt.Printf("cur %+v\n", curr)

		for _, skyline := range mappings[pos] {
			// this for loop solve the corner case
			if skyline.right > pos {
				// Corner case 1: like first come [2, 3], then [2, 5] => only append the [2, 5]
				heap.Push(&h, skyline)
			} else {
				// Corner case 2: like first come [5, 9], then [5, 7] => both will remove from heap
				heap.Remove(&h, skyline.index)
			}
			// Corner case 3: colse [5, 3], start [5, 3].
		}

		if h[0].height != curr {
			ret = append(ret, []int{pos, h[0].height})
		}
	}
	return ret
}

type Skyline struct {
	left, right, height int
	index               int
}

type Heap []*Skyline

func (h Heap) Len() int           { return len(h) }
func (h Heap) Less(i, j int) bool { return h[i].height > h[j].height }
func (h Heap) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
	h[i].index = i
	h[j].index = j
}
func (h *Heap) Push(x interface{}) {
	n := h.Len()
	skyline := x.(*Skyline)
	skyline.index = n
	*h = append(*h, skyline)
}
func (h *Heap) Pop() interface{} {
	n := h.Len()
	old := *h
	x := old[n-1]
	*h = (*h)[:n-1]
	return x
}
