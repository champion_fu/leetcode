package _18_The_Skyline_Problem

import (
	"container/heap"
	"sort"
)

func getSkyline(buildings [][]int) [][]int {
	n := len(buildings)
	ret := [][]int{}
	if n == 0 {
		return ret
	}
	xsorted := make([]*RoofPoint, 2*n)
	rightToLeftMap := make(map[*RoofPoint]*RoofPoint)

	// init and sort the x coordinates
	for i := 0; i < n; i++ {
		xsorted[2*i] = &RoofPoint{height: buildings[i][2], x: buildings[i][0], isLeft: true}
		xsorted[2*i+1] = &RoofPoint{height: buildings[i][2], x: buildings[i][1], isLeft: false}
		rightToLeftMap[xsorted[1+i*2]] = xsorted[i*2]
	}
	sort.Sort(XSorted(xsorted))

	pq := make(PQ, 0)
	heap.Init(&pq)

	// go through all x coordinates
	for i := 0; i < len(xsorted); i++ {
		p := xsorted[i]
		if p.isLeft {
			heap.Push(&pq, p)
			if len(ret) == 0 {
				// first one
				ret = append(ret, []int{p.x, p.height})
				continue
			}

			last := ret[len(ret)-1]
			h := pq[0]
			if last[1] >= h.height {
				// in the shadow
				continue
			}

			// last's height < current's height
			if last[0] == p.x {
				last[1] = h.height
				if len(ret) > 1 && ret[len(ret)-2][1] == h.height {
					// second from last's height == current height
					ret = ret[:len(ret)-1]
				}
			} else {
				ret = append(ret, []int{p.x, p.height})
			}
			continue
		}

		// is right
		heap.Remove(&pq, rightToLeftMap[p].heapIdx)
		last := ret[len(ret)-1]
		if len(pq) == 0 {
			// last one
			if last[0] < p.x {
				ret = append(ret, []int{p.x, 0})
			} else {
				last[1] = 0
			}
			continue
		}

		h := pq[0]
		if last[1] > h.height {
			if last[0] < p.x {
				ret = append(ret, []int{p.x, h.height})
			} else {
				last[1] = h.height
			}
		}
	}
	return ret
}

type RoofPoint struct {
	height  int
	heapIdx int
	isLeft  bool
	x       int
}

// this is for sort points by x direction.
type XSorted []*RoofPoint

func (p XSorted) Len() int      { return len(p) }
func (p XSorted) Swap(i, j int) { p[i], p[j] = p[j], p[i] }
func (p XSorted) Less(i, j int) bool {
	return p[i].x < p[j].x
}

// this is priority queue(heap) by height
type PQ []*RoofPoint

func (pq PQ) Len() int { return len(pq) }
func (pq PQ) Less(i, j int) bool {
	if pq[i].height == pq[j].height {
		return !pq[i].isLeft
	}
	return pq[i].height > pq[j].height
}
func (pq PQ) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].heapIdx = i
	pq[j].heapIdx = j
}
func (pq *PQ) Push(x interface{}) {
	n := len(*pq)
	p := x.(*RoofPoint)
	p.heapIdx = n
	*pq = append(*pq, p)
}
func (pq *PQ) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	old[n-1] = nil
	item.heapIdx = -1
	*pq = old[0 : n-1]
	return item
}
