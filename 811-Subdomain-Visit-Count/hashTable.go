package _11_Subdomain_Visit_Count

import (
	"strconv"
	"strings"
)

func subdomainVisits(cpdomains []string) []string {
	domainsMap := make(map[string]int, 0)

	for _, cpdomain := range cpdomains {
		raw := strings.Split(cpdomain, " ")
		visits, _ := strconv.Atoi(raw[0])
		domain := strings.Split(raw[1], ".")

		for i, _ := range domain {
			domainsMap[strings.Join(domain[i:], ".")] += visits
		}
	}

	answer := make([]string, 0)

	for domain, visits := range domainsMap {
		answer = append(answer, strconv.Itoa(visits)+" "+domain)
	}
	return answer
}
