//
// Created by champ on 2022/7/26.
//

class Solution {
public:
    bool isOneEditDistance(string s, string t) {
        int sSize = s.size();
        int tSize = t.size();

        if (sSize > tSize) return isOneEditDistance(t, s);
        if (tSize - sSize > 1) return false;

        for (int i = 0; i < sSize; i++) {
            if (s[i] != t[i]) {
                // not same
                if (sSize == tSize) {
                    // same len
                    return s.substr(i+1) == t.substr(i+1);
                } else {
                    return s.substr(i) == t.substr(i+1);
                }
            }
        }

        return (sSize + 1 == tSize);
    }
};