class Solution {
public:
    int countSymmetricIntegers(int low, int high) {
        int ans = 0;
        for (int i = low; i <= high; i++) {
            if (isSymmetrict(i)) ans++;
        }
        return ans;
    }

    bool isSymmetrict(int num) {
        int cnt = digit(num);
        if (cnt % 2 == 1) return false;
        int half = cnt /  2;
        int sum = 0;
        for (int i = 0; i < cnt; i++) {
            int val = num % 10;
            if (i < half) {
                sum += val;
            } else {
                sum -= val;
            }
            num /= 10;
        }
        return sum == 0;
    }

    int digit(int num) {
        int cnt = 0;
        while (num > 0) {
            cnt++;
            num /= 10;
        }
        return cnt;
    }
}
