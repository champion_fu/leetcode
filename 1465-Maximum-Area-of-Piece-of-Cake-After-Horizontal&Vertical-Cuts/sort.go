package _465_Maximum_Area_of_Piece_of_Cake_After_Horizontal_Vertical_Cuts

import "math"

func maxArea(h int, w int, horizontalCuts []int, verticalCuts []int) int {
	horizontalInterval := []int{h}
	for _, v := range horizontalCuts {
		horizontalInterval = append(horizontalInterval, v)
	}
	sort.Slice(
		horizontalInterval,
		func(i, j int) bool {
			return horizontalInterval[i] < horizontalInterval[j]
		},
	)

	verticalInterval := []int{w}
	for _, v := range verticalCuts {
		verticalInterval = append(verticalInterval, v)
	}
	sort.Slice(
		verticalInterval,
		func(i, j int) bool {
			return verticalInterval[i] < verticalInterval[j]
		},
	)

	maxHor := horizontalInterval[0]
	for i := 1; i < len(horizontalInterval); i++ {
		maxHor = max(maxHor, horizontalInterval[i]-horizontalInterval[i-1])
	}

	maxVer := verticalInterval[0]
	for i := 1; i < len(verticalInterval); i++ {
		maxVer = max(maxVer, verticalInterval[i]-verticalInterval[i-1])
	}
	mod := math.Pow10(9) + 7
	return (maxVer * maxHor) % int(mod)
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}
