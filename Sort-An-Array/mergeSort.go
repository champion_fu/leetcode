package Sort_An_Array

import "fmt"

func sortArray(nums []int) []int {
	length := len(nums)
	if length <= 1 {
		return nums
	}

	i := int(length / 2)
	left := sortArray(nums[:i])
	right := sortArray(nums[i:])

	return merge(left, right)
}

func merge(left []int, right []int) []int {
	var ret = make([]int, 0)
	l := 0
	r := 0
	fmt.Printf("l %v, left %+v, r %v right %+v\n ", l, left, r, right)
	for l < len(left) && r < len(right) {
		if left[l] < right[r] {
			ret = append(ret, left[l])
			l += 1
		} else {
			ret = append(ret, right[r])
			r += 1
		}
	}
	if l < len(left) {
		for i := l; i < len(left); i++ {
			ret = append(ret, left[i])
		}
	}

	if r < len(right) {
		for i := r; i < len(right); i++ {
			ret = append(ret, right[i])
		}
	}

	return ret
}
