package _2_N_Queens_II

var result int

func totalNQueens(n int) int {
	result = 0
	col := make([]bool, n)
	d1 := make([]bool, 2*n-1)
	d2 := make([]bool, 2*n-1)
	helper(0, n, col, d1, d2)
	return result
}

// looking through all columns(n), place 1 queen on row
func helper(row, n int, col, d1, d2 []bool) {
	if row == n {
		result += 1
		return
	}

	for j := 0; j < n; j++ {
		// iteration all columns
		if col[j] || d1[j+row] || d2[j-row+n-1] {
			continue
		}

		// update the constraint
		col[j] = true
		d1[row+j] = true
		d2[j-row+n-1] = true

		// find another queen on row+1
		helper(row+1, n, col, d1, d2)

		// remove the queen on row and column j
		col[j] = false
		d1[row+j] = false
		d2[j-row+n-1] = false
	}
}
