//
// Created by champ on 2022/3/20.
//

class Solution {
    struct direction {
        int x;
        int y;
    };
public:
    set<pair<int,int>> mp;
    map<int, direction> dirEnum = {
            {
                    0, {0, 1}
            },
            {
                    1, {1, 0}
            },
            {
                    2, {0, -1}
            },
            {
                    3, {-1, 0}
            },
    };
    int robotSim(vector<int>& commands, vector<vector<int>>& obstacles) {
        int dir = 0, ans = 0, x = 0, y = 0;
        for (auto i : obstacles) this->mp.insert({i[0], i[1]});
        for (auto i : commands) {
            cout << "i" << i << endl;
            if (i == -2) {
                cout << "turn left" << endl;
                dir = (dir - 1 + 4) % 4;
                continue;
            }
            if (i == -1) {
                dir = (dir + 1) % 4;
                cout << "turn right" << endl;
                continue;
            }

            direction nowDir = this->dirEnum[dir];
            cout << "nowDir x " << nowDir.x << "y " << nowDir.y << endl;
            for (int j = 0; j < i; j++) {
                if (!isValid(x, y, nowDir)) {
                    break;
                } else {
                    x += nowDir.x;
                    y += nowDir.y;
                }
            }
            cout << "x" << x << "y" << y << endl;
            int d = x*x + y*y;
            if (d > ans) ans = d;
        }
        return ans;
    }
private:
    bool isValid(int x, int y, direction dir) {
        return (this->mp.count({x + dir.x, y + dir.y})) ? false : true;
    }
};
