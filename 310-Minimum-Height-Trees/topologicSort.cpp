//
// Created by champ on 2022/8/2.
//

class Solution {
public:
    vector<int> findMinHeightTrees(int n, vector<vector<int>>& edges) {
        vector<int> res;
        if (n < 2) {
            for (int i = 0; i < n; i++) {
                res.push_back(i);
                return res;
            }
        }

        vector<unordered_set<int>> neighbors(n);
        for (int i = 0; i < edges.size(); i++) {
            int start = edges[i][0];
            int end = edges[i][1];

            neighbors[start].insert(end);
            neighbors[end].insert(start);
        }

        queue<int> leaves;
        for (int i = 0; i < n; i++) {
            if (neighbors[i].size() == 1) {
                leaves.push(i);
            }
        }

        int remainingNodes = n;
        while (remainingNodes > 2) {
            int size = leaves.size();
            remainingNodes -= size;

            for (int i = 0; i < size; i++) {
                int nodeIndex = leaves.front();
                leaves.pop();
                unordered_set<int> neighbor = neighbors[nodeIndex];
                for (int n : neighbor) {
                    neighbors[n].erase(nodeIndex);
                    if (neighbors[n].size() == 1) {
                        leaves.push(n);
                    }
                }
            }
        }
        while (!leaves.empty()) {
            res.push_back(leaves.front());
            leaves.pop();
        }
        return res;
    }
};
