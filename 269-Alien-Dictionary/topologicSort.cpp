//
// Created by champ on 2022/10/31.
//

class Solution {
public:
    string alienOrder(vector<string>& words) {
        if (words.size() == 0) return "";
        unordered_map<char, int> indegrees;
        unordered_map<char, unordered_set<char>> graph;

        for (string word: words) {
            for (char c : word) {
                indegrees[c] = 0;
            }
        }

        for (int i = 0; i < words.size()-1; i++) {
            string word = words[i];
            string next = words[i+1];
            int size = min(word.size(), next.size());
            for (int j = 0; j < size; j++) {
                if (word[j] != next[j]) {
                    // first letter where they differ
                    unordered_set<char> neighbor = graph[word[j]];
                    if (neighbor.find(next[j]) == neighbor.end()) {
                        // new one
                        graph[word[j]].insert(next[j]);
                        indegrees[next[j]]++;
                    }
                    break;
                }
                if (j == size - 1 && word.size() > next.size()) return "";
            }
        }

        string res;
        queue<char> q;

        for (auto [c, indegree] : indegrees) {
            if (indegree == 0) q.push(c);
        }

        while (!q.empty()) {
            char c = q.front();
            q.pop();
            res += c;

            if (graph[c].size() != 0) {
                for (auto& e : graph[c]) {
                    indegrees[e]--;
                    if (indegrees[e] == 0) q.push(e);
                }
            }

        }
        return res.size() == indegrees.size() ? res : "";
    }
};
