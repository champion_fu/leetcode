package _2_Generate_Parentheses

var ret []string
var number int

func generateParenthesis(n int) []string {
	number = n
	ret = make([]string, 0)
	backtracking("", 0, 0)
	return ret
}

// backtracking
func backtracking(candidate string, left, right int) {
	if len(candidate) == 2*number {
		// we get one answer, string don't need deep copy
		// However, 77. combinations []int need deep copy
		ret = append(ret, candidate)
		return
	}

	if left < number {
		candidate += "("
		backtracking(candidate, left+1, right)
		candidate = candidate[:len(candidate)-1]
	}
	if right < left { // valid condition is right < left
		candidate += ")"
		backtracking(candidate, left, right+1)
		candidate = candidate[:len(candidate)-1]
	}
}
