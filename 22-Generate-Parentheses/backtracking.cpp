//
// Created by champ on 2022/4/26.
//

class Solution {
public:
    vector<string> generateParenthesis(int n) {
        vector<string> results;
        backtracking(results, n, "", 0, 0);
        return results;
    }
private:
    void backtracking(vector<string>& results, int n, string s, int left, int right) {
        if (s.size() == 2*n) {
            results.push_back(s);
            return;
        }

        if (left < n) {
            s += '(';
            backtracking(results, n, s, left+1, right);
            s.pop_back();
        }
        if (right < left) {
            s += ')';
            backtracking(results, n, s, left, right+1);
            s.pop_back();
        }
    }
};
