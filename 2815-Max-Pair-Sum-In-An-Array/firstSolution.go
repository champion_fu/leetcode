package _815_Max_Pair_Sum_In_An_Array

func maxSum(nums []int) int {
	m := make(map[int][]int) // maximum digit -> []index
	for i, num := range nums {
		cNum := num
		maxDigit := -1
		for cNum > 0 {
			reminder := cNum % 10
			if reminder > maxDigit {
				maxDigit = reminder
			}
			cNum = cNum / 10
		}
		m[maxDigit] = append(m[maxDigit], i)
	}

	mSum := -1
	for _, candidates := range m {
		length := len(candidates)
		for i := 0; i < length; i++ {
			for j := i + 1; j < length; j++ {
				sum := nums[candidates[i]] + nums[candidates[j]]
				if sum > mSum {
					mSum = sum
				}
			}
		}
	}
	return mSum
}
