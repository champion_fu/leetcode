//
// Created by champ on 2022/7/9.
//

class WordDistance {
private:
    unordered_map<string, vector<int>> m;
public:
    WordDistance(vector<string>& wordsDict) {
        for (int i = 0; i < wordsDict.size(); i++) {
            m[wordsDict[i]].push_back(i);
        }

    }

    int shortest(string word1, string word2) {
        vector<int> index1s = m[word1], index2s = m[word2];
        int result = INT_MAX;
        for (int i = 0, j = 0; i < index1s.size() && j < index2s.size();) {
            result = min(abs(index1s[i]-index2s[j]), result);
            if (index1s[i] < index2s[j]) {
                i++; // looking for closing distance
            } else {
                j++;
            }
        }
        return result;
    }
};

/**
 * Your WordDistance object will be instantiated and called as such:
 * WordDistance* obj = new WordDistance(wordsDict);
 * int param_1 = obj->shortest(word1,word2);
 */
