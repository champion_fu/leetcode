package _71_Sum_of_Two_Integers

func getSum(a int, b int) int {
	// c := a & b << 1 // this may contain carry
	// c = c | (a^b)

	for b != 0 {
		answer := a ^ b
		carry := (a & b) << 1
		a = answer
		b = carry
	}
	return a
}
