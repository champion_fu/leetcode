package _98_House_Robber

func rob(nums []int) int {
	if len(nums) == 1 {
		return nums[0]
	}

	dp := make(map[int]int)

	dp[0] = nums[0]
	dp[1] = max(nums[0], nums[1])

	length := len(nums)
	for i := 2; i < length; i++ {

		dp[i] = max(dp[i-1], dp[i-2]+nums[i])
	}
	return dp[length-1]
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}
