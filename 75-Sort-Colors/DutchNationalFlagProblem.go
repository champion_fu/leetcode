package _5_Sort_Colors

func sortColors(nums []int) {
	l0, cur, l2 := 0, 0, len(nums)-1
	for cur <= l2 {
		if nums[cur] == 0 {
			swap(&nums, l0, cur)
			l0++
			cur++
		} else if nums[cur] == 2 {
			swap(&nums, l2, cur)
			l2--
		} else {
			cur++
		}
	}
}

func swap(nums *[]int, i, j int) {
	tmp := (*nums)[i]
	(*nums)[i] = (*nums)[j]
	(*nums)[j] = tmp
}
