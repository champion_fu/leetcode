//
// Created by champ on 2022/7/28.
//

class Solution {
public:
    void sortColors(vector<int>& nums) {
        unordered_map<int, int> m;
        for (int num : nums) {
            m[num]++;
        }

        for (int i = 0; i < nums.size(); i++) {
            if (m[0]) {
                nums[i] = 0;
                m[0]--;
            } else if (m[1]) {
                nums[i] = 1;
                m[1]--;
            } else if (m[2]) {
                nums[i] = 2;
                m[2]--;
            }
        }
    }
};
