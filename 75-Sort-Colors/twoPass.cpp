//
// Created by champ on 2022/4/28.
//

class Solution {
public:
    void sortColors(vector<int>& nums) {
        int zeroIndex = 0;
        for (int i = 0; i < nums.size(); i++) {
            if (nums[i] == 0) {
                int tmp = nums[i];
                nums[i] = nums[zeroIndex];
                nums[zeroIndex] = tmp;
                zeroIndex++;
            }
        }

        int oneIndex = zeroIndex;
        for (int i = zeroIndex; i < nums.size(); i++) {
            if (nums[i] == 1) {
                int tmp = nums[i];
                nums[i] = nums[oneIndex];
                nums[oneIndex] = tmp;
                oneIndex++;
            }
        }
    }
};
