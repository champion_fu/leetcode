//
// Created by champ on 2022/4/28.
//

class Solution {
public:
    void sortColors(vector<int>& nums) {
        int zeroIndex = 0, curIndex = 0, twoIndex = nums.size()-1;
        while (curIndex <= twoIndex) {
            if (nums[curIndex] == 0) {
                swap(nums, curIndex, zeroIndex);
                curIndex++;
                zeroIndex++;
            } else if (nums[curIndex] == 2) {
                swap(nums, curIndex, twoIndex);
                twoIndex--;
            } else {
                curIndex++;
            }
        }
    }
private:
    void swap(vector<int>& nums, int i, int j) {
        int tmp = nums[i];
        nums[i] = nums[j];
        nums[j] = tmp;
    }
};
