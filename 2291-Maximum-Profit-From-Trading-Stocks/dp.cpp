//
// Created by champ on 2022/9/13.
//

class Solution {
public:
    int maximumProfit(vector<int>& present, vector<int>& future, int budget) {
        // diff = future - present then pick the higher diff first
        vector<int> dp(budget+1, 0);
        vector<int> diff(present.size(), 0);

        for (int i = 0; i < present.size(); i++) {
            diff[i] = future[i] - present[i];
            for (int j = budget; j >= 0; j--) {
                if (j >= present[i] && diff[i] > 0) {
                    dp[j] = max(dp[j], dp[j - present[i]] + diff[i]);
                }
            }
        }
        return dp[budget];
    }
};
