//
// Created by champ on 2022/6/4.
//

class Solution {
public:
    vector<int> findBuildings(vector<int>& heights) {
        int length = heights.size();
        vector<int> results(1, length-1);
        int mostHeight = heights[length-1];

        for (int i = length - 2; i >= 0; i--) {
            if (heights[i] > mostHeight) {
                results.push_back(i);
                mostHeight = heights[i];
            }
        }
        reverse(results.begin(), results.end());
        return results;
    }
};
