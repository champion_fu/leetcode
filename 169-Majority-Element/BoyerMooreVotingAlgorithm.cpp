//
// Created by champ on 2022/7/21.
//

class Solution {
public:
    int majorityElement(vector<int>& nums) {
        int count = 0;
        int res = 0;
        for (auto i : nums) {
            if (count == 0) {
                res = i;
            } else if (res != i) {
                count--;
                continue;
            }
            count++;
        }
        return res;
    }
};
