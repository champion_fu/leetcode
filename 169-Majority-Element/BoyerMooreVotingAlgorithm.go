package _69_Majority_Element

// First, think of hashTable solution which is t:O(N) s:O(N)
// but when I think of majority (n/2), means in the end
// majority will appear than "others"
// count of majority > n/2, count of "others" < n/2
func majorityElement(nums []int) int {
	count := 0
	majority := 0
	for _, v := range nums {
		if count == 0 {
			majority = v
		} else if majority != v {
			// different
			count -= 1
			continue
		}
		count += 1
	}
	return majority
}
