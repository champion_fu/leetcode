//
// Created by champ on 2022/7/21.
//

class Solution {
public:
    int majorityElement(vector<int>& nums) {
        int size = nums.size();
        int threshold = (size + 1)/ 2;
        cout << threshold << endl;
        unordered_map<int, int> m;
        for (auto i : nums) {
            m[i]++;
            if(m[i] >= threshold) return i;
        }
        return -1;
    }
};
