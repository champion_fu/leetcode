//
// Created by champ on 2022/8/7.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> pathSum(TreeNode* root, int targetSum) {
        vector<vector<int>> res;
        if (root == NULL) return res;

        vector<int> tmp;
        bfs(root, targetSum, tmp, res);
        return res;
    }
private:
    void bfs(TreeNode* root, int targetSum, vector<int> path, vector<vector<int>>& res) {
        int val = root->val;
        path.push_back(root->val);
        if (val == targetSum && root->left == NULL && root->right == NULL) {
            res.push_back(path);
            return;
        }
        if (root->left) bfs(root->left, targetSum-val, path, res);
        if (root->right) bfs(root->right, targetSum-val, path, res);
    }
};
