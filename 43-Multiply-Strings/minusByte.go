package _3_Multiply_Strings

func multiply(num1 string, num2 string) string {
	if num1 == "0" || num2 == "0" {
		return "0"
	}

	n1 := len(num1)
	n2 := len(num2)
	if n1 < n2 {
		return multiply(num2, num1)
	}
	ret := make([]byte, n1+n2)
	for i := range ret {
		// important, because []byte initialize random value
		ret[i] = '0'
	}

	for i := n1 - 1; i >= 0; i-- {
		v1 := num1[i] - '0'
		for j := n2 - 1; j >= 0; j-- {
			v2 := num2[j] - '0'
			product := v1 * v2
			tmp := ret[i+j+1] - '0' + product
			ret[i+j+1] = tmp%10 + '0'
			ret[i+j] = (ret[i+j] - '0' + tmp/10) + '0'
		}
	}
	if ret[0] == '0' {
		return string(ret[1:])
	}
	return string(ret)
}
