//
// Created by champ on 2022/8/3.
//

class Solution {
public:
    int maxProduct(vector<int>& nums) {
        if (nums.size() == 0) return 0;

        int maxSoFar = nums[0];
        int minSoFar = nums[0];
        int res = maxSoFar;

        for (int i = 1; i < nums.size(); i++) {
            int cur = nums[i];
            int tmp = max(cur, max(cur*maxSoFar, cur*minSoFar));
            minSoFar = min(cur, min(cur*maxSoFar, cur*minSoFar));

            maxSoFar = tmp;

            res = max(res, maxSoFar);
        }
        return res;
    }
};
