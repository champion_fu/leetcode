package _09_Best_Time_To_Buy_And_Sell_Stock_With_Cooldown

import "math"

func maxProfit(prices []int) int {
	sold := math.MinInt32
	held := math.MinInt32
	reset := 0

	for _, v := range prices {
		preSold := sold

		sold = held + v
		held = max(held, reset-v)
		reset = max(preSold, reset)
	}
	return max(sold, reset)
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}
