//
// Created by champ on 2022/4/14.
//

class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int reset = 0;
        int hold = INT_MIN; // init there is no stock
        int sell = 0;

        for (auto p : prices) {
            int preSell = sell;

            sell = hold+p;
            hold = max(hold, reset-p); // cooldown
            reset = max(reset, preSell);
        }

        return max(sell, reset);
    }
};
