package _1_Merge_Two_Sorted_Lists

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func mergeTwoLists(l1 *ListNode, l2 *ListNode) *ListNode {
	if l1 == nil {
		return l2
	}
	if l2 == nil {
		return l1
	}
	dummyHead := &ListNode{Val: -1, Next: nil}
	p := dummyHead
	for l1 != nil && l2 != nil {
		if l1.Val > l2.Val {
			p.Next = &ListNode{Val: l2.Val, Next: nil}
			p = p.Next
			l2 = l2.Next
		} else {
			p.Next = &ListNode{Val: l1.Val, Next: nil}
			p = p.Next
			l1 = l1.Next
		}
	}
	if l1 != nil {
		p.Next = l1
	}
	if l2 != nil {
		p.Next = l2
	}
	return dummyHead.Next
}
