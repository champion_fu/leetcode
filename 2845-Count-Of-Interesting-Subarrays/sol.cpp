class Solution {
public:
    long long countInterestingSubarrays(vector<int>& nums, int modulo, int k) {
        long long res = 0;
        int acc = 0;
        unordered_map<int, int> count; // number of prefix array that have acc % mod == k
        count[0] = 1;

        // nums     , module, k
        // [3,1,9,6], 3     , 0
        // [0,1,0,0] => [1, 0, 1, 1]
        //               1. 1. 2  3 ->
        // acc           1  1  2  0
        // count        {[1: 1]}           res = 0
        // count        {[1: 2]}           res = 1
        // count        {[1:2], [2: 1]}    res = 1
        // count        {[1:2], [2: 1], [0: 1]} res = 2
        for (int a: nums) {

            acc = (acc + (a % modulo == k ? 1 : 0)) % modulo;
            res += count[(acc - k + modulo) % modulo];
            cout << "acc " << acc << " (acc - k + modulo % modulo) " << (acc - k + modulo) % modulo << endl;
            cout << "res " << res << endl;
            count[acc]++;
            for (auto a : count) {
                cout << "key " << a.first << " value " << a.second << endl;
            }
        }
        return res;
    }
}
