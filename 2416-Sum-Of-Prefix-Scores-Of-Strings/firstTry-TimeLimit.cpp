//
// Created by champ on 2022/9/21.
//

class Solution {
public:
    vector<int> sumPrefixScores(vector<string>& words) {
        unordered_map<string, int> m;
        unordered_map<string, vector<string>> candidates(0);
        for (const string& word : words) {

            bool dup = candidates.count(word) > 0 ? true : false;
            vector<string>& cc = candidates[word];
            for (int i = 0; i < word.size(); i++) {
                string subS = word.substr(0, i+1);
                // cout  << "subS " << subS << endl;
                if (!dup) candidates[word].push_back(subS);
                m[subS]++;
            }
        }
        vector<int> res(0);
        // for (auto& [s, c] : m) {
        //     cout << " s " << s << " c " << c << endl;
        // }
        for (const string& word : words) {
            int tmp = 0;
            for (const string& c : candidates[word]) {
                tmp += m[c];
            }
            res.push_back(tmp);
        }

        // for (int j = 0; j < words.size(); j++) {
        //     for (int i = 0; i < words[j].size(); i++) {
        //         string subS = words[j].substr(0, i+1);
        //         res[j] += m[subS];
        //     }
        // }

        return res;
    }
};
