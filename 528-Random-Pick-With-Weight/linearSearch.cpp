//
// Created by champ on 2022/3/10.
//

class Solution {
    vector<int> prefixSums;
public:
    Solution(vector<int>& w) {
        for (auto n : w) {
            prefixSums.push_back(n + (prefixSums.empty() ? 0 : prefixSums.back()));
        }
        for (auto n : prefixSums) {
            cout << n << endl;
        }
    }

    int pickIndex() {
        // random number [0, 1]
        float randNum = (float) rand() / RAND_MAX;
        float target = randNum * prefixSums.back();

        for (int i = 0; i < prefixSums.size(); i++)
            if (target < prefixSums[i]) return i;
        return prefixSums.size()-1;
    }
};

/**
 * Your Solution object will be instantiated and called as such:
 * Solution* obj = new Solution(w);
 * int param_1 = obj->pickIndex();
 */
