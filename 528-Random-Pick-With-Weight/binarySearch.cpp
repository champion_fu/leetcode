//
// Created by champ on 2022/3/10.
//

class Solution {
    vector<int> prefixSum;
public:
    Solution(vector<int>& w) {
        for (auto m : w) {
            prefixSum.push_back(m + (prefixSum.empty() ? 0 : prefixSum.back()));
        }
    }

    int pickIndex() {
        int target = rand() % prefixSum.back();
        int index = upper_bound(begin(prefixSum), end(prefixSum), target) - begin(prefixSum);
        return index;
    }
};
