package _30_Flatten_A_Multilevel_Doubly_Linked_List

/**
 * Definition for a Node.
 * type Node struct {
 *     Val int
 *     Prev *Node
 *     Next *Node
 *     Child *Node
 * }
 */

func flatten(root *Node) *Node {
	if root == nil {
		return root
	}

	dummyHead := &Node{Val: -1, Prev: nil, Next: root, Child: nil}
	p := dummyHead
	s := Stack{}
	s.Push(p.Next)

	for !s.IsEmpty() {
		cur := s.Pop()
		p.Next = cur
		cur.Prev = p

		if cur.Next != nil {
			s.Push(cur.Next)
		}

		if cur.Child != nil {
			s.Push(cur.Child)
			cur.Child = nil
		}
		p = cur
	}
	dummyHead.Next.Prev = nil
	return dummyHead.Next
}

type Stack []*Node

func (s *Stack) IsEmpty() bool {
	return len(*s) == 0
}

func (s *Stack) Push(a *Node) {
	*s = append(*s, a)
}

func (s *Stack) Pop() *Node {
	if s.IsEmpty() {
		return nil
	} else {
		index := len(*s) - 1
		element := (*s)[index]
		*s = (*s)[:index]
		return element
	}
}
