package _30_Flatten_A_Multilevel_Doubly_Linked_List

/**
 * Definition for a Node.
 * type Node struct {
 *     Val int
 *     Prev *Node
 *     Next *Node
 *     Child *Node
 * }
 */

func flatten(root *Node) *Node {
	if root == nil {
		return root
	}

	dummyHead := &Node{Val: -1, Prev: nil, Next: nil, Child: nil}
	flatten_dfs(dummyHead, root)

	dummyHead.Next.Prev = nil
	return dummyHead.Next
}
func flatten_dfs(prev *Node, cur *Node) (append_head *Node) {
	// return the tail of the flatten list
	if cur == nil {
		append_head = prev
		return
	}

	cur.Prev = prev
	prev.Next = cur

	tempNext := cur.Next
	childTail := flatten_dfs(cur, cur.Child)
	cur.Child = nil

	append_head = flatten_dfs(childTail, tempNext)
	return
}
