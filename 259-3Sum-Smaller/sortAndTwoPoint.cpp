//
// Created by champ on 2022/4/15.
//

class Solution {
public:
    int threeSumSmaller(vector<int>& nums, int target) {
        int ret = 0;
        // sort NLogN
        // find N^2
        if (nums.size() < 3) return 0;
        sort(nums.begin(), nums.end());
        for (int i = 0; i < nums.size()-2; i++) {
            int l = i+1;
            int r = nums.size() - 1;
            while (l < r) {
                // cout << nums[i] << nums[l] << nums[r] << endl;
                if (nums[i]+nums[l]+nums[r] < target) {
                    ret += r-l;
                    l++;
                } else {
                    r--;
                }
            }
        }
        return ret;
    }
};
