package _95_Max_Area_Of_Island

func maxAreaOfIsland(grid [][]int) int {
	m := len(grid)
	n := len(grid[0])
	dirM := []int{-1, 1, 0, 0}
	dirN := []int{0, 0, -1, 1}
	seen := make([][]int, m)
	for i := 0; i < m; i++ {
		seen[i] = make([]int, n)
	}

	s := stack{}
	ret := 0
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if grid[i][j] == 0 {
				// not island
				continue
			}
			if seen[i][j] == 1 {
				// is visited
				continue
			}

			s.Push(&element{m: i, n: j})
			// fmt.Printf("i, %v, j %v, s %+v\n",i, j, s)
			seen[i][j] = 1
			shape := 1
			for !s.IsEmpty() {
				e := s.Pop()
				for dir := 0; dir < 4; dir++ {
					newM := e.m + dirM[dir]
					newN := e.n + dirN[dir]
					if newM < 0 || newM >= m {
						// out of bound
						continue
					}
					if newN < 0 || newN >= n {
						// out of bound
						continue
					}
					if seen[newM][newN] != 1 && grid[newM][newN] == 1 {
						// fmt.Printf("newM, %v, newN %v, s %+v\n", newM, newN, s)
						s.Push(&element{m: newM, n: newN})
						seen[newM][newN] = 1
						shape += 1

					}
				}
			}
			ret = max(ret, shape)
		}
	}
	return ret
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}

type element struct {
	m int
	n int
}
type stack []*element

func (s stack) IsEmpty() bool {
	return len(s) == 0
}
func (s *stack) Push(e *element) {
	*s = append(*s, e)
}
func (s *stack) Pop() *element {
	n := len(*s)
	e := (*s)[n-1]
	*s = (*s)[:n-1]
	return e
}
