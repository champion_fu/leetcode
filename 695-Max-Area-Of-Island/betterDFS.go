package _95_Max_Area_Of_Island

func maxAreaOfIsland(grid [][]int) int {
	m := len(grid)
	n := len(grid[0])
	dirM := []int{-1, 1, 0, 0}
	dirN := []int{0, 0, -1, 1}

	s := stack{}
	ret := 0
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if grid[i][j] == 0 {
				// not island
				continue
			}

			grid[i][j] = 0
			s.Push(&element{m: i, n: j})
			shape := 1
			for !s.IsEmpty() {
				e := s.Pop()
				for dir := 0; dir < 4; dir++ {
					newM := e.m + dirM[dir]
					newN := e.n + dirN[dir]
					if newM < 0 || newM >= m {
						// out of bound
						continue
					}
					if newN < 0 || newN >= n {
						// out of bound
						continue
					}
					if grid[newM][newN] == 1 {
						s.Push(&element{m: newM, n: newN})
						grid[newM][newN] = 0
						shape += 1
					}
				}
			}
			ret = max(ret, shape)
		}
	}
	return ret
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}

type element struct {
	m int
	n int
}
type stack []*element

func (s stack) IsEmpty() bool {
	return len(s) == 0
}
func (s *stack) Push(e *element) {
	*s = append(*s, e)
}
func (s *stack) Pop() *element {
	n := len(*s)
	e := (*s)[n-1]
	*s = (*s)[:n-1]
	return e
}
