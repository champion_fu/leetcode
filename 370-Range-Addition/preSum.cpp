//
// Created by champ on 2022/4/10.
//

class Solution {
public:
    vector<int> getModifiedArray(int length, vector<vector<int>>& updates) {
        vector<int> ret(length, 0);

        for (auto update : updates) {
            int startIndex = update[0];
            int endIndex = update[1];
            int val = update[2];

            ret[startIndex] += val;

            if (endIndex < length - 1) ret[endIndex+1] -= val;
        }

        int sum = 0;
        for (int i = 0; i < length; i++) {
            sum += ret[i];
            ret[i] = sum;
        }
        return ret;
    }
};
