//
// Created by champ on 2022/4/10.
//

class Solution {
public:
    vector<int> getModifiedArray(int length, vector<vector<int>>& updates) {
        vector<int> ret(length);

        for (auto update : updates) {
            int startIndex = update[0];
            int endIndex = update[1];
            int val = update[2];
            for (int i = startIndex; i < endIndex+1; i++) {
                ret[i] += val;
            }
        }
        return ret;
    }
};
