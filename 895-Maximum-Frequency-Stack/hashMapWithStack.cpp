//
// Created by champ on 2022/8/14.
//

class FreqStack {
    unordered_map<int, int> freq; // num: count
    unordered_map<int, stack<int>> group; // freq: stack
    int maxFreq = 0;
public:
    FreqStack() {
    }

    void push(int val) {
        maxFreq = max(maxFreq, ++freq[val]);
        group[freq[val]].push(val);
    }

    int pop() {
        int x = group[maxFreq].top();
        group[maxFreq].pop();
        if (group[freq[x]--].size() == 0) maxFreq--;
        return x;
    }
};

/**
 * Your FreqStack object will be instantiated and called as such:
 * FreqStack* obj = new FreqStack();
 * obj->push(val);
 * int param_2 = obj->pop();
 */
