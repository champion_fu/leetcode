package _82_License_Key_Formatting

import "strings"

func licenseKeyFormatting(s string, k int) string {
	s = strings.ReplaceAll(s, "-", "")
	s = strings.ToUpper(s)

	n := len(s)
	if n < k {
		return s[:n]
	}
	var s1 int
	remain := n % k
	if remain != 0 {
		s1 = remain
	} else {
		s1 = k
	}
	ret := s[:s1]
	for s1 < n {
		ret += "-" + s[s1:s1+k]
		s1 += k
	}
	return ret
}
