package _44_Binary_Tree_Preorder_Traversal

import "fmt"

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

func preorderTraversal(root *TreeNode) []int {
	var ret = make([]int, 0)
	if root == nil {
		return ret
	}

	s := stack{}
	s.Push(root)

	for !s.IsEmpty() {
		cur := s.Pop()
		fmt.Printf("%+v\n", cur)
		ret = append(ret, cur.Val)

		// Pre-order, C -> L -> R so Right first push to stack
		if cur.Right != nil {
			s.Push(cur.Right)
		}
		if cur.Left != nil {
			s.Push(cur.Left)
		}
	}
	return ret
}

type stack []*TreeNode

func (s *stack) IsEmpty() bool {
	return len(*s) == 0
}

func (s *stack) Push(a *TreeNode) {
	*s = append(*s, a)
}

func (s *stack) Pop() *TreeNode {
	if s.IsEmpty() {
		return nil
	} else {
		index := len(*s) - 1
		element := (*s)[index]
		*s = (*s)[:index]
		return element
	}
}
