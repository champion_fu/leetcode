package _44_Binary_Tree_Preorder_Traversal

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

func preorderTraversal(root *TreeNode) []int {
	var ret = make([]int, 0)
	return recursivePreOrder(root, ret)
}

func recursivePreOrder(p *TreeNode, ret []int) []int {
	if p == nil {
		return ret
	}
	ret = append(ret, p.Val)
	if p.Left != nil {
		ret = recursivePreOrder(p.Left, ret)
	}
	if p.Right != nil {
		ret = recursivePreOrder(p.Right, ret)
	}
	return ret
}
