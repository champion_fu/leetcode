//
// Created by champ on 2022/7/20.
//

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    bool hasCycle(ListNode *head) {
        if (head == NULL) return false;
        ListNode* pSlow = head;
        ListNode* pFast = head->next;

        while (pSlow != pFast) {
            if (pFast == NULL || pFast->next == NULL) return false;

            pSlow = pSlow->next;
            pFast = pFast->next->next;
        }
        return true;

    }
};
