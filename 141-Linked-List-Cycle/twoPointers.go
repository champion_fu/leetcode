package _41_Linked_List_Cycle

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func hasCycle(head *ListNode) bool {
	p := head
	pFast := p
	if p == nil {
		return false
	}
	for pFast.Next != nil && pFast.Next.Next != nil {
		p = p.Next
		pFast = pFast.Next.Next
		if p == pFast {
			return true
		}
	}
	return false
}
