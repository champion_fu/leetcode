//
// Created by champ on 2022/7/28.
//

class DSU {
    vector<int> represent;
    vector<int> size;
public:
    DSU(int s): represent(s), size(s) {
        for (int i = 0; i < s; i++) {
            represent[i] = i;
            size[i] = 1;
        }
    }
    int findRepresent(int group) {
        if (represent[group] == group) return group;
        return represent[group] = findRepresent(represent[group]);
    }

    void mergeTwo(int groupA, int groupB) {
        int a = findRepresent(groupA);
        int b = findRepresent(groupB);
        if (a == b) return;

        if (size[groupA] <= size[groupB]) {
            represent[a] = b;
            size[groupB] += size[groupA];
        } else {
            represent[b] = a;
            size[groupA] += size[groupB];
        }
    }
};

class Solution {
public:
    vector<vector<string>> accountsMerge(vector<vector<string>>& accounts) {
        int size = accounts.size();
        DSU dsu(size);

        unordered_map<string, int> group; // email to index of account

        for (int i = 0; i < size; i++) {
            vector<string> account = accounts[i];
            for (int j = 1; j < account.size(); j++) {
                string email = account[j];
                if (group.find(email) == group.end()) {
                    // not visited
                    group[email] = i;
                } else {
                    // merge, because visited before
                    dsu.mergeTwo(i, group[email]);
                }
            }
        }


        unordered_map<int, vector<string>> components;
        for (auto emailGroup : group) {
            string email = emailGroup.first;
            int gIndex = emailGroup.second;

            components[dsu.findRepresent(gIndex)].push_back(email);
        }


        vector<vector<string>> res;
        for (auto component: components) {
            int gIndex = component.first;
            string name = accounts[gIndex][0];
            vector<string> tmp = {name};
            tmp.insert(tmp.end(), component.second.begin(), component.second.end());
            sort(tmp.begin() + 1, tmp.end());
            res.push_back(tmp);
        }

        return res;
    }
};
