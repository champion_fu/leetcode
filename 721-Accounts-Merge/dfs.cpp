//
// Created by champ on 2022/7/28.
//

class Solution {
    unordered_set<string> visited;
    unordered_map<string, vector<string>> adjacent; // email to emails belong to same one;
private:
    void dfs(vector<string>& tmp, string email) {
        visited.insert(email);
        tmp.push_back(email);
        for (string e : adjacent[email]) {
            if (visited.find(e) == visited.end()) {
                dfs(tmp, e);
            }
        }
    }
public:
    vector<vector<string>> accountsMerge(vector<vector<string>>& accounts) {
        for (int i = 0; i < accounts.size(); i++) {
            vector<string> account = accounts[i];

            for (int j = 1; j < account.size(); j++) {
                // skip j = 1: name
                string email = account[j];
                // append all email to adjacent
                adjacent[email].insert(adjacent[email].end(), account.begin()+1, account.end());
            }
        }

        vector<vector<string>> res;
        for (vector<string> account : accounts) {
            string name = account[0];
            string firstEmail = account[1];
            if (visited.find(firstEmail) == visited.end()) {
                // didn't visit before
                vector<string> tmp{name};
                dfs(tmp, firstEmail);
                sort(tmp.begin()+1, tmp.end());
                res.push_back(tmp);
            }
        }
        return res;
    }
};
