//
// Created by champ on 2022/10/12.
//

class Solution {
public:
    bool findSubarrays(vector<int>& nums) {
        vector<int> subSum(nums.size()-1, 0);

        for (int i = 1; i < nums.size(); i++) {
            subSum[i-1] = nums[i] + nums[i-1];
        }

        sort(subSum.begin(), subSum.end());
        for (int i = 1; i < nums.size()-1; i++) {
            if (subSum[i] == subSum[i-1]) return true;
        }
        return false;
    }
};
