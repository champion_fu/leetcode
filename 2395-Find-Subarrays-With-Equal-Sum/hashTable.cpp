//
// Created by champ on 2022/10/12.
//

class Solution {
public:
    bool findSubarrays(vector<int>& nums) {
        set<int> s;
        for (int r = 1; r < nums.size(); r++) {
            if (!s.insert(nums[r]+nums[r-1]).second) {
                return true;
            }
        }
        return false;
    }
};;
