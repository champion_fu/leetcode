//
// Created by champ on 2022/4/13.
//

class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        int ret = 0;
        int i = 0;
        if (s.size() <= 1) {
            return s.size();
        }
        while (i < s.size()) {
            set<char> dup = {s[i]};
            int l = 1;
            while (i+l < s.size()) {
                char c = s[i+l];
                // cout << "i " << i << " l " << l << " c " << c << endl;
                if (dup.find(c) == dup.end()) {
                    dup.insert(c);
                    l++;
                } else {
                    break;
                }
            }
            ret = l > ret ? l : ret;
            i++;
        }
        return ret;
    }
};
