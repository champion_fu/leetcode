package __Longest_Substring_Without_Repeating_Characters

func lengthOfLongestSubstring(s string) int {
	n := len(s)
	if n == 0 {
		return 0
	}

	var strMap map[byte]bool
	ret := 0
	for i := 0; i < n; i++ {
		strMap = make(map[byte]bool, 0)
		strMap[s[i]] = true
		for j := i + 1; j < n; j++ {
			cj := s[j]
			if _, m := strMap[cj]; m {
				break
			} else {
				strMap[cj] = true
			}
		}
		if len(strMap) > ret {
			ret = len(strMap)
		}
	}
	return ret
}
