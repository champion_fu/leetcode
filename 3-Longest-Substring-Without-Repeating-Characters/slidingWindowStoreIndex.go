package __Longest_Substring_Without_Repeating_Characters

func lengthOfLongestSubstring(s string) int {
	var l, r, ret int
	charHashTable := make(map[byte]int, 0)

	for r < len(s) {
		cr := s[r]
		if _, m := charHashTable[cr]; m {
			l = max(charHashTable[cr], l)
		}

		ret = max(ret, r-l+1)
		charHashTable[cr] = r + 1
		r++
	}
	return ret
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
