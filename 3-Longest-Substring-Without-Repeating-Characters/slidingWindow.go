package __Longest_Substring_Without_Repeating_Characters

func lengthOfLongestSubstring(s string) int {
	var l, r, ret int
	charHashTable := make(map[byte]int, 0)

	for r < len(s) {
		cr := s[r]
		if _, m := charHashTable[cr]; m {
			charHashTable[cr] += 1
		} else {
			charHashTable[cr] = 1
		}

		for charHashTable[cr] > 1 {
			cl := s[l]
			charHashTable[cl] -= 1
			l++
		}
		ret = max(ret, r-l+1)
		r++
	}
	return ret
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
