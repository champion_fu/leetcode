//
// Created by champ on 2022/4/13.
//

class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        unordered_map<char, int> charHashTable;

        int ret = 0;
        int l = 0, r = 0;
        while (r < s.size()) {
            char cr = s[r];
            auto it = charHashTable.find(cr);
            if (it != charHashTable.end()) {
                // find
                l = l > it->second ? l : it->second;
            }

            ret = r-l+1 > ret ? r-l+1 : ret;
            charHashTable[cr] = r + 1;
            r++;
        }
        return ret;
    }
};
