package _12_Sort_An_Array

import "math/rand"

var sorted []int

func sortArray(nums []int) []int {
	sorted = nums
	quickSort(0, len(nums)-1)
	return sorted
}

func quickSort(left, right int) {
	if left >= right {
		return
	}
	pIndex := rand.Intn(right-left+1) + left
	pivot := sorted[pIndex]
	l := left
	r := right
	for l <= r {
		for l <= r && sorted[l] < pivot {
			l++
		}
		for l <= r && sorted[r] > pivot {
			r--
		}
		if r >= l {
			// r is smaller than pivot, but on right hand side
			swap(r, l)
			l++
			r--
		}
	}
	quickSort(left, r)
	quickSort(l, right)
}

func swap(i, j int) {
	sorted[i], sorted[j] = sorted[j], sorted[i]
}
