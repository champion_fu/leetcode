package _12_Sort_An_Array

func sortArray(nums []int) []int {
	length := len(nums)
	if length <= 1 {
		return nums
	}

	i := length / 2
	left := sortArray(nums[:i])
	right := sortArray(nums[i:])
	return merge(left, right)
}

func merge(left []int, right []int) []int {
	var ret = make([]int, 0)
	l := 0
	r := 0
	for l < len(left) && r < len(right) {
		if left[l] < right[r] {
			ret = append(ret, left[l])
			l++
		} else {
			ret = append(ret, right[r])
			r++
		}
	}
	if l < len(left) {
		for i := l; i < len(left); i++ {
			ret = append(ret, left[i])
		}
	}
	if r < len(right) {
		for i := r; i < len(right); i++ {
			ret = append(ret, right[i])
		}
	}
	return ret
}
