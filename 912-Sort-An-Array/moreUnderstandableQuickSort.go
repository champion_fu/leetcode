package _12_Sort_An_Array

import "math/rand"

var sorted []int

func sortArray(nums []int) []int {
	sorted = nums
	quickSort(0, len(nums)-1)
	return sorted
}

func quickSort(left, right int) {
	if left >= right {
		return
	}
	pIndex := rand.Intn(right-left+1) + left
	pIndex = partition(left, right, pIndex)
	quickSort(left, pIndex-1)
	quickSort(pIndex+1, right)
}

func partition(left, right, pIndex int) int {
	// move pivot to the end
	pivot := sorted[pIndex]
	swap(pIndex, right)

	l := left
	for j := l; j < right; j++ {
		if sorted[j] < pivot {
			swap(j, l)
			l++
		}
	}

	// move pivot to the final index
	swap(l, right)
	return l
}

func swap(i, j int) {
	sorted[i], sorted[j] = sorted[j], sorted[i]
}
