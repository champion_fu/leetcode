package _5_3Sum

import (
	"sort"
)

func threeSum(nums []int) [][]int {
	n := len(nums)
	if n < 3 {
		return [][]int{}
	}

	// sorted NLogN
	sort.Slice(
		nums,
		func(i, j int) bool {
			return nums[i] < nums[j]
		},
	)

	var ret [][]int
	// iteration i from nums, n*n
	for i := 0; i < n; i++ {
		if i > 0 && nums[i-1] == nums[i] {
			continue
		}
		l := i + 1
		r := n - 1

		// two sum
		for l < r {
			sum := nums[l] + nums[r] + nums[i]
			if sum == 0 {
				values := []int{
					nums[i], nums[l], nums[r],
				}
				ret = append(ret, values)
				l++
				for l < r && nums[l] == nums[l-1] {
					l++
				}
			} else if sum > 0 {
				r--
			} else {
				l++
			}
		}
	}
	return ret
}
