//
// Created by champ on 2022/4/3.
//

class Solution {
public:
    vector<vector<int>> threeSum(vector<int>& nums) {
        set<vector<int>> ret;
        unordered_set<int> dup;
        unordered_map<int, int> seen;
        for (int i = 0; i < nums.size(); i++) {
            if (!dup.insert(nums[i]).second) { // true means insert, then not process before
                continue;
            }
            for (int j = i+1; j < nums.size(); j++) {
                int c = -(nums[i] + nums[j]);
                auto it = seen.find(c);
                if (it != seen.end() && it->second == i) { // this round
                    vector<int> thisOne = vector<int> {nums[i], c, nums[j]};
                    sort(thisOne.begin(), thisOne.end());
                    ret.insert(thisOne);
                }
                seen[nums[j]] = i;
            }
        }
        return vector<vector<int>>(begin(ret), end(ret));
    }
};
