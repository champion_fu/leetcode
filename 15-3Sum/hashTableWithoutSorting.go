package _5_3Sum

import "sort"

func threeSum(nums []int) [][]int {
	n := len(nums)
	var ret [][]int
	dup := make(map[three]bool, 0)

	for i := 0; i < n; i++ {
		// need to initialize hashTable every iteration of i
		hashTable := make(map[int]bool, 0)
		for j := i + 1; j < n; j++ {
			numsI := nums[i]
			numsJ := nums[j]
			target := -(numsI + numsJ)
			if hashTable[target] {
				t := newThree(numsI, numsJ, target)
				if dup[t] {
					continue
				}
				dup[t] = true
				v1 := t.Val1
				v2 := t.Val2
				v3 := t.Val3
				ret = append(ret, []int{v1, v2, v3})
			}
			hashTable[nums[j]] = true
		}
	}
	return ret
}

type three struct {
	Val1 int
	Val2 int
	Val3 int
}

func newThree(val1, val2, val3 int) three {
	var sorted = []int{val1, val2, val3}
	sort.Slice(
		sorted,
		func(i, j int) bool {
			return sorted[i] < sorted[j]
		},
	)

	return three{
		Val1: sorted[0],
		Val2: sorted[1],
		Val3: sorted[2],
	}
}
