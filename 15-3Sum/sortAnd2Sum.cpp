//
// Created by champ on 2022/4/3.
//

class Solution {
public:
    vector<vector<int>> threeSum(vector<int>& nums) {
        vector<vector<int>> res;
        // sort nLogn
        sort(nums.begin(), nums.end());
        for (auto i : nums) {
            cout << i << endl;
        }
        for (int i = 0; i < nums.size() && nums[i] <= 0; i++) { // more than 0 means sum can't be 0
            if (i == 0 || nums[i-1] != nums[i]) {
                twoSum(nums, i, res);
            }
        }
        return res;
    }
    void twoSum(vector<int> nums, int i, vector<vector<int>> &res) {
        int target = nums[i];
        int l = i+1;
        int r = nums.size() - 1;
        // cout << "target" << target << endl;
        while (l < r) {
            // cout << "l" << l << "nums[l]" << nums[l] << endl;
            // cout << "r" << r << "nums[r]" << nums[r] << endl;
            int sum = target + nums[l] + nums[r];
            if (sum == 0) {
                res.push_back({nums[i], nums[l], nums[r]});
                l++;
                while (l < r && nums[l] == nums[l-1]) l++;
            } else if (sum < 0) {
                l++;
            } else {
                r--;
            }
        }
    }
};
