package _53_Meeting_Rooms_II

import "container/heap"

func minMeetingRooms(intervals [][]int) int {
	ans := 1
	ret := 0
	h := &minHeap{}
	start2End := make(map[int]int, 0)
	heap.Init(h)

	for _, startEnd := range intervals {
		start := startEnd[0]
		end := startEnd[1]
		heap.Push(h, &element{
			time:    start,
			isStart: true,
		})
		heap.Push(h, &element{
			time:    end,
			isStart: false,
		})

		if e, m := start2End[start]; m {
			// duplication, update end
			if e > end {
				continue
			}
		}
		start2End[start] = end
	}

	maxTime := -1
	for h.Len() != 0 {
		e := heap.Pop(h).(*element)
		if maxTime == -1 {
			// new one
			ret = 1
			maxTime = start2End[e.time]
			continue
		}
		if e.time < maxTime && e.isStart {
			ret += 1
			// update maxTime
			maxTime = max(maxTime, start2End[e.time])
		}
		if e.time < maxTime && !e.isStart {
			ret -= 1
		}
		if e.time == maxTime {
			// initial
			maxTime = -1
			ret = 0
		}
		ans = max(ans, ret)
	}
	return ans
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}

type element struct {
	time    int
	isStart bool
}
type minHeap []*element

func (h minHeap) Len() int {
	return len(h)
}
func (h minHeap) Less(i, j int) bool {
	if h[i].time == h[j].time {
		// sort end first,
		// because we can return => borrow in the same time
		return !h[i].isStart
	}
	return h[i].time < h[j].time
}
func (h minHeap) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}
func (h *minHeap) Push(a interface{}) {
	e := a.(*element)
	*h = append(*h, e)
}
func (h *minHeap) Pop() interface{} {
	n := len(*h)
	a := (*h)[n-1]
	(*h) = (*h)[:n-1]
	return a
}
