//
// Created by champ on 2022/10/22.
//

class Solution {
public:
    int minMeetingRooms(vector<vector<int>>& intervals) {
        vector<pair<int, bool>> l;
        for (vector<int> interval : intervals) {
            l.push_back({interval[0], true});
            l.push_back({interval[1], false});
        }
        sort(l.begin(), l.end());

        int ans = 0, tmp = 0;

        for (auto [time, open] : l) {
            if (open) {
                tmp++;
                ans = max(ans, tmp);
            } else {
                tmp--;
            }
        }
        return ans;
    }
};
