//
// Created by champ on 2022/10/24.
//

class Solution {
public:
    int minKnightMoves(int x, int y) {
        x = abs(x), y = abs(y);

        queue<pair<int, int>> q;
        q.push({0, 0});

        set<pair<int, int>> visited;
        visited.insert({0, 0});

        int dist = 0;

        while (!q.empty())
        {
            int size = q.size();
            for (int i = 0; i < size; i++)
            {
                auto cur = q.front();
                int cx = cur.first;
                int cy = cur.second;

                if (cx == x && cy == y)
                    return dist;

                q.pop();

                for (auto& move : moves)
                {
                    int nx = cx+move.first;
                    int ny = cy+move.second;

                    if (nx<-2 || ny<-2) continue;
                    if (nx-x>1 || ny-y>1) continue;
                    if (visited.find({nx, ny}) != visited.end()) continue;

                    visited.insert({nx, ny});
                    q.push({nx, ny});
                }
            }

            dist++;
        }

        return 0;
    }
private:
    vector<pair<int, int>> moves = {{2, 1}, {-2, 1}, {-2, -1}, {2, -1}, {1, 2}, {-1, 2}, {-1, -2}, {1, -2}};
};
