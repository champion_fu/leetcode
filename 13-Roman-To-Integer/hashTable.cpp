//
// Created by champ on 2022/7/21.
//

class Solution {
public:
    int romanToInt(string s) {
        unordered_map<string, int> m;
        m["I"] = 1;
        m["V"] = 5;
        m["X"] = 10;
        m["L"] = 50;
        m["C"] = 100;
        m["D"] = 500;
        m["M"] = 1000;
        m["IV"] = 4;
        m["IX"] = 9;
        m["XL"] = 40;
        m["XC"] = 90;
        m["CD"] = 400;
        m["CM"] = 900;

        int result = 0;
        for (int i = 0; i < s.size(); i++) {
            auto it = m.find(s.substr(i, 2));
            if (it != m.end()) {
                result += it->second;
                i++;
                continue;
            }
            result += m[s.substr(i, 1)];
        }
        return result;
    }
};
