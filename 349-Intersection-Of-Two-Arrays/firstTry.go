package _49_Intersection_Of_Two_Arrays

func intersection(nums1 []int, nums2 []int) []int {
	nums1Map := make(map[int]bool)
	nums2Map := make(map[int]bool)

	for _, v := range nums1 {
		nums1Map[v] = true
	}

	var ret []int
	for _, v := range nums2 {
		if _, m := nums2Map[v]; m {
			continue
		} else {
			nums2Map[v] = true
		}
		if _, m := nums1Map[v]; m {
			ret = append(ret, v)
		}
	}
	return ret
}
