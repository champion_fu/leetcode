package _54_Find_Minimun_In_Rotated_Sorted_Array_II

import "fmt"

func findMin(nums []int) int {
	n := len(nums)
	if n < 2 {
		return nums[0]
	}

	l := 0
	r := n - 1
	for l < r {
		m := l + (r-l)/2
		fmt.Printf("l %v, m %v, r %v\n", l, m, r)
		if nums[l] == nums[r] && nums[l] == nums[m] {
			r -= 1
			l += 1
		} else if nums[m] <= nums[r] {
			r = m
		} else {
			l = m + 1
		}
	}
	return nums[l]
}
