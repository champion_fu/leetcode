package _54_Find_Minimun_In_Rotated_Sorted_Array_II

func findMin(nums []int) int {
	n := len(nums)
	if n < 2 {
		return nums[0]
	}

	l := 0
	r := n - 1
	for l < r {
		m := l + (r-l)/2
		if nums[l] == nums[r] && nums[l] == nums[m] {
			l += 1
			r -= 1
			continue
		}

		// below is 153 solution.
		if nums[l] < nums[r] {
			// didn't rotate
			r = m - 1
		} else {
			// l > r
			if nums[m] > nums[r] {
				// l >= r && m > r
				l = m + 1
			} else {
				r = m
			}
		}
	}
	return nums[l]
}
