//
// Created by champ on 2022/8/5.
//

class Solution {
public:
    vector<string> topKFrequent(vector<string>& words, int k) {
        unordered_map<string, int> m;

        for (string word : words) {
            m[word]++;
        }

        priority_queue<pair<string, int>, vector<pair<string, int>>, MyComp> pq;

        for (auto p : m) {
            pq.push({p.first, p.second});
            if (pq.size() > k) {
                pq.pop();
            }
        }

        vector<string> res;
        while (!pq.empty()) {
            auto [s, _] = pq.top();
            pq.pop();
            res.insert(res.begin(), s);
        }
        return res;
    }
private:
    struct MyComp {
        bool operator() (const pair<string, int>&a, const pair<string, int>&b) {
            return a.second > b.second || (a.second == b.second && a.first < b.first);
        };
    };
};
