//
// Created by champ on 2022/10/24.
//

class FileSystem {
private:
    struct fileNode {
        unordered_map<string, fileNode*> next;
        int value;
        fileNode(): value(-1) {};
    };

    fileNode *root;

public:
    FileSystem() {
        root = new fileNode();
    }

    vector<string> split(string path) {
        vector<string> res;
        // path = path.substr(1, path.size());
        stringstream s(path);
        string folder;
        while (getline(s, folder, '/')) {
            if (folder.size()) {
                // cout << " folder " << folder << endl;
                res.push_back(folder);
            }
        }
        return res;
    }

    bool createPath(string path, int value) {
        vector<string> pp = split(path);
        fileNode* cur = root;
        int i = 0;
        string p;
        for (; i < pp.size(); i++) {
            p = pp[i];
            if (!cur->next[p]) break; // means there is no folder or file
            cur = cur->next[p];
        }

        if (i == pp.size() - 1) {
            cur->next[p] = new fileNode();
            cur->next[p]->value = value;
            return true;
        }
        return false;
    }

    int get(string path) {
        vector<string> pp = split(path);
        fileNode* cur = root;
        for (int i = 0; i < pp.size(); i++) {
            if (!cur->next[pp[i]]) return -1;
            cur = cur->next[pp[i]];
        }
        return cur->value;
    }
};

/**
 * Your FileSystem object will be instantiated and called as such:
 * FileSystem* obj = new FileSystem();
 * bool param_1 = obj->createPath(path,value);
 * int param_2 = obj->get(path);
 */
