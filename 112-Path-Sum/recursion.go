package _12_Path_Sum

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func hasPathSum(root *TreeNode, targetSum int) bool {
	if root == nil {
		return false
	}
	return decreaseSum(root, targetSum)
}

func decreaseSum(node *TreeNode, target int) bool {
	leftBool := false
	rightBool := false
	if node.Val == target && node.Left == nil && node.Right == nil {
		return true
	} else {
		target -= node.Val
		if node.Left != nil {
			leftBool = decreaseSum(node.Left, target)
		}
		if node.Right != nil {
			rightBool = decreaseSum(node.Right, target)
		}
	}
	return leftBool || rightBool
}
