package _12_Path_Sum

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func hasPathSum(root *TreeNode, targetSum int) bool {
	if root == nil {
		return false
	}

	s := stack{}
	s.Push(root, targetSum)

	for !s.IsEmpty() {
		pair := s.Pop()
		n := pair.node
		target := pair.target

		if target == n.Val && n.Left == nil && n.Right == nil {
			return true
		}
		target -= n.Val
		if n.Left != nil {
			s.Push(n.Left, target)
		}
		if n.Right != nil {
			s.Push(n.Right, target)
		}
	}
	return false
}

type pair struct {
	node   *TreeNode
	target int
}

type stack []*pair

func (s *stack) IsEmpty() bool {
	return len(*s) == 0
}

func (s *stack) Push(n *TreeNode, target int) {
	*s = append(*s, &pair{node: n, target: target})
}

func (s *stack) Pop() *pair {
	if s.IsEmpty() {
		return nil
	} else {
		index := len(*s) - 1
		element := (*s)[index]
		*s = (*s)[:index]
		return element
	}
}
