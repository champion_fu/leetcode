package _041_Robot_Bounded_In_Circle

func isRobotBounded(instructions string) bool {
	// two condition 1. back to original, 2. direciton is not the north
	dir := [][]int{
		[]int{0, 1},  // north
		[]int{1, 0},  // east
		[]int{0, -1}, // sourth
		[]int{-1, 0}, // west
	}

	x := 0
	y := 0
	idx := 0
	for i := range instructions {
		c := instructions[i]
		if c == byte('G') {
			x += dir[idx][0]
			y += dir[idx][1]
		} else if c == byte('L') {
			idx = (idx + 3) % 4
		} else {
			idx = (idx + 1) % 4
		}
	}

	if (x == 0 && y == 0) || idx != 0 {
		return true
	}
	return false
}
