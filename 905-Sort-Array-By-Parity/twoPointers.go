package _05_Sort_Array_By_Parity

func sortArrayByParity(A []int) []int {
	even, odd := 0, len(A)-1
	for even < len(A) && odd > -1 && even < odd {
		if A[even]%2 == 0 {
			even++
			continue
		}
		if A[odd]%2 == 1 {
			odd--
			continue
		}
		third := A[even]
		A[even] = A[odd]
		A[odd] = third
		even++
		odd--
	}
	return A
}
