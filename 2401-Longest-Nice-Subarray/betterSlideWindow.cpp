//
// Created by champ on 2022/9/28.
//

class Solution {
public:
    int longestNiceSubarray(vector<int>& nums) {
        int AND = 0, i = 0, res = 0, n = nums.size();
        for (int j = 0; j < n; j++) {
            while ((AND & nums[j]) > 0) {
                // we move the head out of the window by doing AND ^= A[i]
                // The ^ (bitwise XOR) The result of XOR is 1 if the two bits are different.
                AND ^= nums[i++];
            }
            AND |= nums[j];
            res = max(res, j-i+1);
        }
        return res;
    }
};
