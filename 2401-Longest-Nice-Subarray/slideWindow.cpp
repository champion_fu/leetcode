//
// Created by champ on 2022/9/28.
//

class Solution {
public:
    int longestNiceSubarray(vector<int>& nums) {
        int l = 0, r = 1;
        int ans = 1;
        while (r < nums.size()) {
            int tmp = l;
            int orBit = nums[tmp];
            // cout << "r " << r << endl;
            while (tmp < r) {
                // cout << "tmp "  << tmp << " nums[tmp] " << nums[tmp] << endl;
                // cout << "orBit " << orBit << endl;
                if (!(orBit & nums[++tmp]) == 0) {
                    l++;
                    break;
                }
                orBit = orBit | nums[tmp];
            }
            ans = max(ans, r-l+1);
            r++;
        }
        return ans;
    }
};
