class Solution {
public:
    int longestEqualSubarray(vector<int>& nums, int k) {
        // want to delete (j-i+1-maxf)
        // this number cann't bigger than k
        int maxf = 0, l = 0, n = nums.size();
        unordered_map<int, int> count;
        for (int r = 0; r < n; r++) {
            maxf = max(maxf, ++count[nums[r]]);
            if (r - l + 1 - maxf > k) {
                --count[nums[l++]]; // delete count of i then i++
            }
        }
        return maxf;
    }
}
