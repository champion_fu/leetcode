package _27_Making_A_Large_Island

var n int
var g [][]int

func largestIsland(grid [][]int) int {
	n = len(grid)
	g = grid
	area := make(map[int]int, 0)

	index := 2
	ret := 0

	// mark index
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			if g[i][j] == 1 {
				indexArea := dfs(i, j, index)
				area[index] = indexArea
				ret = max(ret, indexArea)
				index += 1
			}
		}
	}
	fmt.Printf("ret %v, area %v, g %+v\n", ret, area, g)
	fmt.Printf("move %v\n", move(1, 0))

	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			if g[i][j] == 0 {
				seen := make(map[int]bool)
				cur := 1
				for _, v := range move(i, j) {
					index = g[v[0]][v[1]]
					m := seen[index]
					if index > 1 && !m {
						seen[index] = true
						cur += area[index]
					}
				}
				ret = max(ret, cur)
			}
		}
	}
	return ret
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}

func dfs(i, j, index int) int {
	g[i][j] = index
	area := 1
	for _, v := range move(i, j) {
		if g[v[0]][v[1]] == 1 {
			area += dfs(v[0], v[1], index)
		}
	}
	return area
}

func move(i, j int) [][]int {
	ret := make([][]int, 0)
	// top, down, left, right
	if i > 0 {
		ret = append(ret, []int{i - 1, j})
	}
	if i < n-1 {
		ret = append(ret, []int{i + 1, j})
	}
	if j > 0 {
		ret = append(ret, []int{i, j - 1})
	}
	if j < n-1 {
		ret = append(ret, []int{i, j + 1})
	}
	return ret
}
