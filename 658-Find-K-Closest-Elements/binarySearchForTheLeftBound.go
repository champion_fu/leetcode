package _58_Find_K_Closest_Elements

import "fmt"

func findClosestElements(arr []int, k int, x int) []int {
	// find the most left element
	l, r := 0, len(arr)-k
	for l < r {
		mid := l + (r-l)/2
		fmt.Printf("l %v, m %v, r %v, arr[mid] %v, arr[mid+k] %v\n", l, mid, r, arr[mid], arr[mid+k])
		if x-arr[mid] > arr[mid+k]-x {
			l = mid + 1
		} else {
			r = mid
		}
	}
	return arr[l : l+k]
}
