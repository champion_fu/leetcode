//
// Created by champ on 2022/8/9.
//

class Solution {
    int x;
public:
    vector<int> findClosestElements(vector<int>& arr, int k, int x) {
        int left = 0, right = arr.size() - k;
        while (left < right) {
            int m = left + (right - left) / 2;
            if (x - arr[m] > arr[m + k] - x) {
                // arr[mid] ---- x -- arr[mid+k]
                left = m + 1;
            } else {
                right = m;
            }
        }
        return vector<int>(arr.begin() + left, arr.begin() + left + k);
    }
};
