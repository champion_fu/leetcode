//
// Created by champ on 2022/8/9.
//

class Solution {
    int x;
public:
    vector<int> findClosestElements(vector<int>& arr, int k, int x) {
        priority_queue<pair<int,int>> pq;

        // O(nlogk)
        for (auto num : arr) {
            pq.push({abs(num-x), num});
            if (pq.size() > k) {
                pq.pop();
            }
        }

        vector<int> res;
        while (!pq.empty()) {
            res.push_back(pq.top().second);
            pq.pop();
        }
        sort(res.begin(), res.end());
        // O(klogK)
        return res;
    }
};
