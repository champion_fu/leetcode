//
// Created by champ on 2022/8/4.
//

class Solution {
    vector<pair<int, int>> dirs {
            {0, 1},
            {0, -1},
            {1, 0},
            {-1, 0},
    };
    int m;
    int n;
public:
    int getFood(vector<vector<char>>& grid) {
        m = grid.size();
        n = grid[0].size();

        queue<pair<int, int>> q;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == '*') q.emplace(i, j);
            }
        }

        int ans = 0;
        while (!q.empty()) {
            int size = q.size();
            ans++;
            for (int s = 0; s < size; s++) {
                auto [i, j] = q.front(); q.pop();
                for (pair<int, int> dir : dirs) {
                    int nI = i + dir.first;
                    int nJ = j + dir.second;

                    if (nI < 0 || nI >= m || nJ < 0 || nJ >= n) continue;

                    if (grid[nI][nJ] == '#')  return ans;
                    if (grid[nI][nJ] == 'O') {
                        grid[nI][nJ] = '&';
                        q.emplace(nI, nJ);
                    }

                }
            }
        }
        return -1;
    }
};
