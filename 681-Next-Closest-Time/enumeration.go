package _81__Next_Closest_Time

import "fmt"

var isValids [][]byte
var candidate map[byte]bool

func nextClosestTime(time string) string {
	maxTime := 24 * 60
	ret := ""

	candidate = make(map[byte]bool, 0)
	isValids = make([][]byte, 0)
	ori := make([]byte, 0)
	for i := range time {
		s := time[i]
		if s == ':' {
			continue
		}
		candidate[s-'0'] = true
		ori = append(ori, s-'0')
	}

	generate([]byte{})

	oriHour := int(ori[0])*10 + int(ori[1])
	oriMin := int(ori[2])*10 + int(ori[3])
	if len(candidate) == 1 {
		return time
	}

	for _, v := range isValids {
		times := interval(oriHour, oriMin, v)
		if times == 0 {
			// can not be original time
			continue
		}
		if times < maxTime {
			maxTime = times
			ret = fmt.Sprintf("%v%v:%v%v", v[0], v[1], v[2], v[3])
		}
	}
	return ret
}

func generate(c []byte) {
	if len(c) > 4 {
		return
	}
	if len(c) == 4 && isValid(c) {
		a := make([]byte, 4) // need to relocate new []byte
		copy(a, c)
		isValids = append(isValids, a)
		return
	}
	for key, _ := range candidate {
		c = append(c, key)
		generate(c)
		c = c[:len(c)-1]
	}
}

func interval(oriHour int, oriMin int, c []byte) int {
	h := int(c[0])*10 + int(c[1])
	m := int(c[2])*10 + int(c[3])
	ret := 0
	if oriHour > h || (oriHour == h && oriMin > m) {
		// over night
		ret += (24-oriHour)*60 + (60 - oriMin)
		ret += h*60 + m
	} else {
		ret += (h-oriHour)*60 + (m - oriMin)
	}
	return ret
}

func isValid(c []byte) bool {
	h := int(c[0])*10 + int(c[1])
	if h > 23 {
		return false
	}
	m := int(c[2])*10 + int(c[3])
	if m > 59 {
		return false
	}
	return true
}
