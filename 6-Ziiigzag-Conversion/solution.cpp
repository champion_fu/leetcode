class Solution {
public:
    string convert(string s, int numRows) {
        if (numRows <= 1) return s;
        vector<string> out(numRows);
        int c = 0;
        // dir == 1 -> go down, dir == 2 -> go up
        int dir = 1;
        for (int i = 0; i < s.length(); i++) {
            out[c].push_back(s[i]);

            // check the current column position
            if (c == 0) dir = 1;
            else if ((c+1) == numRows) dir = 2;

            // check the direction
            if (dir == 1) c++;
            else c--;
        }

        string ans = "";
        for (string s : out) ans += s;
        return ans;
    }
}
