package _9_Divide_Two_Integers

import "math"

func divide(dividend int, divisor int) int {
	if dividend == math.MinInt32 && divisor == -1 {
		return math.MaxInt32 // corner case of int32
	}
	if dividend == 0 {
		return 0
	}

	sign := -1
	if (dividend > 0 && divisor > 0) || (dividend < 0 && divisor < 0) {
		sign = 1
	}
	dvd, dvs := abs64(dividend), abs64(divisor)
	ret := 0
	for dvd >= dvs {
		tmp, m := dvs, 1
		for dvd >= tmp<<1 {
			// find the most number we can decrease
			tmp, m = tmp<<1, m<<1
		}
		dvd -= tmp
		ret += m
	}
	if sign < 0 {
		return -ret
	}
	return ret
}

func abs64(n int) int64 {
	ret := int64(n)
	if ret < 0 {
		return -ret
	}
	return ret
}
