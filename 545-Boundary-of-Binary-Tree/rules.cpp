//
// Created by champ on 2022/10/29.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<int> boundaryOfBinaryTree(TreeNode* root) {
        vector<int> res;
        /*
        node.left is left bound if node is left bound;
        node.right could also be left bound if node is left bound && node has no right child;
        Same applys for right bound;
        if node is left bound, add it before 2 child - pre order;
        if node is right bound, add it after 2 child - post order;
        A leaf node that is neither left or right bound belongs to the bottom line;
        */
        if (root) {
            res.push_back(root->val);
            getBounds(root->left, res, true, false);
            getBounds(root->right, res, false, true);
        }

        return res;
    }
private:
    void getBounds(TreeNode* cur, vector<int>& res, bool left, bool right) {
        if (!cur) return;
        if (left) res.push_back(cur->val);
        if (!left && !right && !cur->left && !cur->right) res.push_back(cur->val);
        getBounds(cur->left, res, left, right && !cur->right);
        getBounds(cur->right, res, left && !cur->left, right);
        if (right) res.push_back(cur->val);

    }
};
