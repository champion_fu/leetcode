//
// Created by champ on 2022/10/23.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> verticalOrder(TreeNode* root) {
        vector<vector<int>> res;
        if (root == NULL) return res;
        map<int, vector<int>> m;
        queue<pair<TreeNode*, int>> q;
        q.push({root, 0});

        while (!q.empty()) {
            auto [cur, level] = q.front();
            m[level].push_back(cur->val);
            q.pop();

            if (cur->left) {
                q.push({cur->left, level-1});
            }
            if (cur->right) {
                q.push({cur->right, level+1});
            }
        }
        for (auto [level, nodes] : m) res.push_back(nodes);
        return res;
    }
};
