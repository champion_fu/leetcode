//
// Created by champ on 2022/5/2.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> verticalOrder(TreeNode* root) {
        vector<vector<int>> results;
        if (root == NULL) {
            return results;
        }

        unordered_map<int, vector<int>> columnTable;
        queue<pair<TreeNode*, int>> queue;
        int column = 0;
        queue.push({root, column});

        int minColumn = 0, maxColumn = 0;

        while (!queue.empty()) {
            pair<TreeNode*, int> p = queue.front();
            queue.pop();
            auto node = p.first;
            column = p.second;

            if (node != NULL) {
                columnTable[column].push_back(node->val);
                minColumn = min(minColumn, column);
                maxColumn = max(maxColumn, column);

                queue.push({node->left, column-1});
                queue.push({node->right, column+1});
            }
        }

        for (int i = minColumn; i <= maxColumn; i++) {
            results.push_back(columnTable[i]);
        }
        return results;
    }
};
