package _26_Convert_Binary_Search_Tree_To_Sorted_Doubly_Linked_List

/**
 * Definition for a Node.
 * type Node struct {
 *     Val int
 *     Left *Node
 *     Right *Node
 * }
 */

var last *Node
var dummyHead *Node

func treeToDoublyList(root *Node) *Node {
	if root == nil {
		return nil
	}
	dummyHead = &Node{Val: -1, Left: nil, Right: nil}
	last = nil
	recursiveInorder(root)
	last.Right = dummyHead.Right
	dummyHead.Right.Left = last
	return dummyHead.Right
}

func recursiveInorder(n *Node) {
	if n == nil {
		return
	}
	// go throguh left
	recursiveInorder(n.Left)

	// deal with current value
	if last != nil {
		last.Right = n
		n.Left = last
	} else {
		// find the first
		dummyHead.Right = n
	}
	last = n

	// go right
	recursiveInorder(n.Right)
}
