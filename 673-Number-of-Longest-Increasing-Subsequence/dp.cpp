//
// Created by champ on 2022/10/12.
//

class Solution {
public:
    int findNumberOfLIS(vector<int>& nums) {
        int N = nums.size(), res = 0, maxLen = 0;
        vector<pair<int, int>> dp(N, {1, 1});
        // len[i]: the length of LIS which ends with nums[i].
        // cnt[i]: the number of LIS which ends with nums[i].
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < i; j++) {
                if (nums[i] > nums[j]) {
                    // 1, 3 -> 5
                    // 2, 4 -> 5
                    if (dp[i].first == dp[j].first + 1) dp[i].second += dp[j].second;
                    if (dp[i].first < dp[j].first + 1) {
                        dp[i].first = dp[j].first + 1;
                        dp[i].second = dp[j].second;
                    }
                }
            }
            if (maxLen == dp[i].first) res += dp[i].second;
            if (maxLen < dp[i].first) {
                maxLen = dp[i].first;
                res = dp[i].second;
            }
        }
        return res;
    }
};
