package _23_Reconstruct_Original_Digits_From_English

func originalDigits(s string) string {
	count := make(map[byte]int, 0)
	for i := range s {
		if c, match := count[s[i]]; match {
			count[s[i]] = c + 1
		} else {
			count[s[i]] = 1
		}
	}

	ret := make([]int, 10)

	ret[0] = count[byte('z')]
	ret[2] = count[byte('w')]
	ret[4] = count[byte('u')]
	ret[6] = count[byte('x')]
	ret[8] = count[byte('g')]
	ret[3] = count[byte('h')] - ret[8]
	ret[5] = count[byte('f')] - ret[4]
	ret[7] = count[byte('s')] - ret[6]
	ret[9] = count[byte('i')] - ret[5] - ret[6] - ret[8]
	ret[1] = count[byte('n')] - ret[7] - 2*ret[9]

	ans := ""
	for i, c := range ret {
		for j := 0; j < c; j++ {
			ans += fmt.Sprintf("%d", i)
		}
	}
	return ans
}
