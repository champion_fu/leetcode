//
// Created by champ on 2022/4/30.
//

class Solution {
public:
    string originalDigits(string s) {
        unordered_map<char, int> m;
        for (char c : s) {
            m[c]++;
        }

        for (auto mm : m) {
            cout << mm.first << " " << mm.second << endl;
        }

        vector<int> out(10);
        string result = "";

        out[0] = m['z'];
        out[2] = m['w'];
        out[4] = m['u'];
        out[6] = m['x'];
        out[8] = m['g'];
        out[3] = m['h'] - out[8];
        out[5] = m['f'] - out[4];
        out[7] = m['s'] - out[6];
        out[9] = m['i'] - out[5] - out[6] - out[8];
        out[1] = m['n'] - out[7] - 2*out[9];

        for (int j = 0; j < 10; j++) {
            for (int i = 0; i < out[j]; i++) {
                result += to_string(j);
            }
        }

        return result;
    }
;
