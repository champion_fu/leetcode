impl Solution {
    pub fn minimum_time(time: Vec<i32>, total_trips: i32) -> i64 {
        // given_times / time[i]
        // [1, 2, 3, 5]
        // [6, 3, 2, 1]
        // total 12

        // n = 1
        // sum(1/1, 1/2, 1/3, 1/5) >= total_trips

        // n = 2
        // sum(2/1, 2/2, 2/3, 2/5) >= total_trips
        let total_trips = total_trips as i64;
        // unwrap: give me value if no value panic
        let mut max_time = time.iter().min().unwrap().clone() as i64 * total_trips;
        let mut min_time = 1 as i64;

        while min_time < max_time {
            let mid_time = min_time + (max_time - min_time) / 2;

            // fold subtotal init = 0, each time subtotal + (mid_time / trip_time.clone as i64)
            let trips = time.iter().fold(0, |subtotal, trip_time| {
                subtotal + mid_time / trip_time.clone() as i64
            });

            if trips < total_trips {
                min_time = mid_time + 1;
            } else {
                max_time = mid_time;
            }
        }
        min_time
    }
}
