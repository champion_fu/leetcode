//
// Created by champ on 2022/6/10.
//

class Solution {
public:
    vector<string> fullJustify(vector<string>& words, int maxWidth) {
        vector<string> results;

        for (int i = 0, j; i < words.size(); i = j) {
            int width = 0;
            // j - i means space
            for (j = i; j < words.size() && width + words[j].size() + j - i <= maxWidth; j++)  {
                width += words[j].size();
            }
            int space = 1, extra = 0;
            // there is space && j is not last word
            if (j-i != 1 && j != words.size()) {
                space = (maxWidth - width) / (j - i - 1);
                extra = (maxWidth - width) % (j - i - 1);
            }
            string line(words[i]);
            for (int k = i + 1; k < j; k++) {
                line += string(space, ' ');
                if(extra > 0) {
                    line += " ";
                    extra--;
                }
                line += words[k];
            }

            line += string(maxWidth - line.size(), ' ');
            results.push_back(line);
        }
        return results;
    }
};
