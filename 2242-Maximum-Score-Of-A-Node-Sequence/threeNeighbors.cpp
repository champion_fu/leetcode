//
// Created by champ on 2022/9/3.
//

class Solution {
public:
    int maximumScore(vector<int>& scores, vector<vector<int>>& edges) {
        // pair<int, int> score and node_index
        vector<set<pair<int,int>>> nei(scores.size());
        for (const auto& e : edges) {
            set<pair<int, int>>& s0 = nei[e[0]];
            set<pair<int, int>>& s1 = nei[e[1]];
            s0.emplace(scores[e[1]], e[1]);
            s1.emplace(scores[e[0]], e[0]);
            if (s0.size() > 3) s0.erase(s0.begin());
            if (s1.size() > 3) s1.erase(s1.begin());
        }
        int ans = -1;
        for (const auto& e : edges) {
            const int a = e[0], b = e[1], sa = scores[a], sb = scores[b];
            for (const auto& [sc, c] : nei[a])
                for (const auto& [sd, d]: nei[b])
                    if (sa+sb+sc+sd > ans && c != b && c != d && d != a)
                        ans = sa+sb+sc+sd;
        }
        return ans;
    }
};
