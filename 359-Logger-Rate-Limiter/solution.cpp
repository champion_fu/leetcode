//
// Created by champ on 2022/9/14.
//

class Logger {
    unordered_map<string, int> m;
public:
    Logger() {

    }

    bool shouldPrintMessage(int timestamp, string message) {
        if (m.count(message) == 0 || m[message] + 10 <= timestamp) {
            m[message] = timestamp;
            return true;
        } else {
            return false;
        }

    }
};

/**
 * Your Logger object will be instantiated and called as such:
 * Logger* obj = new Logger();
 * bool param_1 = obj->shouldPrintMessage(timestamp,message);
 */
