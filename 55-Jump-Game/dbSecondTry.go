package _5_Jump_Game

func canJump(nums []int) bool {
	n := len(nums)
	if n < 2 {
		return true
	}

	ret := make([]int, n)
	ret[0] = 1
	for i, v := range nums {
		if ret[i] == 0 {
			return false
		}
		for j := i; j <= i+v && j < n; j++ {
			ret[j] = 1
		}
	}
	return ret[n-1] == 1
}
