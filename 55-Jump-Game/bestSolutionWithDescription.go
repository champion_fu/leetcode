package _5_Jump_Game

func canJump(nums []int) bool {
	n := len(nums)
	if n < 2 {
		return true
	}
	lastPos := n - 1

	for i := n - 1; i >= 0; i-- {
		// from the end of array
		// to check if we can achieve lastPos
		// if yes, reassign lastPos of valid start
		if i+nums[i] >= lastPos {
			lastPos = i
		}
	}
	return lastPos == 0 // can we start from index 0 to n-1
}
