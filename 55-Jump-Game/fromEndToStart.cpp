//
// Created by champ on 2022/5/14.
//

class Solution {
public:
    bool canJump(vector<int>& nums) {
        int length = nums.size();
        if (length < 2) {
            return true;
        }
        int lastIndex = length - 1;

        for (int i = lastIndex; i >= 0; i--) {
            // from this i can reach lastIndex then lastIndex = i
            if (i + nums[i] >= lastIndex) {
                lastIndex = i;
            }
        }
        return lastIndex == 0;
    }
};
