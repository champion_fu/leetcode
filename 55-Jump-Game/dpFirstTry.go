package _5_Jump_Game

func canJump(nums []int) bool {
	n := len(nums)
	var dp []bool = make([]bool, n)
	// first one always true
	dp[0] = true

	for i, v := range nums {
		if !dp[i] {
			return false // end
		}
		for j := 1; j <= v; j++ {
			index := i + j
			if index < n {
				dp[index] = true
			}
		}
		if dp[n-1] {
			return true
		}
	}
	return false
}
