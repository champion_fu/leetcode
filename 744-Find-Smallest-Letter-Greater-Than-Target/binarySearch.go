package _44_Find_Smallest_Letter_Greater_Than_Target

import "fmt"

func nextGreatestLetter(letters []byte, target byte) byte {
	n := len(letters)
	if target >= letters[n-1] {
		return letters[0]
	}

	l := 0
	r := n - 1
	for l < r {
		m := l + (r-l)/2
		fmt.Printf("l %v, m %v, r %v\n", l, m, r)
		if letters[m] <= target {
			l = m + 1
		} else {
			r = m
		}
	}
	// letters[r] also work
	return letters[l]
}
