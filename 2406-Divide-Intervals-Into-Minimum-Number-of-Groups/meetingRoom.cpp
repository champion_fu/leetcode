//
// Created by champ on 2022/9/23.
//

class Solution {
public:
    int minGroups(vector<vector<int>>& intervals) {
        vector<vector<int>> meetings;

        for (auto& interval : intervals) {
            meetings.push_back({interval[0], 1});
            meetings.push_back({interval[1] + 1, -1});
        }

        int res = 0, cur = 0;
        sort(meetings.begin(), meetings.end());
        for (auto& meeting : meetings) {
            res = max(res, cur += meeting[1]);
        }
        return res;
    }
};
