package _28_Longest_Consecutive_Sequence

func longestConsecutive(nums []int) int {
	ret := 0
	hashTable := make(map[int]bool, 0)

	for _, v := range nums {
		hashTable[v] = true
	}

	for _, v := range nums {
		var currentNum, streak int
		if _, m := hashTable[v-1]; m {
			continue
		}

		streak = 1
		currentNum = v
		m := hashTable[currentNum+1]
		for m {
			currentNum += 1
			streak += 1
			m = hashTable[currentNum+1]
		}

		if streak > ret {
			ret = streak
		}
	}
	return ret
}
