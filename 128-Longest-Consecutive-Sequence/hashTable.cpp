//
// Created by champ on 2022/8/8.
//

class Solution {
public:
    int longestConsecutive(vector<int>& nums) {
        unordered_map<int, bool> m;
        for (auto num: nums) {
            m[num] = true;
        }

        int res = 0;
        for (auto num: nums) {
            int streak, curNum;
            if (m[num-1]) continue; // last num has been processed

            curNum = num;
            streak = 1;
            while (m[++curNum]) {
                // find next one
                streak++;
            }
            res = max(res, streak);
        }
        return res;
    }
};
