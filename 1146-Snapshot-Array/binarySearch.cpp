//
// Created by champ on 2022/4/19.
//

class SnapshotArray {
    int snapIndex = 0;
    unordered_map<int, map<int, int>> db;
public:
    SnapshotArray(int length) {
        return;
    }

    void set(int index, int val) {
        db[index][snapIndex] = val;
    }

    int snap() {
        int oldSnapIndex = snapIndex;
        snapIndex += 1;
        return oldSnapIndex;
    }

    int get(int index, int snap_id) {
        auto it = db[index].upper_bound(snap_id);
        return it == db[index].begin() ? 0 : prev(it)->second;
    }
};
