package _51_Flatten_2D_Vector

type Vector2D struct {
	nums     []int
	position int
}

func Constructor(vec [][]int) Vector2D {
	nums := make([]int, 0)
	for _, row := range vec {
		for _, value := range row {
			nums = append(nums, value)
		}
	}

	return Vector2D{
		nums:     nums,
		position: -1,
	}
}

func (this *Vector2D) Next() int {
	this.position += 1
	return this.nums[this.position]
}

func (this *Vector2D) HasNext() bool {
	if this.position+1 < len(this.nums) {
		return true
	}
	return false
}

/**
 * Your Vector2D object will be instantiated and called as such:
 * obj := Constructor(vec);
 * param_1 := obj.Next();
 * param_2 := obj.HasNext();
 */
