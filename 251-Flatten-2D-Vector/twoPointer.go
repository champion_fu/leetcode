package _51_Flatten_2D_Vector

type Vector2D struct {
	vec   [][]int
	inner int
	outer int
}

func Constructor(vec [][]int) Vector2D {
	return Vector2D{
		inner: 0,
		outer: 0,
		vec:   vec,
	}
}

func (this *Vector2D) AdvancedNext() {
	for this.outer < len(this.vec) && this.inner == len(this.vec[this.outer]) {
		this.outer += 1
		this.inner = 0
	}
}

func (this *Vector2D) Next() int {
	this.AdvancedNext()
	element := this.vec[this.outer][this.inner]
	this.inner += 1
	return element
}

func (this *Vector2D) HasNext() bool {
	this.AdvancedNext()
	return this.outer < len(this.vec)
}

/**
 * Your Vector2D object will be instantiated and called as such:
 * obj := Constructor(vec);
 * param_1 := obj.Next();
 * param_2 := obj.HasNext();
 */
