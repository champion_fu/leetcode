package _2_Unique_Paths

func uniquePaths(m int, n int) int {
	var dp [][]int = make([][]int, m)

	for i := 0; i < m; i++ {
		dp[i] = make([]int, n)
	}
	dp[0][0] = 1
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if i == 0 && j == 0 {
				continue
			}
			topXIndex := j
			topYIndex := i - 1
			var topValue, leftValue int
			if topYIndex >= 0 {
				topValue = dp[topYIndex][topXIndex]
			}

			leftXIndex := j - 1
			leftYIndex := i
			if leftXIndex >= 0 {
				leftValue = dp[leftYIndex][leftXIndex]
			}
			dp[i][j] = topValue + leftValue
		}
	}
	return dp[m-1][n-1]
}
