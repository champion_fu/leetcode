//
// Created by champ on 2022/8/14.
//

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* mergeKLists(vector<ListNode*>& lists) {
        int size = lists.size();
        int finish = size;
        ListNode* dummyHead = new ListNode(0);
        ListNode* p = dummyHead;

        while (finish > 0) {
            ListNode* tmp = NULL;
            int index = 0;
            for (int i = 0; i < size; i++) {
                if (lists[i] == NULL) continue;
                if (tmp == NULL || lists[i]->val <= tmp->val) {
                    index = i;
                    tmp = lists[i];
                }
            }
            if (tmp == NULL) return dummyHead->next;
            p->next = new ListNode(tmp->val);
            p = p->next;
            lists[index] = lists[index]->next;
            if (lists[index] == NULL) {
                finish--;
            }
        }
        return dummyHead->next;
    }
};
