package _3_Merge_K_Sorted_Lists

import "container/heap"

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func mergeKLists(lists []*ListNode) *ListNode {
	n := len(lists)
	if n == 0 {
		return nil
	}

	dummyHead := &ListNode{
		Val:  -1,
		Next: nil,
	}

	h := &minHeap{}
	heap.Init(h)
	for i := 0; i < n; i++ {
		n := lists[i]
		if n != nil {
			heap.Push(h, n)
		}
	}

	node := dummyHead
	for h.Len() > 0 {
		// pop from min heap
		item := heap.Pop(h).(*ListNode)
		// push next to min heap
		if item.Next != nil {
			heap.Push(h, item.Next)
		}

		// append item to answer
		node.Next = item
		node = node.Next
	}
	return dummyHead.Next
}

type minHeap []*ListNode

func (h minHeap) Less(i, j int) bool {
	return h[i].Val < h[j].Val
}
func (h minHeap) Len() int {
	return len(h)
}
func (h minHeap) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}
func (h *minHeap) Pop() interface{} {
	n := len(*h)
	e := (*h)[n-1]
	*h = (*h)[:n-1]
	return e
}
func (h *minHeap) Push(a interface{}) {
	n := a.(*ListNode)
	*h = append(*h, n)
}
