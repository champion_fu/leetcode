//
// Created by champ on 2022/10/10.
//

class Solution {
public:
    int longestSubarray(vector<int>& nums) {
        int maxEle = *max_element(nums.begin(), nums.end());

        int res = 1, tmp = 0;
        for (int i = 0; i < nums.size(); i++) {
            if (nums[i] == maxEle) {
                tmp++;
                res = max(res, tmp);
            } else {
                tmp = 0;
            }
        }

        return res;
    }
};
