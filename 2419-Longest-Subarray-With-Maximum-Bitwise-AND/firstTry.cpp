//
// Created by champ on 2022/10/10.
//

class Solution {
public:
    int longestSubarray(vector<int>& nums) {
        vector<int> index(0);
        int maximum = 0;

        for (int i = 0; i < nums.size(); i++) {
            if (maximum < nums[i]) {
                // find the maximum value;
                maximum = nums[i];
            }
        }

        for (int i = 0; i < nums.size(); i++) {
            if (maximum == nums[i]) {
                index.push_back(i);
            }
        }
        for (int i : index) {
            cout << " index " << i << endl;
        }

        int res = -1, tmp = 1;
        for (int i = 1; i < index.size(); i++) {
            if (index[i] == index[i-1]+1) {
                tmp++;
            } else {
                res = max(res, tmp);
                tmp = 1;
            }
        }
        res = max(res, tmp);

        return res;
    }
};
