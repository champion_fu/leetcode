package Height_Checker

func heightChecker(heights []int) int {
	heightCount := make([]int, 101)
	for _, v := range heights {
		heightCount[v] += 1
	}
	result, curHeight := 0, 0

	for _, v := range heights {
		for heightCount[curHeight] == 0 {
			curHeight += 1
		}

		if curHeight != v {
			result += 1
		}
		heightCount[curHeight]--
	}
	return result
}
