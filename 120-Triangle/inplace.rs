
use std::cmp;

impl Solution {
    pub fn minimum_total(mut triangle: Vec<Vec<i32>>) -> i32 {
        for y in (0..triangle.len()-1).rev() {
            // from botton to top
            for x in (0..triangle[y].len()) {
                // println!("y {} x {}", y, x);
                triangle[y][x] += cmp::min(triangle[y+1][x], triangle[y+1][x+1])
            }
        }
        triangle[0][0]
    }
}

// row.    col
// 0    -> 1 (0)
// 1    -> 2 (0, 1
