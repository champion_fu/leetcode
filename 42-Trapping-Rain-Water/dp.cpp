//
// Created by champ on 2022/5/21.
//

class Solution {
public:
    int trap(vector<int>& height) {
        int size = height.size();
        if (size == 0) {
            return 0;
        }
        vector<int> leftMax(size), rightMax(size);

        leftMax[0] = height[0];
        for (int i = 1; i < size; i++) {
            leftMax[i] = max(height[i], leftMax[i-1]);
        }

        rightMax[size-1] = height[size-1];
        for (int j = size - 2; j >= 0; j--) {
            rightMax[j] = max(height[j], rightMax[j+1]);
        }

        int ans = 0;
        for (int i = 1; i < size; i++) {
            ans += min(rightMax[i], leftMax[i]) - height[i];
        }
        return ans;
    }
};
