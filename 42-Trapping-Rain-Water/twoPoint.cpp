//
// Created by champ on 2022/5/21.
//

class Solution {
public:
    int trap(vector<int>& height) {
        int size = height.size();
        if (size == 0) return 0;

        int l = 0, r = size - 1;
        int lMax = 0, rMax = 0;

        int ans = 0;
        while (l < r) {
            if (height[l] < height[r]) {
                height[l] >= lMax ? lMax = height[l] : ans += lMax - height[l];
                l++;
            } else {
                height[r] >= rMax ? rMax = height[r] : ans += rMax - height[r];
                r--;
            }
        }
        return ans;
    }
;
