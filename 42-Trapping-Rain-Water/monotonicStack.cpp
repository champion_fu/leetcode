//
// Created by champ on 2022/5/21.
//

class Solution {
public:
    int trap(vector<int>& height) {
        int size = height.size();
        if (size == 0) {
            return 0;
        }

        stack<int> st;
        int ans = 0;

        for (int i = 0; i < size; i++) {
            while (!st.empty() && height[i] > height[st.top()]) {
                int top = st.top();
                st.pop();

                if (st.empty()) {
                    // there is no left wall
                    break;
                }
                int distance = i - st.top() - 1;
                // min(left wall height, cur wall height) - pop height;
                int h = min(height[i], height[st.top()]) - height[top];
                ans += distance * h;
            }
            st.push(i);
        }

        return ans;
    }
};
