//
// Created by champ on 2022/6/13.
//

class Solution {
public:
    string simplifyPath(string path) {
        stack<string> s;
        string res;

        for (int i = 0; i < path.size(); i++) {
            if (path[i] == '/') continue;

            string tmp;
            while (i < path.size() && path[i] != '/') {
                tmp += path[i];
                i++;
            }

            if (tmp == ".") {
                continue;
            } else if (tmp == "..") {
                if (!s.empty()) s.pop();
            } else {
                s.push(tmp);
            }
        }

        while (!s.empty()) {
            res = "/" + s.top() + res;
            s.pop();
        }

        return res.size() == 0 ? "/" : res;
    }
};
