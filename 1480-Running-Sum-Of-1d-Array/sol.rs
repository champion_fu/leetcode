impl Solution {
    pub fn running_sum(nums: Vec<i32>) -> Vec<i32> {
        let mut sum = 0;
        let mut ret:Vec<i32> = Vec::new();
        for num in nums {
            sum += num;
            ret.push(sum);
        }
        ret
    }
}
