//
// Created by champ on 2022/6/9.
//

class Solution {
public:
    int longestCommonSubsequence(string text1, string text2) {
        // make sure length1 is small
        if (text1.size() > text2.size()) {
            // swap
            string tmp = text1;
            text1 = text2;
            text2 = tmp;
        }
        vector<int> prev (text2.size()+1, 0);
        vector<int> curr (text2.size()+1, 0);

        for (int i = 0; i < text1.size(); i++) {
            for (int j = 0; j < text2.size(); j++) {
                curr[j + 1] = text1[i] == text2[j] ? prev[j] + 1 : max(curr[j], prev[j + 1]);
            }
            vector<int> tmp = curr;
            curr = prev;
            prev = tmp;
        }
        return prev[text2.size()];
    }
};
