package _143_Longest_Common_Subsequence

func longestCommonSubsequence(text1 string, text2 string) int {
	length1 := len(text1)
	length2 := len(text2)
	dp := make([][]int, length1+1)

	for index := 0; index < length1+1; index++ {
		dp[index] = make([]int, length2+1)
	}

	for i := length1-1; i >= 0; i-- {
		for j := length2-1; j >= 0; j-- {
			if text1[i] == text2[j] {
				dp[i][j] = dp[i+1][j+1] + 1
			} else {
				dp[i][j] = max(dp[i+1][j], dp[i][j+1])
			}
		}
	}
	return dp[0][0]
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}
