//
// Created by champ on 2022/4/20.
//

class Solution {
    vector<int> dp;
public:
    int racecar(int target) {
        dp = vector<int>(target+1, 0);
        return recursive(target);
    }
private:
    int recursive(int target) {
        if (dp[target] > 0) return dp[target];

        int n = ceil(log2(target+1));
        // cout << "n " << n << " target " << target << endl;
        // AA...A (nA) best case
        if (1 << n == target+1) return dp[target] = n; // 2^n - 1 = t
        // AA...AR (nA + 1R) + dp(left)
        dp[target] = n + 1 + recursive((1<<n) - 1 - target);
        for (int m = 0; m < n-1; m++) {
            int cur = (1 << (n-1)) - (1 << m);
            //AA...ARA...AR (n-1A + 1R + mA + 1R) + dp(left)
            dp[target] = min(dp[target], recursive(target - cur) + n + 1 + m);
        }
        return dp[target];
    }
};
