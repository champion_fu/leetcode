package _38_Copy_List_With_Random_Pointer

/**
 * Definition for a Node.
 * type Node struct {
 *     Val int
 *     Next *Node
 *     Random *Node
 * }
 */

var visitedMap map[*Node]*Node = make(map[*Node]*Node, 0)

func copyRandomList(head *Node) *Node {
	if head == nil {
		return nil
	}

	p := head
	newNode := &Node{Val: p.Val, Next: nil, Random: nil}
	visitedMap[p] = newNode

	for p != nil {
		newNode.Next = getCloneNode(p.Next)
		newNode.Random = getCloneNode(p.Random)

		p = p.Next
		newNode = newNode.Next
	}

	return visitedMap[head]
}

func getCloneNode(p *Node) *Node {
	if p == nil {
		return nil
	}

	n, get := visitedMap[p]
	if !get {
		n = &Node{Val: p.Val, Next: nil, Random: nil}
		visitedMap[p] = n
	}
	return n
}
