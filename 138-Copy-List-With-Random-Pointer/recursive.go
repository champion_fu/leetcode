package _38_Copy_List_With_Random_Pointer

/**
 * Definition for a Node.
 * type Node struct {
 *     Val int
 *     Next *Node
 *     Random *Node
 * }
 */

var visitedMap map[*Node]*Node = make(map[*Node]*Node, 0)

func copyRandomList(head *Node) *Node {
	if head == nil {
		return nil
	}
	if n, get := visitedMap[head]; get {
		return n
	}

	n := &Node{Val: head.Val, Next: nil, Random: nil}
	visitedMap[head] = n

	n.Next = copyRandomList(head.Next)
	n.Random = copyRandomList(head.Random)

	return n
}
