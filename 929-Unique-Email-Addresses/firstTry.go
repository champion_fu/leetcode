package _29_Unique_Email_Addresses

import (
	"fmt"
	"strings"
)

func numUniqueEmails(emails []string) int {
	emailMap := make(map[string][]string, 0)
	r := strings.NewReplacer(".", "")
	for _, email := range emails {
		e := strings.Split(email, "@")
		domainName := e[1]
		localName := r.Replace(e[0])
		localName = strings.Split(localName, "+")[0]
		validEmail := fmt.Sprintf("%s@%s", localName, domainName)
		if list, m := emailMap[validEmail]; m {
			list = append(list, email)
		} else {
			emailMap[validEmail] = []string{email}
		}
	}
	return len(emailMap)
}
