package _50_Count_Univalue_Subtrees

import "fmt"

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func countUnivalSubtrees(root *TreeNode) int {
	if root == nil {
		return 0
	}
	ret, _ := univalRecursion(root)
	return ret
}

func univalRecursion(n *TreeNode) (int, int) {
	// Default -1000 <= Node.Val < 1000
	if n == nil {
		return 0, 1001
	}
	// n is leaf
	if n.Left == nil && n.Right == nil {
		return 1, n.Val
	}

	// n is not left
	var addLeft, addRight, targetLeft, targetRight int
	addLeft, targetLeft = univalRecursion(n.Left)
	addRight, targetRight = univalRecursion(n.Right)
	ret := addLeft + addRight
	fmt.Printf("ret %+v, addLeft %+v addRight %v, targetLeft %v, targetRight %v\n", ret, addLeft, addRight, targetLeft, targetRight)

	// Consider left node is nil
	if n.Left == nil && targetRight == n.Val {
		return ret + 1, n.Val
	}
	// Consider right node is nil
	if n.Right == nil && targetLeft == n.Val {
		return ret + 1, n.Val
	}
	// Consider left and right node are not nil
	if targetRight == targetLeft && targetRight == n.Val {
		return ret + 1, n.Val
	}
	return ret, 1001
}
