package _295_Find_Numbers_With_Even_Number_Of_Digits

func findNumbers(nums []int) int {
	ret := 0
	for _, value := range nums {
		digit := 1
		for value >= 10 {
			digit += 1
			value = value / 10
		}
		if digit%2 == 0 {
			ret += 1
		}
	}
	return ret
}
