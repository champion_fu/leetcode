//
// Created by champ on 2022/4/26.
//

/**
 * // This is the Master's API interface.
 * // You should not implement it, or speculate about its implementation
 * class Master {
 *   public:
 *     int guess(string word);
 * };
 */
class Solution {
public:
    void findSecretWord(vector<string>& wordlist, Master& master) {
        for (int i = 0; i < 10; i++) {
            string& s = wordlist[rand() % wordlist.size()];
            int matches = master.guess(s);

            vector<string> tmp;
            for (string& w : wordlist) {
                if (similarity(s, w) == matches) {
                    tmp.push_back(w);
                }
            }
            wordlist = tmp;
        }
    }
private:
    int similarity(string w1, string w2) {
        int ret = 0;
        for (int i = 0; i < 6; i++) {
            if (w1[i] == w2[i]) {
                ret++;
            }
        }
        return ret;
    }
};
