//
// Created by champ on 2022/7/19.
//

class Solution {
private:
    void dfs(vector<vector<int>>& image, int i, int j, int val, int newColor) {
        if (i < 0 || i >= image.size() || j < 0 || j >= image[0].size()) return;
        if (image[i][j] == newColor || image[i][j] != val) return;

        image[i][j] = newColor;
        dfs(image, i-1, j, val, newColor);
        dfs(image, i+1, j, val, newColor);
        dfs(image, i, j-1, val, newColor);
        dfs(image, i, j+1, val, newColor);
    }
public:
    vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int color) {
        int c = image[sr][sc];
        dfs(image, sr, sc, c, color);
        return image;
    }
};
