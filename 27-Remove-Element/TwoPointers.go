package _7_Remove_Element

func removeElement(nums []int, val int) int {
	i, j := -1, 0
	length := len(nums)

	for j < length {
		if nums[j] != val {
			nums[i+1] = nums[j]
			i++
		}
		j++
	}
	return i + 1
}
