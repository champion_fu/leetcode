//
// Created by champ on 2022/10/21.
//

class Solution {
public:
    long long minimumRemoval(vector<int>& beans) {
        // 2, 3, 4, 10 -> sum(19)
        // pick 1 -> 2(0), 2(-1), 2(-2), 2(-8) -> remove = 19 - (4 - 0) * 2 = 11 = -1 -2 -8
        // etc ...
        long N = beans.size();
        long long ans = LLONG_MAX;
        long long sum = accumulate(beans.begin(), beans.end(), 0L);
        sort(beans.begin(), beans.end());
        for (int i = 0; i < N; i++) ans = min(ans, sum - (N - i) * beans[i]);
        return ans;
    }
};
