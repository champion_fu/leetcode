package _19_Pascal_s_Triangle_IIo

func getRow(rowIndex int) []int {
	var ret = make([]int, rowIndex+1)
	for i := range ret {
		ret[i] = 1
	}

	for i := 1; i < rowIndex; i++ {
		for j := i; j > 0; j-- {
			ret[j] += ret[j-1]
		}
	}
	return ret
}
