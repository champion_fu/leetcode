package _19_Pascal_s_Triangle_IIo

func getRow(rowIndex int) []int {
	var ret = make([][]int, 0)
	for i := 0; i < rowIndex+1; i++ {
		var level = make([]int, 0)
		for j := 0; j < i+1; j++ {
			if j == 0 || j == i {
				level = append(level, 1)
				continue
			}
			level = append(level, ret[i-1][j-1]+ret[i-1][j])
		}
		ret = append(ret, level)
	}
	return ret[rowIndex]
}
