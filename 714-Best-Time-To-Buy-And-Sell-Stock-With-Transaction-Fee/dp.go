package _14_Best_Time_To_Buy_And_Sell_Stock_With_Transaction_Fee

func maxProfit(prices []int, fee int) int {
	cash, hold := 0, -prices[0]

	for i := 1; i < len(prices); i++ {
		v := prices[i]
		cash = max(cash, hold+v-fee)
		hold = max(hold, cash-v)
	}
	return cash
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}
