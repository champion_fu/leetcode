//
// Created by champ on 2022/10/21.
//

class Solution {
public:
    int xorAllNums(vector<int>& nums1, vector<int>& nums2) {
        // because of a ^ a = 0, so even didn't count
        if (nums1.size() % 2 == 0 && nums2.size() % 2 == 0) return 0;
        int x = 0;
        if (nums2.size() % 2 == 1) for (int n : nums1) x ^= n;
        if (nums1.size() % 2 == 1) for (int n : nums2) x ^= n;
        return x;
    }
};
