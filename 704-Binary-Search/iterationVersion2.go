package _04_Binary_Search

func search(nums []int, target int) int {
	n := len(nums)
	if n == 0 {
		return -1
	}

	l := 0
	r := n
	mid := l + (r-l)/2

	for l < r {
		mid = l + (r-l)/2
		if nums[mid] == target {
			return mid
		} else if nums[mid] > target {
			r = mid
		} else {
			l = mid + 1
		}
	}
	return -1
}
