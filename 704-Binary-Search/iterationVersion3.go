package _04_Binary_Search

func search(nums []int, target int) int {
	n := len(nums)
	if n == 0 {
		return -1
	}

	l := 0
	r := n - 1
	for l+1 < r {
		mid := l + (r-l)/2
		if nums[mid] == target {
			return mid
		} else if nums[mid] > target {
			r = mid
		} else {
			l = mid
		}
	}

	if nums[l] == target {
		return l
	}
	if nums[r] == target {
		return r
	}
	return -1
}
