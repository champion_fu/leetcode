package _04_Binary_Search

func search(nums []int, target int) int {
	n := len(nums)
	if n == 0 {
		return -1
	}

	l := 0
	r := n - 1

	for l <= r {
		mid := (l + r) / 2
		if nums[mid] == target {
			return mid
		} else if nums[mid] > target {
			r = mid - 1
		} else {
			l = mid + 1
		}
	}
	return -1
}
