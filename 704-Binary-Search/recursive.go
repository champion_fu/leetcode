package _04_Binary_Search

var input []int
var t int

func search(nums []int, target int) int {
	input = nums
	t = target
	return binarySearch(0, len(input)-1)
}

func binarySearch(left, right int) int {
	// including left, right index
	if left > right || right > len(input) || left < 0 {
		return -1
	}
	mid := (left + right) / 2
	if input[mid] == t {
		return mid
	} else if input[mid] > t {
		return binarySearch(left, mid-1)
	} else {
		return binarySearch(mid+1, right)
	}
}
