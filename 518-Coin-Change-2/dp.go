package _18_Coin_Change_2

func change(amount int, coins []int) int {
	ret := make([]int, amount+1)
	ret[0] = 1

	for _, c := range coins {
		for i := c; i < amount+1; i++ {
			ret[i] += ret[i-c]
		}
	}
	return ret[amount]
}
