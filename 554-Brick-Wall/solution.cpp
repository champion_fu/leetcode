class Solution {
public:
    int leastBricks(vector<vector<int>>& wall) {
        unordered_map<int, int> edge;
        int max_freq = 0;

        for (int row = 0; row < wall.size(); row++) {
            int edge_position = 0;
            for (int brick_no = 0; brick_no < wall[row].size() - 1; brick_no++) {
                edge_position += wall[row][brick_no];
                edge[edge_position]++;
                max_freq =  std::max(max_freq, edge[edge_position]);
            }
        }

        return wall.size() - max_freq;
    }
}