package _14_Third_Maximun_Number

import "math"

func thirdMax(nums []int) int {
	max1, max2, max3 := math.MinInt64, math.MinInt64, math.MinInt64
	duplication := make(map[int]bool)
	for _, v := range nums {
		if _, exist := duplication[v]; exist {
			continue
		} else {
			duplication[v] = true
		}

		if v > max1 {
			max3 = max2
			max2 = max1
			max1 = v
		} else if v > max2 {
			max3 = max2
			max2 = v
		} else if v > max3 {
			max3 = v
		}
	}
	if max3 == math.MinInt64 {
		return max1
	} else {
		return max3
	}
}
