package K_th_Symbol_In_Grammar

func kthGrammar(n int, k int) int {
	if k == 1 {
		return 0
	}
	if k == 2 {
		return 1
	}

	if k%2 == 0 {
		// if k is even, number need to be XOR
		return kthGrammar(n-1, k/2) ^ 1
	}
	return kthGrammar(n-1, (k/2)+1)
}
