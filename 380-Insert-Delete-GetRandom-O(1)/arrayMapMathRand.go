package _80_Insert_Delete_GetRandom_O_1_

import "math/rand"

type RandomizedSet struct {
	dup  map[int]int
	vals []int
}

/** Initialize your data structure here. */
func Constructor() RandomizedSet {
	return RandomizedSet{
		dup:  make(map[int]int, 0),
		vals: make([]int, 0),
	}
}

/** Inserts a value to the set. Returns true if the set did not already contain the specified element. */
func (this *RandomizedSet) Insert(val int) bool {
	if _, m := this.dup[val]; m {
		return false
	}
	n := len(this.vals)
	this.vals = append(this.vals, val)
	this.dup[val] = n
	return true
}

/** Removes a value from the set. Returns true if the set contained the specified element. */
func (this *RandomizedSet) Remove(val int) bool {
	index, m := this.dup[val]
	if !m {
		return false
	}
	n := len(this.vals)
	delete(this.dup, val)

	if index != n-1 {
		// swap with last one
		this.dup[this.vals[n-1]] = index
		this.vals[index] = this.vals[n-1]
	}

	// delete last one
	this.vals = this.vals[:n-1]
	return true
}

/** Get a random element from the set. */
func (this *RandomizedSet) GetRandom() int {
	if len(this.vals) == 0 {
		return 0
	}
	index := rand.Int() % len(this.vals)
	return this.vals[index]

}

/**
 * Your RandomizedSet object will be instantiated and called as such:
 * obj := Constructor();
 * param_1 := obj.Insert(val);
 * param_2 := obj.Remove(val);
 * param_3 := obj.GetRandom();
 */
