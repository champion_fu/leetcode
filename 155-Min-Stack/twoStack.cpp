//
// Created by champ on 2022/7/25.
//

class MinStack {
    stack<int> s;
    stack<pair<int, int>> minS;
public:
    MinStack() {

    }

    void push(int val) {
        s.push(val);

        if (minS.empty() || val < minS.top().first) {
            minS.push({val, 1});
        } else if (val == minS.top().first) {
            // same val
            minS.top().second++;
        }
    }

    void pop() {
        if (minS.top().first == s.top()) {
            minS.top().second--;
        }
        if (minS.top().second == 0) {
            minS.pop();
        }
        s.pop();
    }

    int top() {
        return s.top();
    }

    int getMin() {
        return minS.top().first;
    }
};

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack* obj = new MinStack();
 * obj->push(val);
 * obj->pop();
 * int param_3 = obj->top();
 * int param_4 = obj->getMin();
 */
