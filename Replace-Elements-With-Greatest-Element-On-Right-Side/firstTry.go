package Replace_Elements_With_Greatest_Element_On_Right_Side

func replaceElements(arr []int) []int {
	length := len(arr)
	maxElement := arr[length-1]
	preElement := -1
	arr[length-1] = -1
	if length == 1 {
		return arr
	}

	for i := length - 2; i > -1; i-- {
		preElement = arr[i]
		arr[i] = maxElement
		if preElement > maxElement {
			maxElement = preElement
		}
	}
	return arr
}
