//
// Created by champ on 2022/9/28.
//

class Solution {
public:
    int mostBooked(int n, vector<vector<int>>& meetings) {
        // greater, small on the top, room number
        priority_queue<int, vector<int>, greater<int>> avail;
        // end time, room number
        priority_queue<pair<long long, int>, vector<pair<long long, int>>, greater<pair<long long, int>>> busy;
        int cnt[101] = {};
        sort(begin(meetings), end(meetings));

        for (int i = 0; i < n; i++) {
            avail.push(i);
        }

        for (auto &m : meetings) {
            while (!busy.empty() && busy.top().first <= m[0]) {
                // busy's smallest end time <= now start time, time is pass -> room is available -> pop busy, push avail
                avail.push(busy.top().second);
                busy.pop();
            }

            long long start = avail.empty() ? busy.top().first : m[0];
            long long duration = m[1] - m[0];
            int room = avail.empty() ? busy.top().second : avail.top();
            if (avail.empty()) {
                busy.pop();
            } else {
                avail.pop();
            }
            ++cnt[room];
            busy.push({start + duration, room});
        }
        return max_element(begin(cnt), end(cnt)) - begin(cnt);

    }
};
