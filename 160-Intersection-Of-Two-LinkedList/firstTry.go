package _60_Intersection_Of_Two_LinkedList

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func getIntersectionNode(headA, headB *ListNode) *ListNode {
	pA := headA
	pB := headB
	listNodeMap := make(map[*ListNode]bool)
	listNodeMap[pA] = true
	for pA.Next != nil {
		pA = pA.Next
		listNodeMap[pA] = true
	}
	if get, _ := listNodeMap[pB]; get == true {
		return pB
	}
	for pB.Next != nil {
		pB = pB.Next
		if get, _ := listNodeMap[pB]; get == true {
			return pB
		}
	}
	return nil
}
