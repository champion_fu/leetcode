package _60_Intersection_Of_Two_LinkedList

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func getIntersectionNode(headA, headB *ListNode) *ListNode {
	var m, n int
	pA := headA
	pB := headB
	if pA != nil {
		m++
	}
	if pB != nil {
		n++
	}
	for pA.Next != nil {
		pA = pA.Next
		m++
	}
	for pB.Next != nil {
		pB = pB.Next
		n++
	}

	if m > n {
		// We set the start pointer for list A
		pA = headA
		pB = headB
		for i := 0; i < (m - n); i++ {
			pA = pA.Next
		}
	} else {
		// We set the start pointer for list B
		pA = headA
		pB = headB
		for i := 0; i < (n - m); i++ {
			pB = pB.Next
		}
	}

	// Compare
	for pA != pB && pA.Next != nil && pB.Next != nil {
		pA = pA.Next
		pB = pB.Next
	}

	if pA == pB {
		return pA
	}
	return nil
}
