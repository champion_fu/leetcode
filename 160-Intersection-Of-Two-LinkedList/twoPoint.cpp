//
// Created by champ on 2022/4/17.
//

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
        int lengthA = 1;
        int lengthB = 1;
        ListNode* pA = headA;
        ListNode* pB = headB;

        while(pA->next) {
            pA = pA->next;
            lengthA++;
        }

        while(pB->next) {
            pB = pB->next;
            lengthB++;
        }

        pA = headA;
        pB = headB;
        if (lengthA > lengthB) {
            for (int i = 0; i < (lengthA-lengthB); i++) {
                pA = pA->next;
            }
        } else {
            for (int i = 0; i < (lengthB-lengthA); i++) {
                pB = pB->next;
            }
        }

        while(pA != pB && pA->next && pB->next) {
            pA = pA->next;
            pB = pB->next;
        }

        if (pA == pB) {
            return pA;
        } else {
            return NULL;
        }
    }
};
