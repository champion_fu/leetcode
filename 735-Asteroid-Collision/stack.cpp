//
// Created by champ on 2022/3/6.
//

class Solution {
public:
    vector<int> asteroidCollision(vector<int>& asteroids) {
        stack<int> collide;

        for (auto asteroid : asteroids) {
            cout << asteroid << endl;
            bool exploded = false;
            while (!collide.empty() && collide.top() > 0 && asteroid < 0) {
                int preAsteroid = collide.top();
                if (preAsteroid > -asteroid) {
                    // if preAsteroid is bigger, then asteroid explode
                    exploded = true;
                    break;
                } else if (preAsteroid == -asteroid) {
                    // same, all explode
                    collide.pop();
                    exploded = true;
                    break;
                } else {
                    collide.pop();
                    // if preAsteroid is smaller, then preAsteroid explode
                }
            }
            if (!exploded) {
                collide.push(asteroid);
            }
        }

        int size = collide.size();
        vector<int> result(size);
        int index = size-1;
        while (!collide.empty()) {
            result[index] = collide.top();
            collide.pop();
            index--;
        }

        return result;
    }
};
