//
// Created by champ on 2022/6/29.
//

class Solution {
public:
    void nextPermutation(vector<int>& nums) {
        int length = nums.size();

        int k, l;
        for (k = length-2; k >= 0; k--) {
            if (nums[k] < nums[k+1]) break;
        }

        if (k < 0) {
            reverse(nums.begin(), nums.end());
        } else {
            for (l = length - 1; l > k; l--) {
                if (nums[l] > nums[k]) break;
            }
            swap(nums[k], nums[l]);
            reverse(nums.begin() + k + 1, nums.end());
        }
    }
};
