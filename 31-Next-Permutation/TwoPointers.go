package _1_Next_Permutation

func nextPermutation(nums []int) {
	n := len(nums)
	var k, l int
	for k = n - 2; k >= 0; k-- {
		if nums[k] < nums[k+1] {
			// find the index of last round start.
			break
		}
	}
	if k == -1 {
		reverse(nums, 0, n-1)
		return
	}

	for l = n - 1; l > k; l-- {
		// find the value of next round should start.
		if nums[l] > nums[k] {
			break
		}
	}
	swap(nums, l, k)
	reverse(nums, k+1, n-1)
	return
}

func swap(nums []int, i, j int) {
	nI := nums[i]
	nums[i] = nums[j]
	nums[j] = nI
}

func reverse(nums []int, i, j int) {
	for i < j {
		swap(nums, i, j)
		i++
		j--
	}
}
