package __Add_Two_Numbers

import "fmt"

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	var carry int
	var remain int

	dummyHead := &ListNode{
		Val:  -1,
		Next: nil,
	}
	cur := dummyHead

	for l1 != nil && l2 != nil {
		carry = l1.Val + l2.Val + carry
		fmt.Printf("carry %v\n", carry)
		if carry >= 10 {
			remain = carry % 10
			carry = carry / 10
		} else {
			remain = carry
			carry = 0
		}
		fmt.Printf("remain %v, carry %v\n", remain, carry)
		node := &ListNode{
			Val:  remain,
			Next: nil,
		}
		cur.Next = node
		cur = cur.Next
		l1 = l1.Next
		l2 = l2.Next
	}

	for l1 != nil {
		carry = l1.Val + carry
		if carry >= 10 {
			remain = carry % 10
			carry = carry / 10
		} else {
			remain = carry
			carry = 0
		}
		cur.Next = &ListNode{
			Val:  remain,
			Next: nil,
		}
		cur = cur.Next
		l1 = l1.Next
	}

	for l2 != nil {
		carry = l2.Val + carry
		if carry >= 10 {
			remain = carry % 10
			carry = carry / 10
		} else {
			remain = carry
			carry = 0
		}
		cur.Next = &ListNode{
			Val:  remain,
			Next: nil,
		}
		cur = cur.Next
		l2 = l2.Next
	}
	if carry != 0 {
		cur.Next = &ListNode{
			Val:  carry,
			Next: nil,
		}
	}
	return dummyHead.Next
}
