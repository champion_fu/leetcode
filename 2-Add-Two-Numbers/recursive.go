package __Add_Two_Numbers

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	dummyHead := &ListNode{
		Val:  -1,
		Next: nil,
	}
	p := dummyHead
	recursive(p, l1, l2, 0)
	return dummyHead.Next
}

func recursive(previous, l1 *ListNode, l2 *ListNode, carry int) *ListNode {
	if l1 == nil && l2 == nil && carry == 0 {
		return previous
	}
	value := carry
	if l1 != nil {
		value += l1.Val
		l1 = l1.Next
	}
	if l2 != nil {
		value += l2.Val
		l2 = l2.Next
	}
	n := &ListNode{
		Val:  value % 10,
		Next: nil,
	}
	previous.Next = recursive(n, l1, l2, value/10)
	return previous
}
