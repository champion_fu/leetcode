package __Add_Two_Numbers

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	dummyHead := &ListNode{Val: -1, Next: nil}
	p := dummyHead
	add := 0
	for l1 != nil || l2 != nil || add != 0 {
		v := 0
		if l1 != nil {
			v += l1.Val
		}
		if l2 != nil {
			v += l2.Val
		}
		v += add

		add = v / 10
		p.Next = &ListNode{Val: v % 10, Next: nil}
		if l1 != nil {
			l1 = l1.Next
		}
		if l2 != nil {
			l2 = l2.Next
		}
		p = p.Next
	}
	return dummyHead.Next
}
