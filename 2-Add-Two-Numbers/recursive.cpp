//
// Created by champ on 2022/4/17.
//

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        ListNode* dummyHead = new ListNode(0);
        recursive(dummyHead, l1, l2, 0);
        return dummyHead->next;
    }
private:
    ListNode* recursive(ListNode* prev, ListNode* l1, ListNode* l2, int carry) {
        if (l1 == NULL && l2 == NULL && carry == 0) {
            return prev;
        }
        ListNode* newOne = new ListNode(carry);
        if (l1) {
            newOne->val += l1->val;
            l1 = l1->next;
        }
        if (l2) {
            newOne->val += l2->val;
            l2 = l2->next;
        }
        carry = newOne->val / 10;
        newOne->val = newOne->val % 10;
        prev->next = recursive(newOne, l1, l2, carry);
        return prev;
    }
};
