//
// Created by champ on 2022/9/28.
//

class Solution {
public:
    bool checkDistances(string s, vector<int>& distance) {
        unordered_map<int, int> m;
        for (int i = 0; i < s.size(); i++) {
            char c = s[i];
            if (m.count(c-'a') > 0 && distance[c-'a'] != (i - m[c-'a'] - 1)) {
                // cout << "c " << c << " distance[c-'a'] " << distance[c-'a'] << endl;
                return false;
            }
            m[c-'a'] = i;
        }
        return true;
    }
};
