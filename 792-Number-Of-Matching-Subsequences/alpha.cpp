//
// Created by champ on 2022/4/23.
//

class Solution {
public:
    int numMatchingSubseq(string s, vector<string>& words) {
        vector<vector<int>> alphas(26);
        for (int i = 0; i < s.size(); i++) alphas[s[i] - 'a'].push_back(i);

        int ret = 0;
        for (auto w : words) {
            int x = -1;
            bool found = true;

            for (auto c : w) {
                auto it = upper_bound(alphas[c - 'a'].begin(), alphas[c - 'a'].end(), x);
                if (it == alphas[c-'a'].end()) {
                    // not found
                    found = false;
                    break;
                } else {
                    // found
                    x = *it;
                }
            }
            if (found) ret++;
        }

        return ret;
    }
};
