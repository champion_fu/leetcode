//
// Created by champ on 2022/4/23.
//

class Solution {
public:
    int numMatchingSubseq(string s, vector<string>& words) {
        vector<const char*> waiting[26];
        for (auto &w : words) { // point
            // cout << w[0] - 'a' << w.c_str() << endl;
            waiting[w[0] - 'a'].push_back(w.c_str()); // store address
        }

        // for (auto wait : waiting) {
        //     for (auto it = wait.begin(); it != wait.end(); it++) {
        //         cout << *it << endl;
        //     }
        // }

        int ret = 0;
        for (char c : s) {
            auto advance = waiting[c - 'a'];
            waiting[c - 'a'].clear();
            for (auto it: advance) {
                // cout << *++it << endl;
                if (!*++it) {
                    // empty
                    ret++;
                } else {
                    // cout << *it - 'a' << endl;
                    waiting[*it - 'a'].push_back(it);
                }
            }
        }
        return ret;
    }
};
