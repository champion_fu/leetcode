package _07_Design_Linked_List

type Node struct {
	val  int
	next *Node
}

type MyLinkedList struct {
	head  *Node
	count int
}

/** Initialize your data structure here. */
func Constructor() MyLinkedList {
	return MyLinkedList{head: nil, count: 0}
}

/** Get the value of the index-th node in the linked list. If the index is invalid, return -1. */
func (this *MyLinkedList) Get(index int) int {
	if this.count == 0 || this.count <= index {
		return -1
	}

	p := this.head
	for i := 0; i < index && p.next != nil; i++ {
		p = p.next
	}
	return p.val
}

/** Add a node of value val before the first element of the linked list. After the insertion, the new node will be the first node of the linked list. */
func (this *MyLinkedList) AddAtHead(val int) {
	p := this.head
	if p == nil && this.count == 0 {
		this.head = &Node{val: val, next: nil}
	} else {
		this.head = &Node{val: val, next: p}
	}
	this.count++
}

/** Append a node of value val to the last element of the linked list. */
func (this *MyLinkedList) AddAtTail(val int) {
	if this.count == 0 {
		this.AddAtHead(val)
		return
	}

	p := this.head
	for p.next != nil {
		p = p.next
	}
	tail := &Node{val: val, next: nil}
	p.next = tail
	this.count++
}

/** Add a node of value val before the index-th node in the linked list. If index equals to the length of linked list, the node will be appended to the end of linked list. If index is greater than the length, the node will not be inserted. */
func (this *MyLinkedList) AddAtIndex(index int, val int) {
	if index == 0 {
		this.AddAtHead(val)
	} else if index == this.count {
		this.AddAtTail(val)
	} else if index < 0 || index > this.count {
		return
	} else {
		p := this.head
		for i := 0; i < index-1 && p.next != nil; i++ {
			p = p.next
		}
		next := p.next
		p.next = &Node{val: val, next: next}
		this.count++
	}
}

/** Delete the index-th node in the linked list, if the index is valid. */
func (this *MyLinkedList) DeleteAtIndex(index int) {
	if index < 0 || index >= this.count {
		return
	} else if index == 0 {
		if this.count == 0 {
			return
		} else if this.count == 1 {
			this.head = nil
			this.count = 0
		} else {
			this.head = this.head.next
			this.count--
		}
	} else {
		p := this.head
		for i := 0; i < index-1 && p.next != nil; i++ {
			p = p.next
		}
		p.next = p.next.next
		this.count--
	}
}

/**
 * Your MyLinkedList object will be instantiated and called as such:
 * obj := Constructor();
 * param_1 := obj.Get(index);
 * obj.AddAtHead(val);
 * obj.AddAtTail(val);
 * obj.AddAtIndex(index,val);
 * obj.DeleteAtIndex(index);
 */
