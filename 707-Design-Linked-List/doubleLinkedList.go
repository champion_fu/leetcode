package _07_Design_Linked_List

type Node struct {
	Prev *Node
	Next *Node
	Val  int
}

type MyLinkedList struct {
	Head  *Node
	Count int
}

/** Initialize your data structure here. */
func Constructor() MyLinkedList {
	return MyLinkedList{Head: nil, Count: 0}
}

/** Get the value of the index-th node in the linked list. If the index is invalid, return -1. */
func (this *MyLinkedList) Get(index int) int {
	if index > this.Count-1 {
		return -1
	}
	p := this.Head
	for i := 0; i < index; i++ {
		p = p.Next
	}
	return p.Val
}

/** Add a node of value val before the first element of the linked list. After the insertion, the new node will be the first node of the linked list. */
func (this *MyLinkedList) AddAtHead(val int) {
	node := &Node{Next: nil, Prev: nil, Val: val}
	defer func() {
		this.Count++
	}()

	if this.Head == nil {
		this.Head = node
	} else {
		node.Next = this.Head
		this.Head.Prev = node
		this.Head = node
	}
}

/** Append a node of value val to the last element of the linked list. */
func (this *MyLinkedList) AddAtTail(val int) {
	n := &Node{Next: nil, Prev: nil, Val: val}
	defer func() {
		this.Count++
	}()

	// count == 0
	if this.Count == 0 {
		this.Head = n
		return
	}

	// count != 0
	p := this.Head
	for p.Next != nil {
		p = p.Next
	}
	n.Prev = p
	p.Next = n
}

/** Add a node of value val before the index-th node in the linked list. If index equals to the length of linked list, the node will be appended to the end of linked list. If index is greater than the length, the node will not be inserted. */
func (this *MyLinkedList) AddAtIndex(index int, val int) {
	if index == 0 {
		this.AddAtHead(val)
	} else if index == this.Count {
		this.AddAtTail(val)
	} else if index > this.Count || index < 0 {
		return
	} else {
		p := this.Head
		for i := 0; i < index; i++ {
			p = p.Next
		}
		n := &Node{Val: val, Next: p, Prev: p.Prev}
		p.Prev.Next = n
		p.Prev = n
		this.Count++
	}
}

/** Delete the index-th node in the linked list, if the index is valid. */
func (this *MyLinkedList) DeleteAtIndex(index int) {
	if index < 0 || index > this.Count-1 {
		return
	} else if index == 0 {
		this.Head = this.Head.Next
		if this.Head != nil {
			this.Head.Prev = nil
		}
	} else {
		p := this.Head
		for i := 0; i < index; i++ {
			p = p.Next
		}
		if p.Prev != nil {
			p.Prev.Next = p.Next
		}
		if p.Next != nil {
			p.Next.Prev = p.Prev
		}
	}
	this.Count--
}

/**
 * Your MyLinkedList object will be instantiated and called as such:
 * obj := Constructor();
 * param_1 := obj.Get(index);
 * obj.AddAtHead(val);
 * obj.AddAtTail(val);
 * obj.AddAtIndex(index,val);
 * obj.DeleteAtIndex(index);
 */
