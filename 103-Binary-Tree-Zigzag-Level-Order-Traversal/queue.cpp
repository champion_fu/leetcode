//
// Created by champ on 2022/4/17.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> zigzagLevelOrder(TreeNode* root) {
        vector<vector<int>> ret;
        if (root == NULL) {
            return ret;
        }

        deque<TreeNode*> queue;
        queue.push_back(root);

        bool leftToRight = true;

        while(!queue.empty()) {
            int size = queue.size();
            vector<int> row(size);

            for (int i = 0; i < size; i++) {
                TreeNode* tmp = queue.front();
                queue.pop_front();

                if (leftToRight) {
                    row[i] = tmp->val;
                } else {
                    row[size-i-1] = tmp->val;
                }

                if (tmp->left) queue.push_back(tmp->left);
                if (tmp->right) queue.push_back(tmp->right);
            }
            ret.push_back(row);
            leftToRight = !leftToRight;
        }
        return ret;
    }
};
