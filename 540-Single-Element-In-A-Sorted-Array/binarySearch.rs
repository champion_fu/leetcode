impl Solution {
    pub fn single_non_duplicate(nums: Vec<i32>) -> i32 {
        // take two items as one combination (as whole)
        let (mut l, mut r) = (0, nums.len() - 1);

        while l < r {
            let mut m = l + (r - l) / 2;
            if m % 2 == 1 {
                m -= 1;
            }
            if (nums[m] != nums[m+1]) {
                r = m;
            } else {
                l = m + 2; // take two as whole
            }
        }
        nums[l]
    }
}
