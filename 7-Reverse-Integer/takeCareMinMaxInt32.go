package __Reverse_Integer

import (
	"fmt"
	"math"
)

func reverse(x int) int {
	maxRange := math.MaxInt32
	minRange := math.MinInt32
	fmt.Printf("maxRagne, minRange %v, %v\n", maxRange, minRange)
	negative := false
	if x < 0 {
		negative = true
		x = -x
	}
	ret := 0
	for x > 0 {
		remain := x % 10
		x = x / 10
		ret = ret*10 + remain

		if negative && -ret < minRange {
			return 0
		}
		if !negative && ret > maxRange {
			return 0
		}
	}

	if negative {
		return -ret
	}
	return ret
}
