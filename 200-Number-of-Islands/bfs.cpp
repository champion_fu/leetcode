//
// Created by champ on 2022/4/18.
//

class Solution {
    int m;
    int n;
public:
    int numIslands(vector<vector<char>>& grid) {
        m = grid.size();
        if (!m) return 0;
        n = grid[0].size();
        int ret = 0;

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == '1') {
                    bfs(grid, i, j);
                    ret++;
                }
            }
        }
        return ret;
    }
private:
    void bfs(vector<vector<char>>& grid, int i, int j) {
        grid[i][j] = '0';

        if (i-1 >= 0 && grid[i-1][j] == '1') bfs(grid, i-1, j); // upper
        if (j+1 < n && grid[i][j+1] == '1') bfs(grid, i, j+1); // right
        if (j-1 >= 0 && grid[i][j-1] == '1') bfs(grid, i, j-1); // left
        if (i+1 < m && grid[i+1][j] == '1') bfs(grid, i+1, j); // down
    }
};
