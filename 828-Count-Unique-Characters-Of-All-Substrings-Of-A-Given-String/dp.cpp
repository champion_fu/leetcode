//
// Created by champ on 2022/7/26.
//

class Solution {
public:
    int uniqueLetterString(string s) {
        vector<int> lastPos(26, 0);
        vector<int> contribution(26, 0);
        int res = 0;

        // Basically, at each it, we count the contribution of all the characters to all the substrings ending till that point.
        for (int i = 0; i < s.size(); i++) {
            int curChar = s[i] - 'A';

            //       Now, we need to update the contribution of curChar.
//       The total number of substrings ending at i are i+1. So if it was a unique character, it'd contribute to all of those
//       and it's contribution would have been i+1.
//       But if it's repeating, it means it has already contributed previously. So remove it's previous contribution.
//       We can do that as we have it's last position.
//       So these are the contributions for strings which start after this character's last occurrence and end at i.
//       A simple example will demonstrate that the number of these strings are i+1 - lastPosition[curChar]
//       For characters not appeared till now, lastPosition[curChar] would be 0.
            int totalNumOfSubstringsEndingHere = i + 1;
            contribution[curChar] = totalNumOfSubstringsEndingHere - lastPos[curChar];

            int cur = 0;
            for (int j = 0; j < 26; j++) {
                cur += contribution[j];
            }

            res += cur;
            lastPos[curChar] = i + 1;
        }
        return res;

    }
};
