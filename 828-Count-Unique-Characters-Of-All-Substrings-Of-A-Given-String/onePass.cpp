//
// Created by champ on 2022/7/26.
//

/*
index[26][2] record last two occurrence index for every upper characters.
Initialise all values in index to -1.
Loop on string S, for every character c, update its last two occurrence index to index[c].
Count when loop. For example, if "A" appears twice at index 3, 6, 9 seperately, we need to count:
    For the first "A": (6-3) * (3-(-1))"
    For the second "A": (9-6) * (6-3)"
    For the third "A": (N-9) * (9-6)"
 */

class Solution {
public:
    int uniqueLetterString(string s) {
        // index record last two occurrence index for every upper characters.
        int res = 0, size = s.size();
        vector<vector<int>> index(26, vector<int>(2, -1));

        for (int i = 0; i < size; i++) {
            int c = s[i] - 'A';
            res = (res + (i - index[c][1]) * (index[c][1] - index[c][0]));
            index[c][0] = index[c][1];
            index[c][1] = i;
        }

        for (int c = 0; c < 26; c++) {
            res = (res + (size - index[c][1]) * (index[c][1] - index[c][0]));
        }

        return res;
    }
};
