//
// Created by champ on 2022/7/19.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

class Solution {
public:
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        int pValue = p->val;
        int qValue = q->val;

        while (root != NULL) {
            int val = root->val;
            if (val > pValue && val > qValue) {
                root = root->left;
            } else if (val < pValue && val < qValue) {
                root = root->right;
            } else {
                return root;
            }
        }
        return NULL;
    }
};
