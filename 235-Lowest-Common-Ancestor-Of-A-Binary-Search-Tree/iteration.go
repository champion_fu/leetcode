package _35_Lowest_Common_Ancestor_Of_A_Binary_Search_Tree

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val   int
 *     Left  *TreeNode
 *     Right *TreeNode
 * }
 */

func lowestCommonAncestor(root, p, q *TreeNode) *TreeNode {
	pVal := p.Val
	qVal := q.Val
	node := root

	for node != nil {
		value := node.Val
		if pVal > value && qVal > value {
			node = node.Right
		} else if pVal < value && qVal < value {
			node = node.Left
		} else {
			return node
		}
	}
	return nil
}
