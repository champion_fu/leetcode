package _01_Symmetric_Tree

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func isSymmetric(root *TreeNode) bool {
	if root == nil {
		return true
	}

	q := queue{}
	q.Push(root)
	q.Push(root)

	for !q.IsEmpty() {
		n1 := q.Pop()
		n2 := q.Pop()
		if n1 == nil && n2 == nil {
			continue
		}
		if n1 == nil || n2 == nil {
			return false
		}
		if n1.Val != n2.Val {
			return false
		}
		q.Push(n1.Left)
		q.Push(n2.Right)
		q.Push(n1.Right)
		q.Push(n2.Left)
	}
	return true
}

type queue []*TreeNode

func (q *queue) IsEmpty() bool {
	return len(*q) == 0
}

func (q *queue) Push(n *TreeNode) {
	*q = append(*q, n)
}

func (q *queue) Pop() *TreeNode {
	if q.IsEmpty() {
		return nil
	} else {
		element := (*q)[0]
		*q = (*q)[1:]
		return element
	}
}
