//
// Created by champ on 2022/7/13.
//

/**
 * // This is the interface that allows for creating nested lists.
 * // You should not implement it, or speculate about its implementation
 * class NestedInteger {
 *   public:
 *     // Return true if this NestedInteger holds a single integer, rather than a nested list.
 *     bool isInteger() const;
 *
 *     // Return the single integer that this NestedInteger holds, if it holds a single integer
 *     // The result is undefined if this NestedInteger holds a nested list
 *     int getInteger() const;
 *
 *     // Return the nested list that this NestedInteger holds, if it holds a nested list
 *     // The result is undefined if this NestedInteger holds a single integer
 *     const vector<NestedInteger> &getList() const;
 * };
 */

class NestedIterator {
private:
    stack<NestedInteger> node;
public:
    NestedIterator(vector<NestedInteger> &nestedList) {
        int size = nestedList.size();
        for (int i = size - 1; i >= 0; i--) node.push(nestedList[i]);
    }

    int next() {
        int result = node.top().getInteger();
        node.pop();
        return result;
    }

    bool hasNext() {
        while (!node.empty()) {
            NestedInteger cur = node.top();
            if (cur.isInteger()) return true;

            node.pop();
            vector<NestedInteger>& inner = cur.getList();
            int size = inner.size();
            for (int i = size - 1; i >= 0; i--) node.push(inner[i]);
        }
        return false;
    }
};

/**
 * Your NestedIterator object will be instantiated and called as such:
 * NestedIterator i(nestedList);
 * while (i.hasNext()) cout << i.next();
 */
