package _21_Best_Time_To_Buy_And_Sell_Stock

import "math"

func maxProfit(prices []int) int {
	minValue := math.MaxInt64
	maxProfit := 0
	for _, value := range prices {
		if value < minValue {
			minValue = value
		}
		profit := value - minValue
		if profit > maxProfit {
			maxProfit = profit
		}
	}
	return maxProfit
}
