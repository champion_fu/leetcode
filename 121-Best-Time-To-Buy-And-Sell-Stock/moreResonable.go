package _21_Best_Time_To_Buy_And_Sell_Stock

import "math"

func maxProfit(prices []int) int {
	minimum := math.MaxInt32
	ret := 0

	for _, v := range prices {
		if v < minimum {
			minimum = v
		} else if v-minimum > ret {
			ret = v - minimum
		}
	}
	return ret
}
