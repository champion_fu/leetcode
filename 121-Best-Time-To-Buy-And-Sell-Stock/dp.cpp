//
// Created by champ on 2022/4/11.
//

#include <algorithm>    // std::min

class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int ret = 0;
        int m = INT_MAX;
        for (auto p : prices) {
            m = min(p, m);
            if (p - m > ret) ret = p - m;
        }
        return ret;
    }
};
