//
// Created by champ on 2022/4/11.
//

class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int ret = 0;
        for (int i = 0; i < prices.size(); i++) {
            int max = 0;
            for (int j = i+1; j < prices.size(); j++) {
                if (prices[j] - prices[i] > max) {
                    max = prices[j] - prices[i];
                }
            }
            if (max > ret) {
                ret = max;
            }
        }
        return ret;
    }
};
