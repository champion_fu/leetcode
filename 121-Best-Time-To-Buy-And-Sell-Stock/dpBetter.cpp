//
// Created by champ on 2022/7/18.
//

class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int res = 0;
        int minimum = INT_MAX;

        for (auto p : prices) {
            minimum = min(p, minimum);
            res = max(res, p-minimum);
        }
        return res;
    }
};
