//
// Created by champ on 2022/4/27.
//

class Solution {
public:
    vector<vector<int>> subsets(vector<int>& nums) {
        vector<vector<int>> results;
        for (int k = 0; k <= nums.size(); k++) {
            vector<int> thisCandidate(0);
            backtracking(results, thisCandidate, 0, nums, k);
        }
        return results;
    }
private:
    void backtracking(vector<vector<int>>& results, vector<int> thisCandidate, int index, vector<int>& nums, int k) {
        if (thisCandidate.size() == k) {
            results.push_back(thisCandidate);
            // for (auto num : results[results.size()-1]) {
            //     cout << num << endl;
            // }
            return;
        }

        for (int i = index; i < nums.size(); i++) {
            thisCandidate.push_back(nums[i]);
            backtracking(results, thisCandidate, i+1, nums, k);
            thisCandidate.pop_back();
        }
    }
};
