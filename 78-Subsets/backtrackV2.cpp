//
// Created by champ on 2022/7/31.
//

class Solution {
    int size;
private:
    void backtrack(vector<vector<int>> &res, vector<int> tmp, vector<int>& nums, int index) {
        if (tmp.size() > size) {
            return;
        }

        res.push_back(tmp);

        for (int i = index; i < size; i++) {
            tmp.push_back(nums[i]);
            backtrack(res, tmp, nums, i+1);
            tmp.pop_back();
        }

    }
public:
    vector<vector<int>> subsets(vector<int>& nums) {
        size = nums.size();
        vector<vector<int>> res;
        vector<int> tmp = {};
        backtrack(res, tmp, nums, 0);
        return res;
    }
};
