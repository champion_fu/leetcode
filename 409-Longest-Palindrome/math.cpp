//
// Created by champ on 2022/7/20.
//

class Solution {
public:
    int longestPalindrome(string s) {
        int freq[128]={};
        for (auto c : s) freq[c]++;
        int use = 0;
        for (auto i : freq) use += i & ~1; // (num / 2) * 2
        return use + (use < s.size());
    }
};
