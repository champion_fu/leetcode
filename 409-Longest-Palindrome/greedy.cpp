//
// Created by champ on 2022/7/20.
//

class Solution {
public:
    int longestPalindrome(string s) {
        unordered_map<char, int> m;

        for (auto c : s) {
            m[c]++;
        }

        int result = 0;
        int extra = 0;
        for (pair<char, int>p : m) {
            int count = p.second;
            if (count / 2 > 0) result += (count / 2) * 2;
            if (count % 2 != 0) extra = 1;
        }
        return result + extra;
    }
};
