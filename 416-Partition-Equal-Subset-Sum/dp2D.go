package _16_Partition_Equal_Subset_Sum

func canPartition(nums []int) bool {
	sum := 0
	for _, v := range nums {
		sum += v
	}

	if sum%2 == 1 {
		return false
	}
	n := len(nums)

	dp := make([][]bool, n+1)
	for i := 0; i < n+1; i++ {
		dp[i] = make([]bool, (sum/2)+1)
	}
	dp[0][0] = true // from index 0 to index i(0), can sum up to j(0)

	for i := 1; i <= n; i++ {
		curVal := nums[i-1]
		for j := 0; j <= sum/2; j++ {
			if j < curVal { // can't decrease
				dp[i][j] = dp[i-1][j]
			} else {
				dp[i][j] = dp[i-1][j] || dp[i-1][j-curVal]
			}
		}
	}
	return dp[n][sum/2]
}
