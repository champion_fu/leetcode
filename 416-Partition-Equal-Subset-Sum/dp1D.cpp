//
// Created by champ on 2022/7/29.
//

class Solution {
public:
    bool canPartition(vector<int>& nums) {
        int sum = 0;
        for (int num : nums) {
            sum += num;
        }
        if (sum & 1) return false;
        int target = sum / 2;
        int size = nums.size();
        vector<bool> dp(target+1);
        dp[0] = true; // from index 0 can sum up to j(0)

        for (int v : nums) {
            for (int j = target; j >= v; j--) {
                dp[j] = dp[j] || dp[j-v];
            }
        }
        return dp[target];
    }
};
