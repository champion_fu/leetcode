package _16_Partition_Equal_Subset_Sum

func canPartition(nums []int) bool {
	sum := 0
	for _, v := range nums {
		sum += v
	}

	if sum%2 == 1 {
		return false
	}

	return dfs(nums, sum/2, len(nums)-1)
}

func dfs(nums []int, target, index int) bool {
	if target == 0 {
		return true
	}
	if index < 0 || target < 0 {
		return false
	}
	return dfs(nums, target-nums[index], index-1) || dfs(nums, target, index-1)
}
