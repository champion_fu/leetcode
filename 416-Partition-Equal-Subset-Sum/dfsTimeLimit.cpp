//
// Created by champ on 2022/7/29.
//

class Solution {
public:
    bool canPartition(vector<int>& nums) {
        int sum = 0;
        for (int num : nums) {
            sum += num;
        }
        if (sum & 1) return false;
        int target = sum / 2;

        return dfs(nums, target, 0);
    }
private:
    bool dfs(vector<int>& nums, int target, int index) {
        if (target == 0) return true;
        if (index == nums.size() || target < 0) return false;
        return dfs(nums, target-nums[index], index+1) || dfs(nums, target, index+1);
    }
};
