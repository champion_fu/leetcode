//
// Created by champ on 2022/7/29.
//


class Solution {
public:
    bool canPartition(vector<int>& nums) {
        int sum = 0;
        for (int num : nums) {
            sum += num;
        }
        if (sum & 1) return false;
        int target = sum / 2;
        vector<vector<bool>> dp(nums.size()+1, vector<bool>(target+1));
        dp[0][0] = true; // from index 0 to index 0(i), can sum up to 0(j)

        for (int i = 1; i <= nums.size(); i++) {
            int curVal = nums[i-1];
            for (int j = 0; j <= target; j++) {
                if (j - curVal < 0) { // can't decrease
                    dp[i][j] = dp[i-1][j];
                } else {
                    dp[i][j] = dp[i-1][j] || dp[i-1][j-curVal];
                }
            }
        }

        return dp[nums.size()][target];
    }
};
