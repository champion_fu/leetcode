package _16_Partition_Equal_Subset_Sum

func canPartition(nums []int) bool {
	sum := 0
	for _, v := range nums {
		sum += v
	}

	if sum%2 == 1 {
		return false
	}

	dp := make([]bool, (sum/2)+1)
	dp[0] = true

	for _, v := range nums {
		for j := sum / 2; j >= v; j-- {
			dp[j] = dp[j] || dp[j-v]
		}
	}
	return dp[sum/2]
}
