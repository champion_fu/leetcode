//
// Created by champ on 2022/7/18.
//

class Solution {
public:
    bool isPalindrome(string s) {
        for (int l = 0, r = s.size() - 1; l < r; l++, r--) {
            while (l < r && !isalnum(s[l])) l++;
            while (l < r && !isalnum(s[r])) r--;

            cout << "l " << l << " s[l] " << tolower(s[l]) << " r " << r << " s[r] " << tolower(s[r]) << endl;
            if (tolower(s[l]) != tolower(s[r])) return false;
        }
        return true;
    }
;
