//
// Created by champ on 2022/10/24.
//

class MovingAverage {
    queue<int> q;
    double sum = 0;
    int size;
public:
    MovingAverage(int size) : size(size) {
    }

    double next(int val) {
        sum += val;
        q.push(val);
        // cout << " sum " << sum << " val " << val << " q size " << q.size() << endl;
        while (q.size() > size) {
            sum -= q.front();
            q.pop();
        }
        // cout << " sum " << sum  << " q size " << q.size() << endl;
        return sum / q.size();
    }
};

/**
 * Your MovingAverage object will be instantiated and called as such:
 * MovingAverage* obj = new MovingAverage(size);
 * double param_1 = obj->next(val);
 */
