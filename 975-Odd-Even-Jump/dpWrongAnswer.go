package _75_Odd_Even_Jump

import "fmt"

// [1,2,3,2,1,4,4,5]

func oddEvenJumps(arr []int) int {
	n := len(arr)
	if n < 2 {
		return n
	}

	var dp = make([][]int, n)
	for i := range dp {
		if i == n-1 {
			// last one, we don't need jump
			dp[i] = []int{1, 1}
		} else {
			dp[i] = []int{0, 0}
		}
	}

	for i := n - 2; i >= 0; i-- {
		curr := arr[i]
		// odd jump
		indexOdd := findMax(arr[i+1:], curr)
		if indexOdd != -1 {
			// can odd jump, next is even jump
			dp[i][1] = dp[i+indexOdd+1][0] // pick next even jump result
		}

		// even jump
		indexEven := findMin(arr[i+1:], curr)
		if indexEven != -1 {
			// can even jump, next is odd jump
			dp[i][0] = dp[i+indexEven+1][1] // pick next old jump result
		}
		fmt.Printf("dp %v, curr %v, indexOdd %v, indexEven %v\n", dp, curr, indexOdd, indexEven)
	}

	var ret int
	for _, v := range dp {
		if v[1] == 1 {
			ret += 1
		}
	}
	return ret
}

func findMax(candidates []int, target int) int {
	// find the number which is bigger or equal than target number.
	// If there are many numbers are bigger than target, pick smallest index.
	for i := 0; i < len(candidates); i++ {
		if candidates[i] >= target {
			return i
		}
	}
	return -1
}

func findMin(candidates []int, target int) int {
	// find the number which is smaller or equal than target number.
	// If there are many numbers are smaller than target, pick smallest index.
	for i := 0; i < len(candidates); i++ {
		if candidates[i] <= target {
			return i
		}
	}
	return -1
}
