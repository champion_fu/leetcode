package _75_Odd_Even_Jump

import (
	"fmt"
	"sort"
)

// [1,2,3,2,1,4,4,5]
// [1,2,1,3,1,4,1]

func oddEvenJumps(arr []int) int {
	n := len(arr)
	if n < 2 {
		return n
	}

	var dp = make([][]int, n)
	for i := range dp {
		if i == n-1 {
			// last one, we don't need jump
			dp[i] = []int{1, 1}
		} else {
			dp[i] = []int{0, 0}
		}
	}

	var sortedValues []*pair
	for i, v := range arr {
		sortedValues = append(sortedValues, &pair{
			val:   v,
			index: i,
		})
	}
	decrease := func(i, j int) bool {
		if sortedValues[i].val == sortedValues[j].val {
			return sortedValues[i].index < sortedValues[j].index
		}
		return sortedValues[i].val < sortedValues[j].val
	}
	increase := func(i, j int) bool {
		if sortedValues[i].val == sortedValues[j].val {
			return sortedValues[i].index < sortedValues[j].index
		}
		return sortedValues[i].val > sortedValues[j].val
	}
	sort.Slice(sortedValues, decrease)
	var nextHigher = make([]int, n)
	s := stack{}
	for _, v := range sortedValues {
		i := v.index
		for !s.IsEmpty() && s.Top() < i {
			nextHigher[s.Pop()] = i
		}
		s.Push(i)
	}

	sort.Slice(sortedValues, increase)
	s = stack{}
	var nextLower = make([]int, n)
	for _, v := range sortedValues {
		i := v.index
		for !s.IsEmpty() && s.Top() < i {
			nextLower[s.Pop()] = i
		}
		s.Push(i)
	}

	fmt.Printf("\nNext Higher")
	for _, v := range nextHigher {
		fmt.Printf("%v ", v)
	}
	fmt.Printf("\nNext Lower")
	for _, v := range nextLower {
		fmt.Printf("%v ", v)
	}

	for i := n - 2; i >= 0; i-- {
		// odd jump, next is even jump
		dp[i][1] = dp[nextHigher[i]][0] // pick next even jump result
		// even jump, next is odd jump
		dp[i][0] = dp[nextLower[i]][1] // pick next old jump result
	}

	var ret int
	for _, v := range dp {
		if v[1] == 1 {
			ret += 1
		}
	}
	return ret
}

type pair struct {
	val   int
	index int
}

type stack []int

func (s *stack) IsEmpty() bool {
	return len(*s) == 0
}
func (s *stack) Push(a int) {
	*s = append(*s, a)
}
func (s *stack) Pop() int {
	if s.IsEmpty() {
		return -1
	} else {
		i := len(*s) - 1
		element := (*s)[i]
		*s = (*s)[:i]
		return element
	}
}
func (s *stack) Top() int {
	if s.IsEmpty() {
		return -1
	} else {
		i := len(*s) - 1
		element := (*s)[i]
		return element
	}
}
