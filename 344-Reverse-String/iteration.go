package _44_Reverse_String

func reverseString(s []byte) {
	leftIndex := 0
	rightIndex := len(s) - 1
	for leftIndex <= rightIndex {
		tmp := s[rightIndex]
		s[rightIndex] = s[leftIndex]
		s[leftIndex] = tmp
		rightIndex--
		leftIndex++
	}
}
