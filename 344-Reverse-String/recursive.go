package _44_Reverse_String

func reverseString(s []byte) {
	recursive(0, len(s)-1, s)
}

func recursive(leftIndex int, rightIndex int, s []byte) []byte {
	if leftIndex >= rightIndex {
		return s
	}
	tmp := s[rightIndex]
	s[rightIndex] = s[leftIndex]
	s[leftIndex] = tmp
	return recursive(leftIndex+1, rightIndex-1, s)
}
