use std::cmp;

impl Solution {
    pub fn min_flips_mono_incr(s: String) -> i32 {
        // 1. s[i+1] == 1, nothing to do, 1 can append to end
        // 2. s[i+1] == 0,
        // 2.1  flip s[i+1]
        // 2.2  flip s[~k] where s[~k] = 1
        let mut ans = 0;
        let mut num = 0; // how many 1
        for c in s.chars() {
            if ( c == '0') {
                ans = cmp::min(num, ans + 1);
            } else {
                num += 1;
            }
        }
        ans
    }
}
