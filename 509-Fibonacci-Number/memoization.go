package _09_Fibonacci_Number

func fib(n int) int {
	var lookUpTable = make(map[int]int, 0)
	if n < 2 {
		return n
	}
	lookUpTable[0] = 0
	lookUpTable[1] = 1
	return fibHelp(n, lookUpTable)
}

func fibHelp(n int, lookUpTable map[int]int) int {
	var fib1, fib2 int
	if val, match := lookUpTable[n-2]; match {
		fib2 = val
	} else {
		fib2 = fibHelp(n-2, lookUpTable)
	}
	if val, match := lookUpTable[n-1]; match {
		fib1 = val
	} else {
		fib1 = fibHelp(n-1, lookUpTable)
	}
	lookUpTable[n] = fib1 + fib2
	return lookUpTable[n]
}
