//
// Created by champ on 2022/8/17.
//

class Solution {
public:
    int fib(int n) {
        if (n < 2) return n;
        int first = 0;
        int second = 1;

        for (int i = 1; i < n; i++) {
            int tmp = first + second;
            first = second;
            second = tmp;
        }
        return second;
    }
};
