package _09_Fibonacci_Number

func fib(n int) int {
	if n < 2 {
		return n
	} else if n == 2 {
		return 1
	}
	current := 0
	prev1 := 1
	prev2 := 1

	for i := 3; i <= n; i++ {
		current = prev1 + prev2
		prev2 = prev1
		prev1 = current
	}
	return current
}
