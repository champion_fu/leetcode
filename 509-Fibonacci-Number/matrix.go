package _09_Fibonacci_Number

import "fmt"

func fib(n int) int {
	if n < 2 {
		return n
	}

	m := [][]int{
		{1, 1},
		{1, 2},
	}

	i := n
	if n&1 > 0 { // equal n & 1 == 1 or n & 1 > 0 or n % 2 == 1
		i -= 1
	}

	res := [][]int{
		{1, 0},
		{0, 1},
	}
	pow := i / 2
	for pow > 0 {
		fmt.Printf("pow %v pow & 1 %v\n", pow, pow&1)
		// if pow % 2 == 0 {
		//     m = multiply(m, m)
		//     pow /= 2
		// } else {
		//     res = multiply(res, m)
		//     pow--
		// }

		if pow&1 > 0 { // equal pow & 1 == 1 or pow & 1 > 0
			res = multiply(res, m)
			pow--
			continue
		}
		m = multiply(m, m)
		pow /= 2
	}

	if n&1 > 0 {
		return res[1][1]
	} else {
		return res[0][1]
	}
}

func multiply(x [][]int, y [][]int) [][]int {
	a := x[0][0]*y[0][0] + x[0][1]*y[1][0]
	b := x[0][0]*y[0][1] + x[0][1]*y[1][1]
	c := x[1][0]*y[0][0] + x[1][1]*y[1][0]
	d := x[1][0]*y[0][1] + x[1][1]*y[1][1]

	return [][]int{
		{a, b},
		{c, d},
	}
}
