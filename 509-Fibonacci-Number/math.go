package _09_Fibonacci_Number

import "math"

func fib(n int) int {
	var goldenRatio float64
	goldenRatio = (1 + math.Sqrt(5)) / 2

	return int(math.Round(math.Pow(goldenRatio, float64(n)) / math.Sqrt(5)))
}
