//
// Created by champ on 2022/7/31.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<int> rightSideView(TreeNode* root) {
        vector<int> res;
        if (root == NULL) return res;

        queue<TreeNode*> q;
        q.push(root);

        while (!q.empty()) {
            int size = q.size();
            cout << size << endl;
            for (int i = 0; i < size; i++) {
                TreeNode* p = q.front();
                q.pop();
                if (p->left != NULL) q.push(p->left);
                if (p->right != NULL) q.push(p->right);
                if (i == size-1) {
                    res.push_back(p->val);
                }
            }
        }
        return res;
    }
};
