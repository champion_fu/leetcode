//
// Created by champ on 2022/4/19.
//

class Solution {
public:
    bool removeOnes(vector<vector<int>>& grid) {
        int m = grid.size();

        for (int i = 1; i < m; i++) {
            if (!compare(grid[0], grid[i])) {
                return false;
            }
        }
        return true;
    }
private:
    bool compare(vector<int> v1, vector<int> v2) {
        if (v1.size() != v2.size()) return false;
        bool ret = true;
        for (int i = 0; i < v1.size(); i++) {
            if (v1[i] != v2[i]) {
                ret = false;
            }
        }
        if (ret == true) {
            return true;
        }

        ret = true;
        for (int i = 0; i < v1.size(); i++) {
            if (v1[i] != !v2[i]) {
                ret = false;
            }
        }
        return ret;
    }
};
