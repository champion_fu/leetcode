//
// Created by champ on 2022/6/10.
//

class Solution {
public:
    double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2) {
        int m = nums1.size();
        int n = nums2.size();
        if (m > n) return findMedianSortedArrays(nums2, nums1);

        int l = 0;
        int r = m;

        // left part      | right part
        // A[0] - A[i-1] | A[i] - A[m-1]
        // B[0] - B[j-1] | B[j] - B[n-1]
        // len(left) == len(right) && max(left) <= max(right)

        while (l <= r) {
            int i = l + (r-l) / 2;
            int j = (m+n+1) / 2 - i;

            int nums1Left, nums1Right, nums2Left, nums2Right;
            nums1Left = i == 0 ? INT_MIN : nums1[i-1];
            nums1Right = i == m ? INT_MAX : nums1[i];
            nums2Left = j == 0 ? INT_MIN : nums2[j-1];
            nums2Right = j == n ? INT_MAX : nums2[j];

            if (nums1Left > nums2Right) {
                r = i - 1;
            } else if (nums2Left > nums1Right) {
                l = i + 1;
            } else {
                double maxLeft, minRight;
                maxLeft = double(max(nums1Left, nums2Left));
                minRight = double(min(nums1Right, nums2Right));

                if ((m+n)%2 == 1) {
                    return maxLeft;
                } else {
                    return (minRight+maxLeft) / 2;
                }
            }
        }
        return -1.0;
    }
};
