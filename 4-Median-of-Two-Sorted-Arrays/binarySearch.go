package __Median_of_Two_Sorted_Arrays

import "math"

func findMedianSortedArrays(nums1 []int, nums2 []int) float64 {
	m := len(nums1)
	n := len(nums2)
	// make sure m <= n
	if m > n {
		return findMedianSortedArrays(nums2, nums1)
	}

	l := 0
	r := m
	for l <= r {
		i := l + (r-l)/2
		j := (m+n+1)/2 - i

		var nums1Left, nums1Right, nums2Left, nums2Right int
		if i == 0 {
			nums1Left = math.MinInt64
		} else {
			nums1Left = nums1[i-1]
		}
		if i == m {
			nums1Right = math.MaxInt64
		} else {
			nums1Right = nums1[i]
		}
		if j == 0 {
			nums2Left = math.MinInt64
		} else {
			nums2Left = nums2[j-1]
		}
		if j == n {
			nums2Right = math.MaxInt64
		} else {
			nums2Right = nums2[j]
		}

		if nums1Left > nums2Right {
			r = i - 1
		} else if nums2Left > nums1Right {
			l = i + 1
		} else {
			var maxLeft, minRight float64
			if nums1Left > nums2Left {
				maxLeft = float64(nums1Left)
			} else {
				maxLeft = float64(nums2Left)
			}
			if nums2Right > nums1Right {
				minRight = float64(nums1Right)
			} else {
				minRight = float64(nums2Right)
			}
			if (m+n)%2 == 1 {
				return maxLeft
			} else {
				return (maxLeft + minRight) / 2
			}
		}
	}
	return -1.0
}
