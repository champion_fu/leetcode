package __Median_of_Two_Sorted_Arrays

import "fmt"

func findMedianSortedArrays(nums1 []int, nums2 []int) float64 {
	n1 := len(nums1)
	n2 := len(nums2)
	if n1 == 0 && n2 == 1 {
		return float64(nums2[0])
	}
	if n2 == 0 && n1 == 0 {
		return float64(nums1[0])
	}

	p1 := 0
	p2 := 0
	lastNumber := 0
	for (p1+p2) < (n1+n2)/2 && p1 < n1 && p2 < n2 {
		if nums1[p1] > nums2[p2] {
			lastNumber = nums2[p2]
			p2++
		} else {
			lastNumber = nums1[p1]
			p1++
		}
	}

	var isOdd bool
	if (n1+n2)&1 > 0 {
		isOdd = true
	}

	fmt.Printf("p1 %v, p2 %v, lastNumber %v\n", p1, p2, lastNumber)
	if p1 == n1 {
		p2 = (n1+n2)/2 - p1
		if isOdd {
			return float64(nums2[p2])
		}

		if p2-1 >= 0 && lastNumber < nums2[p2-1] {
			lastNumber = nums2[p2-1]
		}
		return float64(nums2[p2]+lastNumber) / 2
	} else if p2 == n2 {
		p1 = (n1+n2)/2 - p2
		if isOdd {
			return float64(nums1[p1])
		}

		if p1-1 >= 0 && lastNumber < nums1[p1-1] {
			lastNumber = nums1[p1-1]
		}
		return float64(nums1[p1]+lastNumber) / 2
	}

	var answer float64
	if nums1[p1] > nums2[p2] {
		answer = float64(nums2[p2])
		if !isOdd {
			answer += float64(lastNumber)
			answer /= 2
		}
	} else {
		answer = float64(nums1[p1])
		if !isOdd {
			answer += float64(lastNumber)
			answer /= 2
		}
	}
	return answer
}
