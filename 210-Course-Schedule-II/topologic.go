package _10_Course_Schedule_II

// topologic sort
func findOrder(numCourses int, prerequisites [][]int) []int {
	ret := make([]int, 0)
	if len(prerequisites) == 0 {
		for i := 0; i < numCourses; i++ {
			ret = append(ret, i)
		}
		return ret
	}
	if numCourses <= 1 {
		return ret
	}
	indegree := make(map[int]int, 0)
	for i := 0; i < numCourses; i++ {
		indegree[i] = 0
	}
	preCourses := make(map[int][]int)

	// O(N)
	for _, course := range prerequisites {
		preCourse := course[1]
		curCourse := course[0]
		indegree[curCourse] += 1
		if courses, m := preCourses[preCourse]; m {
			// append the curCourse
			preCourses[preCourse] = append(courses, curCourse)
		} else {
			preCourses[preCourse] = []int{curCourse}
		}
	}
	s := stack{}
	for k, v := range indegree {
		if v == 0 {
			s.Push(k) // push course which doesn't need precourse
		}
	}

	for !s.IsEmpty() {
		course := s.Pop()
		ret = append(ret, course)
		courseCan := preCourses[course]
		for _, c := range courseCan {
			v := indegree[c]
			if v == 1 {
				s.Push(c)
			}
			indegree[c] -= 1
		}
	}

	for _, v := range indegree {
		if v != 0 {
			return []int{}
		}
	}
	return ret
}

type stack []int

func (s stack) IsEmpty() bool {
	return len(s) == 0
}
func (s *stack) Push(a int) {
	*s = append(*s, a)
}
func (s *stack) Pop() int {
	n := len(*s)
	e := (*s)[n-1]
	*s = (*s)[:n-1]
	return e
}
