//
// Created by champ on 2022/8/7.
//

class Solution {
public:
    vector<int> findOrder(int numCourses, vector<vector<int>>& prerequisites) {
        vector<int> res;
        if (prerequisites.size() == 0) {
            for (int i = 0; i < numCourses; i++) {
                res.push_back(i);
            }
            return res;
        }

        if (numCourses <= 1) return res;

        vector<int> indegree(numCourses, 0);
        unordered_map<int, vector<int>> preCourses;

        for (auto prerequisite: prerequisites) {
            int preCourse = prerequisite[1];
            int curCourse = prerequisite[0];
            indegree[curCourse]++;
            preCourses[preCourse].push_back(curCourse);
        }


        stack<int> s;
        for (int i = 0; i < numCourses; i++) {
            if (indegree[i] == 0) s.push(i);
        }

        while (!s.empty()) {
            int course = s.top();
            s.pop();
            res.push_back(course);

            for (auto c : preCourses[course]) {
                indegree[c]--;
                if (indegree[c] == 0) s.push(c);
            }
        }

        for (int i = 0; i < numCourses; i++) {
            if (indegree[i] != 0) return vector<int>();
        }

        return res;
    }
};
