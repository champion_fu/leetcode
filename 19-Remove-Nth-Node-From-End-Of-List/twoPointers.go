package _9_Remove_Nth_Node_From_End_Of_List

/**
 * Definition for singly-linked list
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func removeNthFromEnd(head *ListNode, n int) *ListNode {
	// dummy node point to head
	dummyNode := &ListNode{Val: -1, Next: head}
	p1 := dummyNode
	p2 := dummyNode

	// Move p1 n+1 first
	for i := 0; i < n+1; i++ {
		p1 = p1.Next
	}

	// Move both p1 and p2, until p1 visit tail
	for p1 != nil {
		p1 = p1.Next
		p2 = p2.Next
	}

	p2.Next = p2.Next.Next
	return dummyNode.Next
}
