package _9_Remove_Nth_Node_From_End_Of_List

/**
 * Definition for singly-linked list
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func removeNthFromEnd(head *ListNode, n int) *ListNode {
	if head == nil {
		return nil
	}
	length := 1
	p := head
	for p.Next != nil {
		p = p.Next
		length++
	}
	if length < n {
		return nil
	} else if length == n {
		head = head.Next
		return head
	}

	p = head
	for i := 0; i < (length-n)-1; i++ {
		p = p.Next
	}
	p.Next = p.Next.Next
	return head
}
