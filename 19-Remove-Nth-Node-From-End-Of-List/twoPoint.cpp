//
// Created by champ on 2022/6/10.
//

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* removeNthFromEnd(ListNode* head, int n) {
        ListNode* dummyHead = new ListNode(0, head);
        dummyHead->next = head;

        ListNode* p1 = dummyHead;
        ListNode* p2 = dummyHead;

        for (int i = 0; i < n+1; i++) {
            p1 = p1->next;
        }

        while (p1) {
            p1 = p1->next;
            p2 = p2->next;
        }

        p2->next = p2->next->next;
        return dummyHead->next;
    }
};
