//
// Created by champ on 2022/7/23.
//

class Solution {
public:
    int hammingWeight(uint32_t n) {
        int count = 0;
        while (n > 0) {
            // cout << n << " " << (n % 2) << endl;
            count += (n % 2);
            n = n / 2;
        }
        return count;
    }
};
