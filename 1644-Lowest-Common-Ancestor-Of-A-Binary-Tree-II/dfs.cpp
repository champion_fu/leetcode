//
// Created by champ on 2022/5/21.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
    TreeNode* lca;
    unordered_map<int, bool> mp;
public:
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        mp[p->val] = true;
        mp[q->val] = true;
        find(root, 2);
        return lca;
    }
private:
    int find(TreeNode* root, int target) {
        if (!root) {
            return 0;
        }

        int leftFind = find(root->left, target);
        int rightFind = find(root->right, target);

        int ans = leftFind + rightFind;

        if (mp.count(root->val)) {
            ans++;
        }

        if (ans == target) {
            // all find
            if(!lca) {
                lca = root;
            }
        }

        return ans;
    }
};
