class Solution {
public:
    bool isPalindrome(int x) {
        if (x < 0 || (x % 10 == 0 && x != 0)) {
            return false;
        } else {
            int rNum = 0;
            while (x > rNum) {
                rNum = rNum*10 + x % 10;
                x /= 10;
            }
            return x == rNum || x == rNum / 10;
        }
    }
}
