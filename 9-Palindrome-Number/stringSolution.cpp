class Solution {
public:
    bool isPalindrome(int x) {
        if (x < 0) {
            return false;
        }
        string s = to_string(x);
        // cout << "s " << s << " length " << s.length() << endl;
        for (int l = 0, r = s.length() - 1; l <= r; l++, r--) {
            // cout << "l " << l << " r " << r << endl;
            if (s[l] != s[r]) return false;
        }
        return true;
    }
}
