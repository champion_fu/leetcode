package _58_Read_N_Characters_Given_Read4_II_Call_Multiple_Times

/**
 * The read4 API is already defined for you.
 *
 *     read4 := func(buf4 []byte) int
 *
 * // Below is an example of how the read4 API can be called.
 * file := File("abcdefghijk") // File is "abcdefghijk", initially file pointer (fp) points to 'a'
 * buf4 := make([]byte, 4) // Create buffer with enough space to store characters
 * read4(buf4) // read4 returns 4. Now buf = ['a','b','c','d'], fp points to 'e'
 * read4(buf4) // read4 returns 4. Now buf = ['e','f','g','h'], fp points to 'i'
 * read4(buf4) // read4 returns 3. Now buf = ['i','j','k',...], fp points to end of file
 */

var solution = func(read4 func([]byte) int) func([]byte, int) int {
	tmp := make([]byte, 4)
	pos := 0  // current index in buf
	size := 0 // length of buf from read4
	// implement read below.
	return func(buf []byte, n int) int {
		cnt := 0
		for cnt < n {
			if pos == size {
				// read buf
				pos = 0 // start from 0
				size = read4(tmp)
				if size == 0 {
					// file is end.
					return cnt
				}
			}
			buf[cnt] = tmp[pos]
			cnt++
			pos++
		}
		return cnt
	}
}
