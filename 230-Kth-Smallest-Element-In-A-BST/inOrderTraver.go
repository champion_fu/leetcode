package _30_Kth_Smallest_Element_In_A_BST

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func kthSmallest(root *TreeNode, k int) int {
	sorted := make([]int, 0)
	s := stack{}
	curr := root

	// in order traverse, l, c, r
	for (curr != nil || !s.IsEmpty()) && len(sorted) < k {
		for curr != nil {
			s.Push(curr)
			curr = curr.Left
		}
		curr = s.Pop()
		sorted = append(sorted, curr.Val)
		curr = curr.Right
	}
	return sorted[k-1]
}

type stack []*TreeNode

func (s stack) Len() int {
	return len(s)
}
func (s stack) IsEmpty() bool {
	return len(s) == 0
}
func (s *stack) Push(node *TreeNode) {
	*s = append(*s, node)
}
func (s *stack) Top() *TreeNode {
	if len(*s) == 0 {
		return nil
	}
	return (*s)[len(*s)-1]
}
func (s *stack) Pop() *TreeNode {
	if len(*s) == 0 {
		return nil
	}
	n := len(*s)
	e := (*s)[n-1]
	*s = (*s)[:n-1]
	return e
}
