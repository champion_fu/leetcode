//
// Created by champ on 2022/4/18.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
    vector<int> order;
    int target;
public:
    int kthSmallest(TreeNode* root, int k) {
        // priority_queue<int, vector<int>, greater<vector<int>> pq;
        // l c r -> inorder sequeucne
        target = k;
        recursive(root);
        return order[k-1];
    }
private:
    void recursive(TreeNode* n) {
        if (n->left) recursive(n->left);
        order.push_back(n->val);
        if (order.size() >= target)  {
            return;
        }
        if (n->right) recursive(n->right);
    }
};