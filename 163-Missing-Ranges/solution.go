package _63_Missing_Ranges

import (
	"fmt"
	"strconv"
)

// nums is sorted unique integer
func findMissingRanges(nums []int, lower int, upper int) []string {
	ret := make([]string, 0)
	prev := lower - 1
	nums = append(nums, upper+1)
	for i := 0; i < len(nums); i++ {
		cur := nums[i]
		if prev+1 <= cur-1 {
			ret = append(ret, formatRange(prev+1, cur-1))
		}
		prev = cur
	}

	return ret
}

func formatRange(lower, upper int) string {
	if lower == upper {
		return strconv.Itoa(lower)
	}
	return fmt.Sprintf("%v->%v", lower, upper)
}
