//
// Created by champ on 2022/4/16.
//

class Solution {
public:
    vector<string> findMissingRanges(vector<int>& nums, int lower, int upper) {
        vector<string> ret;
        nums.push_back(upper+1);

        lower--;
        for (auto n : nums) {
            if (lower+1 <= n-1) {
                ret.push_back(format(lower+1, n-1));
            }
            lower = n;
        }
        return ret;
    }
private:
    string format(int lower, int upper) {
        if (lower == upper) {
            return to_string(lower);
        }
        return to_string(lower) + "->" + to_string(upper);
    }
};
