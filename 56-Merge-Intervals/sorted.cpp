class Solution {
public:
    vector<vector<int>> merge(vector<vector<int>>& intervals) {
        unordered_map<int, int> m;
        vector<vector<int>> results;
        vector<int> l;

        for (auto interval : intervals) {
            m[interval[0]] = max(interval[1], m[interval[0]]);
            l.push_back(interval[0]);
        }

        sort(l.begin(), l.end());
        int maxEnd = -1;
        vector<int> tmp;

        for (auto i : l) {
            int end = m[i];

            if (maxEnd == -1) {
                // first time
                tmp.push_back(i);
                maxEnd = end;
                continue;
            }

            if (i <= maxEnd) {
                maxEnd = max(end, maxEnd);
                continue;
            }

            tmp.push_back(maxEnd);
            results.push_back(tmp);

            tmp = vector<int> {};
            tmp.push_back(i);
            maxEnd = end;
        }
        tmp.push_back(maxEnd);
        results.push_back(tmp);

        return results;
    }
};