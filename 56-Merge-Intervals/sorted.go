package _6_Merge_Intervals

import (
	"fmt"
	"sort"
)

func merge(intervals [][]int) [][]int {
	startToEndMap := make(map[int]int)
	ret := make([][]int, 0)
	sorted := make([]int, 0)

	for _, interval := range intervals {
		sorted = append(sorted, interval[0])

		v, m := startToEndMap[interval[0]]
		if m && v > interval[1] {
			continue
		}
		startToEndMap[interval[0]] = interval[1]
	}
	sort.Slice(
		sorted,
		func(i, j int) bool {
			return sorted[i] < sorted[j]
		},
	)

	fmt.Printf("startToEndMap %+v\n", startToEndMap)
	maxInterval := -1
	tmp = make([]int, 0)
	for _, v := range sorted {
		fmt.Printf("v %+v\n", v)
		if maxInterval == -1 {
			// first time
			tmp = append(tmp, v)
			maxInterval = startToEndMap[v]
			continue
		}

		end := startToEndMap[v]
		if v <= maxInterval {
			// we are in interval
			if end > maxInterval {
				// expand the interval
				maxInterval = end
			}
		} else {
			// new one
			tmp = append(tmp, maxInterval)
			ret = append(ret, tmp)

			// initialize new one
			maxInterval = end
			tmp = make([]int, 0)
			tmp = append(tmp, v)
		}
	}
	// cleanup
	tmp = append(tmp, maxInterval)
	ret = append(ret, tmp)
	return ret
}
