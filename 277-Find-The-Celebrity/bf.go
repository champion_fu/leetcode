package _77_Find_The_Celebrity

/**
 * The knows API is already defined for you.
 *     knows := func(a int, b int) bool
 */
func isCelebrity(i, n int, knows func(a int, b int) bool) bool {
	for j := 0; j < n; j++ {
		if i == j {
			continue
		}
		if knows(i, j) || !knows(j, i) {
			return false
		}
	}
	return true
}
func solution(knows func(a int, b int) bool) func(n int) int {
	return func(n int) int {
		for i := 0; i < n; i++ {
			if isCelebrity(i, n, knows) {
				return i
			}
		}
		return -1
	}
}
