package _77_Find_The_Celebrity

import "fmt"

/**
 * The knows API is already defined for you.
 *     knows := func(a int, b int) bool
 */
func solution(knows func(a int, b int) bool) func(n int) int {
	return func(n int) int {
		isCandidate := make([]int, n)
		for i := 0; i < n; i++ {
			for j := 0; j < n; j++ {
				k := knows(j, i)
				if !k {
					break
				} else {
					isCandidate[i] += 1
				}
			}
		}
		fmt.Printf("iscandidate %+v\n", isCandidate)
		celebrity := -1
		for i, v := range isCandidate {
			if v != n {
				continue
			}
			celebrity = i
			for j := 0; j < n; j++ {
				if i == j {
					continue
				}
				if knows(i, j) {
					celebrity = -1
					break
				}
			}
		}
		return celebrity
	}
}
