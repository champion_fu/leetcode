package _53_Find_Minimum_In_Rotated_Sorted_Array

import "fmt"

func findMin(nums []int) int {
	n := len(nums)
	if n == 0 {
		return -1
	}
	l := 0
	r := n - 1
	m := l + (r-l)/2
	for l < r {
		m = l + (r-l)/2
		fmt.Printf("nums %+v m %v, l %v, r %v\n", nums, m, l, r)
		if nums[m] < nums[r] {
			// go left to find the minimum
			r = m
		} else {
			l = m + 1
		}
	}
	return nums[l]
}
