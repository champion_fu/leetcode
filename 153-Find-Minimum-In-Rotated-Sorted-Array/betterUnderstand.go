package _53_Find_Minimum_In_Rotated_Sorted_Array

func findMin(nums []int) int {
	n := len(nums)
	if n < 2 {
		return nums[n-1]
	}

	l := 0
	r := n - 1
	for l < r {
		if nums[l] < nums[r] {
			// array didn't rotate
			return nums[l]
		}

		m := l + (r-l)/2
		// l > r
		if nums[m] >= nums[l] {
			// l > r, m >= l, go right
			/*
			   |   *
			   | *
			   |     (*)
			*/
			l = m + 1
		} else {
			r = m
		}
	}
	return nums[l]
}
