class Solution {
public:
    vector<vector<int>> memo;
    bool predictTheWinner(vector<int>& nums) {
        memo.resize(nums.size(), vector<int>(nums.size(), -1));
        return maxDiff(nums, 0, nums.size()-1) >= 0;
    }

    int maxDiff(vector<int>& nums, int left, int right) {
        if (memo[left][right] != -1) {
            return memo[left][right];
        }
        if (left == right) {
            return nums[left];
        }
        int scoreLeft = nums[left] - maxDiff(nums, left+1, right);
        int scoreRight = nums[right] - maxDiff(nums, left, right-1);
        memo[left][right] = max(scoreLeft, scoreRight);
        return memo[left][right];
    }
}