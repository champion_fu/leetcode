package _40_Search_A_2D_Matrix_II

var mtx [][]int
var t int

func searchMatrix(matrix [][]int, target int) bool {
	m := len(matrix)
	n := len(matrix[0])
	mtx = matrix
	t = target
	return recursiveSearch(0, 0, n-1, m-1)
}

func recursiveSearch(left, up, right, down int) bool {
	// this sub-matrix has no height or no width.
	if left > right || up > down {
		return false
	}

	// target is already larger than the largest element or smaller
	// than the smallest elemnt in this submatrix.
	if t > mtx[down][right] {
		return false
	}
	if t < mtx[up][left] {
		return false
	}

	rowMid := left + (right-left)/2

	row := up
	// locate row such that mtx[row-1][rowMid] < target < mtx[row][mid]
	for row <= down && mtx[row][rowMid] <= t {
		if mtx[row][rowMid] == t {
			return true
		}
		row += 1
	}
	return recursiveSearch(left, row, rowMid-1, down) || recursiveSearch(rowMid+1, up, right, row-1)
}
