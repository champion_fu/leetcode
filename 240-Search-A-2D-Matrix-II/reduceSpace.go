package _40_Search_A_2D_Matrix_II

func searchMatrix(matrix [][]int, target int) bool {
	m := len(matrix)
	n := len(matrix[0])

	c := n - 1
	r := 0
	for c >= 0 && r <= m-1 {
		value := matrix[r][c]
		if value == target {
			return true
		} else if value > target {
			c -= 1
			continue
		} else if value < target {
			r += 1
			continue
		}
	}
	return false
}
