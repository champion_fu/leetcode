//
// Created by champ on 2022/5/2.
//

class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
        int m = matrix.size();
        int n = matrix[0].size();

        int y = n-1;
        int x = 0;

        while (y >= 0 && x <= m-1) {
            int v = matrix[x][y];
            if (v == target) {
                return true;
            }

            if (v > target) {
                y--;
            } else {
                x++;
            }
        }
        return false;
    }
};
