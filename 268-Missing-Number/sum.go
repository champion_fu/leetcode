package _68_Missing_Number

func missingNumber(nums []int) int {
	n := len(nums)
	sum := (1 + n) * n / 2
	for _, v := range nums {
		sum -= v
	}
	return sum
}
