//
// Created by champ on 2022/7/26.
//

class Solution {
    vector<int> origin;
    vector<int> tmp;
public:
    Solution(vector<int>& nums) {
        origin = nums;
        tmp = nums;
    }

    vector<int> reset() {
        return origin;
    }

    vector<int> shuffle() {
        int i = rand() % tmp.size();
        swap(tmp[0], tmp[i]);
        return tmp;
    }
};

