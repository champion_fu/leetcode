//
// Created by champ on 2022/10/12.
//

class Solution {
    int ans = INT_MIN;
    int m;
    int n;
    vector<vector<int>> mat;
private:
    void backtrack(int cols, int idx, vector<int>& vis) {
        if (cols == 0 || idx == n) {
            int tmp = 0;
            for (int i = 0; i < m; i++) {
                // row
                bool valid = true;
                for (int j = 0; j < n; j++) {
                    // col
                    // if cell is 1 and not visited then we cannot take this row
                    if (mat[i][j] == 1 && vis[j] == 0) {
                        valid = false;
                        break;
                    }
                }
                if (valid) tmp++;
            }

            ans = max(ans, tmp);
            return;
        }

        // picking idx th column and marking column as visited
        vis[idx] = 1;
        backtrack(cols-1, idx+1, vis);
        vis[idx] = 0;

        // not picking
        backtrack(cols, idx+1, vis);
    }

public:
    int maximumRows(vector<vector<int>>& matrix, int numSelect) {
        m = matrix.size();
        n = matrix[0].size();
        mat = matrix;
        vector<int> vis(n);
        backtrack(numSelect, 0, vis);
        return ans;
    }
};
