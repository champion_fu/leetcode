//
// Created by champ on 2022/4/19.
//

class Solution {
public:
    long long maxPoints(vector<vector<int>>& points) {
        int m = points.size();
        int n = points[0].size();

        vector<long long> pre(n, -1);
        for (int i = 0; i < n; ++i) pre[i] = points[0][i];

        for (int i = 0; i < m-1; i++) { // from i = 0 ~ m-2
            vector<long long> lft(n, 0), rgt(n, 0), cur(n, 0);
            lft[0] = pre[0];
            rgt[n-1] = pre[n-1];

            for (int j = 1; j < n; j++) {
                lft[j] = max(lft[j-1] - 1, pre[j]);
            }

            for (int j = n-2; j >= 0; j--) {
                rgt[j] = max(rgt[j+1] - 1, pre[j]);
            }

            for (int j = 0; j < n; j++) {
                cur[j] = max(rgt[j], lft[j]) + points[i+1][j];
            }
            // remember to store
            pre = cur;
        }

        long long ret = 0;
        for (auto n: pre) ret = max(ret, n);
        return ret;
    }
};
