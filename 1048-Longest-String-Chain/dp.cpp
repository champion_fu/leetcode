//
// Created by champ on 2022/4/23.
//

class Solution {
public:
    // new first
    static bool compare(string s1, string s2) {
        return s1.length() < s2.length();
    }

    int longestStrChain(vector<string>& words) {
        sort(words.begin(), words.end(), compare);
        unordered_map<string, int> dp;

        int ret = 0;
        for (auto w : words) {
            // cout << w << endl;
            for (int i = 0; i < w.size(); i++) {
                string pre = w.substr(0, i) + w.substr(i+1); // string without i
                int preCount;
                if (dp.find(pre) == dp.end()) {
                    // not find pre
                    preCount = 1;
                } else {
                    preCount = dp[pre] + 1;
                }
                dp[w] = max(dp[w], preCount);
                ret = max(ret, dp[w]);
            }
        }

        return ret;
    }
};
