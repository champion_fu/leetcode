class Solution {
public:
    // from last digit to find [00, 25, 50, 75]
    int minimumOperations(string num) {
        bool fiveFound = false;
        bool zeroFound = false;

        int length = num.size();

        // from the last one to zero index
        for (int i = length - 1; i >= 0; i--) {
            if (zeroFound && num[i] == '0') return length - 2 - i;
            if (zeroFound && num[i] == '5') return length - 2 - i;
            if (fiveFound && num[i] == '2') return length - 2 - i;
            if (fiveFound && num[i] == '7') return length - 2 - i;
            if (num[i] == '0') zeroFound = true;
            if (num[i] == '5') fiveFound = true;
        }
        if (zeroFound) {
            return length - 1;
        }
        return length;
    }
}
