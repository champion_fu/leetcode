package _00_Search_In_A_Binary_Search_Tree

import "fmt"

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func searchBST(root *TreeNode, val int) *TreeNode {
	if root == nil {
		return nil
	}
	fmt.Printf("root %+v, val %+v\n", root.Val, val)
	if root.Val == val {
		return root
	}
	var left, right *TreeNode
	if root.Left != nil {
		left = searchBST(root.Left, val)
	}
	if root.Right != nil {
		right = searchBST(root.Right, val)
	}
	if left != nil {
		return left
	}
	if right != nil {
		return right
	}
	return nil
}
