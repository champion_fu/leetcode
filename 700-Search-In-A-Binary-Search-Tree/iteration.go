package _00_Search_In_A_Binary_Search_Tree

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func searchBST(root *TreeNode, val int) *TreeNode {
	if root == nil {
		return nil
	}

	q := queue{}
	q.Push(root)

	for !q.IsEmpty() {
		n := q.Pop()
		if n.Val == val {
			return n
		}
		if n.Val > val && n.Left != nil {
			q.Push(n.Left)
		}
		if n.Val < val && n.Right != nil {
			q.Push(n.Right)
		}
	}
	return nil
}

type queue []*TreeNode

func (q *queue) Size() int {
	return len(*q)
}

func (q *queue) IsEmpty() bool {
	return q.Size() == 0
}

func (q *queue) Push(n *TreeNode) {
	*q = append(*q, n)
}

func (q *queue) Pop() *TreeNode {
	if q.IsEmpty() {
		return nil
	} else {
		e := (*q)[0]
		*q = (*q)[1:]
		return e
	}
}
