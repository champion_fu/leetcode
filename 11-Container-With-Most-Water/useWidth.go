package _1_Container_With_Most_Water

// brute force O(n^2), O(1)
// min(height[i], height[j]) * (j - i)
// there are len(height) - 1 kinds of width
// 8 width (i = 0, j = 8)
// 7 width (i = 0, j = 7), (i = 1, j = 8)
func maxArea(height []int) int {
	width := len(height) - 1
	ret := 0
	for j := width; j >= 1; j-- { // at least 1 width
		for i := 0; i < len(height)-j; i++ {
			ret = max(ret, min(height[i], height[i+j])*j)
		}
	}
	return ret
}

func min(a, b int) int {
	if a > b {
		return b
	}
	return a
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
