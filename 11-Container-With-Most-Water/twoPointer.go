package _1_Container_With_Most_Water

func maxArea(height []int) int {
	ret, l, r := 0, 0, len(height)-1

	for l < r {
		ret = max(ret, min(height[l], height[r])*(r-l))
		if height[l] < height[r] {
			l++
		} else {
			r--
		}
	}
	return ret
}

func min(a, b int) int {
	if a > b {
		return b
	}
	return a
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
