//
// Created by champ on 2022/6/22.
//

class Solution {
    unordered_map<int, int> counter;
public:
    vector<vector<int>> permuteUnique(vector<int>& nums) {
        vector<vector<int>> results;
        vector<int> comb;

        for (auto num: nums) counter[num] = counter[num] + 1;

        backtrack(comb, nums.size(), counter, results);
        return results;
    }
private:
    void backtrack(vector<int>& comb, int size, unordered_map<int, int>& counter, vector<vector<int>>& results) {
        if (comb.size() == size) {
            results.push_back(comb);
            return;
        }

        for (pair<int, int> m : counter) {
            int num = m.first;
            int count = m.second;

            if (count == 0) continue;

            comb.push_back(num);
            counter[num] = count-1;

            backtrack(comb, size, counter, results);

            comb.pop_back();
            counter[num] = count;
        }
    }
};
