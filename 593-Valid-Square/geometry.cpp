//
// Created by champ on 2022/9/13.
//

class Solution {
public:
    bool validSquare(vector<int>& p1, vector<int>& p2, vector<int>& p3, vector<int>& p4) {
        unordered_set<int> s({
                                     distance(p1, p2), distance(p1, p3), distance(p1, p4),
                                     distance(p2, p3), distance(p2, p4), distance(p3, p4),
                             });
        return !s.count(0) && s.size() == 2;
    }
private:
    int distance(vector<int>& v1, vector<int>& v2) {
        return (v1[0] - v2[0]) * (v1[0] - v2[0]) + (v1[1] - v2[1]) * (v1[1] - v2[1]);
    }
};
