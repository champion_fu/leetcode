//
// Created by champ on 2022/9/23.
//

class Solution {
public:
    int mostFrequentEven(vector<int>& nums) {
        unordered_map<int, int> m;
        int ans = -1, freq = 0;
        for (const int& num : nums) {
            if (num % 2 == 0) {
                m[num]++;
                if (freq < m[num]) {
                    ans = num;
                    freq = m[num];
                } else if (freq == m[num]) {
                    ans = min(ans, num);
                }
            }
        }
        return ans;
    }
};
