//
// Created by champ on 2022/4/11.
//

class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int size = prices.size();
        int profit = 0;
        for (int i = size-1; i > 0; i--) {
            profit += prices[i] - prices[i-1] > 0 ? prices[i] - prices[i-1] : 0;
        }
        return profit;
    }
};
