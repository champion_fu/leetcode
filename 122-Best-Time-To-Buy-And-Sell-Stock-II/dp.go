package _22_Best_Time_To_Buy_And_Sell_Stock_II

func maxProfit(prices []int) int {
	ret := 0

	for i := 1; i < len(prices); i++ {
		if prices[i]-prices[i-1] > 0 {
			ret += prices[i] - prices[i-1]
		}
	}
	return ret
}
