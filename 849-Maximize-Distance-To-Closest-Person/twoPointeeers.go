package _49_Maximize_Distance_To_Closest_Person

func maxDistToClosest(seats []int) int {
	n := len(seats)
	prev, future := -1, 0
	var ret int

	for i := 0; i < n; i++ {
		if seats[i] == 1 {
			prev = i
			continue
		}

		for (future < n && seats[future] == 0) || future < i {
			future++
		}

		var left, right int
		if prev == -1 {
			left = n
		} else {
			left = i - prev
		}

		if future == n {
			right = n
		} else {
			right = future - i
		}
		ret = max(ret, min(left, right))
	}
	return ret

}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}

func min(i, j int) int {
	if i > j {
		return j
	}
	return i
}
