package _49_Maximize_Distance_To_Closest_Person

import (
	"fmt"
	"sort"
)

func maxDistToClosest(seats []int) int {
	n := len(seats)

	sorted := []int{-1}
	for i, v := range seats {
		if v == 1 {
			sorted = append(sorted, i)
		}
	}
	sort.Slice(
		sorted,
		func(i, j int) bool {
			return sorted[i] < sorted[j]
		},
	)
	sorted = append(sorted, n)

	fmt.Printf("sorted %+v\n", sorted)

	var ret int
	for i := 1; i < len(sorted); i++ {
		if i == len(sorted)-1 && sorted[i]-sorted[i-1]-1 > ret {
			return sorted[i] - sorted[i-1] - 1
		}
		if i == 1 {
			ret = max(ret, sorted[i])
			continue
		}
		ret = max(ret, (sorted[i]-sorted[i-1])/2)
	}
	return ret
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}
