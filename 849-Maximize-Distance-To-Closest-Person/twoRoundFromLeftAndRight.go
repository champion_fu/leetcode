package _49_Maximize_Distance_To_Closest_Person

func maxDistToClosest(seats []int) int {
	n := len(seats)
	left := make([]int, n)
	right := make([]int, n)

	for i := 0; i < n; i++ {
		if seats[i] == 1 {
			left[i] = 0
		} else if i > 0 {
			left[i] = left[i-1] + 1
		} else if i == 0 {
			left[i] = 1
		}
	}

	for i := n - 1; i >= 0; i-- {
		if seats[i] == 1 {
			right[i] = 0
		} else if i < n-1 {
			right[i] = right[i+1] + 1
		} else if i == n-1 {
			right[i] = 1
		}
	}
	var ret int
	for i := 0; i < n; i++ {
		if i == 0 {
			ret = max(ret, right[i])
			continue
		}
		if i == n-1 {
			ret = max(ret, left[i])
			continue
		}
		ret = max(min(left[i], right[i]), ret)
	}
	return ret
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}

func min(i, j int) int {
	if i < j {
		return i
	}
	return j
}
