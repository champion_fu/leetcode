//
// Created by champ on 2022/9/21.
//

class Solution {
public:
    int smallestEvenMultiple(int n) {
        // if n is even return n << 0
        // if n is odd return n << 1;
        return n << (n & 1);

        // if (n % 2 == 0) return n;
        // return n * 2;
    }
};
