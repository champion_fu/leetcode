//
// Created by champ on 2022/4/12.
//

class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs) {
        unordered_map<string, vector<string>> mp;

        for (auto s : strs) {
            mp[countStr(s)].push_back(s);
        }
        vector<vector<string>> ret;
        for (auto p : mp) {
            ret.push_back(p.second);
        }
        return ret;
    }
private:
    string countStr(string str) {
        int count[26] = {0};
        for (auto s : str) count[s - 'a']++;

        string p;
        for (int i = 0; i < 26; i++) {
            p += string(count[i], i + 'a');
        }
        return p;
    }
};
