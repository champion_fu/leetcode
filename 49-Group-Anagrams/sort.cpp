//
// Created by champ on 2022/4/12.
//

class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs) {
        unordered_map<string, int> m;
        vector<vector<string>> results;

        for (string str : strs) {
            string sortStr = str;
            sort(sortStr.begin(), sortStr.end());
            cout << "sortStr " << sortStr << " str " << str << endl;
            if (m.count(sortStr) > 0) {
                results[m[sortStr]].push_back(str);
            } else {
                results.push_back(vector<string> {str});
                m[sortStr] = results.size() - 1;
            }
        }
        return results;
    }
};
