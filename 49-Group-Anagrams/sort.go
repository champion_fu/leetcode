package _9_Group_Anagrams

import "sort"

func groupAnagrams(strs []string) [][]string {
	anagramsMap := make(map[string][]string, 0)

	for _, v := range strs {
		sorted := SortString(v)
		if group, m := anagramsMap[sorted]; m {
			anagramsMap[sorted] = append(group, v)
		} else {
			anagramsMap[sorted] = []string{v}
		}
	}

	ret := make([][]string, 0)
	for _, value := range anagramsMap {
		ret = append(ret, value)
	}

	return ret
}

type sortByte []byte

func (s sortByte) Less(i, j int) bool {
	return s[i] < s[j]
}
func (s sortByte) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s sortByte) Len() int {
	return len(s)
}

func SortString(str string) string {
	chars := []byte(str)
	sort.Sort(sortByte(chars))
	return string(chars)
}
