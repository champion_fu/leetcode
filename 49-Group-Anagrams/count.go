package _9_Group_Anagrams

func groupAnagrams(strs []string) [][]string {
	cache := make(map[[26]byte]int)
	result := make([][]string, 0)

	for i := range strs {
		list := [26]byte{}
		for j := range strs[i] {
			list[strs[i][j]-'a']++
		}

		if idx, ok := cache[list]; ok {
			result[idx] = append(result[idx], strs[i])
		} else {
			result = append(result, []string{strs[i]})
			cache[list] = len(result) - 1
		}
	}
	return result
}
