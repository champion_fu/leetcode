package _0_Pow

import "fmt"

func myPow(x float64, n int) float64 {
	if n < 0 {
		x = 1 / x
		n = -n
	}
	ret := 1.0
	for i := n; i != 0; i /= 2 {
		fmt.Printf("i %v, ret %v, x %v\n", i, ret, x)
		if i&1 > 0 {
			ret *= x
		}
		x *= x // always cumulate x = x * x
	}
	return ret
}
