package _0_Pow

func myPow(x float64, n int) float64 {
	if n == 0 {
		return 1.0
	}
	base := x
	ret := x
	var times int
	if n < 0 {
		times = -n
	} else {
		times = n
	}
	for i := 0; i < times-1; i++ {
		ret = ret * base
	}
	if n <= -1 {
		return 1 / ret
	}
	return ret
}
