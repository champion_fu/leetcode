class Solution {
public:
    bool winnerOfGame(string colors) {
        // from left to right -> count continue A and B
        int a = 0, b = 0;
        int count = 1;
        int length = colors.size();
        for (int i = 1; i < length; i++) {
            if (colors[i] == colors[i - 1]) {
                count++;
            } else {
                if (colors[i-1] == 'A') {
                    a += std::max(0, count-2);
                } else {
                    b += std::max(0, count-2);
                }
                count = 1;
            }
        }

        if (colors[length-1] == 'A') {
            a += std::max(0, count-2);
        } else {
            b += std::max(0, count-2);
        }
        return a > b;
    }
}