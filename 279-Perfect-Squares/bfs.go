package _79_Perfect_Squares

func numSquares(n int) int {
	q := queue{}
	q.Push(n)

	squares := make([]int, 0)
	for i := 1; i*i <= n; i++ {
		squares = append(squares, i*i)
	}

	level := 1
	for !q.IsEmpty() {
		count := q.Len()
		for i := 0; i < count; i++ {
			n := q.Pop()
			for _, v := range squares {
				if n == v {
					return level
				} else if n < v {
					break
				} else {
					q.Push(n - v)
				}
			}
		}
		level++
	}
	return -1
}

type queue []int

func (q queue) Len() int {
	return len(q)
}
func (q queue) IsEmpty() bool {
	return len(q) == 0
}
func (q *queue) Push(i int) {
	*q = append(*q, i)
}
func (q *queue) Pop() int {
	e := (*q)[0]
	*q = (*q)[1:]
	return e
}
