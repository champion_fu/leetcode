//
// Created by champ on 2022/9/29.
//

class Solution {
public:
    int numSquares(int n) {
        queue<int> q;
        q.push(n);

        vector<int> squares;
        for (int i = 1; i*i <= n; i++) {
            squares.push_back(i*i);
        }

        int level = 1;

        while (!q.empty()) {
            int count = q.size();
            for (int i = 0; i < count; i++) {
                int n = q.front();
                q.pop();

                for (int& square : squares) {
                    if (n == square) {
                        return level;
                    } else if (n < square) {
                        break;
                    } else {
                        q.push(n-square);
                    }
                }
            }
            level++;
        }
        return level;
    }
};
