package _79_Perfect_Squares

import (
	"fmt"
	"math"
)

func numSquares(n int) int {
	dp := make([]int, n+1)

	squareNum := make([]int, 0)
	for i := 1; i*i <= n; i++ {
		squareNum = append(squareNum, i*i)
	}

	dp[0] = 0
	fmt.Printf("squareNum %+v\n", squareNum)
	for i := 1; i < n+1; i++ {
		dp[i] = math.MaxInt32

		for _, v := range squareNum {
			if i-v >= 0 {
				dp[i] = min(dp[i-v]+1, dp[i])
			}
		}

	}
	return dp[n]
}

func min(i, j int) int {
	if i > j {
		return j
	}
	return i
}
