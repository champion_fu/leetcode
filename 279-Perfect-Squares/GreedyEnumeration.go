package _79_Perfect_Squares

var squares []int

func numSquares(n int) int {
	squares = make([]int, 0)
	for i := 1; i*i <= n; i++ {
		squares = append(squares, i*i)
	}

	for i := 1; i <= n; i++ {
		if isDividedBy(n, i) {
			return i
		}
	}
	return -1
}

func isDividedBy(n, count int) bool {
	if count < 1 {
		return false
	}
	if count == 1 && find(n) {
		return true
	}

	for _, v := range squares {
		if isDividedBy(n-v, count-1) {
			return true
		}
	}
	return false
}

func find(n int) bool {
	for _, v := range squares {
		if n == v {
			return true
		}
	}
	return false
}
