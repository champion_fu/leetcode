//
// Created by champ on 2022/7/19.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    bool isBalanced(TreeNode* root) {
        if (root == NULL) return true;
        int height;
        return dfs(root, height);
    }
private:
    bool dfs(TreeNode* node, int& height) {
        if (node == NULL) {
            height = -1;
            return true;
        }

        int leftDepth, rightDepth;
        if (dfs(node->left, leftDepth) && dfs(node->right, rightDepth)) {
            if(abs(leftDepth-rightDepth)<2){
                height = max(leftDepth, rightDepth) + 1;
                return true;
            }
        }
        return false;
    }
};
