//
// Created by champ on 2022/10/23.
//

class HitCounter {
    queue<int> q; // store timestamp
public:
    HitCounter() {

    }

    void hit(int timestamp) {
        // O(1)
        q.push(timestamp);
    }

    int getHits(int timestamp) {
        // O(query)
        while (!q.empty()) {
            int time = q.front();
            if (timestamp >= time + 300) {
                q.pop();
            } else {
                break;
            }
        }
        return q.size();
    }
};

/**
 * Your HitCounter object will be instantiated and called as such:
 * HitCounter* obj = new HitCounter();
 * obj->hit(timestamp);
 * int param_2 = obj->getHits(timestamp);
 */
