//
// Created by champ on 2022/10/23.
//

class HitCounter {
    vector<int> time;
    vector<int> hits;
public:
    HitCounter() {
        time = vector<int>(300, 0);
        hits = vector<int>(300, 0);
    }

    void hit(int timestamp) {
        // O(1)
        int index = timestamp % 300;
        if (time[index] != timestamp) {
            // has been 300 seconds
            time[index] = timestamp;
            hits[index] = 1;
        } else {
            hits[index]++;
        }
    }

    int getHits(int timestamp) {
        // O(300) constant
        int total = 0;
        for (int i = 0; i < 300; i++) {
            if (timestamp - time[i] < 300) {
                // include
                total += hits[i];
            }
        }
        return total;
    }
};

/**
 * Your HitCounter object will be instantiated and called as such:
 * HitCounter* obj = new HitCounter();
 * obj->hit(timestamp);
 * int param_2 = obj->getHits(timestamp);
 */
