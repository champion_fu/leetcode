package _50_Intersection_Of_Two_Array_II

func intersect(nums1 []int, nums2 []int) []int {
	nums1Map := make(map[int]int)

	for _, num := range nums1 {
		v := nums1Map[num]
		nums1Map[num] = v + 1
	}

	var ret []int
	for _, num := range nums2 {
		v := nums1Map[num]
		if v == 0 {
			continue
		}
		nums1Map[num] = v - 1
		if nums1Map[num] == 0 {
			delete(nums1Map, num)
		}
		ret = append(ret, num)
	}
	return ret
}
