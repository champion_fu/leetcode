//
// Created by champ on 2022/6/3.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
    unordered_map<int, bool> mp;
    TreeNode* lca;
public:
    TreeNode* lowestCommonAncestor(TreeNode* root, vector<TreeNode*> &nodes) {
        for (auto node : nodes) {
            mp[node->val] = true;
        }
        dfs(root, nodes.size());
        return lca;
    }
    int dfs(TreeNode* root, int target) {
        if (!root) {
            return 0;
        }

        int l = dfs(root->left, target);
        int r = dfs(root->right, target);

        int num = l+r;
        if (mp.count(root->val) > 0) {
            num++;
        }

        if (num == target) {
            if (!lca)  {
                lca = root;
            }
        }

        return num;
    }
};
