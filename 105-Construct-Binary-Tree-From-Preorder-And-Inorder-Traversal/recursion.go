package _05_Construct_Binary_Tree_From_Preorder_And_Inorder_Traversal

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func buildTree(preorder []int, inorder []int) *TreeNode {
	inorderMap := make(map[int]int)
	for i, v := range inorder {
		inorderMap[v] = i
	}
	var recur func(int, int) *TreeNode

	recur = func(leftIndex int, rightIndex int) *TreeNode {
		if leftIndex >= rightIndex {
			return nil
		}
		nodeValue := preorder[0]
		preorder = preorder[1:]

		n := &TreeNode{Val: nodeValue, Left: nil, Right: nil}

		if midIndex, ok := inorderMap[nodeValue]; !ok {
			return nil
		} else {
			n.Left = recur(leftIndex, midIndex)
			n.Right = recur(midIndex+1, rightIndex)
		}
		return n
	}
	return recur(0, len(inorder))
}
