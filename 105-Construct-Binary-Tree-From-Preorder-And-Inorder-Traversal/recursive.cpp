//
// Created by champ on 2022/4/17.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
    unordered_map<int, int> m;
private:
    TreeNode* build(int left, int right, vector<int>& preorder) {
        // not include right
        if (left >= right) return NULL;
        if (preorder.size() == 0) return NULL;
        TreeNode* node = new TreeNode(preorder[0]);
        preorder.erase(preorder.begin());

        if (m.find(node->val) == m.end()) {
            return NULL;
        } else {
            int mIndex = m[node->val];
            node->left = build(left, mIndex, preorder);
            node->right = build(mIndex+1, right, preorder);
        }
        return node;
    }
public:
    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
        for (int i = 0; i < inorder.size(); i++) m[inorder[i]] = i;
        return build(0, preorder.size(), preorder);
    }
};