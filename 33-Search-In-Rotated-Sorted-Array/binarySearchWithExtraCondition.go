package _3_Search_In_Rotated_Sorted_Array

func search(nums []int, target int) int {
	n := len(nums)
	l := 0
	r := n - 1

	for l <= r {
		m := (l + r) / 2
		if nums[m] == target {
			return m
		}
		if nums[m] >= nums[l] { // bigger than is because, divide by 2 will remove .5
			// left is sorted
			if target >= nums[l] && target <= nums[m] {
				r = m - 1
			} else {
				l = m + 1
			}
		} else {
			// right is sorted
			if target <= nums[r] && target >= nums[m] {
				l = m + 1
			} else {
				r = m - 1
			}
		}
	}
	return -1
}
