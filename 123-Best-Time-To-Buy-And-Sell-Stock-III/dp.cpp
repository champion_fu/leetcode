//
// Created by champ on 2022/4/11.
//

class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int cost1 = INT_MAX, cost2 = INT_MAX;
        int profit1 = 0, profit2 = 0;
        for (auto price: prices) {
            cost1 = min(cost1, price);
            profit1 = max(profit1, price - cost1);
            cost2 = min(cost2, price - profit1);
            profit2 = max(profit2, price - cost2);
        }
        return profit2;
    }
};
