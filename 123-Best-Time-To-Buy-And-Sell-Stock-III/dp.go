package _23_Best_Time_To_Buy_And_Sell_Stock_III

import "math"

func maxProfit(prices []int) int {
	firstSell, secondSell := 0, 0
	firstBuy, secondBuy := math.MinInt32, math.MinInt32

	for _, price := range prices {
		secondSell = max(secondSell, secondBuy+price)
		secondBuy = max(secondBuy, firstSell-price)
		firstSell = max(firstSell, firstBuy+price)
		firstBuy = max(firstBuy, -price)
	}

	return secondSell
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
