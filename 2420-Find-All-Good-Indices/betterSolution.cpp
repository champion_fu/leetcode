//
// Created by champ on 2022/10/10.
//

class Solution {
public:
    vector<int> goodIndices(vector<int>& nums, int k) {
        int N = nums.size();
        // decreasing: value means how many elements are non-increasing before this index i
        // increasing: value means how many elements are non-decreasing after this index i
        // EX: 2, 1, 1, 1, 3, 4, 1
        // de: 1, 2, 3, 4, 1, 1, 2
        // in: 1, 5, 4, 3, 2, 1, 1
        vector<int> decreasing(N, 1), increasing(N, 1);

        for (int i = 1; i < N; i++) {
            if (nums[i] <= nums[i - 1]) {
                decreasing[i] = 1 + decreasing[i - 1];
            }
        }

        for (int i = N - 2; i >= 0; i--) {
            if (nums[i] <= nums[i + 1]) {
                increasing[i] = 1 + increasing[i + 1];
            }
        }

        vector<int> ans;

        for(int i = k; i < N - k; i++) {
            // then need to bigger than k
            if (increasing[i + 1] >= k && decreasing[i - 1] >= k) {
                ans.push_back(i);
            }
        }

        return ans;
    }
};
