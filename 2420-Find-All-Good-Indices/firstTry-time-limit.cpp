//
// Created by champ on 2022/10/10.
//

class Solution {
public:
    vector<int> goodIndices(vector<int>& nums, int k) {
        vector<int> res(0);
        bool isGoodIndex = true;
        for (int i = k; i < nums.size() - k; i++) {
            // cout << " i " << i << endl;
            isGoodIndex = true;
            for (int l = i-k; l < i-1; l++) {
                // cout << " l " << l << " nums[l] " << nums[l] << " nums[l+1] " << nums[l+1] << endl;
                if (nums[l] < nums[l+1]) {
                    isGoodIndex = false;
                    break;
                }
            }
            // cout << "after l isGoodIndex " << isGoodIndex << endl;
            if (!isGoodIndex) {
                continue;
            }
            for (int r = i+1; r < i+k; r++) {
                // cout << " r " << r << " nums[r] " << nums[r] << " nums[r+1] " << nums[r+1] << endl;
                if (nums[r] > nums[r+1]) {
                    isGoodIndex = false;
                    break;
                }
            }
            // cout << "after r isGoodIndex " << isGoodIndex << endl;
            if (!isGoodIndex) {
                continue;
            } else {
                res.push_back(i);
            }
        }

        return res;
    }
};
