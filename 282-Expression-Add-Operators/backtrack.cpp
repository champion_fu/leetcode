//
// Created by champ on 2022/3/20.
//

class Solution {
public:
    string s;
    int target;
    vector<string> res;
    vector<string> addOperators(string num, int target) {
        this->s = num;
        this->target = target;
        backtrack(0, "", 0, 0);
        return res;
    }
    void backtrack(int i, const string& path, long valSoFar, long preVal) {
        if (i == s.length()) {
            if (valSoFar == target) res.push_back(path);
            return;
        }

        string numStr;
        long num;

        for (int j = i; j < s.length(); j++) {
            if (j > i && s[i] == '0') break;
            numStr += s[j];
            num = num*10 + s[j] - '0';

            if (i == 0) {
                // first just take
                backtrack(j+1, path+numStr, num, num);
            } else {
                backtrack(j+1, path+'+'+numStr, valSoFar+num, num);
                backtrack(j+1, path+'-'+numStr, valSoFar-num, -num);
                backtrack(j+1, path+'*'+numStr, valSoFar-preVal+preVal*num, preVal*num);
            }
        }
    }
};
