package _78_First_Bad_Version

/**
 * Forward declaration of isBadVersion API.
 * @param   version   your guess about first bad version
 * @return 	 	      true if current version is bad
 *			          false if current version is good
 * func isBadVersion(version int) bool;
 */

func firstBadVersion(n int) int {
	l := 1
	r := n
	m := l + (r-1)/2
	for l < r {
		m = l + (r-l)/2
		bad := isBadVersion(m)
		if bad {
			r = m
		} else {
			l = m + 1
		}
	}
	return l
}
