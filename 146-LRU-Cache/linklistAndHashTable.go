package _46_LRU_Cache

type LRUCache struct {
	capacity  int
	head      *node
	tail      *node
	hashTable map[int]*node
}

type node struct {
	next *node
	prev *node
	val  int
	key  int
}

func Constructor(capacity int) LRUCache {
	return LRUCache{
		capacity:  capacity,
		hashTable: make(map[int]*node, 0),
	}
}

func (this *LRUCache) removeFromChain(n *node) {
	if n == this.head {
		this.head = n.next
	}

	if n == this.tail {
		this.tail = n.prev
	}

	if n.prev != nil {
		n.prev.next = n.next
	}

	if n.next != nil {
		n.next.prev = n.prev
	}
}

func (this *LRUCache) addToChain(n *node) {
	n.next = this.head
	n.prev = nil
	if n.next != nil {
		n.next.prev = n
	}
	this.head = n
	if this.tail == nil {
		// fist one
		this.tail = n
	}
}

func (this *LRUCache) Get(key int) int {
	n, m := this.hashTable[key]
	if !m {
		return -1
	}

	// move to first
	this.removeFromChain(n)
	this.addToChain(n)
	return n.val
}

func (this *LRUCache) Put(key int, value int) {
	n, m := this.hashTable[key]
	if !m {
		// not in chain
		n = &node{
			val: value,
			key: key,
		}
		this.hashTable[key] = n
	} else {
		n.val = value
		this.removeFromChain(n)
	}

	this.addToChain(n)

	// more than capacity
	if len(this.hashTable) > this.capacity {
		n = this.tail
		if n != nil {
			this.removeFromChain(n)
			delete(this.hashTable, n.key)
		}
	}
}

/**
 * Your LRUCache object will be instantiated and called as such:
 * obj := Constructor(capacity);
 * param_1 := obj.Get(key);
 * obj.Put(key,value);
 */
