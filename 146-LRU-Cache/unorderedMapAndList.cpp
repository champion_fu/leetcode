//
// Created by champ on 2022/4/28.
//

class LRUCache {
public:
    list<pair<int, int>> m_list;
    int size;
    unordered_map<int, list<pair<int, int>>::iterator> m_map;
    LRUCache(int capacity) {
        size = capacity;
    }

    int get(int key) {
        if (m_map.find(key) == m_map.end()) {
            return -1;
        }
        // for (auto i : it) {
        //     cout << "i first " << i->first  << "i second " << i->second << endl;
        // }

        //move the node corresponding to key to front
        m_list.splice(m_list.begin(), m_list, m_map[key]);
        return m_map[key]->second;
    }

    void put(int key, int value) {
        if (m_map.find(key) != m_map.end()) {
            // exist
            //move the node corresponding to key to front
            m_list.splice(m_list.begin(), m_list, m_map[key]);
            m_map[key]->second = value;
            return;
        }
        if (m_map.size() == size) {
            // remove last one.
            int key_to_del = m_list.back().first;
            m_list.pop_back();
            m_map.erase(key_to_del);
        }
        m_list.push_front({key, value});
        m_map[key] = m_list.begin();
    }
};

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache* obj = new LRUCache(capacity);
 * int param_1 = obj->get(key);
 * obj->put(key,value);
 */
