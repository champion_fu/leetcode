//
// Created by champ on 2022/7/26.
//

class LRUCache {
    list<pair<int,int>> dll;
    unordered_map<int, list<pair<int, int>>::iterator> m;
    int size;
public:
    LRUCache(int capacity) {
        size = capacity;
    }

    int get(int key) {
        if (m.find(key) == m.end()) return -1;
        auto it = m[key];

        dll.splice(dll.begin(), dll, it);
        return it->second;
    }

    void put(int key, int value) {
        if (m.find(key) != m.end()) {
            auto it = m[key];
            dll.splice(dll.begin(), dll, it);
            it->second = value;
            return;
        }

        if (size == m.size()) {
            auto it = dll.back();
            dll.pop_back();

            m.erase(it.first);
        }
        dll.push_front({key, value});
        m[key] = dll.begin();
    }
};

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache* obj = new LRUCache(capacity);
 * int param_1 = obj->get(key);
 * obj->put(key,value);
 */
