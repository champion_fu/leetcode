//
// Created by champ on 2022/8/9.
//

class Solution {
public:
    int findMaxLength(vector<int>& nums) {
        unordered_map<int, int> m;
        m[0] = -1;

        // let 0 change to -1, and
        // using sum == 0 to get contiguous array
        int sum = 0, res = 0;
        for (int i = 0; i < nums.size(); i++) {
            sum += nums[i] == 0 ? -1 : 1;

            if (m.find(sum) != m.end()) {
                res = max(res, i-m[sum]);
            } else {
                m[sum] = i;
            }
        }
        return res;
    }
};
