package _21_Task_Scheduler

import "sort"

func leastInterval(tasks []byte, n int) int {
	if n == 0 {
		return len(tasks)
	}
	counts := make([]int, 26)
	for _, v := range tasks {
		t := v - 'A'
		counts[t] += 1
	}
	sort.Slice(
		counts,
		func(i, j int) bool {
			return counts[i] > counts[j]
		},
	)

	nMax := counts[0]
	same := 0
	for _, c := range counts {
		if nMax == c {
			same++
		}
	}
	return max(len(tasks), (nMax-1)*(n+1)+same)
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
