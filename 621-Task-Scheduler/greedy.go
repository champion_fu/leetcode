package _21_Task_Scheduler

import "sort"

func leastInterval(tasks []byte, n int) int {
	if n == 0 {
		return len(tasks)
	}
	counts := make([]int, 26)
	for _, v := range tasks {
		t := v - 'A'
		counts[t] += 1
	}
	sort.Slice(
		counts,
		func(i, j int) bool {
			return counts[i] > counts[j]
		},
	)

	max := counts[0]
	idleTime := (max - 1) * n
	for _, c := range counts[1:] {
		// assign to time slot
		idleTime -= min(c, max-1)
	}
	if idleTime < 0 {
		return len(tasks)
	}
	return idleTime + len(tasks)
}

func min(a, b int) int {
	if a > b {
		return b
	}
	return a
}
