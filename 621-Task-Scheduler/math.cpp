//
// Created by champ on 2022/8/2.
//

class Solution {
public:
    int leastInterval(vector<char>& tasks, int n) {
        if (n == 0) return tasks.size();
        unordered_map<char, int> m;

        for (char task : tasks) {
            m[task]++;
        }

        int maximum = 0;
        int dup = 0;
        for (auto p : m) {
            maximum = max(maximum, p.second);
        }
        for (auto p : m) {
            if (maximum == p.second) dup++;
        }

        int res = tasks.size();
        res = max(res, ((maximum-1)*(n+1) + dup));
        return res;
    }
};
