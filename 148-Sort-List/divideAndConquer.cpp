//
// Created by champ on 2022/8/11.
//

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* sortList(ListNode* head) {
        if (head == NULL || head->next == NULL) return head;

        ListNode* mid = getMid(head);
        ListNode* left = sortList(head);
        ListNode* right = sortList(mid);
        return merge(left, right);
    }
private:
    ListNode* merge(ListNode* left, ListNode* right) {
        ListNode* dummyHead = new ListNode(0);
        ListNode* p = dummyHead;
        while(left && right) {
            if (left->val < right->val) {
                p->next = left;
                left = left->next;
            } else {
                p->next = right;
                right = right->next;
            }
            p = p->next;
        }
        if (left) p->next = left;
        else p->next = right;
        return dummyHead->next;
    }

    ListNode* getMid(ListNode* head) {
        ListNode* midPrev = NULL;
        while (head && head->next) {
            if (midPrev == NULL) {
                midPrev = head;
            } else {
                midPrev = midPrev->next;
            }
            head = head->next->next;
        }
        ListNode* mid = midPrev->next;
        midPrev->next = NULL; // because need to divide and conquer
        return mid;
    }
};
