//
// Created by champ on 2022/4/19.
//

class StockPrice {
    map<int, int> records;
    multiset<int> count;
public:
    StockPrice() {
    }

    void update(int timestamp, int price) {
        if (records.find(timestamp) != records.end()) {
            // logN because c++ map is binarySearch
            count.erase(count.find(records[timestamp]));
        }
        records[timestamp] = price;
        count.insert(price);
    }

    int current() {
        return records.rbegin()->second;
    }

    int maximum() {
        /*
        https://stackoverflow.com/questions/56612556/what-is-the-difference-between-begin-and-rend
        For the same reason that it’s not safe to dereference the end() iterator (it’s past the end of the range),
        you shouldn’t dereference the rend() iterator, since it doesn’t point to an element within the container.
        */
        return *count.rbegin();
    }

    int minimum() {
        return *count.begin();
    }
};

/**
 * Your StockPrice object will be instantiated and called as such:
 * StockPrice* obj = new StockPrice();
 * obj->update(timestamp,price);
 * int param_2 = obj->current();
 * int param_3 = obj->maximum();
 * int param_4 = obj->minimum();
 */
