package main

import (
	"fmt"
	"sort"
)

func main() {
	s := []float64{85.201, 14.74, 965.25, 125.32, 63.14} // unsorted
	sort.Sort(sort.Float64Slice(s))
	fmt.Println(s)                                                                          // sorted
	fmt.Println("Length of Slice: ", sort.Float64Slice.Len(s))                              // 5
	fmt.Println("123.32 found in Slice at position: ", sort.Float64Slice(s).Search(125.32)) //	3
	fmt.Println("999.15 found in Slice at position: ", sort.Float64Slice(s).Search(999.15)) //	5
	fmt.Println("12.14 found in Slice at position: ", sort.Float64Slice(s).Search(12.14))   //	0
}
