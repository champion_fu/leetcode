package main

import (
	"container/heap"
	"fmt"
)

type RoofPoint struct {
	index  int
	isLeft bool
	x      int
	y      int
}

type PQ []*RoofPoint

func (pq PQ) Len() int {
	return len(pq)
}

func (pq PQ) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index = i
	pq[j].index = j
}

func (pq PQ) Less(i, j int) bool {
	return pq[i].y < pq[j].y
}

func (pq *PQ) Push(item interface{}) {
	newP := item.(*RoofPoint)
	newP.index = len(*pq)
	*pq = append(*pq, newP)
}

func (pq *PQ) Pop() interface{} {
	tmp := (*pq)[len(*pq)-1]
	*pq = (*pq)[:len(*pq)-1]
	return tmp
}

func main() {
	pq := make(PQ, 0)
	heap.Init(&pq)

	points := [][]int{
		{2, 9, 10},
		{5, 7, 12},
		{5, 12, 12},
		{15, 20, 10},
		{19, 24, 8},
	}

	for _, v := range points {
		heap.Push(&pq, &RoofPoint{
			isLeft: true, x: v[0], y: v[2],
		})
		heap.Push(&pq, &RoofPoint{
			isLeft: false, x: v[1], y: v[2],
		})
	}

	for pq.Len() != 0 {
		fmt.Printf("value %+v\n", pq.Pop())
	}
}
