package main

import (
	"fmt"
	"sort"
)

func main() {
	// a := []int{1, 3, 6, 10, 15, 21, 28, 36, 45, 55}
	a := []int{55, 45, 36, 28, 21, 15, 10, 6, 3, 1}
	target := 6

	i := sort.Search(len(a), func(i int) bool {
		// notice the descending or ascending order
		return a[i] <= target
	})
	if i < len(a) && a[i] == target {
		fmt.Printf("found %d at index %d in %v\n", target, i, a)
	} else {
		fmt.Printf("%d not found in %v\n", target, a)
	}
}
