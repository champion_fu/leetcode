class Node:
	def __init__(self, data=None, next=None):
		self.data = data
		self.next = next

class LinkedList:
	def __init__(self, head=None):
		self.head = head

	def append(self, data):
		n = Node(data)
		if not self.head:
			self.head = n
			return
		cur = self.head
		while cur.next:
			cur = cur.next
		cur.next = n

	def remove(self, data):
		cur = self.head
		prev = None
		while cur:
			if cur.data == data:
				if prev:
					prev.next = cur.next
					return
				else:
					self.head = cur.next
			else:
				prev = cur
				cur = cur.next

	def isEmpty(self):
		return self.head is None

	def print(self):
		if not self.head:
			print(self.head)
		node = self.head
		while node:
			end = " -> "
			print(node.data, end=end)
			node = node.next

ll = LinkedList()
print(ll.isEmpty())
ll.append(10)
ll.append(20)
print(ll.isEmpty())
print(ll.print())
ll.remove(20)
print(ll.print())