package _1_Rotate_List

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func rotateRight(head *ListNode, k int) *ListNode {
	if head == nil || head.Next == nil {
		return head
	}
	p := head
	length := 1
	for p.Next != nil {
		p = p.Next
		length++
	}
	p.Next = head
	k = length - (k % length)

	dummyHead := &ListNode{Val: -1, Next: head}
	pre := dummyHead
	cur := dummyHead.Next

	for i := 0; i < k; i++ {
		pre = cur
		cur = cur.Next
	}
	pre.Next = nil
	return cur
}
