//
// Created by champ on 2022/7/21.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int maxDepth(TreeNode* root) {
        if (root == NULL) return 0;

        stack<pair<TreeNode*, int>> s;
        s.push(pair<TreeNode*, int>{root, 1});

        int result = 0;
        while (!s.empty()) {
            auto p = s.top();
            s.pop();
            TreeNode* node = p.first;
            int depth = p.second;

            result = max(result, depth);
            if (node->left) s.push(pair<TreeNode*, int>{node->left, depth+1});
            if (node->right) s.push(pair<TreeNode*, int>{node->right, depth+1});
        }

        return result;
    }
};
