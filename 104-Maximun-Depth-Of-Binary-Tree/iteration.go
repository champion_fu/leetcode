package _04_Maximun_Depth_Of_Binary_Tree

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

func maxDepth(root *TreeNode) int {
	if root == nil {
		return 0
	}
	s := stack{}
	s.Push(root, 1)

	depth := 0

	for !s.IsEmpty() {
		pair := s.Pop()
		currentDepth := pair.depth
		node := pair.node
		if node != nil {
			if currentDepth > depth {
				depth = currentDepth
			}
			s.Push(node.Left, currentDepth+1)
			s.Push(node.Right, currentDepth+1)
		}
	}
	return depth
}

type pair struct {
	node  *TreeNode
	depth int
}

type stack []*pair

func (s *stack) IsEmpty() bool {
	return len(*s) == 0
}

func (s *stack) Push(a *TreeNode, depth int) {
	*s = append(*s, &pair{node: a, depth: depth})
}

func (s *stack) Pop() *pair {
	if s.IsEmpty() {
		return nil
	} else {
		index := len(*s) - 1
		element := (*s)[index]
		*s = (*s)[:index]
		return element
	}
}
