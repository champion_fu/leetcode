//
// Created by champ on 2022/9/28.
//

class Solution {
    int p = 1e9 + 7;
public:
    int numberOfWays(int startPos, int endPos, int k) {
        if (abs(startPos - endPos) > k) return 0;
        int diff = endPos - startPos;
        if ((diff+k) % 2 != 0) return 0;

        long long res = 1L;
        for (int i = 0; i < (diff + k) / 2; ++i) {
            res = res * (k - i) % p;
            res = res * inv(i+1) % p;

        }
        return res;
    }
    long inv(long a) {
        if (a == 1) return 1;
        return (p - p / a) * inv(p%a) % p;
    }
};
