package _15_Kth_Largest_Element_In_An_Array

import "math/rand"

// [1, 3, 4, 2], k = 2, return 3
// [x, x, 3, x] index = 4-2
var num []int

func findKthLargest(nums []int, k int) int {
	num = nums
	n := len(nums)
	return kSelect(0, n-1, n-k)
}

func kSelect(left, right, kIndex int) int {
	if left == right {
		return num[left]
	}

	// select a random index
	pIndex := rand.Intn(right-left+1) + left
	pIndex = partition(left, right, pIndex)
	if pIndex == kIndex {
		return num[pIndex]
	} else if pIndex > kIndex {
		return kSelect(left, pIndex-1, kIndex)
	}
	return kSelect(pIndex+1, right, kIndex)
}

func partition(left, right, pIndex int) int {
	pivot := num[pIndex]
	// move pivot to the end
	swap(right, pIndex)

	// move all smaller elements to the left
	i := left
	for j := left; j < right; j++ {
		if num[j] < pivot {
			swap(i, j)
			i++ // until i index is fixed
		}
	}

	// move pivot to the final index
	swap(right, i)
	return i
}

func swap(i, j int) {
	num[i], num[j] = num[j], num[i]
}
