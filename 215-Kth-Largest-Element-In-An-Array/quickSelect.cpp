//
// Created by champ on 2022/4/28.
//

class Solution {
public:
    int findKthLargest(vector<int>& nums, int k) {
        int size = nums.size();
        return kSelect(nums, 0, size-1, size-k);
    }
private:
    int kSelect(vector<int>& nums, int left, int right, int kIndex) {
        if (left == right) {
            return nums[left];
        }

        int pIndex = rand() % (right - left + 1) + left;
        pIndex = partition(nums, left, right, pIndex);
        if (kIndex == pIndex) {
            return nums[kIndex];
        } else if (kIndex > pIndex) {
            // search right
            return kSelect(nums, pIndex+1, right, kIndex);
        } else {
            return kSelect(nums, left, pIndex-1, kIndex);
        }
    }

    int partition(vector<int>& nums, int left, int right, int pIndex) {
        int pivot = nums[pIndex];
        swap(nums, pIndex, right);

        int i = left;
        for (int j = left; j < right; j++) {
            if (nums[j] < pivot) {
                // swap to the left
                swap(nums, j, i);
                i++;
            }
        }

        swap(nums, i, right);
        return i; // before i index is less than pivot
    }

    void swap(vector<int>& nums, int i, int j) {
        int tmp = nums[j];
        nums[j] = nums[i];
        nums[i] = tmp;
    }
};
