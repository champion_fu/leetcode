//
// Created by champ on 2022/4/28.
//

class Solution {
public:
    int findKthLargest(vector<int>& nums, int k) {
        priority_queue<int, vector<int>> pq;

        for (auto n : nums) {
            pq.push(n);
        }

        int ret;
        while(k--) {
            ret = pq.top();
            pq.pop();
        }
        return ret;
    }
};
