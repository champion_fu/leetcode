package _15_Kth_Largest_Element_In_An_Array

import "container/heap"

func findKthLargest(nums []int, k int) int {
	h := &IntHeap{}
	heap.Init(h)

	for _, v := range nums {
		heap.Push(h, v)
		if len(*h) > k {
			heap.Pop(h)
		}
	}
	return heap.Pop(h).(int)
}

type IntHeap []int

func (h IntHeap) Len() int {
	return len(h)
}
func (h IntHeap) Less(i, j int) bool {
	return h[i] < h[j]
}
func (h IntHeap) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}
func (h *IntHeap) Pop() interface{} {
	old := *h
	n := len(*h)
	e := old[n-1]
	*h = old[:n-1]
	return e
}
func (h *IntHeap) Push(x interface{}) {
	e := x.(int)
	*h = append(*h, e)
}
