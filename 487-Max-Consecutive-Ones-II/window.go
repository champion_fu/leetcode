package _87_Max_Consecutive_Ones_II

func findMaxConsecutiveOnes(nums []int) int {
	l, r, acceptOneNumber, zero, result := 0, 0, 1, 0, 0
	for r < len(nums) {
		if nums[r] == 0 {
			zero++
		}

		for zero > acceptOneNumber {
			if nums[l] == 0 {
				zero--
			}
			l++
		}

		// Get the result.
		if r-l+1 > result {
			result = r - l + 1
		}
		r++
	}
	return result
}
