//
// Created by champ on 2022/4/27.
//

class Solution {
public:
    vector<vector<int>> permute(vector<int>& nums) {
        vector<vector<int>> result;
        backtracking(result, 0, nums);
        return result;
    }
private:
    void backtracking(vector<vector<int>>& result, int start, vector<int>& nums) {
        if (start >= nums.size()) {
            result.push_back(nums);
            return;
        }

        // 1, 2, 3
        // index = 0, i = 0, 1, 2,
        //   -> index = 1, i = 1, 2
        //      -> index = 2, i = 2
        for (int i = start; i < nums.size(); i++) {
            int tmp = nums[i];
            nums[i] = nums[start];
            nums[start] = tmp;
            backtracking(result, start+1, nums);
            tmp = nums[i];
            nums[i] = nums[start];
            nums[start] = tmp;
        }
    }
};
