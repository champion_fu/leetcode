package _6_Permutations

func permute(nums []int) [][]int {
	ret := make([][]int, 0)
	backtracking(&ret, nums, 0)
	return ret
}

func backtracking(ret *[][]int, nums []int, start int) {
	if start >= len(nums) {
		// make sure we have deep copy
		var r = make([]int, len(nums))
		copy(r, nums)
		*ret = append(*ret, r)
		return
	}

	for i := start; i < len(nums); i++ {
		// swap start and i
		nums[start], nums[i] = nums[i], nums[start]
		backtracking(ret, nums, start+1)
		nums[start], nums[i] = nums[i], nums[start]
	}
}
