//
// Created by champ on 2022/9/22.
//

class Solution {
public:
    long long minimumMoney(vector<vector<int>>& transactions) {
        long long res = 0; int v = 0;
        for (auto& a : transactions) {
            res += max(a[0] - a[1], 0);
            if (a[0] > a[1]) {
                // lost money, take max the cashback
                v = max(v, a[1]);
            } else {
                // earn money, take max the cost
                v = max(v, a[0]);
            }
        }
        //
        return res + v;
    }
};
