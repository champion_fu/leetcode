//
// Created by champ on 2022/4/3.
//

class Solution {
    random_device rd;
    int mod;
    unordered_map<int, int> updated_index;
public:
    Solution(int n, vector<int>& blacklist) {
        mod = n - blacklist.size();
        set<int> blacklist_set(blacklist.begin(), blacklist.end());

        int swap_num = n -1;
        for (int b : blacklist_set) {
            if (b >= mod) break;
            while (blacklist_set.count(swap_num) > 0)  {
                swap_num -= 1;
            }
            updated_index[b] = swap_num;
            swap_num--;
        }
    }

    int pick() {
        int index = rd() % mod;
        if (updated_index.count(index) > 0) return updated_index[index];
        return index;
    }
};
