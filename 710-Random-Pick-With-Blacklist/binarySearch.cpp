//
// Created by champ on 2022/4/3.
//

class Solution {
    vector<int> transformedBlack;
    random_device rd;
    int mod;
public:
    Solution(int n, vector<int>& blacklist) {
        mod = n - blacklist.size();
        sort(blacklist.begin(), blacklist.end());
        transformedBlack = blacklist;
        transformedBlack.push_back(n);
        for (int i = 0; i < transformedBlack.size(); i++) transformedBlack[i] -= i;
    }

    int pick() {
        int index = rd() % mod;
        auto it = upper_bound(transformedBlack.begin(), transformedBlack.end(), index);
        return index + (it -transformedBlack.begin());
    }
};
