//
// Created by champ on 2022/4/3.
//

class Solution {
    vector<int> candidates;
public:
    unordered_map<int, bool> blacklistMap;
    Solution(int n, vector<int>& blacklist) {
        for (auto b : blacklist) {
            blacklistMap[b] = true;
        }
        for (int i = 0; i < n; i++) {
            if (blacklistMap.count(i) > 0) {
                // skip blacklist
                continue;
            }
            candidates.push_back(i);
        }
        // for (auto c : candidates) {
        //     cout << c << endl;
        // }
    }

    int pick() {
        return candidates[rand() % candidates.size()];
    }
};
