package _7_Sudoku_Solver

var validCol [][]int
var validRow [][]int
var validBox [][]int
var values = []byte{'1', '2', '3', '4', '5', '6', '7', '8', '9'}
var valuesMap = map[byte]int{
	'1': 0,
	'2': 1,
	'3': 2,
	'4': 3,
	'5': 4,
	'6': 5,
	'7': 6,
	'8': 7,
	'9': 8,
}

func solveSudoku(board [][]byte) {
	// initialize value
	validCol = make([][]int, 9)
	for i := 0; i < 9; i++ {
		// don't use first index (0)
		validCol[i] = make([]int, 10)
	}
	validRow = make([][]int, 9)
	for i := 0; i < 9; i++ {
		// don't use first index (0)
		validRow[i] = make([]int, 10)
	}

	validBox = make([][]int, 9)
	for i := 0; i < 9; i++ {
		// don't use first index (0)
		validBox[i] = make([]int, 10)
	}
	for i := 0; i < 9; i++ {
		for j := 0; j < 9; j++ {
			if v, m := valuesMap[board[i][j]]; m {
				place(&board, i, j, v)
			}
		}
	}
	backtracking(&board, 0, 0)
}

func canPlace(row, col, valIndex int) bool {
	boxIndex := (row/3)*3 + col/3
	return (validCol[col][valIndex+1] + validRow[row][valIndex+1] + validBox[boxIndex][valIndex+1]) == 0
}

func place(board *[][]byte, row, col, valIndex int) {
	boxIndex := (row/3)*3 + col/3
	(*board)[row][col] = values[valIndex]
	validCol[col][valIndex+1] += 1
	validRow[row][valIndex+1] += 1
	validBox[boxIndex][valIndex+1] += 1
}

// solve sudoku on matrix with giving row, col
func backtracking(board *[][]byte, row, col int) bool {
	if col == 9 {
		if row == 8 {
			// solved
			return true
		}
		col = 0
		row += 1
	}

	if (*board)[row][col] != '.' {
		return backtracking(board, row, col+1)
	}

	// iteration all numbers
	for i := 0; i < 9; i++ {
		if canPlace(row, col, i) {
			place(board, row, col, i)
			if backtracking(board, row, col+1) {
				return true
			}
			clean(board, row, col, i)
		}
	}
	return false
}

func clean(board *[][]byte, row, col, valIndex int) {
	(*board)[row][col] = '.'
	boxIndex := (row/3)*3 + col/3
	validCol[col][valIndex+1] -= 1
	validRow[row][valIndex+1] -= 1
	validBox[boxIndex][valIndex+1] -= 1
}
