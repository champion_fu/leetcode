//
// Created by champ on 2022/7/21.
//

class Solution {
public:
    string addBinary(string a, string b) {
        string res = "";
        int indexB = b.size() - 1;
        int indexA = a.size() - 1;
        int extra = 0;
        while (indexB >= 0 || indexA >= 0 || extra == 1) {
            extra += indexA >= 0 ? a[indexA--] - '0': 0;
            extra += indexB >= 0 ? b[indexB--] - '0': 0;

            res = char(extra % 2 + '0') + res;
            extra /= 2;
        }
        return res;
    }
};
