//
// Created by champ on 2022/7/21.
//

class Solution {
public:
    string addBinary(string a, string b) {
        string res;
        int sizeA = a.size();
        int sizeB = b.size();

        if (sizeA < sizeB) return addBinary(b, a);

        int extra = 0;
        int indexB = sizeB - 1;
        int indexA = sizeA - 1;
        for (; indexB >= 0 || indexA >= 0; indexB--, indexA--) {
            int cB = 0;
            if (indexB >= 0) {
                cB = b[indexB] - '0';
            }
            int cA = 0;
            if (indexA >= 0) {
                cA = a[indexA] - '0';
            }
            int s = cA + cB + extra;
            res += to_string(s % 2);
            cout << res << endl;
            extra = s / 2;
        }
        if (extra) {
            res += "1";
        }
        reverse(res.begin(), res.end());
        return res;
    }
};
