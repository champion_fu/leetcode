package _71_Excel_Sheet_Column_Number

import "math"

func titleToNumber(columnTitle string) int {
	ret := 0
	n := len(columnTitle)
	for i, r := range columnTitle {
		v := int(r - 'A')
		ret += int(float64(v+1) * math.Pow(26.0, float64(n-i-1)))
	}
	return ret
}
