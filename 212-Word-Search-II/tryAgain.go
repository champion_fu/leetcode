package _12_Word_Search_II

var ret []string
var b [][]byte
var m, n int
var rowDir, colDir []int

func findWords(board [][]byte, words []string) []string {
	trie := Constructor()
	for _, word := range words {
		trie.Insert(word)
	}

	m = len(board)
	n = len(board[0])
	b = board
	ret = make([]string, 0)
	rowDir = []int{1, -1, 0, 0}
	colDir = []int{0, 0, -1, 1}

	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if trie.root.get(board[i][j]) != nil {
				backtrack(i, j, trie.root)
			}
		}
	}
	return ret
}

func backtrack(row, col int, parent *TrieNode) {
	letter := b[row][col]
	curNode := parent.get(letter)

	if curNode.isEnd() {
		ret = append(ret, curNode.word)
		curNode.unsetEnd()
	}

	b[row][col] = byte('#')

	for i := 0; i < 4; i++ {
		newRow := row + rowDir[i]
		newCol := col + colDir[i]
		if newRow < 0 || newCol < 0 || newRow >= m || newCol >= n {
			continue
		}
		if curNode.get(b[newRow][newCol]) != nil {
			backtrack(newRow, newCol, curNode)
		}
	}

	b[row][col] = letter
}

type Trie struct {
	root *TrieNode
}

/** Initialize your data structure here. */
func Constructor() Trie {
	return Trie{
		root: newTrieNode(),
	}
}

/** Inserts a word into the trie. */
func (this *Trie) Insert(word string) {
	cur := this.root
	for i := range word {
		a := word[i]
		if cur.get(a) == nil {
			cur.push(a, newTrieNode())
		}
		cur = cur.get(a)
	}
	cur.setEnd()
	cur.setWord(word)
	return
}

func (this *Trie) searchWithPrefix(prefix string) *TrieNode {
	cur := this.root
	for i := range prefix {
		a := prefix[i]
		if cur.get(a) == nil {
			return nil
		}
		cur = cur.get(a)
	}
	return cur
}

/** Returns if the word is in the trie. */
func (this *Trie) Search(word string) bool {
	n := this.searchWithPrefix(word)
	if n != nil && n.isEnd() {
		return true
	}
	return false
}

/** Returns if there is any word in the trie that starts with the given prefix. */
func (this *Trie) StartsWith(prefix string) bool {
	n := this.searchWithPrefix(prefix)
	if n != nil {
		return true
	}
	return false
}

func newTrieNode() *TrieNode {
	return &TrieNode{
		link: make(map[byte]*TrieNode, 0),
	}
}

type TrieNode struct {
	link map[byte]*TrieNode
	end  bool
	word string
}

func (n *TrieNode) get(a byte) *TrieNode {
	return n.link[a]
}

func (n *TrieNode) push(a byte, trieNode *TrieNode) {
	n.link[a] = trieNode
}

func (n *TrieNode) isEnd() bool {
	return n.end
}

func (n *TrieNode) setEnd() {
	n.end = true
}

func (n *TrieNode) unsetEnd() {
	n.end = false
}

func (n *TrieNode) setWord(w string) {
	n.word = w
}

func (n *TrieNode) remove(a byte) {
	delete(n.link, a)
}
