//
// Created by champ on 2022/8/14.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
    int res = INT_MIN;
public:
    int maxPathSum(TreeNode* root) {
        maxGain(root);
        return res;
    }
private:
    int maxGain(TreeNode* root) {
        if (root == NULL) return 0;

        int left = max(maxGain(root->left), 0);
        int right = max(maxGain(root->right), 0);

        int values = root->val + left + right;

        res = max(res, values);

        return root->val + max(left, right);
    }
};
