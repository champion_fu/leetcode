//
// Created by champ on 2022/6/21.
//

class Solution {
public:
    int maximalSquare(vector<vector<char>>& matrix) {
        int m = matrix.size();
        int n = matrix[0].size();
        vector<int> dp(n+1, 0);
        int maxLen = 0, prev = 0;
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                int temp = dp[j]; // temp == i-1, j
                if (matrix[i-1][j-1] == '1') {
                    // dp[j-1] == i, j-1
                    // dp[j] == i-1, j
                    // prev == i-1, j-1
                    dp[j] = min(min(dp[j-1], dp[j]), prev) +1;
                    maxLen = max(maxLen, dp[j]);
                } else {
                    dp[j] = 0;
                }
                prev = temp;
            }
        }

        return maxLen*maxLen;
    }
};
