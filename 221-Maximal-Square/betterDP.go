package _21_Maximal_Square

func maximalSquare(matrix [][]byte) int {
	m := len(matrix)
	n := len(matrix[0])

	prev := 0 // keep update top(i-1) left(j-1)
	dp := make([]int, n)

	maxLength := 0
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			tmp := dp[j] // tmp == i-1, j
			if i < 1 || j < 1 || matrix[i][j] == byte('0') {
				dp[j] = int(matrix[i][j] - byte('0'))
			} else {
				// dp[j-1] == [i, j-1]
				// dp[j] == [i-1, j]
				// prev == [i-1, j-1]
				dp[j] = min(prev, min(dp[j-1], dp[j])) + 1
			}
			prev = tmp // after j++, tmp == prev == [i-1, j-1]
			maxLength = max(maxLength, dp[j])
		}
	}
	return maxLength * maxLength
}

func min(i, j int) int {
	if i < j {
		return i
	}
	return j
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}
