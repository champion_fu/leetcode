package _21_Maximal_Square

func maximalSquare(matrix [][]byte) int {
	m := len(matrix)
	n := len(matrix[0])

	dp := make([][]int, m)
	for i := 0; i < m; i++ {
		dp[i] = make([]int, n)
	}
	var maxLength int
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if matrix[i][j] == byte('1') {
				// set default value
				dp[i][j] = 1
				maxLength = max(maxLength, dp[i][j])
			}

			if i-1 < 0 || j-1 < 0 {
				continue
			}

			if dp[i][j] == 1 {
				dp[i][j] = min(min(dp[i][j-1], dp[i-1][j]), dp[i-1][j-1]) + 1
			}
			maxLength = max(maxLength, dp[i][j])
		}
	}
	return maxLength * maxLength
}

func min(i, j int) int {
	if i > j {
		return j
	}
	return i
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}
