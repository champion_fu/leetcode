//
// Created by champ on 2022/9/21.
//

class Solution {
public:
    int longestContinuousSubstring(string s) {
        int ans = 0;
        int curr = 1;
        for (int i = 1; i < s.size(); i++) {
            if (s[i-1]+1 == s[i]) curr++;
            else {
                ans = max(curr, ans);
                curr = 1;
            }
        }
        return max(curr, ans);
    }
};
