//
// Created by champ on 2022/9/21.
//

class Solution {
public:
    int longestContinuousSubstring(string s) {
        string alphabet = "abcdefghijklmnopqrstuvwxyz";
        unordered_map<char, int> m;
        for (int i = 0; i < alphabet.size(); i++) {
            m[alphabet[i]] = i;
        }

        int i = 0, ans = 1, tmp = 1;
        int j = 0;
        while (i < s.size()) {
            // cout << " i " << i << " s[i] " << s[i] << " j " << m[s[i]] << endl;
            j = m[s[i]];

            int z = i;
            while (z < s.size()) {
                if (j+1 == m[s[z+1]]) {
                    tmp++;
                    ans = max(ans, tmp);
                } else {
                    i = z;
                    tmp = 1;
                    break;
                }
                z++;
                j++;
            }
            i++;
        }
        return ans;
    }
};
