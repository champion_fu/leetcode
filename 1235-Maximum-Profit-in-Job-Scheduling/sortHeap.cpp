//
// Created by champ on 2022/8/12.
//

class Solution {
public:
    int jobScheduling(vector<int>& startTime, vector<int>& endTime, vector<int>& profit) {
        vector<vector<int>> jobs;

        for (int i = 0; i < startTime.size(); i++) {
            jobs.push_back({startTime[i], endTime[i], profit[i]});
        }

        sort(jobs.begin(), jobs.end());
        return findMaxProfit(jobs);
    }
private:
    int findMaxProfit(vector<vector<int>>& jobs) {
        int n = jobs.size(), maxProfit = 0;
        // store {endTime, profit} with minium endTime first (min heap)
        priority_queue<vector<int>, vector<vector<int>>, greater<vector<int>>> pq;

        for (int i = 0; i < n; i++) {
            int start = jobs[i][0], end = jobs[i][1], profit = jobs[i][2];

            // keep popping while the heap is not empty and jobs are not conflicting
            while (!pq.empty() && start >= pq.top()[0]) {
                // this start time is >= pq.end time can makeProfit on this one
                maxProfit = max(maxProfit, pq.top()[1]);
                pq.pop();
            }
            pq.push({end, profit+maxProfit});
        }

        // need to clear pq
        while (!pq.empty()) {
            maxProfit = max(maxProfit, pq.top()[1]);
            pq.pop();
        }
        return maxProfit;
    }
};
