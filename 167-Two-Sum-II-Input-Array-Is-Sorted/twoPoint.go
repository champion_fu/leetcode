package _67_Two_Sum_II_Input_Array_Is_Sorted

func twoSum(numbers []int, target int) []int {
	var ret []int
	l := 0
	r := len(numbers) - 1
	for l < r {
		sum := numbers[l] + numbers[r]
		if sum == target {
			ret = []int{l + 1, r + 1}
			return ret
		} else if sum > target {
			r -= 1
		} else {
			l += 1
		}
	}
	return ret
}
