package _67_Two_Sum_II_Input_Array_Is_Sorted

func twoSum(numbers []int, target int) []int {
	var ret []int
	n := len(numbers)
	for i, v := range numbers {
		l := i
		r := n
		for l < r {
			m := l + (r-l)/2
			if numbers[m] == target-v {
				ret = append(ret, i+1)
				ret = append(ret, m+1)
				return ret
			} else if numbers[m] > target-v {
				r = m
			} else {
				l = m + 1
			}
		}
	}
	return ret
}
