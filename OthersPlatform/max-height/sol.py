def bi_sol(case: list[int]) -> int:
	inorder = sol(case.copy())
	reverse = sol(case[::-1].copy())
	if inorder > reverse:
		return inorder
	else:
		return reverse

def sol(case: list[int]) -> int:
	# make the array look like pyramid
	increase = case[0] <= case[1]

	for i in range(2, len(case)):
		if increase:
			# in increase status
			if case[i] > case[i-1]:
				# don't change any thing
				continue
			elif case[i] < case[i-1]:
				# change status
				increase = False
		else: 
			# in decrease status
			if case[i] > case[i-1]:
				case[i] = case[i-1]
			elif case[i] < case[i-1]:
				# don't change any thing
				continue

	return sum(case)


if __name__ == "__main__":
	cases = [
		[4, 7, 7, 6, 6, 5, 8],
		[4, 4, 7, 6, 8, 8],
		[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 85, 43, 43, 82, 15, 15, 87, 18, 43]
	]
	ans = [
		[4, 7, 7, 6, 6, 5, 5], # 40
		[4, 4, 6, 6, 8, 8], # 36
		[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 85, 43, 43, 43, 15, 15, 15, 15, 15] # 43
	]
	for i in range(len(cases)):
		s = bi_sol(cases[i])
		expected = sum(ans[i])
		assert(s == expected)
