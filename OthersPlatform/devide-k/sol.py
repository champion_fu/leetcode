
def bf_sol(case, k):
	for i in range(len(case)):
		case[i] = case[i] % k

	ans = 0
	for i in range(len(case)):
		for j in range(i+1, len(case)):
			for z in range(j+1, len(case)):
				if (case[i] + case[j] + case[z]) % k == 0:
					ans += 1
	return ans

def sol(case, k):
	m = {}
	for c in case:
		r = c % k
		if r not in m:
			m[r] = 1
		else:
			m[r] += 1
	print(m)



if __name__ == "__main__":
	cases = [
		[[4, 7, 8, 5, 3], 3],
	]
	ans = [
		4,
	]

	# 3: 0+0+0, 0+1+2, 1+1+1
	# 4: 0+0+0, 0+1+3, 0+2+2, 1+0+3, 1+1+2
	for i in range(len(cases)):
		# 1. brute force solution: O(N**3)
		assert(bf_sol(cases[i][0], cases[i][1]) == ans[i])
		# 2. mapping solution: 
		assert(sol(cases[i][0], cases[i][1]) == ans[i])
