import sys

def bf_sol(case, max_price, min_price):
	ans = 0
	for i in range(len(case)):
		for j in range(i+1, len(case)):
			if max(case[i:j+1]) == max_price and min(case[i:j+1])  == min_price:
				ans += 1
	return ans

def sol(case, max_price, min_price):
	dp_max = [[-sys.maxsize - 1for i in range(len(case))] for j in range(len(case))]
	dp_min = [[sys.maxsize for i in range(len(case))] for j in range(len(case))]

	for i in range(len(case)):
		dp_max[i][i] = case[i]
		dp_min[i][i] = case[i]

	for i in range(0, len(case)):
		for j in range(i+1, len(case))
		# print(dp_max[0][i], dp_max[i][i])
		# input('pause')
		if dp_max[i][i] < dp_max[0][i]:
			tmp_max = dp_max[0][i]
		else:
			tmp_max = dp_max[i][i]
		for j in range(0, i):
			dp_max[j][i] = tmp_max

		if dp_min[i][i] > dp_min[0][i]:
			tmp_min = dp_min[0][i]
		else:
			tmp_min = dp_min[i][i]
		for j in range(0, i):
			dp_min[j][i] = tmp_min
	for a in dp_max:
		print(a)
	# for a in dp_min:
	# 	print(a)


if __name__ == "__main__":
	cases = [
		[[4, 5, 3, 3, 1], 5, 3],
		[[2, 2, 1, 5, 1], 2, 1],
	]
	ans = [
		4,
		2,
	]
	for i in range(len(cases)):
		expected = ans[i]
		# 1. brute force solution: O(N**3)
		# print(bf_sol(cases[i][0], cases[i][1], cases[i][2]), expected)
		assert(bf_sol(cases[i][0], cases[i][1], cases[i][2]) == expected)
		assert(sol(cases[i][0], cases[i][1], cases[i][2]) == expected)
