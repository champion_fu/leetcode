string expand(string s) {
    int N = s.size();
    int firstNumber = 0;
    int firstNumberIndex = 0;
    for (firstNumberIndex = 0; firstNumberIndex < N; firstNumberIndex++) {
        char c = s[firstNumberIndex];
        if (isdigit(c)) {
            firstNumber = firstNumber * 10 + c - '0';
        } else {
            break;
        }
    }
    // cout << "firstNumber " << firstNumber << " firstNumberIndex " << firstNumberIndex << endl;
    
    stack<string> st;
    
    string tmp = "";
    int i = firstNumberIndex;
    while (i < N) {
        while (isalpha(s[i])) {
            tmp += s[i];
            i++;
            if (islower(s[i])) {
                st.push(tmp);
                tmp = "";
            }
        }
        st.push(tmp);
        tmp = "";
        int k = 0;
        while (isdigit(s[i])) {
            k = k* 10 + s[i] - '0';
            i++;
        }
        if (k > 0) {
            string topString = st.top();
            st.pop();
            tmp = "";
            while (k-- > 0) {
                tmp += topString;
            }
            st.push(tmp);
            tmp = "";
        }
    }
    
    tmp = "";
    while (!st.empty()) {
        tmp += st.top();
        st.pop();
    }
    reverse(tmp.begin(), tmp.end());
    string res = "";
    if (firstNumber > 0) {
        while (firstNumber-- > 0) {
            res += tmp;
        }   
    } else {
        res = tmp;
    }
    return res;
}

string dealWithReaction(string s) {
    int start = 0;
    int end = s.find("+");
    string res = "";
    while (end != -1) {
        string oneChemical = s.substr(start, end - start - 1);
        // cout << "oneChemical " << oneChemical << " size "  << oneChemical.size() << endl;
        res += expand(oneChemical);
        start = end + 2;
        end = s.find("+", start);
    }
    string oneChemical = s.substr(start);
    // cout << "oneChemical " << oneChemical << " size "  << oneChemical.size() << endl;
    res += expand(oneChemical);
    return res;
}

bool solution(string s) {
    string leftHandSide = s.substr(0, s.find("=")-1);
    string rightHandSide = s.substr(s.find("=") + 2);
    // cout << "leftHandSide " << leftHandSide << " size " << leftHandSide.size() << endl;
    // cout << "rightHandSide " << rightHandSide << " size " << rightHandSide.size() << endl;
    // cout << " exapnd right " << expand(rightHandSide) << endl;
    cout << dealWithReaction(leftHandSide) << endl;
    cout << dealWithReaction(rightHandSide) << endl;
    
    unordered_map<char, int> leftMp;
    for (const char c : dealWithReaction(leftHandSide)) {
        leftMp[c]++;
    }
    
    for (const char c : dealWithReaction(rightHandSide)) {
        leftMp[c]--;
    }
    for (auto& [c, count] : leftMp) {
        if (count != 0) return false;
    }
    return true;
}
