package _0_Climbing_Stairs

func climbStairs(n int) int {
	var lookUpTable map[int]int = make(map[int]int, n)
	lookUpTable[1] = 1
	lookUpTable[2] = 2
	return help(n, lookUpTable)

}

func help(n int, lookUpTable map[int]int) int {
	if v, match := lookUpTable[n]; match {
		return v
	} else {
		ways := help(n-1, lookUpTable) + help(n-2, lookUpTable)
		lookUpTable[n] = ways
		return ways
	}
}
