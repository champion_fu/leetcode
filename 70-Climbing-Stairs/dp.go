package _0_Climbing_Stairs

func climbStairs(n int) int {
	if n == 1 {
		return 1
	}
	var climbs []int = make([]int, n+1)
	climbs[1] = 1
	climbs[2] = 2
	for i := 3; i < n+1; i++ {
		climbs[i] = climbs[i-1] + climbs[i-2]
	}
	return climbs[n]
}
