package _0_Climbing_Stairs

import "fmt"

func climbStairs(n int) int {
	n = n + 1
	if n < 2 {
		return n
	}
	m := [][]int{
		{1, 1},
		{1, 2},
	}

	i := n
	if n&1 == 1 {
		i -= 1
	}
	ret := [][]int{
		{1, 0},
		{0, 1},
	}
	pow := i / 2
	for pow > 0 {
		if pow&1 == 0 {
			m = multiply(m, m)
			pow /= 2
		} else {
			ret = multiply(ret, m)
			pow -= 1
		}
	}
	fmt.Printf("ret %+v, n %v\n", ret, n)
	if n&1 == 1 {
		return ret[1][1]
	} else {
		return ret[0][1]
	}
}

func multiply(a [][]int, b [][]int) [][]int {
	c := [][]int{
		{0, 0},
		{0, 0},
	}

	for i := 0; i < 2; i++ {
		for j := 0; j < 2; j++ {
			c[i][j] = a[i][0]*b[0][j] + a[i][1]*b[1][j]
		}
	}
	return c
}
