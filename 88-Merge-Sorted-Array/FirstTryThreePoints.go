package _8_Merge_Sorted_Array

func merge(nums1 []int, m int, nums2 []int, n int) {
	length := m + n
	m_index := m - 1
	n_index := n - 1
	if n_index < 0 {
		return
	}
	if m_index < 0 {
		for i, v := range nums2 {
			nums1[i] = v
		}
		return
	}
	i := length - 1
	for i > -1 {
		if nums1[m_index] > nums2[n_index] {
			nums1[i] = nums1[m_index]
			m_index -= 1
			i -= 1
			if m_index == -1 {
				for n_index > -1 {
					nums1[i] = nums2[n_index]
					i -= 1
					n_index -= 1
				}
			}
		} else {
			nums1[i] = nums2[n_index]
			n_index -= 1
			i -= 1
			if n_index == -1 {
				break
			}
		}
	}
}
