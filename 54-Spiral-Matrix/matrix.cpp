//
// Created by champ on 2022/7/31.
//

class Solution {
    unordered_map<int, pair<int, int>> dirs {
            {0, {0, 1}},
            {1, {1, 0}},
            {2, {0, -1}},
            {3, {-1, 0}},
    };
public:
    vector<int> spiralOrder(vector<vector<int>>& matrix) {
        int m = matrix.size();
        int n = matrix[0].size();
        int i = 0, j = 0, dir = 0;
        int total = m * n;

        vector<int> res;

        while (total > 0) {
            res.push_back(matrix[i][j]);
            matrix[i][j] = 101;
            total--;
            if (total == 0) return res;

            pair<int,int> p = dirs[dir];
            int newI = i + p.first;
            int newJ = j + p.second;
            while (newI >= m || newI < 0 || newJ >= n || newJ < 0 || matrix[newI][newJ] == 101) {
                dir = (dir + 1) % 4;
                p = dirs[dir];
                newI = i + p.first;
                newJ = j + p.second;
            }
            i = newI;
            j = newJ;
        }
        return res;
    }
};
