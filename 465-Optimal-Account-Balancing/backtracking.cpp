//
// Created by champ on 2022/10/27.
//

class Solution {
public:
    int minTransfers(vector<vector<int>>& transactions) {
        // debt:
        // EX: debt[i] = 10 means a person owes others 10
        // EX: debt[i] = -3 means others owes a person -3

        vector<int> debts = buildDebt(transactions);

        return getMinTxAfter(0, debts);
    }

private:
    int getMinTxAfter(int curID, vector<int> debts) {
        // find not zero
        while (curID < debts.size() && !debts[curID]) curID++;

        // Base case
        if (curID == debts.size()) return 0;

        int res = INT_MAX;
        for (int i = curID + 1; i < debts.size(); i++) {
            if (debts[i] * debts[curID] < 0) {
                // skip same sign debt
                debts[i] += debts[curID];
                int next = getMinTxAfter(curID+1, debts);
                if (next == INT_MAX) {
                    res = min(res, next);
                } else {
                    res = min(res, next + 1);
                }
                debts[i] -= debts[curID];
            }
        }
        return res < INT_MAX? res : 0;
    }

    vector<int> buildDebt(vector<vector<int>>& txs) {
        unordered_map<int, long> debtMap;
        for (vector<int>& tx : txs) {
            int giver = tx[0];
            int taker = tx[1];
            int amount = tx[2];
            debtMap[giver] -= amount;
            debtMap[taker] += amount;
        }

        vector<int> debts;
        for (auto [_, amount] : debtMap) {
            if (amount) debts.push_back(amount);
        }
        return debts;
    }
};
