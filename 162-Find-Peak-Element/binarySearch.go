package _62_Find_Peak_Element

import "fmt"

func findPeakElement(nums []int) int {
	n := len(nums)
	if n == 0 {
		return -1
	}
	l := 0
	r := n - 1

	for l < r {
		m := l + (r-l)/2
		fmt.Printf("nums %+v m %v, l %v, r %v\n", nums, m, l, r)
		if nums[m] > nums[m+1] {
			// go left to find the pick
			r = m
		} else {
			// nums[m] < nums[m+1]
			// go right to find the pick
			l = m + 1
		}
	}
	return l
}
