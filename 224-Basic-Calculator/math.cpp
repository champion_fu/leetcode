//
// Created by champ on 2022/8/12.
//

class Solution {
public:
    int calculate(string s) {
        int total = 0;
        vector<int> signs(2, 1);
        for (int i = 0; i < s.size(); i++) {
            char c = s[i];
            if (isdigit(s[i])) {
                int number = 0;
                while (i < s.size() && isdigit(s[i])) {
                    number = number*10 - '0' + s[i]; // minus first because overflow
                    i++;
                }
                total += signs.back() * number;
                signs.pop_back();
                i--; // while add extra 1, minus 1 here
            } else if (c == ')') {
                signs.pop_back();
            } else if (c != ' ') {
                signs.push_back(signs.back() * (c == '-' ? -1 : 1));
            }
        }
        return total;
    }
};

/*
 remaining   sign stack      total
3-(2+(9-4))   [1, 1]            0
 -(2+(9-4))   [1]               3
  (2+(9-4))   [1, -1]           3
   2+(9-4))   [1, -1, -1]       3
    +(9-4))   [1, -1]           1
     (9-4))   [1, -1, -1]       1
      9-4))   [1, -1, -1, -1]   1
       -4))   [1, -1, -1]      -8
        4))   [1, -1, -1, 1]   -8
         ))   [1, -1, -1]      -4
          )   [1, -1]          -4
              [1]              -4

*/
