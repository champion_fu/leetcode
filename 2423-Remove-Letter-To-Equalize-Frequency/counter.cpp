//
// Created by champ on 2022/10/19.
//

class Solution {
public:
    bool equalFrequency(string word) {
        unordered_map<char, int> m;
        for (char c : word) {
            m[c]++;
        }

        unordered_map<int, int> freqs;
        for (pair<char, int> mp : m) {
            // cout << " mp first "  << mp.first << " mp second " << mp.second << endl;
            freqs[mp.second]++;
        }
        // cout << "before freq size " << freqs.size() << endl;
        // for (pair<int, int> freq: freqs) {
        //     cout << " freq first " << freq.first << " second " << freq.second << endl;
        // }

        for (char c : word) {
            // cout << " m[c] " << m[c] << " freqs[m[c]] " << freqs[m[c]] << endl;
            int freq = m[c];
            freqs[freq]--; // times of freq --
            if (freq-1 != 0) freqs[freq-1]++; //  times of (freq-1) ++
            // cout << "freqs[freq] " << freqs[freq] << " size " << freqs.size() << endl;
            if (freqs[freq] == 0) freqs.erase(freq);
            if (freqs.size() == 1) return true;
            freqs[freq]++; // times of freq ++
            if (freq-1 != 0) freqs[freq-1]--; // times of (freq-1) --
            if (freqs[freq-1] == 0) freqs.erase(freq-1);
        }
        return false;
    }
};
