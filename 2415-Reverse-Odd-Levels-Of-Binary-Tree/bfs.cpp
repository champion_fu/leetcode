//
// Created by champ on 2022/9/21.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    TreeNode* reverseOddLevels(TreeNode* root) {
        if (!root) return root;

        queue<TreeNode*> q;
        vector<int> values;
        q.push(root);
        int level = 0;

        while(!q.empty()) {
            int sz = q.size();
            vector<int> tmp;
            for (int i = 0; i < sz; i++) {
                TreeNode* node = q.front(); q.pop();

                // odd level
                if (level%2) node->val = values[sz-i-1];

                if (node->left) {
                    q.push(node->left);
                    tmp.push_back(node->left->val);
                }

                if (node->right) {
                    q.push(node->right);
                    tmp.push_back(node->right->val);
                }
            }
            values = tmp;
            level++;
        }
        return root;
    }
};
