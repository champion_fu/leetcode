//
// Created by champ on 2022/9/21.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    TreeNode* reverseOddLevels(TreeNode* root) {
        queue<TreeNode*> q;
        q.push(root);
        vector<int> values(0);

        int l = 0;
        while (!q.empty()) {
            int n = (int)pow(double(2), double(l));
            stack<int> s;
            for (int i = 0; i < n; i++) {
                TreeNode* tmp = q.front();
                q.pop();
                if (l % 2 == 1) {
                    s.push(tmp->val);
                } else {
                    values.push_back(tmp->val);
                }
                if (tmp->left) q.push(tmp->left);
                if (tmp->right) q.push(tmp->right);
            }
            while(!s.empty()) {
                values.push_back(s.top());
                s.pop();
            }
            l++;
        }

        l = 1;
        root = new TreeNode(values[0]);
        q.push(root);
        int i = 1;
        while (i < values.size()) {
            int n = (int)pow(double(2), double(l));
            int count = i+n;
            for (; i < count; i+=2) {
                // cout << " i " << i << " n " << n << " l " << l << endl;
                TreeNode* lT = new TreeNode(values[i]);
                TreeNode* rT = new TreeNode(values[i+1]);
                TreeNode* tmp = q.front();
                q.pop();
                tmp->left = lT;
                tmp->right = rT;
                q.push(lT);
                q.push(rT);
            }
            l++;
        }
        while (!q.empty()) q.pop();

        return root;
    }
};
