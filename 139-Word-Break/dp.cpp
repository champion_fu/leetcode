//
// Created by champ on 2022/7/8.
//

class Solution {
public:
    bool wordBreak(string s, vector<string>& wordDict) {
        set<string> dict(wordDict.begin(), wordDict.end());
        int length = s.size();
        vector<bool> dp(length+1, false);
        dp[0] = true;

        for (int i = 1; i < length+1; i++) {
            for (int j = i-1; j >=0; j--) {
                if (dp[j]) {
                    string word = s.substr(j, i-j);
                    if (dict.find(word) != dict.end()) {
                        dp[i] = true;
                        break;
                    }
                }
            }
        }
        return dp[length];
    }
};
