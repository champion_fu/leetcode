//
// Created by champ on 2022/7/23.
//

class Solution {
public:
    string longestCommonPrefix(vector<string>& strs) {
        if (strs.size() <= 1) return strs[0];

        int length = INT_MAX;
        for (auto str : strs) {
            if (str.size() < length) {
                length = str.size();
            }
        }

        int i = 0;
        for (; i < length; i++) {
            char c = strs[0][i];
            for (int j = 1; j < strs.size(); j++) {
                if (c != strs[j][i]) {
                    cout << "i " << i << " " << strs[0].substr(0, i) << endl;
                    return strs[0].substr(0, i);
                }
            }
        }
        return strs[0].substr(0, i);
    }
};
