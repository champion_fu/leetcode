package _4_Largest_Rectangle_In_Histogram

var h []int

func largestRectangleArea(heights []int) int {
	h = heights
	return calArea(0, len(heights)-1)
}

func calArea(start, end int) int {
	if start > end {
		return 0
	}

	minHeightIndex := start
	for i := start; i < end+1; i++ {
		if h[minHeightIndex] > h[i] {
			minHeightIndex = i
		}
	}
	area := h[minHeightIndex] * (end - start + 1)
	areLeft := calArea(start, minHeightIndex-1)
	areRight := calArea(minHeightIndex+1, end)
	max := 0
	if area > max {
		max = area
	}
	if areLeft > max {
		max = areLeft
	}
	if areRight > max {
		max = areRight
	}
	return max
}
