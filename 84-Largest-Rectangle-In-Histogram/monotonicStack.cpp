//
// Created by champ on 2022/8/14.
//

class Solution {
public:
    int largestRectangleArea(vector<int>& heights) {
        stack<int> s;
        s.push(-1);
        int res = 0;
        for (int i = 0; i < heights.size(); i++) {
            while (s.top() != -1 && heights[s.top()] >= heights[i]) {
                // top is bigger than now, pop and calculate
                int h = heights[s.top()];
                s.pop();
                int w = i - s.top() - 1;
                res = max(res, h*w);
            }
            s.push(i);
        }

        while(s.top() != -1) {
            int h = heights[s.top()];
            s.pop();
            int w = heights.size() - s.top() - 1;
            res = max(res, h*w);
        }
        return res;

    }
};
