package _4_Largest_Rectangle_In_Histogram

func largestRectangleArea(heights []int) int {
	s := stack{}
	maxArea := 0

	for i := range heights {
		for !s.IsEmpty() && heights[s.Top()] >= heights[i] {
			// calculate area
			currentHeight := heights[s.Pop()]
			currentWidth := i - s.Top() - 1
			maxArea = max(maxArea, currentHeight*currentWidth)
		}
		// push the current index
		s.Push(i)
	}

	for !s.IsEmpty() {
		currentHeight := heights[s.Pop()]
		currentWidth := len(heights) - s.Top() - 1
		maxArea = max(maxArea, currentHeight*currentWidth)
	}
	return maxArea
}

func max(a, b int) int {
	if a >= b {
		return a
	}
	return b
}

type stack []int

func (s *stack) IsEmpty() bool {
	return len(*s) == 0
}

func (s *stack) Push(a int) {
	*s = append(*s, a)
}

func (s *stack) Top() int {
	if s.IsEmpty() {
		return -1 // default -1
	}
	i := len(*s) - 1
	e := (*s)[i]
	return e
}

func (s *stack) Pop() int {
	if s.IsEmpty() {
		return -1 // default -1
	}
	i := len(*s) - 1
	e := (*s)[i]
	(*s) = (*s)[:i]
	return e
}
