package _7_Letter_Combinations_Of_A_Phone_Number

var mapping = map[string]string{
	"2": "abc",
	"3": "def",
	"4": "ghi",
	"5": "jkl",
	"6": "mno",
	"7": "pqrs",
	"8": "tuv",
	"9": "wxyz",
}

var length int
var ret []string

func letterCombinations(digits string) []string {
	ret = make([]string, 0)
	if digits == "" {
		return ret
	}
	length = len(digits)
	backtracking("", digits)
	return ret
}

func backtracking(oneCandidate string, remainingDigits string) {
	if len(oneCandidate) == length {
		ret = append(ret, oneCandidate)
		return
	}

	candidates, matched := mapping[string(remainingDigits[0])]
	if !matched {
		return
	}
	for _, c := range candidates {
		oneCandidate = oneCandidate + string(c)
		backtracking(oneCandidate, remainingDigits[1:])
		oneCandidate = oneCandidate[:len(oneCandidate)-1]
	}
}
