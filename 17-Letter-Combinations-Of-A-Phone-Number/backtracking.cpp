//
// Created by champ on 2022/4/26.
//

class Solution {
public:
    unordered_map<char, vector<char>> _m {
            {'2', {'a', 'b', 'c'}},
            {'3', {'d', 'e', 'f'}},
            {'4', {'g', 'h', 'i'}},
            {'5', {'j', 'k', 'l'}},
            {'6', {'m', 'n', 'o'}},
            {'7', {'p', 'q', 'r', 's'}},
            {'8', {'t', 'u', 'v'}},
            {'9', {'w', 'x', 'y', 'z'}},
    };
    int length = 0;
    vector<string> letterCombinations(string digits) {
        vector<string> results;
        length = digits.size();
        if (length == 0) return results;

        backtracking(results, "", digits, 0);
        return results;
    }
private:
    // if string *digits then digits[index] => string
    void backtracking(vector<string>& results, string oneResult, string digits, int index) {
        if (oneResult.size() == length) {
            results.push_back(oneResult);
            return;
        }

        char c = digits[index];
        // cout << "c " << c << endl;
        vector<char> candidates = _m[c];
        for (auto candidate: candidates) {
            oneResult += candidate;
            backtracking(results, oneResult, digits, index+1);
            oneResult.pop_back();
        }
    }
};