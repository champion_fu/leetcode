package _38_Product_Of_Array_Except_Self

func productExceptSelf(nums []int) []int {
	// Initialize ans to all 1.
	ans := make([]int, len(nums))
	for i := 0; i < len(ans); i++ {
		ans[i] = 1
	}

	for i := 1; i < len(nums); i++ {
		ans[i] = ans[i-1] * nums[i-1] // Product left
	}
	right := 1
	for i := len(nums) - 1; i >= 0; i-- {
		ans[i] *= right
		right *= nums[i]
	}
	return ans
}
