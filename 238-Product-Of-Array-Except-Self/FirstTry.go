package _38_Product_Of_Array_Except_Self

func productExceptSelf(nums []int) []int {
	all := 1
	indexOfZero := -1

	// Initialize ans to zero
	ans := make([]int, len(nums))
	for i := 0; i < len(ans); i++ {
		ans[i] = 0
	}

	for i, value := range nums {
		if value == 0 && indexOfZero >= 0 {
			return ans
		} else if value == 0 && indexOfZero == -1 {
			all *= 1
			indexOfZero = i
		} else {
			all *= value
		}
	}

	if indexOfZero >= 0 {
		ans[indexOfZero] = all
	} else {
		for i := 0; i < len(ans); i++ {
			ans[i] = all / nums[i]
		}
	}
	return ans
}
