//
// Created by champ on 2022/9/28.
//

class Solution {
public:
    string reverseOnlyLetters(string s) {
        string res = "";
        int j = s.size() - 1;
        for (int i = 0; i < s.size(); i++) {
            if (isalpha(s[i])) {
                while (!isalpha(s[j])) {
                    j--;
                }
                res += s[j--];
            } else {
                res += s[i];
            }
        }
        return res;
    }
};
