package _6_Minimum_Window_Substring

func minWindow(s string, t string) string {
	if len(s) == 0 || len(t) == 0 {
		return ""
	}
	if len(s) < len(t) {
		return ""
	}

	// O(len(t))
	target := make(map[byte]int)
	for i, _ := range t {
		if v, m := target[t[i]]; m {
			target[t[i]] = v + 1
		} else {
			target[t[i]] = 1
		}
	}

	l := 0
	r := 0
	ret := ""
	valid := 0
	for r < len(s) {
		if _, m := target[s[r]]; !m {
			r++
			continue
		}
		target[s[r]]--
		if target[s[r]] == 0 {
			valid++
		}
		for valid == len(target) {
			// valid, means we find one candidate
			// first time
			if ret == "" {
				ret = s[l : r+1]
			}
			if len(ret) > len(s[l:r+1]) {
				ret = s[l : r+1]
			}
			if _, m := target[s[l]]; !m {
				l++
				continue
			}
			target[s[l]]++
			if target[s[l]] == 1 {
				valid--
			}
			l++
		}
		r++
	}
	return ret
}
