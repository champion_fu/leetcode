//
// Created by champ on 2022/8/11.
//

class Solution {
public:
    string minWindow(string s, string t) {
        if (t.size() == 0 || s.size() == 0) return "";
        vector<int> m(128, 0);
        int l = 0, r = 0, begin = 0;
        int d = INT_MAX;
        for (auto c : t) {
            m[c]++;
        }

        int count = t.size();
        while (r < s.size()) {
            if (m[s[r++]]-- > 0) count--;
            while (count == 0) {
                // valid
                if (r - l < d) {
                    d = r - l;
                    begin = l;
                }
                if (m[s[l++]]++ == 0) count++; // invalid
            }
        }

        return d == INT_MAX ? "" : s.substr(begin, d);
    }
};
