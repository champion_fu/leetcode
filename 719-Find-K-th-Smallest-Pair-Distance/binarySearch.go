package _19_Find_K_th_Smallest_Pair_Distance

import (
	"sort"
)

func smallestDistancePair(nums []int, k int) int {
	// sort O(NlogN)
	sort.Slice(
		nums,
		func(i, j int) bool {
			return nums[i] < nums[j]
		},
	)

	n := len(nums)
	l := 0
	r := nums[n-1] - nums[0]
	for l < r {
		m := l + (r-l)/2
		count := 0
		// solve f()
		for i := 0; i < n; i++ {
			// fmt.Printf("i %v: ", i)
			j := i + 1
			for j < n && nums[j]-nums[i] <= m {
				// fmt.Printf("j %v ", j)
				j++
			}
			// fmt.Printf("\ni %v, j %v, j-i-1 %v\n", i, j, j-i-1)
			count += j - i - 1
			// fmt.Printf("count %v\n", count)
		}

		if count < k {
			// means m is too small
			l = m + 1
		} else {
			// means m is too big, or m is not smallest
			r = m
		}
	}
	return l
}
