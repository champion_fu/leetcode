//
// Created by champ on 2022/4/18.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
    vector<TreeNode*> inorder;
public:
    TreeNode* inorderSuccessor(TreeNode* root, TreeNode* p) {
        recursive(root);
        for (int i = 0; i < inorder.size(); i++) {
            if (inorder[i] == p && i+1 != inorder.size()) {
                return inorder[i+1];
            }
        }
        return NULL;
    }
private:
    void recursive(TreeNode* n) {
        if (n->left) recursive(n->left);
        inorder.push_back(n);
        if (n->right) recursive(n->right);
    }
};
