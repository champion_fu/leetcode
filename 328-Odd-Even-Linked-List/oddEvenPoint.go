package _28_Odd_Even_Linked_List

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func oddEvenList(head *ListNode) *ListNode {
	if head == nil || head.Next == nil || head.Next.Next == nil {
		return head
	}

	oddPoint, evenPoint := head, head.Next
	evenHead := evenPoint

	for oddPoint != nil && evenPoint != nil && oddPoint.Next != nil && evenPoint.Next != nil {
		oddPoint.Next = oddPoint.Next.Next
		oddPoint = oddPoint.Next

		evenPoint.Next = evenPoint.Next.Next
		evenPoint = evenPoint.Next
	}

	oddPoint.Next = evenHead
	return head
}
