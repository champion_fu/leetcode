//
// Created by champ on 2022/8/8.
//

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* oddEvenList(ListNode* head) {
        if (head == NULL || head->next == NULL || head->next->next == NULL) return head;
        ListNode* odd = head;
        ListNode* evenHead = head->next;
        ListNode* even = evenHead;

        while (even->next && even->next->next) {
            odd->next = even->next;
            even->next = even->next->next;

            odd = odd->next;
            even = even->next;
        }
        if (even->next) {
            // cout << "even " << even->val << " even next " << even->next->val << endl;
            odd->next = even->next;
            // cout << "odd next " << odd->next->val << endl;
            odd = odd->next;
            even->next = NULL; // prevent endless linked list
        }
        // cout << odd->next << endl;
        odd->next = evenHead;

        return head;
    }
};
