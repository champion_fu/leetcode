//
// Created by champ on 2022/4/17.
//

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* oddEvenList(ListNode* head) {
        if (head == NULL || head->next == NULL || head->next->next == NULL) return head;

        ListNode* oddP = head;
        ListNode* evenP = head->next;

        ListNode* evenHead = evenP;

        while(oddP && evenP && oddP->next && evenP->next) {
            oddP->next = oddP->next->next;
            oddP = oddP->next;

            evenP->next = evenP->next->next;
            evenP = evenP->next;
        }

        oddP->next = evenHead;
        return head;
    }
};
