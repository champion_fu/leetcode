//
// Created by champ on 2022/8/2.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<int> postorderTraversal(TreeNode* root) {
        vector<int> res;
        stack<TreeNode*> s;
        TreeNode* last = NULL;
        TreeNode* curr = root;

        while (curr != NULL || !s.empty()) {
            while (curr != NULL) {
                s.push(curr);
                curr = curr->left;
            }
            TreeNode* p = s.top();
            if (p->right == NULL || p->right == last) {
                // no right or right has been processed
                res.push_back(p->val);
                s.pop();
                last = p;
            }
            if (p->right != NULL && p != last) {
                // right has something and didn't process
                curr = p->right;
            }
        }
        return res;
    }
};
