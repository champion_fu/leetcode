package _45_Binary_Tree_Postorder_Traversal

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func postorderTraversal(root *TreeNode) []int {
	ret := make([]int, 0)
	if root == nil {
		return ret
	}
	var last *TreeNode

	s := stack{}
	cur := root

	for cur != nil || !s.IsEmpty() {
		for cur != nil {
			s.Push(cur)
			cur = cur.Left
		}
		top := s.Top()
		if top.Right == nil || top.Right == last {
			ret = append(ret, top.Val)
			last = s.Pop()
		}
		if top.Right != nil && top != last {
			cur = top.Right
		}
	}

	return ret
}

type stack []*TreeNode

func (s *stack) IsEmpty() bool {
	return len(*s) == 0
}

func (s *stack) Push(a *TreeNode) {
	*s = append(*s, a)
}

func (s *stack) Pop() *TreeNode {
	if s.IsEmpty() {
		return nil
	} else {
		index := len(*s) - 1
		element := (*s)[index]
		*s = (*s)[:index]
		return element
	}
}

func (s *stack) Top() *TreeNode {
	if s.IsEmpty() {
		return nil
	} else {
		index := len(*s) - 1
		return (*s)[index]
	}
}
