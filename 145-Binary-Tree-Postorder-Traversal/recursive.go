package _45_Binary_Tree_Postorder_Traversal

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
var ret []int

func postorderTraversal(root *TreeNode) []int {
	ret = make([]int, 0)
	recursive(root)
	return ret
}

func recursive(root *TreeNode) {
	if root == nil {
		return
	}
	if root.Left != nil {
		recursive(root.Left)
	}
	if root.Right != nil {
		recursive(root.Right)
	}
	ret = append(ret, root.Val)
}
