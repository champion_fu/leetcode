package _03_Remove_Linked_List_Elements

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func removeElements(head *ListNode, val int) *ListNode {
	if head == nil {
		return nil
	}
	dummyHead := &ListNode{Val: -1, Next: head}

	p := head
	preNode := dummyHead
	for p != nil {
		if p.Val == val {
			preNode.Next = p.Next
		} else {
			preNode = p
		}
		p = p.Next
	}
	return dummyHead.Next
}
