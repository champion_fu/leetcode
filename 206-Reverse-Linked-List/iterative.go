package _06_Reverse_Linked_List

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func reverseList(head *ListNode) *ListNode {
	if head == nil {
		return head
	}

	curHead := head
	for head.Next != nil {
		curNext := head.Next.Next
		head.Next.Next = curHead

		curHead = head.Next
		head.Next = curNext
	}
	return curHead
}
