//
// Created by champ on 2022/8/7.
//

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* swapPairs(ListNode* head) {
        if (head == NULL || head->next == NULL) return  head;

        ListNode* dummyHead = new ListNode(-1, head);
        ListNode* cur = dummyHead;
        while (cur->next != NULL && cur->next->next != NULL) {
            ListNode* first = cur->next;
            ListNode* second = first->next;

            ListNode* three = second->next;
            second->next = first;
            first->next = three;
            cur->next = second;
            cur = first;
        }

        return dummyHead->next;
    }
};
