package _4_Swap_Nodes_In_Pairs

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func swapPairs(head *ListNode) *ListNode {
	return recursiveSwapTwo(head)
}

func recursiveSwapTwo(n *ListNode) *ListNode {
	if n == nil || n.Next == nil {
		return n
	}
	nextRoundNode := n.Next.Next

	// Swap two
	next := n.Next
	next.Next = n
	n.Next = recursiveSwapTwo(nextRoundNode)
	return next
}
