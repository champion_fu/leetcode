package _4_Swap_Nodes_In_Pairs

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func swapPairs(head *ListNode) *ListNode {
	if head == nil || head.Next == nil {
		return head
	}

	dummyHead := &ListNode{0, head}
	current := dummyHead
	for current.Next != nil && current.Next.Next != nil {
		first := current.Next
		second := first.Next

		nextRound := second.Next
		second.Next = first
		first.Next = nextRound
		current.Next = second
		current = first // or current = current.Next.Next
	}
	return dummyHead.Next
}
