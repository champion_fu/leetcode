//
// Created by champ on 2022/4/21.
//

class DetectSquares {
public:
    int cnt[1001][1001] = {};
    vector<pair<int, int>> points;
    DetectSquares() {

    }

    void add(vector<int> point) {
        int x = point[0], y = point[1];
        cnt[x][y]++;
        points.push_back(pair<int, int>(x, y));
    }

    int count(vector<int> point) {
        int x1 = point[0], y1 = point[1];
        int ans = 0;
        for (auto [x3, y3] : points) {
            if (x1 == x3 || abs(x1-x3) != abs(y1-y3)) continue;
            ans += cnt[x1][y3] * cnt[x3][y1];
        }
        return ans;
    }
};
