package _6_3Sum_Closest

import (
	"math"
	"sort"
)

func threeSumClosest(nums []int, target int) int {
	n := len(nums)
	sort.Slice(
		nums,
		func(i, j int) bool {
			return nums[i] < nums[j]
		},
	)

	diff := math.MaxInt32
	for i, v := range nums {
		l := i + 1
		r := n - 1
		for l < r {
			sum := nums[l] + nums[r] + v
			if abs(target-sum) < abs(diff) {
				diff = target - sum
			}
			if sum < target {
				l++
			} else {
				r--
			}
		}
		if diff == 0 {
			return target - diff
		}
	}
	return target - diff
}

func abs(i int) int {
	if i < 0 {
		return -i
	}
	return i
}
