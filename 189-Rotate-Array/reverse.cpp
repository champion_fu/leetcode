//
// Created by champ on 2022/8/8.
//

// 1. use another O(N) space to do it on O(N) time
// 2. reverse -> c++ reverse linear in half the distance between first and last

class Solution {
public:
    void rotate(vector<int>& nums, int k) {
        k = k % nums.size();

        reverse(nums.begin(), nums.end());
        reverse(nums.begin(), nums.begin()+k);
        reverse(nums.begin()+k, nums.end());
    }
};
