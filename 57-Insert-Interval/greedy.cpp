//
// Created by champ on 2022/7/23.
//

class Solution {
public:
    vector<vector<int>> insert(vector<vector<int>>& intervals, vector<int>& newInterval) {
        vector<vector<int>> res;
        int i = 0;

        while (i < intervals.size() && intervals[i][1] < newInterval[0]) {
            // cur end less than newInterval.start
            res.push_back(intervals[i]);
            i++;
        }

        // cur end bigger than newInterval.start
        while (i < intervals.size() && intervals[i][0] <= newInterval[1]) {
            // cur start smaller than newInterval.end, update start and end
            newInterval[0] = min(newInterval[0], intervals[i][0]);
            newInterval[1] = max(newInterval[1], intervals[i][1]);
            i++;
        }

        res.push_back(newInterval);

        while (i < intervals.size()) {
            res.push_back(intervals[i]);
            i++;
        }
        return res;
    }
};
