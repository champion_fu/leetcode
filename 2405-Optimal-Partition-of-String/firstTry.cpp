//
// Created by champ on 2022/9/23.
//

class Solution {
public:
    int partitionString(string s) {
        int ans = 1;

        int i = 0;
        int alpha[26] = {};
        while (i < s.size()) {
            // cout << "s[i] " << s[i] << " alpha " << alpha[s[i]-'a'] << endl;
            alpha[s[i] - 'a']++;
            if (alpha[s[i] - 'a'] != 1) {
                memset(alpha, 0, 26 * sizeof(int));
                ans++;
                alpha[s[i]-'a'] = 1;
            }
            i++;
        }
        return ans;
    }
};
