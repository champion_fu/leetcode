//
// Created by champ on 2022/5/14.
//

/*
// Definition for a Node.
class Node {
public:
    int val;
    Node* left;
    Node* right;
    Node* parent;
};
*/

class Solution {
public:
    Node* lowestCommonAncestor(Node* p, Node * q) {
        // this is variation of intersection of two linked lists
        Node* a = p;
        Node* b = q;

        while (a != b) {
            a = a->parent == NULL ? q : a->parent;
            b = b->parent == NULL ? p : b->parent;
        }
        return a;
    }
};
