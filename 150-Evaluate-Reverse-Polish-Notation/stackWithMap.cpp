//
// Created by champ on 2022/4/19.
//

class Solution {
public:
    int evalRPN(vector<string>& tokens) {
        stack<int> s;
        unordered_map<string, function<int (int, int) >> op = {
                {"+", [] (int a, int b) {return a + b; }},
                {"-", [] (int a, int b) {return a - b; }},
                {"*", [] (int a, int b) {return a * b; }},
                {"/", [] (int a, int b) {return a / b; }},
        };

        for (auto token : tokens) {
            if (!op.count(token)) {
                s.push(stoi(token));
            } else {
                int op1 = s.top();
                s.pop();
                int op2 = s.top();
                s.pop();

                s.push(op[token](op2, op1));
            }
        }
        return s.top();
    }
};
