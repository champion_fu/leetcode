package _50_Evaluate_Reverse_Polish_Notation

import "strconv"

var operationMap = map[string]string{
	"+": "+",
	"-": "-",
	"/": "/",
	"*": "*",
}

func evalRPN(tokens []string) int {
	s := stack{}
	i := 0
	val, _ := strconv.Atoi(tokens[i])
	s.Push(val)
	i++

	n := len(tokens)
	for i < n {
		if v, m := operationMap[tokens[i]]; m {
			// pop and calculate and push back
			// pop
			val1 := s.Pop()
			val2 := s.Pop()

			// calculate
			var pushBackValue int
			switch v {
			case "+":
				pushBackValue = val2 + val1
			case "-":
				pushBackValue = val2 - val1
			case "*":
				pushBackValue = val2 * val1
			case "/":
				pushBackValue = val2 / val1
			default:
			}

			// push back
			s.Push(pushBackValue)
		} else {
			// push number into stack
			val, _ := strconv.Atoi(tokens[i])
			s.Push(val)
		}
		i++
	}
	return s.Pop()
}

type stack []int

func (s *stack) IsEmpty() bool {
	return len(*s) == 0
}

func (s *stack) Push(a int) {
	*s = append(*s, a)
}

func (s *stack) Pop() int {
	length := len(*s)
	if length == 0 {
		return -1
	}
	e := (*s)[length-1]
	(*s) = (*s)[:length-1]
	return e
}
