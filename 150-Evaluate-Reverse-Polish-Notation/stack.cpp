//
// Created by champ on 2022/4/19.
//

class Solution {
public:
    int evalRPN(vector<string>& tokens) {
        stack<int> s;
        unordered_map<string, bool> op = {
                {"+", true},
                {"-", true},
                {"*", true},
                {"/", true},
        };

        for (auto token : tokens) {
            if (!op.count(token)) {
                s.push(stoi(token));
            } else {
                int op1 = s.top();
                s.pop();
                int op2 = s.top();
                s.pop();

                int next = 0;
                if (token == "+") {
                    next = op2 + op1;
                } else if (token == "-") {
                    next = op2 - op1;
                } else if (token == "*") {
                    next = op2 * op1;
                } else {
                    next = op2 / op1;
                }
                s.push(next);
            }
        }
        return s.top();
    }
};
