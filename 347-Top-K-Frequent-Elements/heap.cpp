//
// Created by champ on 2022/4/28.
//

class Solution {
public:
    vector<int> topKFrequent(vector<int>& nums, int k) {
        if (k == nums.size()) {
            return nums;
        }

        map<int, int> m;

        for (auto num : nums) {
            m[num]++;
        }

        priority_queue<pair<int, int>, vector<pair<int, int>>> heap;
        for (auto p : m) {
            heap.push(pair<int, int> {p.second, p.first});
        }

        vector<int> results(0);
        while (k--){
            results.push_back(heap.top().second);
            heap.pop();
        }
        return results;
    }
};
