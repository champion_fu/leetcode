package _47_Top_K_Frequent_Elements

import (
	"container/heap"
)

func topKFrequent(nums []int, k int) []int {
	if len(nums) == k {
		return nums
	}

	p := make(pq, 0)
	heap.Init(&p)

	frequent := make(map[int]int)
	for _, v := range nums {
		frequent[v] += 1
	}

	for value, freq := range frequent {
		heap.Push(&p, &element{value: value, priority: freq})
	}

	ret := make([]int, 0)
	for i := 0; i < k; i++ {
		ret = append(ret, heap.Pop(&p).(*element).value)
	}
	return ret
}

type element struct {
	value    int
	priority int
	index    int
}
type pq []*element

func (h pq) Len() int {
	return len(h)
}
func (h pq) Less(i, j int) bool {
	return h[i].priority > h[j].priority
}
func (h pq) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
	h[i].index = i
	h[j].index = j
}
func (h *pq) Push(x interface{}) {
	n := len(*h)
	item := x.(*element)
	item.index = n
	*h = append(*h, item)
}
func (h *pq) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	x.index = -1
	*h = old[:n-1]
	return x
}
