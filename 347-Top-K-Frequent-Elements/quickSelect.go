package _47_Top_K_Frequent_Elements

import (
	"fmt"
	"math/rand"
)

// nums: [1, 2, 1, 3, 2, 1] k = 2
// sorted [1, 2, 3]  return sorted[:k]

var frequency map[int]int
var sorted []int

func topKFrequent(nums []int, k int) []int {
	frequency = make(map[int]int)
	sorted = make([]int, 0)
	for _, v := range nums {
		if _, m := frequency[v]; !m {
			sorted = append(sorted, v)
			frequency[v] = 1
		} else {
			frequency[v]++
		}
	}
	kSelect(0, len(sorted)-1, k)
	fmt.Printf("sorted %+v\n", sorted)
	return sorted[:k]
}

func kSelect(left, right, kIndex int) {
	if left >= right {
		return
	}
	pIndex := rand.Intn(right-left+1) + left
	pIndex = partition(left, right, pIndex)
	if pIndex == kIndex {
		return
	} else if pIndex > kIndex {
		kSelect(left, pIndex-1, kIndex)
	}
	kSelect(pIndex+1, right, kIndex)
}

func partition(left, right, pIndex int) int {
	pivot := sorted[pIndex]
	// move pivot to the end
	swap(pIndex, right)

	// move all more frequency elements to the left
	i := left
	for j := left; j < right; j++ {
		if frequency[pivot] < frequency[sorted[j]] {
			swap(i, j)
			i++
		}
	}

	// swap pivot to the final index
	swap(right, i)
	return i
}

func swap(i, j int) {
	sorted[i], sorted[j] = sorted[j], sorted[i]
}
