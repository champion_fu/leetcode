package _72_Factorial_Trailing_Zeroes

// n == 3 => 1*2*3, 0
// n == 4 => 1*2*3*4, 0
// n == 5 => 1*2*3*4*5, 1
// always 2 is more than 5, only care 5
func trailingZeroes(n int) int {
	ret := 0
	for n > 0 {
		n = n / 5 // log(n)
		ret += n
	}
	return ret
}
