//
// Created by champ on 2022/8/4.
//

class Solution {
    vector<pair<int, int>> dirs {
            {0, 1},
            {1, 0},
            {-1, 0},
            {0, -1},
    };
    int m;
    int n;
public:
    vector<vector<int>> pacificAtlantic(vector<vector<int>>& heights) {
        m = heights.size();
        n = heights[0].size();
        vector<vector<int>> res;

        vector<vector<bool>> pacificReachable(m, vector<bool>(n, 0));
        vector<vector<bool>> atlanticReachable(m, vector<bool>(n, 0));

        for (int i = 0; i < m; i++) {
            dfs(i, 0, pacificReachable, heights);
            dfs(i, n-1, atlanticReachable, heights);
        }

        for (int i = 0; i < n; i++) {
            dfs(0, i, pacificReachable, heights);
            dfs(m-1, i, atlanticReachable, heights);
        }

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (pacificReachable[i][j] && atlanticReachable[i][j]) {
                    res.push_back({i, j});
                }
            }
        }
        return res;
    }
private:
    void dfs(int row, int col, vector<vector<bool>>& reachable, vector<vector<int>>& heights) {
        reachable[row][col] = true;
        for (pair<int, int> dir : dirs) {
            int newRow = row + dir.first;
            int newCol = col + dir.second;

            if (newRow < 0 || newRow >= m || newCol < 0 || newCol >= n) continue;
            if (reachable[newRow][newCol]) continue;

            // Check that the new cell has a higher or equal height,
            // So that water can flow from the new cell to the old cell
            if (heights[newRow][newCol] < heights[row][col]) continue;

            dfs(newRow, newCol, reachable, heights);
        }
    }
};
