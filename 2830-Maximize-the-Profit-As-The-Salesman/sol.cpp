class Solution {
public:
    int maximizeTheProfit(int n, vector<vector<int>>& offers) {
        vector<int> dp(n+1); // zero index is only for esaily calculate
        vector<vector<vector<int>>> m(n); // store the offer by end
        for (auto& offer : offers) {
            m[offer[1]].push_back(offer);
        }

        // e -1 == index of house
        for (int e = 1; e <= n; e++) {
            dp[e] = dp[e - 1];
            for (auto& offer: m[e-1]) {
                dp[e] = max(dp[e], (dp[offer[0]] + offer[2]));
            }
        }
        return dp[n];
    }
}
