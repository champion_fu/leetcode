package _66_Fraction_To_Recurring_Decimal

import "strconv"

func fractionToDecimal(numerator int, denominator int) string {
	negative := 1
	if numerator == 0 {
		return "0"
	}
	if numerator < 0 {
		numerator = -numerator
		negative *= -1
	}
	if denominator < 0 {
		denominator = -denominator
		negative *= -1
	}

	r := numerator / denominator
	numerator -= r * denominator
	if numerator == 0 {
		// mod == 0
		if negative < 0 {
			return "-" + strconv.Itoa(r)
		}
		return strconv.Itoa(r)
	}

	var prefix string
	if negative < 0 {
		prefix = "-" + strconv.Itoa(r) + "."
	} else {
		prefix = strconv.Itoa(r) + "."
	}
	return prefix + frac("", make(map[int]int), numerator, denominator)
}

func frac(str string, valInx map[int]int, numerator int, denominator int) string {
	if numerator == 0 {
		return str
	}

	if idx, m := valInx[numerator]; m {
		// recursive
		return str[:idx] + "(" + str[idx:] + ")"
	}
	valInx[numerator] = len(str)

	numerator *= 10
	r := numerator / denominator
	val := strconv.Itoa(r)
	numerator -= r * denominator
	return frac(str+val, valInx, numerator, denominator)
}
