//
// Created by champ on 2022/4/19.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    string getDirections(TreeNode* root, int startValue, int destValue) {
        string startPath = "";
        string destPath = "";
        find(root, startValue, startPath);
        find(root, destValue, destPath);
        cout << startPath << endl;
        cout << destPath << endl;
        while (!startPath.empty() && !destPath.empty() && startPath.back() == destPath.back()) {
            startPath.pop_back();
            destPath.pop_back();
        }

        return string(startPath.size(), 'U') + string(rbegin(destPath), rend(destPath));
    }
private:
    // path store the path from root to target with inverse order
    bool find(TreeNode* node, int target, string &path) {
        if (node->val == target) {
            return true;
        }
        if (node->left && find(node->left, target, path)) {
            path.push_back('L');
        } else if (node->right && find(node->right, target, path)) {
            path.push_back('R');
        }
        return !path.empty();
    }
};
