//
// Created by champ on 2022/9/9.
//

class Solution {
public:
    int maxProfit(int k, vector<int>& prices) {
        int length = prices.size();
        if (k >= length / 2) return quickSolve(prices);

        vector<int> buy(k+1, INT_MAX);
        vector<int> sell(k+1, 0);

        for (int& p : prices) {
            for (int i = 1; i <= k; i++) {
                // p = 3 - 0 = buy i
                buy[i] = min(buy[i], p-sell[i-1]);
                // p = 6 - 4 = sell i
                sell[i] = max(sell[i], p-buy[i]);
            }
        }
        return sell[k];
    }
private:
    int quickSolve(vector<int>& prices) {
        int profit = 0;
        for (int i = 1; i < prices.size(); i++) {
            int diff = prices[i] - prices[i-1];
            profit += diff > 0 ? diff : 0;
        }
        return profit;
    }
};
