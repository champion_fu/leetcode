//
// Created by champ on 2022/4/14.
//

class Solution {
public:
    int maxProfit(int k, vector<int>& prices) {
        int length = prices.size();
        if (k > length / 2) {
            return quickSolve(prices);
        }

        vector<vector<int>> dp(k+1, vector<int> (length, 0)); // [k+1][length]

        for (int i = 1; i < k+1; i++) {
            int curMax = -prices[0];
            for (int j = 1; j < length; j++) {
                dp[i][j] = max(dp[i][j-1], curMax+prices[j]); // max(self, sell stock)
                curMax = max(curMax, dp[i-1][j-1]-prices[j]); // max(self, last time (k time) remaining [i-1][j-1]
            }
        }
        return dp[k][length-1];
    }
private:
    int quickSolve(vector<int>& prices) {
        int profit = 0;

        for (int i = 1; i < prices.size(); i++) {
            if (prices[i] - prices[i-1] > 0) {
                profit += (prices[i] - prices[i-1]);
            }
        }
        return profit;
    }
};
