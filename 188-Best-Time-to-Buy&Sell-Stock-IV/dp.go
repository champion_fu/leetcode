package _88_Best_Time_to_Buy_Sell_Stock_IV

var length int

func maxProfit(k int, prices []int) int {
	length = len(prices)
	if k >= length/2 {
		// we can un-limit buy and sell
		return quickSolve(prices)
	}

	// DP: t(i,j) is the max profit for up to i transactions by time j (0<=i<=K, 0<=j<=T).
	dp := make([][]int, k+1)
	for i := 0; i < k+1; i++ {
		dp[i] = make([]int, length)
	}

	for i := 1; i <= k; i++ {
		curMax := -prices[0] // curMax of i index(round)
		for j := 1; j < length; j++ {
			dp[i][j] = max(dp[i][j-1], prices[j]+curMax)
			curMax = max(curMax, dp[i-1][j-1]-prices[j])
		}
	}
	return dp[k][length-1]
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}

func quickSolve(prices []int) int {
	profit := 0

	for i := 1; i < length; i++ {
		if prices[i] > prices[i-1] {
			profit += prices[i] - prices[i-1]
		}
	}
	return profit
}
