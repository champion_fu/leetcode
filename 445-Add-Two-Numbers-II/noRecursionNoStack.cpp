//
// Created by champ on 2022/8/1.
//

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        int s1 = getLinkedListLength(l1);
        int s2 = getLinkedListLength(l2);

        ListNode* head = NULL;

        // add number but without handle carry
        while (l1 || l2) {
            int v1 = 0;
            int v2 = 0;
            if (s1 >= s2) {
                v1 = l1 ? l1->val : 0;
                s1--;
                l1 = l1->next;
            }
            if (s2 >= s1 + 1) {
                v2 = l2 ? l2->val : 0;
                s2--;
                l2 = l2->next;
            }
            ListNode* newOne = new ListNode(v1 + v2);
            newOne->next = head;
            head = newOne;
        }

        int carry = 0;
        ListNode* n = NULL;
        while (head) {
            head->val += carry;
            if (head->val >= 10) {
                head->val = head->val % 10;
                carry = 1;
            } else {
                carry = 0;
            }
            ListNode* buf = head->next;
            head->next = n;
            n = head;
            head = buf;
        }

        if (carry) {
            ListNode* newOne = new ListNode(carry);
            newOne->next = n;
            n = newOne;
        }
        return n;
    }
private:
    int getLinkedListLength(ListNode* node) {
        if (node == NULL) return 0;
        return getLinkedListLength(node->next) + 1;
    }
};
