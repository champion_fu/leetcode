package _45_Add_Two_Numbers_II

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	s1 := stack{}
	s2 := stack{}

	for l1 != nil {
		s1.Push(l1.Val)
		l1 = l1.Next
	}
	for l2 != nil {
		s2.Push(l2.Val)
		l2 = l2.Next
	}

	carry := 0

	dummyHead := &ListNode{Val: -1}
	for !s1.IsEmpty() || !s2.IsEmpty() || carry != 0 {
		v := carry
		if !s1.IsEmpty() {
			v += s1.Pop()
		}
		if !s2.IsEmpty() {
			v += s2.Pop()
		}
		carry = v / 10
		if dummyHead.Val == -1 {
			// firstTime
			dummyHead.Val = v % 10
		} else {
			node := &ListNode{
				Val:  v % 10,
				Next: dummyHead,
			}
			dummyHead = node
		}
	}
	return dummyHead
}

type stack []int

func (s stack) IsEmpty() bool {
	return len(s) == 0
}
func (s *stack) Push(a int) {
	*s = append(*s, a)
}
func (s *stack) Pop() int {
	n := len(*s)
	e := (*s)[n-1]
	*s = (*s)[:n-1]
	return e
}
