package _77_Squares_Of_A_Sorted_Array

func sortedSquares(nums []int) []int {
	ret := make([]int, len(nums))
	left, right := 0, len(nums)-1
	square := 0
	for i := right; i > -1; i-- {
		if abs(nums[left]) > abs(nums[right]) {
			square = nums[left]
			left += 1
		} else {
			square = nums[right]
			right -= 1
		}
		ret[i] = square * square
	}
	return ret
}

func abs(n int) int {
	if n > 0 {
		return n
	}
	return -n
}
