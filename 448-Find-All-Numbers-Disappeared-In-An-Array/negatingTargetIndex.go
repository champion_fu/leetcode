package _48_Find_All_Numbers_Disappeared_In_An_Array

func findDisappearedNumbers(nums []int) []int {
	duplication := 0
	for i := 0; i < len(nums); i++ {
		abstractIndex := abs(nums[i]) - 1
		if nums[abstractIndex] < 0 {
			duplication++
			continue
		} else {
			nums[abstractIndex] = -nums[abstractIndex]
		}
	}

	ret := make([]int, 0)
	for i, _ := range nums {
		if nums[i] < 0 {
			continue
		} else {
			ret = append(ret, i+1)
		}
	}
	return ret
}

func abs(n int) int {
	if n < 0 {
		return -n
	}
	return n
}
