package _089_Duplicate_Zeros

func duplicateZeros(arr []int) {
	size := len(arr)
	copy_from_index := 0
	edge_case := false
	for i, v := range arr {
		if v != 0 {
			size -= 1
		} else {
			size -= 2
		}
		if size <= 0 {
			copy_from_index = i
			if size < 0 {
				edge_case = true
			}
			break
		}
	}

	for copy_to_index := len(arr) - 1; copy_to_index > -1; copy_to_index-- {
		if arr[copy_from_index] == 0 && edge_case {
			arr[copy_to_index] = 0
			edge_case = false
		} else if arr[copy_from_index] == 0 && !edge_case {
			arr[copy_to_index] = 0
			copy_to_index -= 1
			if copy_to_index < 0 {
				break
			}
			arr[copy_to_index] = 0
		} else {
			arr[copy_to_index] = arr[copy_from_index]
		}
		copy_from_index -= 1
	}
}
