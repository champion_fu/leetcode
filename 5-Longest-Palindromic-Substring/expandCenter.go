package __Longest_Palindromic_Substring

func longestPalindrome(s string) string {
	left, maxLength := 0, 0
	n := len(s)
	if n < 2 {
		return s
	}

	for i := 0; i < n; i++ {
		expand(s, i, i, &left, &maxLength)
		expand(s, i, i+1, &left, &maxLength)
	}

	return s[left : left+maxLength]
}

func expand(s string, indexLeft, indexRight int, left, maxLength *int) {
	for indexLeft >= 0 && indexRight <= len(s)-1 && s[indexLeft] == s[indexRight] {
		indexLeft--
		indexRight++
	}
	if (indexRight - indexLeft - 1) > *maxLength {
		*maxLength = indexRight - indexLeft - 1
		*left = indexLeft + 1
	}
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}
