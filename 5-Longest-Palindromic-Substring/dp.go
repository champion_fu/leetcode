package __Longest_Palindromic_Substring

func longestPalindrome(s string) string {
	n := len(s)
	if n < 2 {
		return s
	}

	var ret string
	dp := make([][]bool, n)
	for i := 0; i < n; i++ {
		one := make([]bool, n)
		dp[i] = one
		dp[i][i] = true
		ret = s[i : i+1]
	}

	for i := 0; i < n; i++ {
		if i+1 > n-1 {
			continue
		}
		if s[i] == s[i+1] {
			dp[i][i+1] = true
		}
		if dp[i][i+1] {
			ret = s[i : i+2]
		}
	}

	for i := n; i >= 0; i-- {
		for j := i + 2; j < n; j++ {
			if s[i] == s[j] && dp[i+1][j-1] {
				dp[i][j] = true
			}
			if dp[i][j] && j-i+1 > len(ret) {
				ret = s[i : j+1]
			}
		}
	}
	return ret
}
