//
// Created by champ on 2022/7/31.
//

class Solution {
    int size;
    string res;
public:
    string longestPalindrome(string s) {
        size = s.size();
        if (size <= 1) return s;

        vector<vector<bool>> dp(size, vector<bool>(size));

        for (int i = 0; i < size; i++) {
            dp[i][i] = true;
            res = s.substr(i, 1);
        }

        for (int i = 0; i < size; i++) {
            if (i+1 > size-1) {
                continue;
            }
            if (s[i] == s[i+1]) {
                dp[i][i+1] = true;
            }
            if (dp[i][i+1]) {
                res = s.substr(i, 2);
            }
        }

        for (int i = size; i >= 0; i--) {
            for (int j = i+2; j < size; j++) {
                if (s[i] == s[j] && dp[i+1][j-1]) {
                    // i == 1, j == 3, dp[2][2] == true
                    dp[i][j] = true;
                }
                if (dp[i][j] && j-i+1 > res.size()) {
                    res = s.substr(i, j-i+1);
                }
            }
        }
        return res;
    }
};
