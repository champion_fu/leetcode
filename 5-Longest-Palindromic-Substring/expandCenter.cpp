//
// Created by champ on 2022/4/13.
//

class Solution {
public:
    int leftIndex = 0;
    int length = 0;
    int size = 0;
    string longestPalindrome(string s) {

        size = s.size();
        if (size <= 1) {
            return s;
        }

        for (int i = 0; i < size; i++) {
            expand(s, i, i);
            expand(s, i, i+1);
        }
        return s.substr(leftIndex, length);
    }
private:
    void expand(string s, int l, int r) {
        while(l >= 0 && r < size && s[l] == s[r]) {
            l--;
            r++;
        }
        if ((r-l-1) > length) {
            length = r-l-1;
            leftIndex = l+1;
        }
    }
};
