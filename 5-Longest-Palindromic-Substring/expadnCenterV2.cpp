//
// Created by champ on 2022/7/31.
//

class Solution {
    int size;
    string res;
public:
    string longestPalindrome(string s) {
        size = s.size();
        if (size <= 1) return s;

        for (int i = 0; i < size; i++) {
            expand(s, i, i+1);
            expand(s, i, i);
        }

        return res;
    }
private:
    void expand(string s, int l, int r) {
        while (l >= 0 && r < size && s[l] == s[r]) {
            l--;
            r++;
        }
        if ((r-l-1) > res.size()) {
            res = s.substr(l+1, r-l-1);
        }
    }
};
