package _7_Combinations

var ret [][]int
var number int
var length int

func combine(n int, k int) [][]int {
	number = n
	length = k
	ret = make([][]int, 0)
	first := make([]int, 0)
	backtracking(first, 1)
	return ret
}

func backtracking(oneCandidate []int, start int) {
	if len(oneCandidate) == length {
		// make sure we have deep copy
		var r = make([]int, len(oneCandidate))
		copy(r, oneCandidate)
		ret = append(ret, r)
		return
	}
	// go through from start to n
	for i := start; i <= number; i++ {
		// append the value
		oneCandidate = append(oneCandidate, i)
		backtracking(oneCandidate, i+1)
		oneCandidate = oneCandidate[:len(oneCandidate)-1]
	}
}
