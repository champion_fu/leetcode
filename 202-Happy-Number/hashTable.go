package _02_Happy_Number

import "strconv"

func isHappy(n int) bool {
	hashTable := make(map[int]bool)
	value := calculate(n)
	for value != 1 {
		if _, m := hashTable[value]; m {
			return false
		}
		hashTable[value] = true
		value = calculate(value)
	}
	return true
}

func calculate(n int) int {
	numbers := strconv.Itoa(n)
	ret := 0
	for _, v := range numbers {
		value := int(v - '0')
		ret += value * value
	}
	return ret
}
