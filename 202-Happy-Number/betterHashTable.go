package _02_Happy_Number

func isHappy(n int) bool {
	hashTable := make(map[int]bool)
	for n != 1 {
		if _, m := hashTable[n]; m {
			return false
		}
		hashTable[n] = true
		n = calculate(n)
	}
	return true
}

func calculate(n int) int {
	ret := 0
	for n > 0 {
		d := n % 10
		n = n / 10
		ret += d * d
	}
	return ret
}
