//
// Created by champ on 2022/9/6.
//

class Solution {
    unordered_map<string, string> parents;
public:
    string findSmallestRegion(vector<vector<string>>& regions, string region1, string region2) {
        for (const vector<string>& region : regions) {
            string parent = region[0];
            for (int i = 1; i < region.size(); i++)
                storeParents(parent, region[i]);
        }

        unordered_map<string, bool> visited;
        while (region1 != "") {
            visited[region1] = true;
            if (parents.count(region1) > 0) {
                // there is a parent
                region1 = parents[region1];
            } else {
                region1 = "";
            }
        }

        bool visit = visited[region2];
        while (!visit) {
            region2 = parents[region2];
            visit = visited[region2];
        }
        return region2;
    }
private:
    void storeParents(string parent, string children) {
        parents[children] = parent;
    }
};
