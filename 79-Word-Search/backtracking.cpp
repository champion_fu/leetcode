//
// Created by champ on 2022/4/27.
//

class Solution {
    int m, n;
    vector<int> dirRow {-1, 1, 0, 0};
    vector<int> dirCol {0, 0, -1, 1};
public:
    bool exist(vector<vector<char>>& board, string word) {
        m = board.size();
        n = board[0].size();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                // from each
                if (backtracking(board, i, j, word)) return true;
            }
        }
        return false;
    }
private:
    bool backtracking(vector<vector<char>>& board, int x, int y, string wordRemaining) {
        if (wordRemaining.size() == 0) {
            // condition
            return true;
        }

        if (x < 0 || x >= m || y < 0 || y >= n) {
            return false;
        }

        if (wordRemaining[0] != board[x][y]) {
            return false;
        }

        // replace
        board[x][y] = '#';

        for (int i = 0; i < 4; i++) {
            int newX = x + dirRow[i];
            int newY = y + dirCol[i];
            bool ret = backtracking(board, newX, newY, wordRemaining.substr(1));
            if (ret) {
                return true;
            }
        }

        // recover
        board[x][y] = wordRemaining[0];
        return false;
    }
};
