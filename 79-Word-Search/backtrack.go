package _9_Word_Search

var m, n int
var b [][]byte

var dirRow []int
var dirCol []int

func exist(board [][]byte, word string) bool {
	// up, down, left, right

	dirRow = []int{-1, 1, 0, 0}
	dirCol = []int{0, 0, -1, 1}
	m = len(board)
	n = len(board[0])
	b = board

	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if backtrack(i, j, word) {
				return true
			}
		}
	}
	return false
}

func backtrack(row, col int, wordRemain string) bool {
	if len(wordRemain) == 0 {
		return true
	}
	if row < 0 || col < 0 || row >= m || col >= n {
		return false
	}
	if wordRemain[0] != b[row][col] {
		return false
	}

	ret := false
	b[row][col] = byte('#')
	for i := 0; i < 4; i++ {
		newRow := row + dirRow[i]
		newCol := col + dirCol[i]
		ret = backtrack(newRow, newCol, wordRemain[1:])
		if ret {
			return true
		}
	}

	b[row][col] = byte(wordRemain[0])
	return ret
}
