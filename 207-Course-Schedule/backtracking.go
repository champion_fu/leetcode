package _07_Course_Schedule

// time: O(|E| + |V|^2)
// space: O(|E| + |V|)

func canFinish(numCourses int, prerequisites [][]int) bool {
	courseDict := make(map[int][]int, 0)
	for _, v := range prerequisites {
		curCourse := v[0]
		requistCourse := v[1]
		if courses, m := courseDict[curCourse]; m {
			courses = append(courses, requistCourse)
			courseDict[curCourse] = courses
		} else {
			// add new course
			requistCourses := make([]int, 0)
			requistCourses = append(requistCourses, requistCourse)
			courseDict[curCourse] = requistCourses
		}
	}

	path := make([]bool, numCourses)
	for i := 0; i < numCourses; i++ {
		if isCycle(i, courseDict, path) {
			return false
		}
	}
	return true
}

func isCycle(i int, courseDict map[int][]int, path []bool) bool {
	if path[i] {
		// come across a previously visited node
		return true
	}

	var m bool
	var requistCourses []int
	if requistCourses, m = courseDict[i]; !m {
		// there is no need requist
		return false
	}

	// marked
	path[i] = true

	// backtracking
	ret := false
	for _, course := range requistCourses {
		ret = isCycle(course, courseDict, path)
		if ret {
			break
		}
	}

	// remove marked
	path[i] = false
	return ret
}
