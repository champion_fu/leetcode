//
// Created by champ on 2022/7/9.
//

class Solution {
public:
    bool canFinish(int numCourses, vector<vector<int>>& prerequisites) {
        vector<int> indegree(numCourses);
        // 1: [0, 2] means if you take 1, then you can take 0, 2
        unordered_map<int, vector<int>> t;

        for (vector<int>& prerequest : prerequisites) {
            int curCourse = prerequest[0];
            int preCourse = prerequest[1];

            indegree[curCourse]++;
            t[preCourse].push_back(curCourse);
        }

        queue<int> q;
        for (int i = 0; i < indegree.size(); i++) {
            if (indegree[i] == 0) q.push(i);
        }

        while (!q.empty()) {
            int course = q.front();
            q.pop();

            vector<int> courses = t[course];

            if (courses.size() == 0) {
                // take this course don't change what you can take for next course
                continue;
            }

            for (auto c : courses) {
                indegree[c]--;
                if (indegree[c] == 0) {
                    q.push(c);
                }
            }
        }

        for (auto course : indegree) {
            if (course != 0) {
                return false;
            }
        }
        return true;
    }
};
