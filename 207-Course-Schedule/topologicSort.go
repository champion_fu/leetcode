package _07_Course_Schedule

import "fmt"

func canFinish(numCourses int, prerequisites [][]int) bool {
	if len(prerequisites) == 0 {
		return true
	}

	indegree := make([]int, numCourses)
	graph := make(map[int][]int)
	for _, v := range prerequisites {
		curCourse := v[0]
		requistCourse := v[1]

		// update inDegree
		indegree[curCourse] += 1
		// update graph
		var nextCourses []int
		var m bool
		if nextCourses, m = graph[requistCourse]; !m {
			// fist time
			nextCourses = make([]int, 0)
		}
		nextCourses = append(nextCourses, curCourse)
		graph[requistCourse] = nextCourses
	}

	q := queue{}
	fmt.Printf("indegree %+v\n", indegree)
	for i, v := range indegree {
		if v == 0 {
			// there is no prequiest for this i course
			q.Push(i)
		}
	}
	fmt.Printf("q %v\n", q)
	for !q.IsEmpty() {
		course := q.Pop()
		courses := graph[course]
		if len(courses) == 0 {
			continue
		}
		for _, v := range courses {
			indegree[v] -= 1
			if indegree[v] == 0 {
				q.Push(v)
			}
		}
	}
	for _, v := range indegree {
		if v != 0 {
			return false
		}
	}
	return true
}

type queue []int

func (q *queue) IsEmpty() bool {
	return len(*q) == 0
}
func (q *queue) Pop() int {
	if q.IsEmpty() {
		return -1
	}
	e := (*q)[0]
	(*q) = (*q)[1:]
	return e
}
func (q *queue) Push(a int) {
	(*q) = append((*q), a)
}
