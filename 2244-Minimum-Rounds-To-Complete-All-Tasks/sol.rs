impl Solution {
    pub fn minimum_rounds(tasks: Vec<i32>) -> i32 {
        use std::collections::HashMap;
        let mut task_map = HashMap::new();

        for task in tasks {
            task_map.entry(task).and_modify(|v| *v += 1).or_insert(1);
        }

        let mut res = 0;
        for (_, v) in task_map {
            if v == 1 {
                return -1;
            } else {
                // 2 -> 1
                // 3 -> 1
                // 4 -> 2
                // 5 -> 2
                res += (v + 2) / 3;
            }
        }
        res
    }
}
