//
// Created by champ on 2022/6/29.
//

class Solution {
public:
    int coinChange(vector<int>& coins, int amount) {
        int max = amount + 1;
        vector<int> dp(max, max);
        dp[0] = 0;

        for (int i = 1; i < max; i++) {
            for (auto c: coins) {
                if (i-c >= 0)  {
                    dp[i] = min(dp[i-c]+1, dp[i]);
                }
            }
        }

        return dp[amount] > amount ? -1 : dp[amount];
    }
};
