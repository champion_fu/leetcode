package _22_Coin_Change

import "sort"
import "fmt"
import "math"

var coin []int
var ret int

func coinChange(coins []int, amount int) int {
	ret = math.MaxInt64
	sort.Slice(
		coins,
		func(i, j int) bool {
			return coins[i] > coins[j]
		},
	)
	coin = coins
	backtracking(amount, 0, make([]int, 0))
	if ret == math.MaxInt64 {
		return -1
	}
	return ret
}

func backtracking(amount, index int, candidate []int) {
	if amount == 0 {
		ret = min(ret, len(candidate))
		return
	}

	for i := index; i < len(coin); i++ {
		v := coin[i]
		if v > amount {
			continue
		}
		candidate = append(candidate, v)
		backtracking(amount-v, i, candidate)
		candidate = candidate[:len(candidate)-1]
	}
}

func min(i, j int) int {
	if i < j {
		return i
	}
	return j
}
