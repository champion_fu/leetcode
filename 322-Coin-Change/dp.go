package _22_Coin_Change

import "math"

func coinChange(coins []int, amount int) int {
	max := amount + 1
	dp := make([]int, max)
	dp[0] = 0

	for i := 1; i < max; i++ {
		dp[i] = math.MaxInt32
		for _, c := range coins {
			if i-c >= 0 {
				dp[i] = min(dp[i-c]+1, dp[i])
			}
		}
	}
	ret := dp[amount]
	if ret == math.MaxInt32 {
		return -1
	}
	return ret
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
