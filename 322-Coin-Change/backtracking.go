package _22_Coin_Change

import (
	"fmt"
	"math"
	"sort"
)

var kinds []int

func coinChange(coins []int, amount int) int {
	if amount == 0 {
		return 0
	}

	sort.Slice(
		coins,
		func(i, j int) bool {
			return coins[i] > coins[j]
		},
	)
	kinds = coins
	res := backtracking(amount)
	fmt.Printf("res %v res, maxInt64 %v\n", res, math.MaxInt64)
	if res == math.MaxInt32 {
		return -1
	}
	return res
}

func backtracking(amount int) int {
	res := math.MaxInt32
	if amount == 0 {
		// don't need to add coin
		return 0
	}
	for _, v := range kinds {
		// from biggest
		if amount-v >= 0 {
			x := 1 + backtracking(amount-v)
			if res > x {
				res = x
			}
		}
	}
	return res
}
