//
// Created by champ on 2022/8/7.
//

class Solution {
public:
    bool validTree(int n, vector<vector<int>>& edges) {
        // number of tree edges need to be number of tree node - 1
        if (edges.size() != n-1) return false;

        vector<vector<int>> adjacencyList(n);

        for (int i = 0; i < edges.size(); i++) {
            int node1 = edges[i][0];
            int node2 = edges[i][1];
            adjacencyList[node1].push_back(node2);
            adjacencyList[node2].push_back(node1);
        }

        stack<int> s;
        set<int> seen;
        s.push(0);
        seen.insert(0);

        while (!s.empty()) {
            int node = s.top();
            s.pop();
            for (int neighbour : adjacencyList[node]) {
                if (seen.find(neighbour) != seen.end()) continue;
                seen.insert(neighbour);
                s.push(neighbour);
            }
        }

        return seen.size() == n;
    }
};
