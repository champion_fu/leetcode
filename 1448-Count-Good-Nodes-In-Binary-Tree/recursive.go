package _448_Count_Good_Nodes_In_Binary_Tree

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
import "math"

var count int

func goodNodes(root *TreeNode) int {
	count = 0
	if root == nil {
		return 0
	}
	recursive(root, math.MinInt64)
	return count
}

func recursive(node *TreeNode, maxVal int) {
	if maxVal <= node.Val {
		count += 1
	}
	maxVal = max(maxVal, node.Val)

	// node.Val >= max
	if node.Left != nil {
		recursive(node.Left, maxVal)
	}
	if node.Right != nil {
		recursive(node.Right, maxVal)
	}
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}
