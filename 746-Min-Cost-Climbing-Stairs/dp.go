package _46_Min_Cost_Climbing_Stairs

func minCostClimbingStairs(cost []int) int {
	length := len(cost)

	// dp[length] = cost[length-1] or cost[length-2]
	// dp[0] == 0
	// dp[1] == 0
	// dp[2] = dp[0]+cost[0] or dp[1]+cost[1]
	dp := make(map[int]int)
	dp[0] = 0
	dp[1] = 0

	for i := 2; i <= length; i++ {
		dp[i] = min(dp[i-1]+cost[i-1], dp[i-2]+cost[i-2])
	}
	return dp[length]
}

func min(i, j int) int {
	if i > j {
		return j
	}
	return i
}
