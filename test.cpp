//
// Created by champ on 2022/4/26.
//
#include <iostream>
using namespace std;

class Test {
public:

    void run() {
        cout << "run.." << endl;
    }
};

int main() {
    int intMulAr[2][3] = {{0, 1, 2}, {3, 4, 5}};
    cout << intMulAr << endl;
    int* pInt1 = intMulAr[1];
    cout << pInt1 << endl;  // 會是 intMulAr address + intMulAr[0].size()
    cout << "4 = " << *(pInt1+1) << endl; // [1][1] -> 4

    int (*pInt3)[3]  = intMulAr;
    cout << pInt3[0] << " = " << pInt3 << " = " << &intMulAr << endl;
    cout << *(pInt3)[1] << " = " << intMulAr[1][0] << endl; // 3
    cout << "3 = " << *(pInt3[1]) << ", 5 = " << *(pInt3[1]+2) << endl;

    Test* t = new Test();
    t->run();
    return 0;
}
