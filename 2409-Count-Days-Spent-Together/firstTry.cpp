//
// Created by champ on 2022/9/22.
//

class Solution {
    vector<int> days {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
private:
    int intoDays(string date) {
        int ret = 0;
        for (int m = 0; m < stoi(date.substr(0,2))-1; m++) ret += days[m];
        return ret+stoi(date.substr(3, 2));
    }

public:
    int countDaysTogether(string arriveAlice, string leaveAlice, string arriveBob, string leaveBob) {
        // '08-15' -> 31+28+....+31 + 15 = x      = aA
        // '08-18' -> ...           + 18 = x + 3  = lA
        // '08-16' ->                    = x + 1  = aB
        // '08-19' ->                    = x + 4  = lB

        // 1. aB <= lA
        // aB aA lA lB -> aA ~ lA
        // aB aA lB lA -> aA ~ lB
        // aB lB aA lA -> check lB == aA
        // aA aB lA lB -> aB ~ lA
        // aA aB lB lA -> aB ~ lB
        // aA lA aB lB -> check lA == aB

        // 2. aA <= lB
        // aA aB lA lB -> aB ~ lA
        // aA aB lB lA -> aB ~ lB
        // aA lA aB lB -> check lA == aB
        // aB aA lA lB -> aA ~ lA
        // aB aA lB lA -> aA ~ lB
        // aB lB aA lA -> check lA == aB

        // cout << " arriveAlice " << intoDays(arriveAlice) << endl;
        int aA = intoDays(arriveAlice);
        int aB = intoDays(arriveBob);
        int lA = intoDays(leaveAlice);
        int lB = intoDays(leaveBob);

        cout << "aA " << aA << " lA " << lA << " aB " << aB << " lB " << lB << endl;

        if (aB <= lA) {
            if (lB < aA) return 0;
            if (aB == lA) return 1;
            if (lA == aB) return 1;
            if (aB <= aA && lA <= lB) return lA-aA+1;
            if (aB <= aA && lB <= lA) return lB-aA+1;
            if (aA <= aB && lA <= lB) return lA-aB+1;
            if (aA <= aB && lB <= lA) return lB-aB+1;
        }

        if (aA <= lB) {
            if (lA < aB) return 0;
            if (aB == lA) return 1;
            if (lB == aA) return 1;
            if (aA <= aB && lA <= lB) return aB - lA + 1;
            if (aA <= aB && lB <= lA) return aB - lB + 1;
            if (aB <= aA && lB <= lA) return aA - lB + 1;
            if (aB <= aA && lA <= lB) return aA - lA + 1;
        }

        return 0;
    }
};
