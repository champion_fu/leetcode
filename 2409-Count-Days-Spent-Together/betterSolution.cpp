//
// Created by champ on 2022/9/22.
//

class Solution {
    vector<int> days {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
private:
    int intoDays(string date) {
        int ret = 0;
        for (int m = 0; m < stoi(date.substr(0,2))-1; m++) ret += days[m];
        return ret+stoi(date.substr(3, 2));
    }

public:
    int countDaysTogether(string arriveAlice, string leaveAlice, string arriveBob, string leaveBob) {
        // '08-15' -> 31+28+....+31 + 15 = x      = aA
        // '08-18' -> ...           + 18 = x + 3  = lA
        // '08-16' ->                    = x + 1  = aB
        // '08-19' ->                    = x + 4  = lB
        // check every day from 01-01 to 12-31 if in these interval

        int aA = intoDays(arriveAlice);
        int aB = intoDays(arriveBob);
        int lA = intoDays(leaveAlice);
        int lB = intoDays(leaveBob);

        int ans = 0;
        for (int i = 0; i < 367; i++) {
            if (aA <= i && aB <= i && i <= lA && i <= lB) {
                ans++;
            }
        }
        return ans;
    }
};
