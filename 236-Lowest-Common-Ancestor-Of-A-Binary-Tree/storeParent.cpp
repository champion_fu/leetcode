//
// Created by champ on 2022/7/28.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
    unordered_map<TreeNode*, TreeNode*> parents;
private:
    void storeParent(TreeNode* n, TreeNode* parent) {
        if (n != NULL) {
            parents[n] = parent;
        }
        if (n->left) storeParent(n->left, n);
        if (n->right) storeParent(n->right, n);
    }

public:
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        storeParent(root, NULL);

        unordered_map<int, bool> visited;
        while (p != NULL) {
            visited[p->val] = true;
            p = parents[p];
        }
        bool visit = false;
        TreeNode* prev = NULL;
        while(visit == false) {
            prev = q;
            visit = visited[q->val];
            q = parents[q];
        }

        return prev;
    }
};
