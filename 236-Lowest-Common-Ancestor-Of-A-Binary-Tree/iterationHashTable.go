package _36_Lowest_Common_Ancestor_Of_A_Binary_Tree

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func lowestCommonAncestor(root, p, q *TreeNode) *TreeNode {
	s := stack{}
	parent := make(map[*TreeNode]*TreeNode)
	parent[root] = nil
	s.Push(root)

	_, mP := parent[p]
	_, mQ := parent[q]
	for !mP || !mQ {
		n := s.Pop()
		if n.Left != nil {
			s.Push(n.Left)
			parent[n.Left] = n
		}
		if n.Right != nil {
			s.Push(n.Right)
			parent[n.Right] = n
		}
		_, mP = parent[p]
		_, mQ = parent[q]
	}

	ancestors := make(map[*TreeNode]bool)
	for p != nil {
		ancestors[p] = true
		p = parent[p]
	}

	mQ = ancestors[q]
	for !mQ {
		q = parent[q]
		mQ = ancestors[q]
	}
	return q
}

type stack []*TreeNode

func (s *stack) IsEmpty() bool {
	return len(*s) == 0
}
func (s *stack) Push(a *TreeNode) {
	*s = append(*s, a)
}
func (s *stack) Pop() *TreeNode {
	if s.IsEmpty() {
		return nil
	} else {
		index := len(*s) - 1
		element := (*s)[index]
		*s = (*s)[:index]
		return element
	}
}
