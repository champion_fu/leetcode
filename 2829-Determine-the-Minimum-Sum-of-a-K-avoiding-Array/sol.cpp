class Solution {
public:
    int minimumSum(int n, int k) {
        int sum = 0;
        set<int> st;

        int num = 1;
        while(n > 0) {
            int prev = k - num;
            if (st.find(prev) == st.end()) {
                sum += num;
                n--;

                st.insert(num);
            }
            num++;
        }
        return sum;
    }
}
