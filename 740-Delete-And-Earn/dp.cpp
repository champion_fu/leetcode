//
// Created by champ on 2022/3/1.
//

class Solution {
public:
    int deleteAndEarn(vector<int>& nums) {
        vector<int> count(10001, 0);

        for (int i = 0; i < nums.size(); i++) {
            count[nums[i]] += 1;
        }

        int ans, ans_1, ans_2 = 0;
        for (int i = 1; i < count.size(); i++) {
            int num = i*count[i];
            ans = max(ans_2+num, ans_1);
            ans_2 = ans_1;
            ans_1 = ans;
        }
        return ans;
    }
};
