package Check_If_N_And_Its_Double_Exist

func checkIfExist(arr []int) bool {
	lookup := make(map[int]bool)
	for _, v := range arr {
		fmt.Printf("lookup %+v, v %+v\n", lookup, v)
		if _, match := lookup[v]; match {
			if v == 0 {
				return true
			}
			continue
		} else {
			_, match := lookup[v/2]
			if v%2 == 0 && match {
				return true
			}
			if _, match := lookup[v*2]; match {
				return true
			}
			lookup[v] = true
		}
	}
	return false
}
