package _8_Rotate_Image

func rotate(matrix [][]int) {
	n := len(matrix)

	// Rotated == Transposed + Reversed

	// Transposed
	for i := 0; i < n; i++ {
		for j := i; j < n; j++ {
			tmp := matrix[i][j]
			matrix[i][j] = matrix[j][i]
			matrix[j][i] = tmp
		}
	}

	// Reversed
	for i := 0; i < n; i++ {
		// x direction
		for j := 0; j < n/2; j++ {
			tmp := matrix[i][j]
			matrix[i][j] = matrix[i][n-j-1]
			matrix[i][n-j-1] = tmp
		}
	}
}
