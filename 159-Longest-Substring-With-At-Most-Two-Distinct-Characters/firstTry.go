package _59_Longest_Substring_With_At_Most_Two_Distinct_Characters

func lengthOfLongestSubstringTwoDistinct(s string) int {
	counts := make(map[byte]int)
	ret := 0
	l := 0
	r := 0
	for r < len(s) {
		c := s[r]
		if _, m := counts[c]; m {
			counts[c]++
		} else {
			counts[c] = 1
		}
		for len(counts) > 2 {
			// need decrease window
			cL := s[l]
			v := counts[cL]
			v -= 1
			if v == 0 {
				delete(counts, cL)
			} else {
				counts[cL] = v
			}
			l++
		}
		if ret < r-l+1 {
			ret = r - l + 1
		}
		r++
	}
	return ret
}
