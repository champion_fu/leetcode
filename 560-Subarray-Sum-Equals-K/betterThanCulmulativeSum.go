package _60_Subarray_Sum_Equals_K

func subarraySum(nums []int, k int) int {
	ret := 0
	for i := 0; i < len(nums); i++ {
		sum := 0
		for j := i; j < len(nums); j++ {
			sum += nums[j]
			if sum == k {
				ret += 1
			}
		}
	}
	return ret
}
