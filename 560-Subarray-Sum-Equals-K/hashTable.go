package _60_Subarray_Sum_Equals_K

func subarraySum(nums []int, k int) int {
	ret := 0
	sum := 0
	culmulativeSumFreq := make(map[int]int)
	culmulativeSumFreq[0] = 1

	for _, v := range nums {
		sum += v
		if v, m := culmulativeSumFreq[sum-k]; m {
			// match
			ret += v
		}
		culmulativeSumFreq[sum] += 1
	}
	return ret
}
