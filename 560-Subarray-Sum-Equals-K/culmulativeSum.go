package _60_Subarray_Sum_Equals_K

func subarraySum(nums []int, k int) int {
	sum := make([]int, len(nums))
	for i, v := range nums {
		if i == 0 {
			sum[i] = v
		} else {
			sum[i] = sum[i-1] + v
		}
	}

	ret := 0
	for i := 0; i < len(nums); i++ {
		for j := i + 1; j <= len(nums); j++ {
			tmp := 0
			if i == 0 {
				tmp = sum[j-1]
			} else {
				tmp = sum[j-1] - sum[i-1]
			}
			if tmp == k {
				ret += 1
			}
		}
	}
	return ret
}
