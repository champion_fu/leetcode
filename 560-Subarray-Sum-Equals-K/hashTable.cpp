//
// Created by champ on 2022/5/1.
//

class Solution {
public:
    int subarraySum(vector<int>& nums, int k) {
        unordered_map<int, int> cumulativeSum;
        cumulativeSum[0] = 1;
        int result = 0;
        int sum = 0;

        for (auto n : nums) {
            sum += n;

            // means there is some where sum == target
            result += cumulativeSum[sum - k];

            cumulativeSum[sum]++;
        }
        return result;
    }
};
