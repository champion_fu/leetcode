//
// Created by champ on 2022/7/9.
//

class Solution {
public:
    int lengthOfLongestSubstringKDistinct(string s, int k) {
        int n = s.size();
        if (n * k == 0) return 0;

        int left = 0, right = 0;
        unordered_map<char, int> rightMost;

        int maxLength = 0;

        while (right < n) {
            rightMost[s[right]] = right;
            right++;

            if (rightMost.size() > k) {
                int lowestIndex = INT_MAX;
                for (pair<char, int> r : rightMost) {
                    // fint lowestIndex
                    lowestIndex = min(lowestIndex, r.second);
                }
                rightMost.erase(s[lowestIndex]);
                left = lowestIndex + 1;
            }
            maxLength = max(maxLength, right - left);
        }
        return maxLength;
    }
};
