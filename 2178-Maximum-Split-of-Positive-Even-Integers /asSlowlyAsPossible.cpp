//
// Created by champ on 2022/4/25.
//

class Solution {
public:
    vector<long long> maximumEvenSplit(long long finalSum) {
        vector<long long> ret(0);
        if (finalSum % 2 != 0) {
            return ret;
        }

        long long i = 2;
        long long curSum = 0;

        while ((curSum + i) <= finalSum) {
            ret.push_back(i);
            curSum += i;
            i += 2;
        }

        // add remaining difference to the last value in answer list
        int sz = ret.size();
        ret[sz-1] += (finalSum-curSum);
        return ret;
    }
};
