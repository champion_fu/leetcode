class Solution {
public:
    int minAbsoluteDifference(vector<int>& nums, int x) {
        int ans = INT_MAX;
        set<int> s;
        // insert the value but with reverse order so that we don't delete
        for (int i = x; i < nums.size(); ++i) {
            // insert
            s.insert(nums[i-x]);
            // find the upper_bound (minimum num which is bigger than nums[i])
            auto it = s.upper_bound(nums[i]);
            // find distance so that -> a(*prev(it)), nums[i], b(*it)
            if (it != s.end()) ans = min(ans, abs(nums[i] - *it));
            if (it != s.begin()) ans = min(ans, abs(nums[i] - *prev(it)));
        }
        return ans;
    }
}
