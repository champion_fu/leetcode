package _817_Minimum_Absolute_Difference_Between_Elements_With_Constraint

func minAbsoluteDifference(nums []int, x int) int {
	result := math.MaxInt32
	for i := 0; i < len(nums); i++ {
		for j := i + x; j < len(nums); j++ {
			num := 0
			if nums[i] > nums[j] {
				num = nums[i] - nums[j]
			} else {
				num = nums[j] - nums[i]
			}
			if result > num {
				result = num
			}
		}
	}

	return result
}
