package _00_Longest_Increasing_Subsequence

func lengthOfLIS(nums []int) int {
	var dp []int = make([]int, 0)
	for _, v := range nums {
		if len(dp) == 0 {
			dp = append(dp, v)
			continue
		}
		last := dp[len(dp)-1]
		if v > last {
			dp = append(dp, v)
		} else if v == last {
			continue
		} else {
			index := binarySearch(dp, v)
			dp[index] = v
		}
	}
	return len(dp)
}

func binarySearch(dp []int, target int) (index int) {
	l := 0
	r := len(dp) - 1

	for l < r {
		m := l + (r-l)/2
		if target > dp[m] {
			l = m + 1
		} else {
			r = m
		}
	}
	return l
}
