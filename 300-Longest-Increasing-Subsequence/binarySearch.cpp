//
// Created by champ on 2022/4/16.
//

class Solution {
public:
    int lengthOfLIS(vector<int>& nums) {
        vector<int> lis;

        for (auto n : nums) {
            if (lis.size() == 0) {
                // lis is empty
                lis.push_back(n);
                continue;
            }

            auto it = lower_bound(lis.begin(), lis.end(), n);
            if (it == lis.end()) {
                lis.push_back(n);
            } else {
                lis[(it - lis.begin())] = n;
            }
        }
        return lis.size();

    }
};
