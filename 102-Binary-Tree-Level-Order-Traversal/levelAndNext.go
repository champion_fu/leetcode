package _02_Binary_Tree_Level_Order_Traversal

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func levelOrder(root *TreeNode) [][]int {
	var ret [][]int
	if root == nil {
		return ret
	}

	result, next := valuesAndNext([]*TreeNode{root})
	ret = append(ret, result)
	for len(next) != 0 {
		result, next = valuesAndNext(next)
		ret = append(ret, result)
	}
	return ret
}

func valuesAndNext(thisLevel []*TreeNode) (ret []int, next []*TreeNode) {
	for _, n := range thisLevel {
		ret = append(ret, n.Val)
		if n.Left != nil {
			next = append(next, n.Left)
		}
		if n.Right != nil {
			next = append(next, n.Right)
		}
	}
	return
}
