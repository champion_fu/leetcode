//
// Created by champ on 2022/6/4.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> levelOrder(TreeNode* root) {
        vector<vector<int>> results;
        queue<TreeNode*> q;

        if (!root) {
            return results;
        }

        q.push(root);

        while (!q.empty()) {
            int length = q.size();
            vector<int> tmp(0);
            for (int i = 0; i < length; i++) {
                TreeNode* n = q.front();
                tmp.push_back(n->val);
                if (n->left) q.push(n->left);
                if (n->right) q.push(n->right);
                q.pop();
            }
            results.push_back(tmp);
        }
        return results;
    }
};
