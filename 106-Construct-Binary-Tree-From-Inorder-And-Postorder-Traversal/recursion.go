package _06_Construct_Binary_Tree_From_Inorder_And_Postorder_Traversal

import "fmt"

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func buildTree(inorder []int, postorder []int) *TreeNode {
	inorderMap := make(map[int]int)
	for i, v := range inorder {
		inorderMap[v] = i
	}

	var recur func(int, int) *TreeNode

	recur = func(leftIndex, rightIndex int) *TreeNode {
		if leftIndex >= rightIndex {
			return nil
		}

		lastIndex := len(postorder) - 1
		nodeValue := postorder[lastIndex]
		fmt.Printf("val %v\n", nodeValue)
		postorder = postorder[:lastIndex]

		node := &TreeNode{
			Val: nodeValue,
		}

		if index, ok := inorderMap[nodeValue]; !ok {
			return nil
		} else {
			node.Right = recur(index+1, rightIndex)
			node.Left = recur(leftIndex, index)
		}
		return node
	}
	return recur(0, len(inorder))
}
