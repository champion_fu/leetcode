//
// Created by champ on 2022/7/23.
//

class Solution {
public:
    vector<vector<int>> kClosest(vector<vector<int>>& points, int k) {
        priority_queue<pair<int, int>> maxPQ;

        // while dis < h.top() then h.pop(), push(thisOne)
        for (int i = 0; i < points.size(); i++) {
            pair<int, int> entry = {distance(points[i]), i};
            if (maxPQ.size() < k) {
                maxPQ.push(entry);
            } else if (entry.first < maxPQ.top().first) {
                maxPQ.pop();
                maxPQ.push(entry);
            }
        }

        vector<vector<int>> ans;
        while (!maxPQ.empty()) {
            int entryIndex = maxPQ.top().second;
            maxPQ.pop();
            ans.push_back(points[entryIndex]);
        }
        return ans;
    }
private:
    int distance(vector<int>& point) {
        return point[0] * point[0] + point[1] * point[1];
    }
};
