package _73_K_Closest_Points_To_Origin

import "container/heap"

func kClosest(points [][]int, k int) [][]int {
	h := &minHeap{}
	heap.Init(h)

	for i := 0; i < len(points); i++ {
		heap.Push(
			h,
			&element{
				area: points[i][0]*points[i][0] + points[i][1]*points[i][1],
				x:    points[i][0],
				y:    points[i][1],
			},
		)
	}

	var e *element
	ret := make([][]int, 0)
	for k > 0 {
		e = heap.Pop(h).(*element)
		ret = append(ret, []int{e.x, e.y})
		k--
	}
	return ret
}

type element struct {
	area int
	x    int
	y    int
}

type minHeap []*element

func (h minHeap) Len() int {
	return len(h)
}
func (h minHeap) Less(i, j int) bool {
	return h[i].area < h[j].area
}
func (h minHeap) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}
func (h *minHeap) Push(x interface{}) {
	*h = append(*h, x.(*element))
}
func (h *minHeap) Pop() interface{} {
	n := len(*h)
	e := (*h)[n-1]
	*h = (*h)[:n-1]
	return e
}
