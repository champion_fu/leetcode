//
// Created by champ on 2022/7/21.
//

class Solution {
public:
    bool containsDuplicate(vector<int>& nums) {
        unordered_map<int, int> m;

        for (auto n : nums) {
            if (m.count(n) > 0) return true;
            m[n]++;
        }
        return false;
    }
};
