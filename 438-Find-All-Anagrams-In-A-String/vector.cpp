//
// Created by champ on 2022/8/1.
//

class Solution {
public:
    vector<int> findAnagrams(string s, string p) {
        int sizeP = p.size();
        int sizeS = s.size();
        vector<int> res;

        if (sizeP > sizeS) return res;

        vector<int> mS(26, 0);
        vector<int> mP(26, 0);

        int l = 0, r = 0;
        while (r < sizeP) {
            mS[s[r] - 'a']++;
            mP[p[r] - 'a']++;
            r++;
        }
        r--;
        while (r < sizeS) {
            cout << " l " << l << " r " << r << endl;
            //for (auto p : mS) {
            //    cout << " char " << p.first << " count " << p.second << endl;
            //}
            if (mS == mP) res.push_back(l);
            r++;
            if (r != sizeS) {
                mS[s[r] - 'a']++;
            }
            mS[s[l] - 'a']--;
            l++;
        }
        return res;
    }
};
