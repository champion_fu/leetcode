//
// Created by champ on 2022/9/22.
//

class Solution {
public:
    vector<int> smallestSubarrays(vector<int>& nums) {
        int last[30] = {}, n = nums.size();
        vector<int> res(n, 1);
        for (int i = n-1; i >= 0; i--) {

            // check all bit & nums[i] is 1 or 0
            for (int j = 0; j < 30; j++) {
                if ((nums[i]>>j & 1) > 0) {
                    last[j] = i;
                }
                // if one can not then this is not maximum bitwise OR
                // so take the max
                res[i] = max(res[i], last[j] - i + 1);
            }
        }
        return res;
    }
};
