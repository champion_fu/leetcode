//
// Created by champ on 2022/10/31.
//

/**
 * // This is the robot's control interface.
 * // You should not implement it, or speculate about its implementation
 * class Robot {
 *   public:
 *     // Returns true if the cell in front is open and robot moves into the cell.
 *     // Returns false if the cell in front is blocked and robot stays in the current cell.
 *     bool move();
 *
 *     // Robot will stay in the same cell after calling turnLeft/turnRight.
 *     // Each turn will be 90 degrees.
 *     void turnLeft();
 *     void turnRight();
 *
 *     // Clean the current cell.
 *     void clean();
 * };
 */

class Solution {
    vector<vector<int>> dirs;
    unordered_set<string> visited;
public:
    void cleanRoom(Robot& robot) {
        // dirs is important with order
        dirs = vector<vector<int>>{{-1, 0}, {0, 1}, {1, 0}, {0, -1}};
        visited = unordered_set<string>{};
        backtrack(0, 0, 0, robot);
    }

private:
    void backtrack(int x, int y, int dir, Robot& robot) {
        visited.insert(to_string(x) + "," + to_string(y));
        robot.clean();
        for (int i = 0; i < 4; i++) {
            int newDir = (dir + i) % 4;
            int newX = x + dirs[newDir][0];
            int newY = y + dirs[newDir][1];
            if (!visited.count(to_string(newX) + "," + to_string(newY)) &&
                robot.move()) {
                backtrack(newX, newY, newDir, robot);

                // undo
                robot.turnRight();
                robot.turnRight();
                robot.move();
                robot.turnRight();
                robot.turnRight();
            }
            robot.turnRight();
        }
    }
};
