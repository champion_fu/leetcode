package _89_Robot_Room_Cleaner

/**
 * // This is the robot's control interface.
 * // You should not implement it, or speculate about its implementation
 * type Robot struct {
 * }
 *
 * // Returns true if the cell in front is open and robot moves into the cell.
 * // Returns false if the cell in front is blocked and robot stays in the current cell.
 * func (robot *Robot) Move() bool {}
 *
 * // Robot will stay in the same cell after calling TurnLeft/TurnRight.
 * // Each turn will be 90 degrees.
 * func (robot *Robot) TurnLeft() {}
 * func (robot *Robot) TurnRight() {}
 *
 * // Clean the current cell.
 * func (robot *Robot) Clean() {}
 */

type cell struct {
	row int
	col int
}

var visited map[cell]bool
var r *Robot

func (r *Robot) goBack() {
	r.TurnRight()
	r.TurnRight()
	r.Move()
	r.TurnRight()
	r.TurnRight()
}

var directions []cell = []cell{
	cell{-1, 0},
	cell{0, 1},
	cell{1, 0},
	cell{0, -1},
}

func backtrack(row, col, direction int) {
	visited[cell{row: row, col: col}] = true
	// fmt.Printf("clean row %+v, col %+v\n", row, col)
	r.Clean()

	// go through {0: top, 1: right, 2: down, 3: left}
	// fmt.Printf("visited %+v\n", visited)
	for i := 0; i < 4; i++ {
		newDirection := (i + direction) % 4
		newRow := row + directions[newDirection].row
		newCol := col + directions[newDirection].col

		// fmt.Printf("i %+v, direction %+v, newDirection %v\n", i, direction, newDirection)
		// fmt.Printf("newRow %+v, newCol %+v\n", newRow, newCol)
		_, match := visited[cell{row: newRow, col: newCol}]
		if !match && r.Move() {
			backtrack(newRow, newCol, newDirection)
			r.goBack()
		}
		r.TurnRight()
	}
}

func cleanRoom(robot *Robot) {
	// we don't know the map, so set initial point as (0, 0),
	// Then go though all available cells.
	r = robot
	visited = make(map[cell]bool, 0)
	backtrack(0, 0, 0)
}
