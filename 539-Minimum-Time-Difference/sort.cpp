//
// Created by champ on 2022/4/21.
//

class Solution {
public:
    int findMinDifference(vector<string>& timePoints) {
        sort(timePoints.begin(), timePoints.end());
        int ret = INT_MAX;
        int n = timePoints.size();
        for (int i = 0; i < n; i++) {
            int diff = timeDiff(timePoints[i], timePoints[(i-1+n)%n]);
            // cout << diff << endl;
            int mindiff = min(diff, 1440-diff);
            ret = min(ret, mindiff);
        }
        return ret;
    }
private:
    int timeDiff(string t1, string t2) {
        // substr (index, length)
        int h1 = stoi(t1.substr(0, 2));
        int h2 = stoi(t2.substr(0, 2));
        int m1 = stoi(t1.substr(3, 2));
        int m2 = stoi(t2.substr(3, 2));
        int time1 = h1*60+m1;
        int time2 = h2*60+m2;
        return abs(time1-time2);
    }
};
