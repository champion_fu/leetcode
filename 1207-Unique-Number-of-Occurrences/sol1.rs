impl Solution {
    pub fn unique_occurrences(arr: Vec<i32>) -> bool {
        use std::collections::{HashMap, HashSet};
        let mut m: HashMap<i32, i32> = HashMap::new();
        for x in arr {
            *m.entry(x).or_insert(0) += 1;
            if m.len() > 44 { return false; } // (1 + 45) * 45 / 2 > 1000
        }
        let mut s = HashSet::new();
        for count in m.values() {
            if s.contains(count) { return false; }
            s.insert(count);
        }
        true
    }
}
