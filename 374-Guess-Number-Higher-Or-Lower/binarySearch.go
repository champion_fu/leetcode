package _74_Guess_Number_Higher_Or_Lower

import "fmt"

/**
 * Forward declaration of guess API.
 * @param  num   your guess
 * @return 	     -1 if num is lower than the guess number
 *			      1 if num is higher than the guess number
 *               otherwise return 0
 * func guess(num int) int;
 */

func guessNumber(n int) int {
	l := 0
	r := n
	for l <= r {
		m := (l + r) / 2
		direction := guess(m)
		fmt.Printf("direction %v, m %v\n", direction, m)
		if direction == 0 {
			return m
		} else if direction == 1 {
			l = m + 1
		} else {
			r = m - 1
		}
	}
	return l
}
