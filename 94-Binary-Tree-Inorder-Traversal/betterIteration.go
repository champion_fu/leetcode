package _4_Binary_Tree_Inorder_Traversal

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func inorderTraversal(root *TreeNode) []int {
	var ret = make([]int, 0)
	if root == nil {
		return ret
	}

	s := stack{}
	curr := root

	for curr != nil || !s.IsEmpty() {
		for curr != nil {
			s.Push(curr)
			curr = curr.Left
		}
		curr = s.Pop()
		ret = append(ret, curr.Val)
		curr = curr.Right
	}
	return ret
}

type stack []*TreeNode

func (s *stack) IsEmpty() bool {
	return len(*s) == 0
}

func (s *stack) Push(a *TreeNode) {
	*s = append(*s, a)
}

func (s *stack) Top() *TreeNode {
	if s.IsEmpty() {
		return nil
	} else {
		return (*s)[len(*s)-1]
	}
}

func (s *stack) Pop() *TreeNode {
	if s.IsEmpty() {
		return nil
	} else {
		index := len(*s) - 1
		element := (*s)[index]
		*s = (*s)[:index]
		return element
	}
}
