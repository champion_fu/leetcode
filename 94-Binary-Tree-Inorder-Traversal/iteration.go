package _4_Binary_Tree_Inorder_Traversal

import "fmt"

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func inorderTraversal(root *TreeNode) []int {
	var ret = make([]int, 0)
	var visited map[*TreeNode]bool = make(map[*TreeNode]bool, 0)
	if root == nil {
		return ret
	}

	s := stack{}
	s.Push(root)
	visited[root] = true

	for !s.IsEmpty() {
		n := s.Top()
		fmt.Printf("top %+v\n", n)
		match := visited[n.Left]
		if n.Left != nil && !match {
			s.Push(n.Left)
			visited[n.Left] = true
			continue
		}
		n = s.Pop()
		fmt.Printf("pop %+v\n", n)
		ret = append(ret, n.Val)
		match = visited[n.Right]
		if n.Right != nil && !match {
			s.Push(n.Right)
			visited[n.Right] = true
		}
	}
	return ret
}

type stack []*TreeNode

func (s *stack) IsEmpty() bool {
	return len(*s) == 0
}

func (s *stack) Push(a *TreeNode) {
	*s = append(*s, a)
}

func (s *stack) Top() *TreeNode {
	if s.IsEmpty() {
		return nil
	} else {
		return (*s)[len(*s)-1]
	}
}

func (s *stack) Pop() *TreeNode {
	if s.IsEmpty() {
		return nil
	} else {
		index := len(*s) - 1
		element := (*s)[index]
		*s = (*s)[:index]
		return element
	}
}
