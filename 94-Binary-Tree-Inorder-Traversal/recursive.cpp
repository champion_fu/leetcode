//
// Created by champ on 2022/4/17.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<int> inorderTraversal(TreeNode* root) {
        vector<int> ret(0);
        if (root == NULL) {
            return ret;
        }
        recursive(root, &ret);
        return ret;
    }
private:
    void recursive(TreeNode* node, vector<int>* vec) {
        if (node->left) {
            recursive(node->left, vec);
        }
        vec->push_back(node->val);
        if (node->right) {
            recursive(node->right, vec);
        }
    }
};
