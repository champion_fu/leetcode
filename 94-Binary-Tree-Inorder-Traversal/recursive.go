package _4_Binary_Tree_Inorder_Traversal

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func inorderTraversal(root *TreeNode) []int {
	var ret []int
	return recursiveInorder(root, ret)
}

func recursiveInorder(p *TreeNode, ret []int) []int {
	if p == nil {
		return nil
	}
	if p.Left != nil {
		ret = recursiveInorder(p.Left, ret)
	}
	ret = append(ret, p.Val)
	if p.Right != nil {
		ret = recursiveInorder(p.Right, ret)
	}
	return ret
}
