//
// Created by champ on 2022/4/18.
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
    vector<vector<int>> ret;
public:
    vector<vector<int>> findLeaves(TreeNode* root) {
        // leaf is level one, level above leaf is level two.
        dfs(root);
        return ret;
    }
private:
    int dfs(TreeNode* node) {
        if (!node) return 0;
        int level = max(dfs(node->left), dfs(node->right)) + 1;
        if (level > ret.size()) ret.push_back(vector<int>());
        ret[level-1].push_back(node->val);
        return level;
    }
};
