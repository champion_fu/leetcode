//
// Created by champ on 2022/9/14.
//

class MyCalendar {
    set<pair<int, int>> calendar;
public:
    MyCalendar() {

    }

    // [10, 20], insert [15, 25]
    bool book(int start, int end) {
        // cout << " start " << start << " end " << end << endl;
        const pair<int, int> event{start, end};
        const auto nextEvent = calendar.lower_bound(event);
        // cout << " nextEvent first " << nextEvent->first << " nextEvent second " << nextEvent->second << endl;
        //
        // if (nextEvent == calendar.begin()) cout << " nextEvent == begin" << endl;
        // if (nextEvent == calendar.end()) cout << " nextEvent == end" << endl;
        if (nextEvent != calendar.end() && nextEvent->first < end) {
            return false;
        }

        if (nextEvent != calendar.begin()) {
            const auto preEvent = prev(nextEvent);
            // cout << " preEvent first " << preEvent->first << " preEvent second " << preEvent->second << endl;
            if (preEvent->second > start) {
                return false;
            }
        }

        calendar.insert(event);
        return true;
    }
};

/**
 * Your MyCalendar object will be instantiated and called as such:
 * MyCalendar* obj = new MyCalendar();
 * bool param_1 = obj->book(start,end);
 */
