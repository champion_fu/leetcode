//
// Created by champ on 2022/9/14.
//

class MyCalendar {
    set<pair<int, int>> cals;
public:
    MyCalendar() {

    }

    bool book(int start, int end) {
        for (const auto& [s, e] : cals) {
            if (start < e && s < end) return false;
        }
        cals.emplace(start, end);
        return true;
    }
};

/**
 * Your MyCalendar object will be instantiated and called as such:
 * MyCalendar* obj = new MyCalendar();
 * bool param_1 = obj->book(start,end);
 */
