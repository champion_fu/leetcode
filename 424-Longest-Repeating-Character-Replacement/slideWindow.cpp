//
// Created by champ on 2022/8/9.
//

class Solution {
public:
    int characterReplacement(string s, int k) {
        int l = 0, res = 0;
        vector<int> count(26, 0);
        int maxCount = 0;

        for (int r = 0; r < s.size(); r++) {
            maxCount = max(maxCount, ++count[s[r] - 'A']);
            while (r - l + 1 - maxCount > k) {
                // l ........ r in this arrary there are maxCount size of same char
                count[s[l++] - 'A']--;
            }
            res = max(res, r-l+1);
        }
        return res;
    }
};
