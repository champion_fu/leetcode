//
// Created by champ on 2022/10/12.
//

class Solution {
public:
    bool increasingTriplet(vector<int>& nums) {
        vector<int> res(0);

        for (const int& num : nums) {
            if (res.size() == 0) {
                res.push_back(num);
                continue;
            }

            auto it = lower_bound(res.begin(), res.end(), num);
            if (it == res.end()) {
                res.push_back(num);
            } else {
                res[it-res.begin()] = num;
            }
        }

        return res.size() >= 3 ? true : false;
    }
};
