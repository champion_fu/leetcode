//
// Created by champ on 2022/4/16.
//

class Solution {
public:
    bool increasingTriplet(vector<int>& nums) {
        int num1 = INT_MAX;
        int num2 = INT_MAX;

        for (auto n : nums) {
            if (n <= num1) {
                num1 = n;
            } else if (n <= num2) {
                num2 = n;
            } else {
                return true;
            }
        }
        return false;
    }
};
