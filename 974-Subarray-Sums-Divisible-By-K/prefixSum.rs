impl Solution {
    pub fn subarrays_div_by_k(nums: Vec<i32>, k: i32) -> i32 {
        // prefixSum[i] = sum(:i)
        // sum(i+1 ~ j) = prefixSum[j] - prefixSum[i]
        // c_n_2 = n * (n - 1) / 2 -> TLE

        // (prefixSum[j] - prefix[i]) % k = 0
        // -> prefixSum[i] % k = prefixSum[j] % k
        // based on simple math -> prefixSum[j] - prefixSum[i] = k * (B - A) + (R1 - R0)
        //                                                           % k        %k
        // R1 - R0 = C * K -> R1 = C * K + R0, then R0 and R1 are 0 ~ k-1 so C = 0
        // R0 = R1
        // prefixMod = 0 to store the remainder
        // when the sum of the elements of a subarry that start from index 0 is divided by k
        // modGroups[R] stores the number of times R was the remainder

        let mut modGroups = vec![0;k as usize];
        modGroups[0] = 1;
        let mut sum = 0;
        let mut ans = 0;
        for num in nums {
            sum += num;
            let rem = (sum%k + k) % k; // handle the negative one
            ans += modGroups[rem as usize];
            modGroups[rem as usize]+=1;
        }
        ans
    }
}
