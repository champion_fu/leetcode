//
// Created by champ on 2022/6/23.
//

class FileSystem {
private:
    struct fileNode {
        unordered_map<string, fileNode*> next;
        bool isFile;
        string name;
        string content;
        fileNode(string name): isFile(false), content(""), name(name) {}
    };

    fileNode *root;

public:
    FileSystem() {
        root = new fileNode("");
    }

    fileNode* goToPathFolder(string path) {
        fileNode *cur = root;
        stringstream s(path);
        string folder;
        while (getline(s, folder, '/')) {
            if (folder.size()) {
                if (cur->next[folder] == NULL) {
                    cur->next[folder] = new fileNode(folder);
                }
                cur = cur->next[folder];
            }
        }
        return cur;
    }

    vector<string> ls(string path) {
        fileNode *cur = goToPathFolder(path);

        if (cur->isFile) {
            // return vector<string>{path.substr(path.find_last_of('/') + 1)};
            return vector<string>{cur->name};
        }

        vector<string> res;
        for (auto &p : cur->next) {
            res.push_back(p.first);
        }
        sort(res.begin(), res.end());
        return res;
    }

    void mkdir(string path) {
        fileNode *cur = goToPathFolder(path);
    }

    void addContentToFile(string filePath, string content) {
        fileNode *cur = goToPathFolder(filePath);
        cur->content += content;
        cur->isFile = true;
    }

    string readContentFromFile(string filePath) {
        fileNode *cur = goToPathFolder(filePath);
        return cur->content;
    }
};

/**
 * Your FileSystem object will be instantiated and called as such:
 * FileSystem* obj = new FileSystem();
 * vector<string> param_1 = obj->ls(path);
 * obj->mkdir(path);
 * obj->addContentToFile(filePath,content);
 * string param_4 = obj->readContentFromFile(filePath);
 */
