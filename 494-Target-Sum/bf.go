package _94_Target_Sum

var t int
var count int

func findTargetSumWays(nums []int, target int) int {
	t = target
	count = 0
	recur(nums, 0, 0)
	return count
}

func recur(nums []int, index int, sum int) {
	if index == len(nums) {
		if sum == t {
			count++
		}
		return
	}
	recur(nums, index+1, sum+nums[index])
	recur(nums, index+1, sum-nums[index])
}
