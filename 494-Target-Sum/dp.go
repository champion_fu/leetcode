package _94_Target_Sum

import "fmt"

func findTargetSumWays(nums []int, target int) int {
	dp := make(map[int]int) // sum: number of ways sum up to sum
	dp[0] = 1               // 1 ways to sump up to 0

	for _, v := range nums {
		dp2 := make(map[int]int)
		fmt.Printf("dp %v\n", dp)
		for sum, ways := range dp {
			tmp1 := sum + v
			dp2[tmp1] += ways

			tmp2 := sum - v
			dp2[tmp2] += ways
		}
		dp = dp2
	}
	return dp[target]
}
