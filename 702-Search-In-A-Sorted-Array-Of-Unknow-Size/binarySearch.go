package _02_Search_In_A_Sorted_Array_Of_Unknow_Size

import "fmt"

/**
 * // This is the ArrayReader's API interface.
 * // You should not implement it, or speculate about its implementation
 * type ArrayReader struct {
 * }
 *
 * func (this *ArrayReader) get(index int) int {}
 */

func search(reader ArrayReader, target int) int {
	l := 0
	r := 1
	for reader.get(r) < target {
		l = r
		r <<= 1
	}

	fmt.Printf("l %v, r %v\n", l, r)
	for l <= r {
		mid := l + (r-l)/2
		fmt.Printf("l %v, m %v r %v\n", l, mid, r)
		i := reader.get(mid)
		if i == target {
			return mid
		}
		if i > target {
			r = mid - 1
		} else {
			l = mid + 1
		}
	}
	return -1
}
