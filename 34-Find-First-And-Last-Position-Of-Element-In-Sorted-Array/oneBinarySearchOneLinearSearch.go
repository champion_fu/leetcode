package _4_Find_First_And_Last_Position_Of_Element_In_Sorted_Array

func searchRange(nums []int, target int) []int {
	n := len(nums)
	if n == 0 {
		return []int{-1, -1}
	}

	l := 0
	r := n - 1
	leftMostIndex := -1
	for l <= r {
		m := l + (r-l)/2
		if nums[m] == target {
			leftMostIndex = m
			r = m - 1
		} else if nums[m] > target {
			r = m - 1
		} else {
			l = m + 1
		}
	}

	if leftMostIndex == -1 {
		return []int{-1, -1}
	}

	rightMostIndex := -1
	for i := leftMostIndex; i < n; i++ {
		if nums[i] == target {
			rightMostIndex = i
		} else {
			break
		}
	}
	return []int{leftMostIndex, rightMostIndex}
}
