package _42_Linked_List_Cycle_II

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func detectCycle(head *ListNode) *ListNode {
	if head == nil {
		return nil
	}
	pFast := head
	p := pFast
	for pFast.Next != nil && pFast.Next.Next != nil {
		p = p.Next
		pFast = pFast.Next.Next
		if pFast == p {
			break
		}
	}
	if (pFast != p) || pFast.Next == nil || pFast.Next.Next == nil {
		// no cycle
		return nil
	}

	p = head
	p2 := pFast
	for p != p2 {
		p = p.Next
		p2 = p2.Next
	}
	return p
}
