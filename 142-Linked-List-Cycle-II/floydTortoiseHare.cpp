//
// Created by champ on 2022/8/20.
//

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *detectCycle(ListNode *head) {
        if (head == NULL) return NULL;

        ListNode* pSlow = head;
        ListNode* pFast = head;

        while (pFast->next != NULL && pFast->next->next != NULL) {
            pSlow = pSlow->next;
            pFast = pFast->next->next;
            if (pFast == pSlow) break;
        }

        // no cycle
        if (pSlow != pFast || pFast->next == NULL || pFast->next->next == NULL) {
            return NULL;
        }

        pSlow = head;
        while (pSlow != pFast) {
            pSlow = pSlow->next;
            pFast = pFast->next;
        }
        return pFast;
    }
};
