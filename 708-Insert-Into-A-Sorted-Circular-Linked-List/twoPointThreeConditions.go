package _08_Insert_Into_A_Sorted_Circular_Linked_List

/**
 * Definition for a Node.
 * type Node struct {
 *     Val int
 *     Next *Node
 * }
 */

func insert(aNode *Node, x int) *Node {
	n := &Node{Val: x, Next: nil}
	if aNode == nil {
		n.Next = n
		return n
	} else if aNode.Next == aNode {
		aNode.Next = n
		n.Next = aNode
		return aNode
	}

	pre := aNode
	cur := aNode.Next
	isInserted := false
	for {
		fmt.Printf("pre %+v cur %+v value %+v \n", pre, cur, x)
		if pre.Val <= x && x <= cur.Val {
			isInserted = true
		} else if pre.Val > cur.Val {
			if x >= pre.Val || x <= cur.Val {
				isInserted = true
			}
		}
		if isInserted {
			n.Next = cur
			pre.Next = n
			return aNode
		}
		pre = cur
		cur = cur.Next
		if pre == aNode {
			break
		}
	}
	// means we back to the root
	n.Next = aNode.Next
	aNode.Next = n
	return aNode
}
