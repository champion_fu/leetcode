package _16_Longest_Palindromic_Subsequence

func longestPalindromeSubseq(s string) int {
	n := len(s)
	dp := make([]int, n)

	for i := n - 1; i >= 0; i-- {
		dp[i] = 1
		prev := 0
		for j := i + 1; j < n; j++ {
			temp := dp[j] // for j move on, i-1, j-1
			if s[i] == s[j] {
				dp[j] = prev + 2
			} else {
				dp[j] = max(dp[j], dp[j-1])
			}
			prev = temp
		}
	}
	return dp[n-1]
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}
