package _5_Unique_Binary_Search_Trees_II

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

type pair struct {
	start int
	end   int
}

var lookUpTable map[pair][]*TreeNode

func generateTrees(n int) []*TreeNode {
	lookUpTable = make(map[pair][]*TreeNode, 0)
	return generateTreesFrom(1, n)
}

func generateTreesFrom(start int, end int) []*TreeNode {
	if start > end {
		return []*TreeNode{nil}
	}

	p := pair{
		start: start,
		end:   end,
	}
	var ret []*TreeNode
	if ret, match := lookUpTable[p]; match {
		return ret
	}

	for i := start; i <= end; i++ {
		// pick i as root val
		leftTrees := generateTreesFrom(start, i-1)
		rightTrees := generateTreesFrom(i+1, end)
		for _, j := range leftTrees {
			for _, k := range rightTrees {
				n := &TreeNode{Val: i}
				n.Left = j
				n.Right = k
				ret = append(ret, n)
			}
		}
	}
	lookUpTable[p] = ret
	return ret
}
