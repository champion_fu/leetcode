package _87_Find_the_Duplicate_Number

func findDuplicate(nums []int) int {
	// Floyd's Tortoise and Hare (Cycle Detection)
	slow := nums[0]
	fast := nums[0]

	// find first interaction
	for {
		slow = nums[slow]
		fast = nums[nums[fast]]
		if slow == fast {
			break
		}
	}

	// find cycle
	slow = nums[0]
	for slow != fast {
		slow = nums[slow]
		fast = nums[fast]
	}
	return slow
}
