//
// Created by champ on 2022/8/4.
//

class Solution {
public:
    int findDuplicate(vector<int>& nums) {
        int fast = nums[0];
        int slow = nums[0];

        do {
            fast = nums[nums[fast]]; // 2
            slow = nums[slow]; // 1
        } while (fast != slow);

        // fast is in the interaction
        slow = nums[0];
        while (slow != fast) {
            fast = nums[fast];
            slow = nums[slow];
        }

        return fast;
    }
};
