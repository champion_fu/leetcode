//
// Created by champ on 2022/9/23.
//

class MaxSegmentTree {
public:
    int n;
    vector<int> tree;
    MaxSegmentTree(int depth) : n(depth) {
        int size = (int)(ceil(log2(n)));
        size = (2 * pow(2, size)) - 1;
        // depth = 2 -> size = 7 cout << "size " << size << endl;
        cout << "after size " << size << endl;
        tree = vector<int>(size);
    }

    int maxValue() {
        return tree[0];
    }

    int query(int l, int r) {
        return queryUtil(0, l, r, 0, n-1);
    }

    int queryUtil(int i, int qL, int qR, int l, int r) {
        if (l >= qL && r <= qR) return tree[i];
        if (l > qR || r < qL) return INT_MIN;

        int m = (l + r) / 2;
        return max(queryUtil(2*i+1, qL, qR, l, m), queryUtil(2*i+2, qL, qR, m+1, r));
    }

    void update(int i, int val) {
        updateUtil(0, 0, n-1, i, val);
    }

    void updateUtil(int i, int l, int r, int pos, int val) {
        if (pos < l || pos > r) return;
        if (l == r) {
            tree[i] = max(val, tree[i]);
            return;
        }
        int m = (l + r)/2;
        updateUtil(2*i+1, l, m, pos, val);
        updateUtil(2*i+2, m+1, r, pos, val);
        tree[i] = max(tree[2*i+1], tree[2*i+2]);
    }
};

class Solution {
public:
    int lengthOfLIS(vector<int>& nums, int k) {
        MaxSegmentTree tree(1e5+1);
        for (int i : nums) {
            int lower = max(0, i-k);
            int cur = 1 + tree.query(lower, i-1);
            tree.update(i, cur);
        }
        return tree.maxValue();
    }
};