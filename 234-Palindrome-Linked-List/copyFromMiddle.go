package _34_Palindrome_Linked_List

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func isPalindrome(head *ListNode) bool {
	if head.Next == nil {
		return true
	}

	fastPoint := head
	slowPoint := head
	for fastPoint.Next != nil && fastPoint.Next.Next != nil {
		fastPoint = fastPoint.Next.Next
		slowPoint = slowPoint.Next
	}
	slowPoint = slowPoint.Next // move point to right of middle

	reverseListDummyHead := &ListNode{Val: -1, Next: nil}
	for slowPoint != nil {
		newNode := &ListNode{Val: slowPoint.Val, Next: reverseListDummyHead.Next}
		reverseListDummyHead.Next = newNode
		slowPoint = slowPoint.Next
	}

	p := head
	rP := reverseListDummyHead.Next
	for p != nil && rP != nil {
		if p.Val != rP.Val {
			return false
		}
		p = p.Next
		rP = rP.Next
	}
	return true
}
