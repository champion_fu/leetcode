package _34_Palindrome_Linked_List

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func isPalindrome(head *ListNode) bool {
	if head.Next == nil {
		return true
	}

	fastPoint := head
	slowPoint := head
	for fastPoint.Next != nil && fastPoint.Next.Next != nil {
		fastPoint = fastPoint.Next.Next
		slowPoint = slowPoint.Next
	}
	secondHead := reverseList(slowPoint.Next)

	secondPoint := secondHead
	firstPoint := head
	isPal := true
	for isPal && firstPoint != nil && secondPoint != nil {
		if firstPoint.Val != secondPoint.Val {
			isPal = false
		}
		firstPoint = firstPoint.Next
		secondPoint = secondPoint.Next
	}

	firstPoint.Next = reverseList(secondHead)
	return isPal
}

func reverseList(head *ListNode) *ListNode {
	if head == nil {
		return head
	}

	curHead := head
	for head.Next != nil {
		curNext := head.Next.Next
		head.Next.Next = curHead

		curHead = head.Next
		head.Next = curNext
	}
	return curHead
}
