package _34_Palindrome_Linked_List

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func isPalindrome(head *ListNode) bool {
	if head == nil || head.Next == nil {
		return true
	}

	_, is := recursivelyCheck(head, head.Next)
	return is
}

func recursivelyCheck(first *ListNode, second *ListNode) (*ListNode, bool) {
	if second == nil {
		return first, true
	}

	f, is := recursivelyCheck(first, second.Next)
	return f.Next, (is && f.Val == second.Val)
}
