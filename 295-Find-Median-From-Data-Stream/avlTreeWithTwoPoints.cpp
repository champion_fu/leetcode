//
// Created by champ on 2022/7/27.
//

class MedianFinder {
    multiset<int> data;
    multiset<int>::iterator loM, hiM;
public:
    MedianFinder(): loM(data.end()), hiM(data.end()) {

    }

    void addNum(int num) {
        int n = data.size();

        data.insert(num);

        if (!n) {
            // no elements before, one element now
            loM = hiM = data.begin();
        } else if (n & 1) {
            // odd size before (i.e. lo == hi), even size now
            if (num < *loM) { // num < lo
                loM--;
            } else { // num >= hi
                hiM++;
            }
        } else {
            // even size before (i.e. hi == lo + 1), odd size now
            if (num > *loM && num < *hiM) {
                // in the between
                loM++;
                hiM--;
            } else if (num >= *hiM) {
                // bigger then hi, num inserted after hi
                loM++;
            } else {
                // less than lo, num <= lo < hi
                loM = --hiM; // insert at end of equal range spoils lo
            }
        }
    }

    double findMedian() {
        return ((double) *loM + *hiM) * 0.5;
    }
};

/**
 * Your MedianFinder object will be instantiated and called as such:
 * MedianFinder* obj = new MedianFinder();
 * obj->addNum(num);
 * double param_2 = obj->findMedian();
 */
