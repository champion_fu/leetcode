//
// Created by champ on 2022/7/27.
//

class MedianFinder {
    priority_queue<int> lo; // max heap, store half smaller data, length can be longer than hi;
    priority_queue<int, vector<int>, greater<int>> hi; // min heap, store half larger data;
public:
    MedianFinder() {

    }

    void addNum(int num) {
        lo.push(num);

        hi.push(lo.top());
        lo.pop();

        if (lo.size() < hi.size()) {
            lo.push(hi.top());
            hi.pop();
        }
    }

    double findMedian() {
        return lo.size() > hi.size() ? lo.top() : ((double) lo.top() + hi.top()) * 0.5;
    }
};

/**
 * Your MedianFinder object will be instantiated and called as such:
 * MedianFinder* obj = new MedianFinder();
 * obj->addNum(num);
 * double param_2 = obj->findMedian();
 */
