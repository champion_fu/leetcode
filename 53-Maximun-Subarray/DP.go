package _3_Maximun_Subarray

func maxSubArray(nums []int) int {
	sum := nums[0]
	max := nums[0]
	for i := 1; i < len(nums); i++ {
		if sum < 0 {
			sum = nums[i]
		} else {
			sum = sum + nums[i]
		}

		if max < sum {
			max = sum
		}
	}
	return max
}
