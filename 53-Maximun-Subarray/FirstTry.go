package _3_Maximun_Subarray

import "math"

func maxSubArray(nums []int) int {
	ans := make([]int, len(nums))
	maxSubarray := 0
	for i := 0; i < len(nums); i++ {
		if i == 0 {
			ans[i] = nums[i]
			maxSubarray = nums[i]
			continue
		}
		tmp := maxSubarray + nums[i]
		if tmp < nums[i] {
			ans[i] = nums[i]
			maxSubarray = nums[i]
		} else {
			ans[i] = tmp
			maxSubarray = tmp
		}
	}
	max := math.MinInt32
	for i := 0; i < len(ans); i++ {
		if max < ans[i] {
			max = ans[i]
		}
	}
	return max
}
