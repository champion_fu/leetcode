//
// Created by champ on 2022/7/19.
//

class Solution {
public:
    int maxSubArray(vector<int>& nums) {
        int maxSubArray = nums[0];
        int curSubArray = nums[0];
        for (int i = 1; i < nums.size(); i++) {
            int num = nums[i];

            curSubArray = max(curSubArray + num, num);
            maxSubArray = max(maxSubArray, curSubArray);
        }

        return maxSubArray;
    }
};
