//
// Created by champ on 2022/5/14.
//

class Solution {
public:
    vector<int> dailyTemperatures(vector<int>& temperatures) {
        int length = temperatures.size();
        vector<int> results(length, 0);
        int hottest = 0;

        for (int i = length - 1; i >= 0; i--) {
            int curTemperature = temperatures[i];
            if (curTemperature >= hottest) {
                hottest = curTemperature;
                continue;
            }

            // means there is hottest day later.

            int day = 1;
            // stop, when find the day hotter then current day
            while (temperatures[i + day] <= curTemperature) {
                day += results[i+day];
            }
            results[i] = day;
        }
        return results;
    }
};
