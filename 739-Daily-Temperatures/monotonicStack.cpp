//
// Created by champ on 2022/5/14.
//

class Solution {
public:
    vector<int> dailyTemperatures(vector<int>& temperatures) {
        int length = temperatures.size();
        vector<int> results(length, 0);
        stack<pair<int, int>> s;

        for (int i = 0; i < length; i++) {
            int curTemperature = temperatures[i];
            while (!s.empty() && s.top().first < curTemperature) {
                // cur temperature > previous temperature
                int preDay = s.top().second;
                results[preDay] = i - preDay;
                s.pop();
            }
            s.push({curTemperature, i});
        }
        return results;
    }
};
