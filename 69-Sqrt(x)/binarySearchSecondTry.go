package _9_Sqrt_x_

import "fmt"

func mySqrt(x int) int {
	if x < 2 {
		return x
	}
	l := 0
	r := x / 2
	for l < r {
		mid := l + (r-l)/2
		fmt.Printf("l %v, r %v, mid %v\n", l, r, mid)
		if mid*mid == x {
			return mid
		} else if mid*mid > x {
			// means. mid is too large
			r = mid
		} else {
			l = mid + 1
		}
	}
	if l*l > x {
		l = l - 1
	}
	return l
}
