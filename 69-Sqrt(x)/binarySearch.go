package _9_Sqrt_x_

func mySqrt(x int) int {
	if x == 1 || x == 0 {
		return x
	}

	l := 2
	r := x / 2
	pivot := (l + r) / 2

	for l <= r {
		pivot = (l + r) / 2
		num := pivot * pivot
		if num > x {
			r = pivot - 1
		} else if num < x {
			l = pivot + 1
		} else {
			return pivot
		}
	}
	if pivot*pivot > x {
		return pivot - 1
	}
	return pivot
}
