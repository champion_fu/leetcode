package _9_Sqrt_x_

func mySqrt(x int) int {
	if x < 2 {
		return x
	}

	l := mySqrt(x>>2) << 1
	r := l + 1
	if r*r > x {
		return l
	}
	return r
}
