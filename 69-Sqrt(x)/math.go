package _9_Sqrt_x_

import "math"

// Pocket Calculator Algorithm.
func mySqrt(x int) int {
	if x < 2 {
		return x
	}

	l := int(math.Pow(math.E, 0.5*math.Log(float64(x))))
	r := l + 1
	if r*r > x {
		return l
	} else {
		return r
	}
}
