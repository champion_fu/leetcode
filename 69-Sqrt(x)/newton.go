package _9_Sqrt_x_

import "math"

func mySqrt(x int) int {
	if x < 2 {
		return x
	}

	xF := float64(x)
	x0 := xF
	x1 := (x0 + xF/x0) / 2.0
	for math.Abs(x0-x1) >= 1 {
		x0 = x1
		x1 = (x0 + xF/x0) / 2.0
	}

	return int(x1)
}
