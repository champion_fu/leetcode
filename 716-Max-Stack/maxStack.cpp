//
// Created by champ on 2022/7/13.
//

class MaxStack {
    // double-linked list O(1) for insert/delete
    list<int> dll;
    // sorted map, largest key at begin(); O(1) to find max, O(log(n)) ot insert/delete
    map<int, vector<list<int>::iterator>, greater<int>> m;
public:
    MaxStack() {

    }

    void push(int x) {
        dll.push_front(x);
        m[x].push_back(dll.begin());
    }

    int pop() {
        int x = dll.front();
        m[x].pop_back();
        if (m[x].empty()) m.erase(x);
        dll.pop_front();
        return x;
    }

    int top() {
        return dll.front();
    }

    int peekMax() {
        return m.begin()->first;
    }

    int popMax() {
        int x = m.begin()->first;
        auto it = (m.begin()->second).back();
        dll.erase(it);
        m[x].pop_back();
        if (m[x].empty()) m.erase(x);
        return x;
    }
};

/**
 * Your MaxStack object will be instantiated and called as such:
 * MaxStack* obj = new MaxStack();
 * obj->push(x);
 * int param_2 = obj->pop();
 * int param_3 = obj->top();
 * int param_4 = obj->peekMax();
 * int param_5 = obj->popMax();
 */
