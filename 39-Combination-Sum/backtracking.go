package _9_Combination_Sum

import "sort"

var cand []int
var ret [][]int

func combinationSum(candidates []int, target int) [][]int {
	ret = make([][]int, 0)
	sort.Slice(
		candidates,
		func(i, j int) bool {
			return candidates[i] < candidates[j]
		},
	)
	cand = candidates
	backtracking(make([]int, 0), target, 0)
	return ret
}

func backtracking(candidate []int, target, index int) {
	if target == 0 {
		// need copy
		r := make([]int, len(candidate))
		copy(r, candidate)
		ret = append(ret, r)
		return
	}

	for i := index; i < len(cand); i++ {
		v := cand[i]
		if v > target {
			return
		}
		candidate = append(candidate, v)
		backtracking(candidate, target-v, i)
		candidate = candidate[:len(candidate)-1]
	}
	return
}
