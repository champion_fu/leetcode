//
// Created by champ on 2022/7/27.
//

class Solution {
    vector<vector<int>> res;
private:
    void backtrack(vector<int>& candidates, vector<int> sample, int sum, int index, int target) {
        // for (int i = 0; i < sample.size(); i++) {
        //      cout << sample[i] << endl;
        // }
        // cout << "sum " << sum << "target " << target << endl;
        if (sum == target) {
            res.push_back(sample);
            return;
        }

        for (int i = index; i < candidates.size(); i++) {
            int candidate = candidates[i];
            if (sum + candidate > target) {
                continue;
            }
            // cout << "candidate " << candidate << endl;
            sample.push_back(candidate);
            backtrack(candidates, sample, sum+candidate, i, target);
            sample.pop_back();
        }
    }
public:
    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
        backtrack(candidates, {}, 0, 0, target);
        return res;
    }
};
