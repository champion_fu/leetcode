//
// Created by champ on 2022/7/18.
//

class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        unordered_map<int, int> m;
        vector<int> result;

        for (int i = 0; i < nums.size(); i++) {
            int num = nums[i];
            int t = target - num;
            if (m.find(t) != m.end()) {
                result.push_back(m[t]);
                result.push_back(i);
            }
            m[num] = i;
        }
        return result;
    }
};
