package __Two_Sum

func twoSum(nums []int, target int) []int {
	var ret []int
	valueIndexMapper := make(map[int]int)
	for i, v := range nums {
		ans := target - v
		if index, match := valueIndexMapper[ans]; match && i != index {
			ret = append(ret, index, i)
			break
		}
		valueIndexMapper[v] = i
	}
	return ret
}
