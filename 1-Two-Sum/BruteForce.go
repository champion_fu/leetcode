package __Two_Sum

func twoSum(nums []int, target int) []int {
	var ret []int
	for i, v := range nums {
		ans := target - v
		for j := i + 1; j < len(nums); j++ {
			if nums[j] == ans {
				ret = append(ret, i, j)
				break
			}
		}
	}
	return ret
}
