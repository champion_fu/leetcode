package _6_Plus_One

func plusOne(digits []int) []int {
	n := len(digits)
	i := n - 1

	var carry int
	for i >= 0 {
		digits[i] += 1
		carry = digits[i] / 10
		if carry == 0 {
			break
		} else {
			digits[i] = digits[i] % 10
		}
		i--
	}
	if carry == 0 {
		return digits
	}
	ret := make([]int, 1)
	ret[0] = 1
	for _, v := range digits {
		ret = append(ret, v)
	}
	return ret
}
